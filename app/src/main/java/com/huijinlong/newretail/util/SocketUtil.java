package com.huijinlong.newretail.util;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.huijinlong.newretail.bean.OrderBookSocketEntity;
import com.huijinlong.newretail.bean.SocketBeanKLine;
import com.huijinlong.newretail.bean.SocketBeanLogin;
import com.huijinlong.newretail.bean.TickerSocketEntity;
import com.huijinlong.newretail.net.WebSocketWorker;


import org.java_websocket.WebSocket;
import org.java_websocket.WebSocketAdapter;
import org.java_websocket.client.DefaultSSLWebSocketClientFactory;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft;
import org.java_websocket.drafts.Draft_10;
//import org.java_websocket.drafts.Draft_6455;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.channels.ByteChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.List;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * Created by penghuaijin on 2018/10/12.
 */

public class SocketUtil {
    private static SocketUtil instance = new SocketUtil();

    public SocketUtil() {

    }

    public final String URI_5000 = "https://apittt.nr.exchange:5000";
//    public final String URI_5005 = "wss://ttt.nr.exchange:5005";

    public final String URI_5005 = "https://apittt.nr.exchange:5005";

    private Socket mSocket_5000;
    private Socket mSocket_5005;

    private String instrument;
    private String span;

    private OrderBookListener orderBookListener;
    private OrderBookListener orderBookListenerSell;
    private TickerListener tickerListener;
    private KlineListener klineListener;


    public static SocketUtil getInstance() {

        return instance;
    }

    public void init5000(String instrument, boolean isOrderBook, boolean isHistory) {

        try {
            this.instrument = instrument;


            mSocket_5000 = IO.socket(URI_5000);

            mSocket_5000.on(Socket.EVENT_CONNECT, onConnect5000);
            if (isOrderBook) {
                mSocket_5000.on("orderbook", onOrderBook);
            }
            if (isHistory) {
                mSocket_5000.on("history", onHistory);
            }
            mSocket_5000.on("ticker", onTicker);

            mSocket_5000.connect();

        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

    }

    public void init5005(String instrument, String span) {
        try {
            this.instrument = instrument;
            this.span = span;

            mSocket_5005 = IO.socket(URI_5005);

            mSocket_5005.on(Socket.EVENT_CONNECT, onConnect5005);

            mSocket_5005.on("kline", onKline);

            mSocket_5005.connect();

        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

    }

    public void init5005(String sub) {

        URI uri = null;

        try {
            uri = new URI(URI_5005);
            WebSocketWorker webSocketWorker = new WebSocketWorker(uri, new Draft_10());


//            webSocketWorker.connect();
            trustAllHosts(webSocketWorker);
            webSocketWorker.connectBlocking();

//
            SocketBeanLogin socketBeanLogin = new SocketBeanLogin("login", SpUtil.getString(SpUtil.AUTHORIZATION, ""));

            SocketBeanKLine socketBeanKLine = new SocketBeanKLine("sub", sub);

            String jsonLogin = new Gson().toJson(socketBeanLogin);
            LogUtils.e("WebSocketWorker---jsonLogin:" + jsonLogin);
            webSocketWorker.send(jsonLogin);
//
            String jsonsub = new Gson().toJson(socketBeanKLine);
            LogUtils.e("WebSocketWorker---jsonsub:" + jsonsub);
            webSocketWorker.send(jsonsub);


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public Socket getSocket5000() {
        return mSocket_5000;
    }


    public Socket getSocket5005() {
        return mSocket_5005;
    }

    private Emitter.Listener
            onConnect5000 = args -> {
        LogUtils.e("5000---onConnect5000:" + args.toString());

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("instrument", instrument);
            jsonObject.put("type", "android");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mSocket_5000.emit("join", jsonObject);
        LogUtils.e("5000---emit:-----" + jsonObject);
    };


    private Emitter.Listener
            onConnect5005 = args -> {
//        LogUtils.e("WebSocketWorker---onConnect5005:" + args[0]);


        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("instrument", instrument);
            jsonObject.put("span", span);
            jsonObject.put("authorized", SpUtil.getString(SpUtil.AUTHORIZATION, ""));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        LogUtils.e(jsonObject.toString());
        mSocket_5005.emit("join", jsonObject);


    };

    //这里是现货页面的2个list的数据返回地方
    private Emitter.Listener onOrderBook = args -> {
        String s = args[0].toString();
//        LogUtils.e("5000---onOrderBook:" + s);
//        OrderBookSocketEntity orderBookSocketEntity = new Gson().fromJson(s, OrderBookSocketEntity.class);
        OrderBookSocketEntity orderBookSocketEntity = new OrderBookSocketEntity();
        JsonObject asJsonObject = new JsonParser().parse(s).getAsJsonObject();

        orderBookSocketEntity.setInstrument(asJsonObject.getAsJsonPrimitive("instrument").getAsString());
        orderBookSocketEntity.setType(asJsonObject.getAsJsonPrimitive("type").getAsString());
        orderBookSocketEntity.setSeq(asJsonObject.getAsJsonPrimitive("seq").getAsString());
        orderBookSocketEntity.setTimestamp(asJsonObject.getAsJsonPrimitive("timestamp").getAsString());
        orderBookSocketEntity.setLastUpdateId(asJsonObject.getAsJsonPrimitive("lastUpdateId").getAsInt());
        JsonObject orderbook = asJsonObject.getAsJsonObject("orderbook");

        JsonObject ask = null;
        JsonObject bid = null;
        OrderBookSocketEntity.OrderBean orderBean = new OrderBookSocketEntity.OrderBean();
        OrderBookSocketEntity.OrderBean.AskBean askBean = new OrderBookSocketEntity.OrderBean.AskBean();
        OrderBookSocketEntity.OrderBean.BidBean bidBean = new OrderBookSocketEntity.OrderBean.BidBean();
        String currentPrice = "";
        try {
            if (orderbook.get("ask") != null) {
                ask = orderbook.get("ask").getAsJsonObject();
                String string = ask.toString();
                String replace = string
                        .replace("{", "")
                        .replace("}", "")
                        .replace("\"", "");
                String[] strings = replace.split(",");


                HashMap<Double, Double> hashMap = new HashMap<>();
                for (int i = 0; i < strings.length; i++) {
                    String[] split = strings[i].split(":");
                    hashMap.put(Double.parseDouble(split[0]), Double.parseDouble(split[1]));
                    currentPrice = split[0];
                }


                askBean.setHashMap(hashMap);

                orderBean.setAsk(askBean);

            }
            if (orderbook.get("bid") != null) {
                bid = orderbook.get("bid").getAsJsonObject();
                String string = bid.toString();
                String replace = string
                        .replace("{", "")
                        .replace("}", "")
                        .replace("\"", "");
                String[] strings = replace.split(",");


                HashMap<Double, Double> hashMap = new HashMap<>();
                for (int i = 0; i < strings.length; i++) {
                    String[] split = strings[i].split(":");
                    hashMap.put(Double.parseDouble(split[0]), Double.parseDouble(split[1]));
                    currentPrice = split[0];

                }

                bidBean.setHashMap(hashMap);
                orderBean.setBid(bidBean);
            }

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        orderBookSocketEntity.setOrderbook(orderBean);
//        LogUtils.d("-----ben" + new Gson().toJson(orderBookSocketEntity));

        if (orderBookListener != null) {
            orderBookListener.onOrderBookMsg(orderBookSocketEntity, currentPrice);
        }
        if (orderBookListenerSell != null) {
            orderBookListenerSell.onOrderBookMsg(orderBookSocketEntity, currentPrice);
        }

    };

    private Emitter.Listener onHistory = args -> LogUtils.e("5000---onHistory:" + args[0].toString());
    private Emitter.Listener onTicker = args -> {
        if (tickerListener != null) {
            TickerSocketEntity tickerSocketEntity = new Gson().fromJson(args[0].toString(), TickerSocketEntity.class);
            tickerListener.onTickerMsg(tickerSocketEntity);
        }
    };

    private Emitter.Listener onKline = args -> {
        //这里是接收到kline返回值的地方需要处理的地方,
        //以后socket对接klin记得在这里处理。
        LogUtils.e("onkline---" + args[0].toString());
        if (klineListener != null) {
            TickerSocketEntity tickerSocketEntity = new Gson().fromJson(args[0].toString(), TickerSocketEntity.class);
            klineListener.onKlineMsg(tickerSocketEntity);
        }
    };

    public void setKlineListener(KlineListener klineListener) {
        this.klineListener = klineListener;
    }


    public void destroy() {
        if (mSocket_5000 != null) {
            mSocket_5000.disconnect();
            mSocket_5000 = null;
        }
        if (mSocket_5005 != null) {
            mSocket_5005.disconnect();
            mSocket_5005 = null;
        }

        if (tickerListener != null) {
            tickerListener = null;
        }

    }

    public interface OrderBookListener {
        void onOrderBookMsg(OrderBookSocketEntity orderBookSocketEntity, String currentPrice);
    }

    public void setOrderBookListener(OrderBookListener orderBookListener) {
        this.orderBookListener = orderBookListener;
    }

    public OrderBookListener getOrderBookListenerSell() {
        return orderBookListenerSell;
    }

    public void setOrderBookListenerSell(OrderBookListener orderBookListenerSell) {
        this.orderBookListenerSell = orderBookListenerSell;
    }

    public interface TickerListener {
        void onTickerMsg(TickerSocketEntity tickerSocketEntity);
    }

    public void setTickerListener(TickerListener tickerListener) {
        this.tickerListener = tickerListener;
    }

    public interface KlineListener {
        void onKlineMsg(TickerSocketEntity tickerSocketEntity);
    }


    //这里是以前为了防止同一个项目有2个不同版本的websocket库造成影响处理的地方
    private static void trustAllHosts(WebSocketClient appClient) {
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {


            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return new java.security.cert.X509Certificate[]{};
            }


            public void checkClientTrusted(X509Certificate[] chain,
                                           String authType) throws CertificateException {
            }


            public void checkServerTrusted(X509Certificate[] chain,
                                           String authType) throws CertificateException {
            }

        }};


        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            appClient.setWebSocketFactory(new DefaultSSLWebSocketClientFactory(sc));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
