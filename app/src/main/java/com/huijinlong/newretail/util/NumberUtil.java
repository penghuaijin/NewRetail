package com.huijinlong.newretail.util;

import android.text.TextUtils;

import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author penghuaijin
 * @date 2018/10/19 3:01 PM
 * @Description: ()
 */
public class NumberUtil {

    /**
     * 根据string获取小数点为最后一位的操作单位
     * @param value
     * @return
     */
    public static BigDecimal getLastNum(String value,boolean isSub) {
        if (Double.parseDouble(value)==0&&isSub){
            ToastUtils.toast("数值必须大于0");
            return  new BigDecimal(0);
        }
        int i = value.indexOf(".");
        if (i < 0) {
            return new BigDecimal(1);
        } else {
            int index = value.length() - i - 1;
            BigDecimal item = new BigDecimal("0.1");
            BigDecimal itemKey = new BigDecimal("1");
            for (int i1 = 0; i1 < index; i1++) {
                itemKey = itemKey.multiply(item);
            }
            return itemKey;
        }

    }

    /**
     * //是否保留n小数正则
     * @param number editText的值
     * @param n 小数点位数
     * @return
     */
    public static boolean isOnlyPointNumber(String number, int n) {
        Pattern pattern = Pattern.compile(String.format("^\\d+\\.?\\d{0,%s}$", n));
        Matcher matcher = pattern.matcher(number);
        return matcher.matches();
    }

    public static String getFormatTextForNum(String str) {
        str = TextUtils.isEmpty(str) ? "0" : str;
        return str;
    }

    public static String getFormatText(String str) {
        if ("null".equals(str)) {
            return "";
        } else {
            str = TextUtils.isEmpty(str) ? "" : str;
            return str;
        }
    }

    public static String rvZeroAndDot(String s) {
        if (TextUtils.isEmpty(s)) {
            return "0";
        }
        if (s.indexOf(".") > 0) {
            s = s.replaceAll("0+?$", "");//去掉多余的0
            s = s.replaceAll("[.]$", "");//如最后一位是.则去掉
        }
        return s;
    }

    public static String rvZeroAndDot(double num) {
        String s = num + "";
        if (TextUtils.isEmpty(s)) {
            return "0";
        }
        if (s.indexOf(".") > 0) {
            s = s.replaceAll("0+?$", "");//去掉多余的0
            s = s.replaceAll("[.]$", "");//如最后一位是.则去掉
        }
        return s;
    }
}
