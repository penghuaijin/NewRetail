package com.huijinlong.newretail.util;


import android.os.Build;

import com.google.gson.Gson;
import com.huijinlong.newretail.requestbean.DepthBean;
import com.huijinlong.newretail.requestbean.SpotProductsBean;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

/**
 * Created by penghuaijin on 2018/10/16.
 */

public class ListUtil {

    public static ArrayList<DepthBean.DataBean.AsksBean> getSpotBuyAskList(HashMap<Double, Double> socketHashMap, ArrayList<DepthBean.DataBean.AsksBean> oldList) {

        HashMap<Double, Double> oldMap = new HashMap<>();

        for (DepthBean.DataBean.AsksBean asksBean : oldList) {
            oldMap.put(Double.parseDouble(asksBean.getPrice()), Double.parseDouble(asksBean.getQuantity()));
        }

        oldMap.putAll(socketHashMap);

        for (Double socketKeyItem : socketHashMap.keySet()) {
            if (socketHashMap.get(socketKeyItem) == 0) {
                oldMap.remove(socketKeyItem);
            }
        }

        oldList.clear();
        for (Double keyItem : oldMap.keySet()) {
            oldList.add(new DepthBean.DataBean.AsksBean(keyItem, oldMap.get(keyItem)));
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            oldList.sort((o1, o2) -> (int) (10000 * (Float.parseFloat(o1.getPrice())
                    - Float.parseFloat(o2.getPrice()))));
        }else {
            Collections.sort(oldList, new Comparator<DepthBean.DataBean.AsksBean>() {
                @Override
                public int compare(DepthBean.DataBean.AsksBean o1, DepthBean.DataBean.AsksBean o2) {
                    return (int) (10000 * (Float.parseFloat(o1.getPrice())
                            - Float.parseFloat(o2.getPrice())));
                }
            });
        }

        int size = oldList.size();

        if (size > 7) {
            oldList.subList(size - 7, size);
        }

        return oldList;
    }

    public static ArrayList<DepthBean.DataBean.BidsBean> getSpotBuyBidList(HashMap<Double, Double> socketHashMap, ArrayList<DepthBean.DataBean.BidsBean> oldList) {

        HashMap<Double, Double> oldMap = new HashMap<>();

        for (DepthBean.DataBean.BidsBean asksBean : oldList) {
            oldMap.put(Double.parseDouble(asksBean.getPrice()), Double.parseDouble(asksBean.getQuantity()));
        }

        oldMap.putAll(socketHashMap);

        for (Double socketKeyItem : socketHashMap.keySet()) {
            if (socketHashMap.get(socketKeyItem) == 0) {
                oldMap.remove(socketKeyItem);
            }
        }

        oldList.clear();
        for (Double keyItem : oldMap.keySet()) {
            oldList.add(new DepthBean.DataBean.BidsBean(keyItem, oldMap.get(keyItem)));
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            oldList.sort((o1, o2) -> (int) (10000 * (Float.parseFloat(o2.getPrice())
                    - Float.parseFloat(o1.getPrice()))));
        }else {
            Collections.sort(oldList, new Comparator<DepthBean.DataBean.BidsBean>() {
                @Override
                public int compare(DepthBean.DataBean.BidsBean o1, DepthBean.DataBean.BidsBean o2) {
                    return (int) (10000 * (Float.parseFloat(o2.getPrice())
                            - Float.parseFloat(o1.getPrice())));
                }
            });
        }

        int size = oldList.size();

        if (size > 7) {
            oldList.subList(size - 7, size);
        }

        return oldList;
    }

    public static List<SpotProductsBean.DataBean.ProductsBean.AreaBean.MainBean> getMarketList(List<SpotProductsBean.DataBean.ProductsBean.AreaBean.MainBean> oldList, int type_currency, int type_updatedPrice, int type_UpsAndDowns) {


        //有且只有一个排序type，否则不排序
        if (Math.abs(type_currency) +
              Math.abs(type_updatedPrice) +
              Math.abs(type_UpsAndDowns) != 1) {
            return oldList;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            oldList.sort(new Comparator<SpotProductsBean.DataBean.ProductsBean.AreaBean.MainBean>() {
                @Override
                public int compare(SpotProductsBean.DataBean.ProductsBean.AreaBean.MainBean o1, SpotProductsBean.DataBean.ProductsBean.AreaBean.MainBean o2) {
                    if (type_currency != 0) {
                        LogUtils.e("--------------type_currency:"+type_currency);
                        return type_currency * (o1.getSymbol().compareTo(o2.getSymbol()));
                    }
                    if (type_updatedPrice != 0) {
                        LogUtils.e("--------------type_currency:"+type_currency);

                        return type_updatedPrice * (
                                Double.parseDouble(o1.getLast())>
                                Double.parseDouble(o2.getLast())
                                ?1:-1);
                    }
                    if (type_UpsAndDowns != 0) {
                        LogUtils.e("--------------type_currency:"+type_currency);
                        return type_UpsAndDowns * (
                                Double.parseDouble(o1.getRate().replace("%",""))>
                                        Double.parseDouble(o2.getRate().replace("%",""))
                                        ?1:-1);
                    }
                    else {
                        return 0;
                    }
                }
            });
        }else {
            Collections.sort(oldList, new Comparator<SpotProductsBean.DataBean.ProductsBean.AreaBean.MainBean>() {
                @Override
                public int compare(SpotProductsBean.DataBean.ProductsBean.AreaBean.MainBean o1, SpotProductsBean.DataBean.ProductsBean.AreaBean.MainBean o2) {
                    if (type_currency != 0) {
                        LogUtils.e("--------------type_currency:"+type_currency);
                        return type_currency * (o1.getSymbol().compareTo(o2.getSymbol()));
                    }
                    if (type_updatedPrice != 0) {
                        LogUtils.e("--------------type_currency:"+type_currency);

                        return type_updatedPrice * (
                                Double.parseDouble(o1.getLast())>
                                        Double.parseDouble(o2.getLast())
                                        ?1:-1);
                    }
                    if (type_UpsAndDowns != 0) {
                        LogUtils.e("--------------type_currency:"+type_currency);
                        return type_UpsAndDowns * (
                                Double.parseDouble(o1.getRate().replace("%",""))>
                                        Double.parseDouble(o2.getRate().replace("%",""))
                                        ?1:-1);
                    }
                    else {
                        return 0;
                    }
                }
            });
        }
        LogUtils.e("----------oldList:"+new Gson().toJson(oldList));
        return oldList;

    }

}
