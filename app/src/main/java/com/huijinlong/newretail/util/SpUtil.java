package com.huijinlong.newretail.util;

import android.content.Context;
import android.content.SharedPreferences;


import com.huijinlong.newretail.base.BaseApplication;

import java.util.Set;

/**
 * Created by shoulder on 16/4/7.
 */
public class SpUtil {


    public static final String USER_NAME = "username";//用户名
    public static final String NICK_NAME = "nick_name";//nick_name
    public static final String MOBILE = "mobile";//mobile
    public static final String EMAIL = "email";//email
    public static final String USER_ID = "user_id";//user_id
    public static final String PSD = "user_psd";//用户密码

    public static final String REFRESH_TOKEN = "REFRESH_TOKEN";
    public static final String AUTHORIZATION = "AUTHORIZATION";
    public static final String ISLOGIN = "isLogin";//登录状态
    public static final String IS_FOREGROUND = "isForeground";
    public static final String COME_TIME = "come_time";
    public static final String OUT_TIME = "out_time";
    public static final String INTERVAL_TIME = "interval_time";
    public static final String SIGN = "SIGN";
    public static final String LANGUAGE = "language";//语言
    public static final String NATIONALITY = "nationality";//国籍id
    public static final String MOBILE_CODE = "mobileCode";//区号code
    public static final String LOGINGESTURE = "logingesture";//手势密码
    public static final String FINGER = "logingfinger";//指纹密码


    //应用级别的设置
    private static SharedPreferences mPreferences = BaseApplication.getInstance().getSharedPreferences("nr_sp",
            Context.MODE_PRIVATE);
    //用户级别的设置
    private static SharedPreferences sUserPreferences;

    public static SharedPreferences getUserPreferences() {
        if (sUserPreferences == null) {
            sUserPreferences = BaseApplication.getInstance().getSharedPreferences("nr_sp" + getString(SpUtil.USER_ID, ""),
                    Context.MODE_PRIVATE);
        }
        return sUserPreferences;
    }

    public static SharedPreferences getDefault() {
        return mPreferences;
    }

    public static void putString(String key, String val) {
        getDefault().edit().putString(key, val).apply();
        LogUtils.d("putString-----"+key+"-----"+val);
    }

    public static void putStringImmediately(String key, String val) {
        getDefault().edit().putString(key, val).apply();
    }

    public static void putSetList(String key, Set val) {
        getDefault().edit().putStringSet(key, val).apply();
    }

    public static void putUserString(String key, String val) {
        getUserPreferences().edit().putString(key, val).apply();
    }

    public static String getUserString(String key, String val) {
        return getUserPreferences().getString(key, val);
    }

    public static String getString(String key, String defaultValue) {
        return getDefault().getString(key, defaultValue);
    }

    public static void putInt(String key, int value) {
        SharedPreferences settings = getDefault();
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public static void clear() {
        putUserString(SpUtil.USER_NAME, "");
        putString(SpUtil.NICK_NAME, "");
        putString(SpUtil.EMAIL, "");
        putString(SpUtil.MOBILE, "");
        putString(SpUtil.USER_ID, "");
        putString(SpUtil.AUTHORIZATION, "");
        putString(SpUtil.SIGN, "");
        putString(SpUtil.LANGUAGE, "");
        putString(SpUtil.NATIONALITY, "");
        putString(SpUtil.MOBILE_CODE, "");
    }

    public static void putUserInt(String key, int value) {
        getUserPreferences().edit().putInt(key, value).apply();
    }

    public static void putIntImediately(String key, int value) {
        SharedPreferences settings = getDefault();
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public static int getInt(String key, int defaultValue) {
        SharedPreferences settings = getDefault();
        return settings.getInt(key, defaultValue);
    }

    public static void putBoolean(String key, boolean val) {
        getDefault().edit().putBoolean(key, val).apply();
    }

    public static void putUserBoolean(String key, boolean val) {
        getUserPreferences().edit().putBoolean(key, val).apply();
    }

    public static boolean getBoolean(String key, boolean defaultValue) {
        return getDefault().getBoolean(key, defaultValue);
    }

    public static void putLong(String key, long val) {
        getDefault().edit().putLong(key, val).apply();
    }

    public static long getLong(String key, long defaultValue) {
        return getDefault().getLong(key, defaultValue);
    }

    public static Set getSetList(String key, Set defaultValue) {
        return getDefault().getStringSet(key, defaultValue);
    }

    /**
     * 是否存在
     *
     * @param key
     * @return
     */
    public static boolean isExits(String key) {
        return getDefault().contains(key);
    }

    /**
     * 是否存在
     *
     * @param key
     * @return
     */
    public static boolean isUserExits(String key) {
        return getUserPreferences().contains(key);
    }

    public static void release() {
        sUserPreferences = null;
    }
}