package com.huijinlong.newretail.util;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.huijinlong.newretail.R;
import com.huijinlong.newretail.base.BaseBean;
import com.huijinlong.newretail.net.MyCallback;
import com.huijinlong.newretail.net.RequestBuilder;

import java.util.TreeMap;

/**
 * @author penghuaijin
 * @date 2018/10/26 10:49 AM
 * @Description: (进行安全认证认用于绑定账号, 开启或关闭安全认证工具类)
 */
public class VerifySecurityUtil {
    private static VerifySecurityUtil instance = new VerifySecurityUtil();

    private String action = "";
    private AlertDialog.Builder builder;
    private TimeCountUtils phoneTimeCountUtils;
    private TimeCountUtils emailTimeCountUtils;
    private AlertDialog dialog;
    private VerifySecurityUtilListener listener;


    public VerifySecurityUtil() {

    }

    public static VerifySecurityUtil getInstance() {

        return instance;

    }


    public void init(Activity activity, String action, int verifyMobile, int verifyEmail, int verifyGoogleAuthenticator) {

        this.action = action;


        builder = new AlertDialog.Builder(activity, R.style.verifySecurityDialogStyle);
        LayoutInflater inflater = activity.getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_verify_security, null);

        TextView tvPhone = view.findViewById(R.id.tv_phone);
        TextView tvEmail = view.findViewById(R.id.tv_email);
        TextView tvSendPhoneCaptcha = view.findViewById(R.id.tv_send_phone_captcha);
        TextView tvSendEmailCaptcha = view.findViewById(R.id.tv_send_email_captcha);
        EditText etPhoneNum = view.findViewById(R.id.et_phone_num);
        EditText etGoogle = view.findViewById(R.id.et_google);
        EditText etEmailNum = view.findViewById(R.id.et_email_num);
        LinearLayout llPhone = view.findViewById(R.id.ll_phone);
        LinearLayout llEmail = view.findViewById(R.id.ll_email);
        LinearLayout llGoogle = view.findViewById(R.id.ll_google);

        tvPhone.setText(SpUtil.getString(SpUtil.MOBILE, ""));
        tvEmail.setText(SpUtil.getString(SpUtil.EMAIL, ""));

        tvSendPhoneCaptcha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPhoneCode();
            }
        });
        tvSendEmailCaptcha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getEmailCode();
            }
        });


        phoneTimeCountUtils = new TimeCountUtils(60000, 1000, tvSendPhoneCaptcha);
        emailTimeCountUtils = new TimeCountUtils(60000, 1000, tvSendEmailCaptcha);
        if (verifyMobile != 1) {
            llPhone.setVisibility(View.GONE);
        }
        if (verifyEmail != 1) {
            llEmail.setVisibility(View.GONE);
        }
        if (verifyGoogleAuthenticator != 1) {
            llGoogle.setVisibility(View.GONE);
        }
        dialog = builder.setView(view)
                .setTitle(activity.getString(R.string.aqrz))
                .setPositiveButton(activity.getString(R.string.qd), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        verifySecurity(etPhoneNum.getText().toString(),
                                etEmailNum.getText().toString(),
                                etGoogle.getText().toString());

                    }
                }).setNegativeButton(activity.getString(R.string.qx), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        listener.onCancelDialog();
                    }
                }).setCancelable(false)
                .show();

        //设置dialog大小，这里是一个小赠送，模块好的控件大小设置
        Window dialogWindow = dialog.getWindow();
        WindowManager manager = activity.getWindowManager();
        WindowManager.LayoutParams params = dialogWindow.getAttributes(); // 获取对话框当前的参数值
        dialogWindow.setGravity(Gravity.CENTER);//设置对话框位置
        Display d = manager.getDefaultDisplay(); // 获取屏幕宽、高度
        params.width = (int) (d.getWidth() * 0.8); // 宽度设置为屏幕的0.65，根据实际情况调整
        dialogWindow.setAttributes(params);


    }


    private void cancel() {
        phoneTimeCountUtils.cancel();
        emailTimeCountUtils.cancel();
        dialog = null;
        builder = null;

    }

    public void getPhoneCode() {
        TreeMap<String, String> params = new TreeMap<>();
        params.put("action", action);
        params.put("deviceType", "android");
        params.put("language", SpUtil.getString(SpUtil.LANGUAGE, "zh-cn"));

        RequestBuilder.execute(RequestBuilder.getNetService(true).getPhoneCodeVerify(MD5Util.getSignValue(params), params), "", new MyCallback<BaseBean>() {
            @Override
            public void onSuccess(BaseBean result) {
                super.onSuccess(result);
                phoneTimeCountUtils.start();
            }

            @Override
            public void onEmpty(BaseBean result) {
                super.onEmpty(result);

            }

            @Override
            public void onBusinessFailure(Exception e) {
                super.onBusinessFailure(e);
                ToastUtils.toast(e.toString());
            }

            @Override
            public void onFinalResponse(boolean isSuccess) {
                super.onFinalResponse(isSuccess);
            }
        });
    }


    public void getEmailCode() {
        TreeMap<String, String> params = new TreeMap<>();
        params.put("action", action);
        params.put("deviceType", "android");
        params.put("language", SpUtil.getString(SpUtil.LANGUAGE, "zh-cn"));

        RequestBuilder.execute(RequestBuilder.getNetService(true).getEmailCodeVerify(MD5Util.getSignValue(params), params), "", new MyCallback<BaseBean>() {
            @Override
            public void onSuccess(BaseBean result) {
                super.onSuccess(result);
                emailTimeCountUtils.start();
            }

            @Override
            public void onEmpty(BaseBean result) {
                super.onEmpty(result);

            }

            @Override
            public void onBusinessFailure(Exception e) {
                super.onBusinessFailure(e);
                ToastUtils.toast(e.toString());
            }

            @Override
            public void onFinalResponse(boolean isSuccess) {
                super.onFinalResponse(isSuccess);
            }
        });
    }

    public void verifySecurity(String mobileVerificationCode, String emailVerificationCode, String googleVerificationCode) {
        TreeMap<String, String> params = new TreeMap<>();
        params.put("action", action);
        params.put("deviceType", "android");
        params.put("mobileVerificationCode", mobileVerificationCode);
        params.put("emailVerificationCode", emailVerificationCode);
        params.put("googleVerificationCode", googleVerificationCode);
        params.put("language", SpUtil.getString(SpUtil.LANGUAGE, "zh-cn"));

        RequestBuilder.execute(RequestBuilder.getNetService(true).verifySecurity(MD5Util.getSignValue(params), params), "", new MyCallback<BaseBean>() {
            @Override
            public void onSuccess(BaseBean result) {
                super.onSuccess(result);

                dialog.dismiss();
                listener.onVerifySecuritySuccess();
            }

            @Override
            public void onEmpty(BaseBean result) {
                super.onEmpty(result);

            }

            @Override
            public void onBusinessFailure(Exception e) {
                super.onBusinessFailure(e);
                listener.onVerifySecurityFailed();
                ToastUtils.toast(e.toString());
            }

            @Override
            public void onFinalResponse(boolean isSuccess) {
                super.onFinalResponse(isSuccess);
            }
        });
    }

    public interface VerifySecurityUtilListener {
        void onVerifySecuritySuccess();

        void onVerifySecurityFailed();

        void onCancelDialog();


    }


    public void setListener(VerifySecurityUtilListener listener) {
        this.listener = listener;
    }
}
