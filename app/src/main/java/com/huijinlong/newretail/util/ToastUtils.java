package com.huijinlong.newretail.util;


import android.widget.Toast;

import com.haoge.easyandroid.easy.EasyToast;
import com.huijinlong.newretail.R;


/**
 * Created by scanor on 15/10/2.
 */
public class ToastUtils {

    public static void toast(String text) {
//        EasyToast.newBuilder(R.layout.toast_view, R.id.tv_toast).build().show(text);
        EasyToast.newBuilder().build().show(text);

    }

    public static void toastLong(String text) {
       EasyToast.newBuilder().setDuration(Toast.LENGTH_LONG).build().show(text);

    }

}
