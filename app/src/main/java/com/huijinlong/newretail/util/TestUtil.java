package com.huijinlong.newretail.util;

import com.google.gson.Gson;
import com.huijinlong.newretail.base.BaseApplication;
import com.huijinlong.newretail.bean.TestBean;
import com.huijinlong.newretail.requestbean.SpotProductsBean;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by penghuaijin on 2018/9/19.
 */

public class TestUtil {
    public static SpotProductsBean getAssetsTestBean(){
        SpotProductsBean bean=null;
        try {
            InputStream is= BaseApplication.getInstance().getResources().getAssets().open("atest.json");
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuilder builder = new StringBuilder();
            while ((line = bufferedReader.readLine()) != null) {
                builder.append(line);
            }
            is.close();
            bufferedReader.close();
            bean = new Gson().fromJson(builder.toString(), SpotProductsBean.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return  bean;
    }
}
