package com.huijinlong.newretail.util;

import android.util.Log;

import com.huijinlong.newretail.net.SortParams;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.TreeMap;

/**
 * Created by penghuaijin on 2018/9/10.
 */

public class MD5Util {

    public static String md5(String string)  {
        byte[] hash;
        try {

            hash = MessageDigest.getInstance("MD5").digest(string.getBytes("UTF-8"));
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Huh, MD5 should be supported?", e);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Huh, UTF-8 should be supported?", e);
        }

        StringBuilder hex = new StringBuilder(hash.length * 2);
        for (byte b : hash) {
            if ((b & 0xFF) < 0x10) hex.append("0");
            hex.append(Integer.toHexString(b & 0xFF));
        }
        return hex.toString();
    }

    public static String getSignValue(TreeMap<String,String> params)
    {
        Log.e("params", SortParams.getParams(params));
        Log.e("sign",md5(SortParams.getParams(params)+"c7e0bd6b299cc94db4a28b947bac5e63").toLowerCase());
        return md5(SortParams.getParams(params)+"c7e0bd6b299cc94db4a28b947bac5e63").toLowerCase();
    }
}
