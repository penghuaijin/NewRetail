package com.huijinlong.newretail.util;

import android.text.SpannableString;
import android.text.Spanned;
import android.text.SpannedString;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.widget.EditText;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by penghuaijin on 2018/9/26.
 */

public class ViewUtil {

    /**
     * 设置EditText的hint字体大小
     *
     * @param editText
     * @param hintText
     * @param size
     */
    public static void setEditTextHintSize(EditText editText, String hintText, int size) {
        SpannableString ss = new SpannableString(hintText);//定义hint的值
        AbsoluteSizeSpan ass = new AbsoluteSizeSpan(size, false);// dip：true为size的单位是dip，false为px。
        ss.setSpan(ass, 0, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        editText.setHint(new SpannedString(ss));
    }

    public static boolean checkEditTextForPassword(EditText editText, int minLength, String description) {
        if (editText.getText().toString().length() < minLength) {
            ToastUtils.toast(String.format("密码长度不得低于%s位", minLength));
            return false;
        }

//        String regEx = "[^a-zA-Z0-9]";  //只能输入字母或数字
//        Pattern p = Pattern.compile(regEx);
//        if (!p.matcher(editText.getText().toString()).matches()) {
//            ToastUtils.toast(String.format("%s只能是字母和数字组合", description));
//            return false;
//        }

        return true;
    }

    /**
     * 检查类型为num的输入框是否为不为零不为空的数字
     * @param s
     * @return
     */

    public static boolean checkTextNum(String s) {
        if (TextUtils.isEmpty(s)) {
            return false;
        }
        if (Double.parseDouble(s) == 0) {
            return false;
        }
        return true;
    }

    public static boolean checkTextNum(EditText editText) {
        if (TextUtils.isEmpty(editText.getText().toString())) {
            return false;
        }
        if (Double.parseDouble(editText.getText().toString()) == 0) {
            return false;
        }
        return true;
    }


}
