package com.huijinlong.newretail.util;

import android.content.Context;
import android.os.CountDownTimer;
import android.support.v4.content.ContextCompat;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.widget.TextView;

import com.huijinlong.newretail.R;


public class TimeCountUtils extends CountDownTimer {
    private SpannableStringBuilder sb;
    private ForegroundColorSpan colorSpan;
    private Context context;
    private TextView mTextView;


    /**
     * @param millisInFuture    The number of millis in the future from the call
     *                          to {@link #start()} until the countdown is done and {@link #onFinish()}
     *                          is called.
     * @param countDownInterval The interval along the way to receive
     *                          {@link #onTick(long)} callbacks.
     */
    public TimeCountUtils(long millisInFuture, long countDownInterval, TextView textView) {
        super(millisInFuture, countDownInterval);
        context = textView.getContext();
        sb = new SpannableStringBuilder();
        mTextView = textView;
        colorSpan = new ForegroundColorSpan(ContextCompat.getColor(context, android.R.color.holo_blue_dark));
    }

    /**
     * 倒计时过程中调用
     *
     * @param millisUntilFinished
     */
    @Override
    public void onTick(long millisUntilFinished) {
        //处理后的倒计时数值
        int time = (int) (Math.round((double) millisUntilFinished / 1000) - 1);
        //拼接要显示的字符串
        sb.clear(); //先把之前的字符串清除
        sb.append(String.valueOf(time));
        sb.append("s");
        //给秒数和单位设置蓝色前景色
        mTextView.setText(sb);
        //设置倒计时中的按钮外观
        mTextView.setClickable(false);//倒计时过程中将按钮设置为不可点击
//        mTextView.setBackgroundColor(Color.parseColor("#c7c7c7"));
        mTextView.setTextColor(ContextCompat.getColor(context, R.color.c_b5b));
        mTextView.setBackground(context.getDrawable(R.drawable.verification));
    }

    /**
     * 倒计时完成后调用
     */
    @Override
    public void onFinish() {
        //设置倒计时结束之后的按钮样式
        mTextView.setBackgroundColor(ContextCompat.getColor(context,R.color.theme_color));
        mTextView.setTextColor(ContextCompat.getColor(context, R.color.theme_color));
        mTextView.setText(context.getString(R.string.fsyzm));
        mTextView.setBackground(context.getDrawable(R.drawable.verification));
        mTextView.setClickable(true);
    }
}
