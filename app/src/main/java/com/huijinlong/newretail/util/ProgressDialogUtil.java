package com.huijinlong.newretail.util;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;


import com.huijinlong.newretail.R;

import butterknife.BindView;

/**
 * Created by ZX on 2018/6/5.
 */
public class ProgressDialogUtil {
    @BindView(R.id.dialog_view)
    LinearLayout mLinearLayout;
    private static ProgressDialog sProgressDialog;
    private static Dialog sDialog;

    /**
     * 显示加载中对话框
     *
     * @param context
     */
    public static void showLoadingDialog(Context context, String message, boolean isCancelable) {
        if (sProgressDialog == null) {
            sProgressDialog = new ProgressDialog(context, R.style.DialogMsgStyle);
            //点击提示框外面是否取消提示框
            sProgressDialog.setCanceledOnTouchOutside(true);
            //点击返回键是否取消提示框
            sProgressDialog.setCancelable(isCancelable);
            sProgressDialog.setIndeterminate(true);
            sProgressDialog.setMessage(message);
            sProgressDialog.show();
        }
    }

    /**
     * 关闭加载对话框
     */
    public static void closeLoadingDialog() {
        if (sProgressDialog != null) {
            if (sProgressDialog.isShowing()) {
                sProgressDialog.cancel();
            }
            sProgressDialog = null;
        }
    }

//    /**
//     * 得到自定义的progressDialog
//     *
//     * @param context
//     * @param msg
//     * @return
//     */
//    public static Dialog createLoadingDialog(Context context, String msg, boolean isCancelable) {
//
//        LayoutInflater inflater = LayoutInflater.from(context);
//        View v = inflater.inflate(R.layout.weiget_dialog, null);// 得到加载view
//        LinearLayout layout = (LinearLayout) v.findViewById(R.id.dialog_view);// 加载布局
//
//        TextView tipTextView = (TextView) v.findViewById(R.id.tipTextView);// 提示文字
//        TextView tvReset = (TextView) v.findViewById(R.id.tv_reset);
//        TextView tvSubimt = (TextView) v.findViewById(R.id.tv_subimt);
//
//        tvReset.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                closeDialog();
//            }
//        });
//
//        tvSubimt.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (mOnClickSubimtLister != null) {
//                    mOnClickSubimtLister.onSubimt();
//                }
//            }
//        });
//
//        // 使用ImageView显示动画
//        tipTextView.setText(msg);// 设置加载信息
//
//        Dialog loadingDialog = new Dialog(context, R.style.AppLadingDailog);// 创建自定义样式dialog
//
//        loadingDialog.setCancelable(isCancelable);// false不可以用“返回键”取消
//        loadingDialog.setCanceledOnTouchOutside(false);//点击提示框外面是否取消提示框
//        loadingDialog.setContentView(layout, new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.MATCH_PARENT,
//                LinearLayout.LayoutParams.MATCH_PARENT));// 设置布局
//        return loadingDialog;
//
//    }
//
//
//    /**
//     * 得到自定义的缓存progressDialog
//     *
//     * @param context
//     * @return
//     */
//    public static Dialog createLoadingDialogCache(Context context, boolean isCancelable) {
//        LayoutInflater inflater = LayoutInflater.from(context);
//        View v = inflater.inflate(R.layout.loading_dialog_cache, null);// 得到加载view
//        LinearLayout layout = (LinearLayout) v.findViewById(R.id.dialog_view);// 加载布局
//        // main.xml中的ImageView
//        TextView tipTextView = (TextView) v.findViewById(R.id.tipTextView);// 提示文字
//        TextView tvReset = (TextView) v.findViewById(R.id.tv_reset);
//        TextView tvSubimt = (TextView) v.findViewById(R.id.tv_subimt);
//
//        tvReset.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                closeDialog();
//            }
//        });
//
//        tvSubimt.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (mOnClickSubimtLister != null) {
//                    mOnClickSubimtLister.onSubimt();
//                }
//            }
//        });
//
//
//        // 使用ImageView显示动画
////        tipTextView.setText(msg);// 设置加载信息
//
//        Dialog loadingDialog = new Dialog(context, R.style.AppLadingDailog);// 创建自定义样式dialog
//
//        loadingDialog.setCancelable(isCancelable);// false不可以用“返回键”取消
//        loadingDialog.setCanceledOnTouchOutside(false);//点击提示框外面是否取消提示框
//        loadingDialog.setContentView(layout, new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.MATCH_PARENT,
//                LinearLayout.LayoutParams.MATCH_PARENT));// 设置布局
//        return loadingDialog;
//
//    }

//
//    /**
//     * 得到自定义的是否拨打电话progressDialog
//     *
//     * @param context
//     * @return
//     */
//    public static Dialog createLoadingDialoIsCall(Context context, String msg, boolean isCancelable) {
//
//        LayoutInflater inflater = LayoutInflater.from(context);
//        View v = inflater.inflate(R.layout.loading_dialog_iscall, null);// 得到加载view
//        LinearLayout layout = (LinearLayout) v.findViewById(R.id.dialog_view);// 加载布局
//        // main.xml中的ImageView
//        TextView tipTextView = (TextView) v.findViewById(R.id.tv_top_tips);// 提示文字
//        TextView tvReset = (TextView) v.findViewById(R.id.tv_reset);
//        TextView tvSubimt = (TextView) v.findViewById(R.id.tv_subimt);
//        tipTextView.setText(msg);
//        tvReset.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                closeDialog();
//            }
//        });
//
//        tvSubimt.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (mOnClickSubimtLister != null) {
//                    mOnClickSubimtLister.onSubimt();
//                }
//            }
//        });
//
//
//        Dialog loadingDialog = new Dialog(context, R.style.loading_dialog);// 创建自定义样式dialog
//
//        loadingDialog.setCancelable(isCancelable);// false不可以用“返回键”取消
//        loadingDialog.setCanceledOnTouchOutside(false);//点击提示框外面是否取消提示框
//        loadingDialog.setContentView(layout, new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.MATCH_PARENT,
//                LinearLayout.LayoutParams.MATCH_PARENT));// 设置布局
//        return loadingDialog;
//
//    }

//    /**
//     * 显示对话框
//     *
//     * @param context
//     * @param msg
//     * @param isCancelable
//     */
//    public static void showDialog(Context context, String msg, boolean isCancelable) {
//        if (dialog == null) {
//            dialog = createLoadingDialog(context, msg, isCancelable);
//            dialog.show();
//        }
//    }
//
//    /**
//     * 显示消除缓存对话框
//     *
//     * @param context
//     * @param isCancelable
//     */
//    public static void showDialogCache(Context context, boolean isCancelable) {
//        if (dialog == null) {
//            dialog = createLoadingDialogCache(context, isCancelable);
//            dialog.show();
//        }
//    }
//
//    /**
//     * 显示拨打电话对话框
//     *
//     * @param context
//     * @param isCancelable
//     */
//    public static void showDialogIsCall(Context context, String msg, boolean isCancelable) {
//        if (dialog == null) {
//            dialog = createLoadingDialoIsCall(context, msg, isCancelable);
//            dialog.show();
//        }
//    }

    /**
     * 关闭对话框
     */
    public static void closeDialog() {
        if (sDialog != null && sDialog.isShowing()) {
            sDialog.dismiss();
            sDialog = null;
        }
    }

    private static OnClickSubimtLister mOnClickSubimtLister;

    public static void setOnClickSubimtLister(OnClickSubimtLister onClickSubimtLister) {
        mOnClickSubimtLister = onClickSubimtLister;
    }

    public interface OnClickSubimtLister {
        void onSubimt();
    }


    /**
     * 创建自定义ProgressDialog
     *
     * @param context
     * @return
     */
    public static Dialog createLoadingDialog(Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
//        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ScreenUtil.dp2px(120), ScreenUtil.dp2px(120));
        View v = inflater.inflate(R.layout.dialog_loading, null);
        RelativeLayout layout = (RelativeLayout) v.findViewById(R.id.dialog_view);
        Dialog loadingDialog = new Dialog(context, R.style.dialogTheme);
        loadingDialog.setCanceledOnTouchOutside(false);
        loadingDialog.setCancelable(false);
//        loadingDialog.setCanceledOnTouchOutside(true);
//        loadingDialog.setCancelable(true);
        loadingDialog.setContentView(layout);
        return loadingDialog;
    }
}
