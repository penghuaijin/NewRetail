package com.huijinlong.newretail.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.huijinlong.newretail.R;


@SuppressLint("NewApi")
public class RootLoadingLayout extends RelativeLayout {

	private TextView mTvTips;
	private ProgressBar mBar;
	private Context mContext;
	private OnTipsClickListener mListener;

	public interface OnTipsClickListener {
		void onTipsClickListener();
	}

	public RootLoadingLayout(Context context) {
		super(context);
		mContext = context;
	}

	public RootLoadingLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		mContext = context;
	}

	public RootLoadingLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		mContext = context;
	}

	private void init() {
		mTvTips = (TextView) findViewById(R.id.loadTips);
		mTvTips.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(mListener !=null)
					mListener.onTipsClickListener();
			}
		});
		mBar = (ProgressBar) findViewById(R.id.progressBar);
	}

	@SuppressLint("MissingSuperCall")
	@Override
	protected void onFinishInflate() {
		init();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.LinearLayout#onLayout(boolean, int, int, int, int)
	 */
	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		super.onLayout(changed, l, t, r, b);
	}

	public void setVisible(int visible) {
		if (visible == View.VISIBLE || visible == View.INVISIBLE
				|| visible == View.GONE) {
			this.setVisibility(visible);
		}
	}

	public void setPbVisible(int visible) {
		if (visible == View.VISIBLE || visible == View.INVISIBLE
				|| visible == View.GONE) {
			mBar.setVisibility(visible);
			mTvTips.setVisibility(visible == View.VISIBLE ? View.GONE
					: View.VISIBLE);
		}
	}

	public void setTipsVisible(int visible) {
		if (visible == View.VISIBLE || visible == View.INVISIBLE
				|| visible == View.GONE) {
			mTvTips.setVisibility(visible);
			mBar.setVisibility(visible == View.VISIBLE ? View.GONE
					: View.VISIBLE);
		}
	}
	public void setTipsText(String text) {
		mTvTips.setText(text);
	}
	public void setOnTipsClickListener(OnTipsClickListener listener) {
		mListener = listener;
	}
}
