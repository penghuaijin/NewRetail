package com.huijinlong.newretail.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.huijinlong.newretail.R;
import com.huijinlong.newretail.util.PxUtil;

/**
 * Created by penghuaijin on 2018/9/20.
 */

public class SimpleDividerDecoration extends RecyclerView.ItemDecoration {
    private int dividerHeight, paddingLeft, paddingRight;
    private Paint dividerPaint;
    private Paint paddingPaint;
    private Context mContext;

    public SimpleDividerDecoration(Context context, int paddingLeft, int paddingRight, int color, int dividerHeight) {
        dividerPaint = new Paint();
        paddingPaint = new Paint();
        dividerPaint.setColor(color);
        paddingPaint.setColor(context.getResources().getColor(R.color.theme_color));
//        this.dividerHeight = context.getResources().getDimensionPixelSize(R.dimen.divider_height);
        this.dividerHeight = dividerHeight;
        this.paddingLeft = paddingLeft;
        this.paddingRight = paddingRight;
        this.mContext = context;
    }


    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        outRect.bottom = dividerHeight;
    }

    @Override
    public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
        int childCount = parent.getChildCount();
        int left = parent.getPaddingLeft();
        int right = parent.getWidth() - parent.getPaddingRight();

        for (int i = 0; i < childCount - 1; i++) {
            View view = parent.getChildAt(i);
            float top = view.getBottom();
            float bottom = view.getBottom() + dividerHeight;
            c.drawRect(0, top, PxUtil.dpToPx(mContext, paddingLeft), bottom, paddingPaint);
            c.drawRect(left + PxUtil.dpToPx(mContext, paddingLeft), top, right, bottom, dividerPaint);
            c.drawRect(right - PxUtil.dpToPx(mContext, paddingRight), top, right, bottom, paddingPaint);
        }
    }

}
