package com.huijinlong.newretail.view;

import android.content.Context;
import android.view.View;
import android.widget.Button;

import com.huijinlong.newretail.R;

/**
 * @author penghuaijin
 * @date 2018/11/30 2:55 PM
 * @Description: ()
 */
public class SelectLanguagePopupWindow extends BasePopupWindow {
    private Button btnChinese;
    private Button btnChineseX;
    private Button btnEnglish;
    private Button btnCancel;

    private OnLanguageSelectListener onLanguageSelectListener;

    public SelectLanguagePopupWindow(Context context, OnLanguageSelectListener languageSelectListener) {
        super(context);
        this.onLanguageSelectListener = languageSelectListener;
    }

    @Override
    public boolean isNeedBackgroundHalfTransition() {
        return true;
    }

    @Override
    protected int bindLayout() {
        return R.layout.popup_window_language_change;
    }

    @Override
    protected void initView(View parentView) {
        btnChinese = (Button) parentView.findViewById(R.id.btnChinese);
        btnChineseX = (Button) parentView.findViewById(R.id.btnChineseX);
        btnEnglish = (Button) parentView.findViewById(R.id.btnEnglish);
        btnCancel = (Button) parentView.findViewById(R.id.btnCancel);
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initListener() {
        btnChinese.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (onLanguageSelectListener != null) {
                    onLanguageSelectListener.OnSelectChinese();
                }
            }
        });

        btnChineseX.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (onLanguageSelectListener != null) {
                    onLanguageSelectListener.OnSelectChineseX();
                }
            }
        });

        btnEnglish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (onLanguageSelectListener != null) {
                    onLanguageSelectListener.OnSelectEnglish();
                }
            }
        });


        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    public interface OnLanguageSelectListener {

        public void OnSelectChinese();

        public void OnSelectChineseX();

        public void OnSelectEnglish();
    }

}
