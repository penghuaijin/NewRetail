package com.huijinlong.newretail.view;

import android.widget.LinearLayout;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.huijinlong.newretail.R;

/**
 * @author penghuaijin
 * @date 2018/10/25 8:50 PM
 * @Description: ()
 */
public class IPEditText extends LinearLayout {

    private EditText mFirstIP;
    private EditText mSecondIP;
    private EditText mThirdIP;
    private EditText mFourthIP;

    private String mText;
    private String mText1;
    private String mText2;
    private String mText3;
    private String mText4;


    public IPEditText(Context context, AttributeSet attrs) {
        super(context, attrs);

        /**
         * 初始化控件
         */
        View view = LayoutInflater.from(context).inflate(
                R.layout.view_ip_edittext, this);
        mFirstIP = view.findViewById(R.id.ip_first);
        mSecondIP = view.findViewById(R.id.ip_second);
        mThirdIP = view.findViewById(R.id.ip_third);
        mFourthIP = view.findViewById(R.id.ip_fourth);



        OperatingEditText(context);
    }

    /**
     * 获得EditText中的内容,当每个Edittext的字符达到三位时,自动跳转到下一个EditText,当用户点击.时,
     * 下一个EditText获得焦点
     */
    private void OperatingEditText(final Context context) {
        mFirstIP.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                /**
                 * 获得EditTe输入内容,做判断,如果大于255,提示不合法,当数字为合法的三位数下一个EditText获得焦点,
                 * 用户点击啊.时,下一个EditText获得焦点
                 */

                mText1 = s.toString().trim();



                if ( s.length() > 0) {
                    if (Integer.parseInt(mText1) > 255) {
                        Toast.makeText(context, "请输入合法的ip地址",
                                Toast.LENGTH_LONG).show();
                        return;
                    }
                    if (s.length() > 2) {

                        mSecondIP.setFocusable(true);
                        mSecondIP.requestFocus();
                    }
                }


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mSecondIP.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                /**
                 * 获得EditTe输入内容,做判断,如果大于255,提示不合法,当数字为合法的三位数下一个EditText获得焦点,
                 * 用户点击啊.时,下一个EditText获得焦点
                 */
                mText2 = s.toString().trim();


                if (s.length() > 0) {

                    if (Integer.parseInt(mText2) > 255) {
                        Toast.makeText(context, "请输入合法的ip地址",
                                Toast.LENGTH_LONG).show();
                        return;
                    }
                    if (s.length() > 2 ) {

                        mThirdIP.setFocusable(true);
                        mThirdIP.requestFocus();
                    }
                }


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mThirdIP.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                /**
                 * 获得EditTe输入内容,做判断,如果大于255,提示不合法,当数字为合法的三位数下一个EditText获得焦点,
                 * 用户点击啊.时,下一个EditText获得焦点
                 */
                mText3 = s.toString().trim();




                if ( s.length() > 0) {
                    if (Integer.parseInt(mText3) > 255) {
                        Toast.makeText(context, "请输入合法的ip地址",
                                Toast.LENGTH_LONG).show();
                        return;
                    }
                    if (s.length() > 2) {


                        mFourthIP.setFocusable(true);
                        mFourthIP.requestFocus();
                    }
                }
                mText3 = s.toString().trim();



            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mFourthIP.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                /**
                 * 获得EditTe输入内容,做判断,如果大于255,提示不合法,当数字为合法的三位数下一个EditText获得焦点,
                 * 用户点击啊.时,下一个EditText获得焦点
                 */
                if (s.length() > 0) {
                    mText4 = s.toString().trim();

                    if (Integer.parseInt(mText4) > 255) {
                        Toast.makeText(context, "请输入合法的ip地址", Toast.LENGTH_LONG)
                                .show();
                        return;
                    }

                }


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public String getText(Context context) {
        if (TextUtils.isEmpty(mText1) || TextUtils.isEmpty(mText2)
                || TextUtils.isEmpty(mText3) || TextUtils.isEmpty(mText4)) {
            Toast.makeText(context, "请输入合法的ip地址", Toast.LENGTH_LONG).show();
        }
        return mText1 + "." + mText2 + "." + mText3 + "." + mText4;
    }

    public boolean isEmpty() {
        if (TextUtils.isEmpty(mText1) || TextUtils.isEmpty(mText2)
                || TextUtils.isEmpty(mText3) || TextUtils.isEmpty(mText4)) {
            return true;
        }
        return false;
    }
}
