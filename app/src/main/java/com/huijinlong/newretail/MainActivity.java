package com.huijinlong.newretail;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RemoteViews;
import android.widget.TextView;

import com.google.gson.Gson;
import com.huijinlong.newretail.base.BaseActivity;
import com.huijinlong.newretail.base.BaseApplication;
import com.huijinlong.newretail.bean.CheckUpdateBean;
import com.huijinlong.newretail.constant.Constant;
import com.huijinlong.newretail.eventbus.MarketItemCLickEvent;
import com.huijinlong.newretail.eventbus.WalletEvent;
import com.huijinlong.newretail.ui.CheckUpdatePresent;
import com.huijinlong.newretail.ui.CheckUpdateView;
import com.huijinlong.newretail.ui.market.MarketFragment;
import com.huijinlong.newretail.ui.market.MenuItemAdapter;
import com.huijinlong.newretail.ui.market.ProjectInfoPagerAdapter;
import com.huijinlong.newretail.ui.mine.UserFragment;
import com.huijinlong.newretail.ui.spotgoods.SpotFragment;
import com.huijinlong.newretail.ui.wallet.WalletFragment;
import com.huijinlong.newretail.util.CommonAlertDialog;
import com.huijinlong.newretail.util.DownloadManagerUtil;
import com.huijinlong.newretail.util.LogUtils;
import com.huijinlong.newretail.util.ToastUtils;

import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.ViewPagerHelper;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.CommonPagerTitleView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity<CheckUpdateView, CheckUpdatePresent> implements CheckUpdateView {
    @BindView(R.id.lin)
    LinearLayout lin;
    private ViewPager viewPager;
    private MagicIndicator magicIndicator;
    private ArrayList<Drawable> iconList = new ArrayList<>();
    private List<String> stringList = new ArrayList<>();
    private ProjectInfoPagerAdapter projectInfoPagerAdapter;

    private ArrayList<Fragment> fragmentList = new ArrayList<>();
    private DrawerLayout drawerLayout;
    private ListView mLvLeftMenu;

    private CommonAlertDialog commonAlertDialog;

    @Override
    protected void initView() {
        setToolbarVisibility(false);
        initTab();
        init();
        initMagicIndicator();

        initDrawerLayout();
        initData();
        EventBus.getDefault().register(this);
        mPresenter.getCheckUpdate();
    }

    //返回值就是导航栏的高度,得到的值单位px
    public float getNavigationBarHeight() {
        float result = 0;
        int resourceId = getResources().getIdentifier("navigation_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimension(resourceId);
        }
        return result;
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_main;
    }

    private void initDrawerLayout() {
        drawerLayout = findViewById(R.id.dl_wallet);

        drawerLayout.closeDrawer(Gravity.START);
        mLvLeftMenu = findViewById(R.id.id_lv_left_menu);
        mLvLeftMenu.setAdapter(new MenuItemAdapter(this, drawerLayout));
    }

    private void initData() {

        fragmentList.add(new MarketFragment());
        fragmentList.add(new SpotFragment());
        WalletFragment walletFragment = WalletFragment.newInstance("资产");
        walletFragment.setOnNavigationShowListener(new WalletFragment.OnNavigationShowListener() {
            @Override
            public void OnNavigationShow() {
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawers();
                } else {

                    drawerLayout.openDrawer(GravityCompat.START);
                }
            }
        });
        fragmentList.add(walletFragment);
        fragmentList.add(new UserFragment());
        projectInfoPagerAdapter = new ProjectInfoPagerAdapter(getSupportFragmentManager(), fragmentList);
        viewPager.setAdapter(projectInfoPagerAdapter);


    }


    private void initTab() {
        iconList.add(getResources().getDrawable(R.mipmap.ic_market_selected));
        iconList.add(getResources().getDrawable(R.mipmap.ic_spot_selected));
        iconList.add(getResources().getDrawable(R.mipmap.ic_wallet_selected));
        iconList.add(getResources().getDrawable(R.mipmap.ic_user_selected));
        iconList.add(getResources().getDrawable(R.mipmap.ic_market_deselected));
        iconList.add(getResources().getDrawable(R.mipmap.ic_spot_deselected));
        iconList.add(getResources().getDrawable(R.mipmap.ic_wallet_deselected));
        iconList.add(getResources().getDrawable(R.mipmap.ic_user_deselected));

        stringList = Arrays.asList(getResources().getStringArray(R.array.arr_main_tab));
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initMagicIndicator() {
        magicIndicator.setBackgroundColor(getResources().getColor(R.color.theme_color));

        CommonNavigator commonNavigator = new CommonNavigator(this);
        commonNavigator.setAdjustMode(true);

        commonNavigator.setAdapter(new CommonNavigatorAdapter() {
            @Override
            public int getCount() {
                return stringList.size();

            }

            @Override
            public IPagerTitleView getTitleView(Context context, final int i) {
                final CommonPagerTitleView commonPagerTitleView = new CommonPagerTitleView(MainActivity.this);
                View inflate = LayoutInflater.from(MainActivity.this).inflate(R.layout.simple_pager_title_layout, null);
                final TextView textView = inflate.findViewById(R.id.title_text);
                textView.setText(stringList.get(i));
                final ImageView icon = inflate.findViewById(R.id.title_icon);
                final View paddingView = inflate.findViewById(R.id.view_padding);
                paddingView.setVisibility(View.GONE);
                if (i == 0) {
                    icon.setImageDrawable(iconList.get(i));
                } else {
                    icon.setImageDrawable(iconList.get(i + 4));
                }
                commonPagerTitleView.setContentView(inflate);
                commonPagerTitleView.setOnPagerTitleChangeListener(new CommonPagerTitleView.OnPagerTitleChangeListener() {
                    @Override
                    public void onSelected(int i, int i1) {
                        textView.setTextColor(getResources().getColor(R.color.c_55a));
                        icon.setImageDrawable(iconList.get(i));
                        if (i == 2) {
                            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                            //打开手势滑动

                        } else {
                            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                            //禁止手势滑动

                        }

                    }

                    @Override
                    public void onDeselected(int i, int i1) {
                        textView.setTextColor(getResources().getColor(R.color.c_b5b));
                        icon.setImageDrawable(iconList.get(i + 4));

                    }

                    @Override
                    public void onLeave(int i, int i1, float v, boolean b) {

                    }

                    @Override
                    public void onEnter(int i, int i1, float v, boolean b) {

                    }
                });
                commonPagerTitleView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        viewPager.setCurrentItem(i);
                        if (i == 2) {
                            EventBus.getDefault().post(new WalletEvent(true));
                        }

                    }
                });

                return commonPagerTitleView;
            }

            @Override
            public IPagerIndicator getIndicator(Context context) {
                return null;
            }
        });

        magicIndicator.setNavigator(commonNavigator);

        ViewPagerHelper.bind(magicIndicator, viewPager);
    }

    private void init() {
        viewPager = findViewById(R.id.view_pager);
        magicIndicator = findViewById(R.id.magic_indicator1);

    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();

    }

    @Override
    public CheckUpdatePresent initPresenter() {
        return new CheckUpdatePresent(this);
    }

    private long exitTime = 0;

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            exit();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }


    public void exit() {
        if ((System.currentTimeMillis() - exitTime) > 2000) {

            ToastUtils.toast("再按一次退出程序");
            exitTime = System.currentTimeMillis();
        } else {
            finish();
            System.exit(0);
        }
    }

    @Subscribe()
    public void onEvent(MarketItemCLickEvent event) {
        viewPager.setCurrentItem(1);
    }


    @Override
    public void getCheck(CheckUpdateBean checkUpdateBean) {
        String a = new Gson().toJson(checkUpdateBean);
        Log.e("1111", a);
        showSureCancelDialog(checkUpdateBean);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }


    private DownloadManagerUtil downloadManagerUtil;
    long downloadId = 0;

    public void showSureCancelDialog(CheckUpdateBean updateBean) {
        downloadManagerUtil = new DownloadManagerUtil(this);
        if (commonAlertDialog != null && commonAlertDialog.isShowing()) commonAlertDialog.dismiss();

        commonAlertDialog = new CommonAlertDialog(this).builder();
        commonAlertDialog.setTitle("发现了新版本");
        if (!TextUtils.isEmpty(updateBean.getData().getVersionDesc())) {
            commonAlertDialog.setMsg(updateBean.getData().getVersionDesc().replace("\\r\\n", " "));

        }
        commonAlertDialog.setCancelable(false);
        commonAlertDialog.setPositiveButton(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (downloadId != 0) {
                    downloadManagerUtil.clearCurrentTask(downloadId);
                }
                downloadId = downloadManagerUtil.download(updateBean.getData().getUrl(), "发现了新版本", "");
               // finish();
            }
        });

        if(updateBean.getData().getStatusX()==2)
        {
            //强制更新

        }else
        {
            //建议更新
            commonAlertDialog.setNegativeButton(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
        }


        commonAlertDialog.show();

    }

    @Override
    public void showLoading() {
        mLoadingDialog.show();
    }

    @Override
    public void hideLoading() {
        mLoadingDialog.dismiss();
    }

    @Override
    public void showError(String error) {
        ToastUtils.toast(error);
    }


}
