package com.huijinlong.newretail.bean;

import com.huijinlong.newretail.base.BaseBean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author zsg
 * @date 2018/10/26 11:03
 * @Version 1.0
 * @Description: ()
 */
public class AssetBean extends BaseBean {

    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable{
        /**
         * cid : 1
         * currency : BTC
         * balance : 2000.05503100
         * frozen : 0.22260000
         * withdraw : 2000.05503100
         * fee : 1.0E-4
         * feeType : 1
         * transferFee : 0
         * transferFeeType : 1
         * limit : 0
         * transferLimit : 0
         * depositLimit : 0.001
         * depositStatus : 2
         * withdrawStatus : 1
         * transferStatus : 3
         * depositTip : 充值暂时关闭
         * withdrawTip :
         * transferTip :
         * confirmations : 1
         * withdrawConfirmations : 6
         * rate : 3.4556798
         */

        private int cid;
        private String currency;
        private String balance;
        private String frozen;
        private String withdraw;
        private BigDecimal fee;
        private int feeType;
        private int transferFee;
        private int transferFeeType;
        private BigDecimal limit;
        private int transferLimit;
        private double depositLimit;
        private int depositStatus;
        private int withdrawStatus;
        private int transferStatus;
        private String depositTip;
        private String withdrawTip;
        private String transferTip;
        private int confirmations;
        private int withdrawConfirmations;
        private BigDecimal rate;

        public int getCid() {
            return cid;
        }

        public void setCid(int cid) {
            this.cid = cid;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public String getBalance() {
            return balance;
        }

        public void setBalance(String balance) {
            this.balance = balance;
        }

        public String getFrozen() {
            return frozen;
        }

        public void setFrozen(String frozen) {
            this.frozen = frozen;
        }

        public String getWithdraw() {
            return withdraw;
        }

        public void setWithdraw(String withdraw) {
            this.withdraw = withdraw;
        }

        public BigDecimal getFee() {
            return fee;
        }

        public void setFee(BigDecimal fee) {
            this.fee = fee;
        }

        public int getFeeType() {
            return feeType;
        }

        public void setFeeType(int feeType) {
            this.feeType = feeType;
        }

        public int getTransferFee() {
            return transferFee;
        }

        public void setTransferFee(int transferFee) {
            this.transferFee = transferFee;
        }

        public int getTransferFeeType() {
            return transferFeeType;
        }

        public void setTransferFeeType(int transferFeeType) {
            this.transferFeeType = transferFeeType;
        }

        public BigDecimal getLimit() {
            return limit;
        }

        public void setLimit(BigDecimal limit) {
            this.limit = limit;
        }

        public int getTransferLimit() {
            return transferLimit;
        }

        public void setTransferLimit(int transferLimit) {
            this.transferLimit = transferLimit;
        }

        public double getDepositLimit() {
            return depositLimit;
        }

        public void setDepositLimit(double depositLimit) {
            this.depositLimit = depositLimit;
        }

        public int getDepositStatus() {
            return depositStatus;
        }

        public void setDepositStatus(int depositStatus) {
            this.depositStatus = depositStatus;
        }

        public int getWithdrawStatus() {
            return withdrawStatus;
        }

        public void setWithdrawStatus(int withdrawStatus) {
            this.withdrawStatus = withdrawStatus;
        }

        public int getTransferStatus() {
            return transferStatus;
        }

        public void setTransferStatus(int transferStatus) {
            this.transferStatus = transferStatus;
        }

        public String getDepositTip() {
            return depositTip;
        }

        public void setDepositTip(String depositTip) {
            this.depositTip = depositTip;
        }

        public String getWithdrawTip() {
            return withdrawTip;
        }

        public void setWithdrawTip(String withdrawTip) {
            this.withdrawTip = withdrawTip;
        }

        public String getTransferTip() {
            return transferTip;
        }

        public void setTransferTip(String transferTip) {
            this.transferTip = transferTip;
        }

        public int getConfirmations() {
            return confirmations;
        }

        public void setConfirmations(int confirmations) {
            this.confirmations = confirmations;
        }

        public int getWithdrawConfirmations() {
            return withdrawConfirmations;
        }

        public void setWithdrawConfirmations(int withdrawConfirmations) {
            this.withdrawConfirmations = withdrawConfirmations;
        }

        public BigDecimal getRate() {
            return rate;
        }

        public void setRate(BigDecimal rate) {
            this.rate = rate;
        }
    }
}
