package com.huijinlong.newretail.bean;

import com.google.gson.annotations.SerializedName;
import com.huijinlong.newretail.base.BaseBean;

import java.util.List;

/**
 * @author zsg
 * @date 2018/10/25 16:58
 * @Version 1.0
 * @Description: ()
 */
public class WithDrawsBean extends BaseBean {


    /**
     * data : {"currentPage":1,"total":4,"pageSize":5,"list":[{"currency":"ETH","amount":"32.0000000000","status":"已发送","time":"2018-08-15 20:12:15","url":"https://etherscan.io/tx/53j34i9583495r345rh3988h39r4395rj43","type":"聚宝盆充币"},{"currency":"LTC","amount":"56.0000000000","status":"不可发送","time":"2018-08-15 20:12:15","url":"https://live.blockcypher.com/ltc/tx/82394ry5uwhre89q234r5yh39r","type":"钱包充币"}]}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * currentPage : 1
         * total : 4
         * pageSize : 5
         * list : [{"currency":"ETH","amount":"32.0000000000","status":"已发送","time":"2018-08-15 20:12:15","url":"https://etherscan.io/tx/53j34i9583495r345rh3988h39r4395rj43","type":"聚宝盆充币"},{"currency":"LTC","amount":"56.0000000000","status":"不可发送","time":"2018-08-15 20:12:15","url":"https://live.blockcypher.com/ltc/tx/82394ry5uwhre89q234r5yh39r","type":"钱包充币"}]
         */

        private int currentPage;
        private int total;
        private int pageSize;
        private List<ListBean> list;

        public int getCurrentPage() {
            return currentPage;
        }

        public void setCurrentPage(int currentPage) {
            this.currentPage = currentPage;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public static class ListBean {
            /**
             * currency : ETH
             * amount : 32.0000000000
             * status : 已发送
             * time : 2018-08-15 20:12:15
             * url : https://etherscan.io/tx/53j34i9583495r345rh3988h39r4395rj43
             * type : 聚宝盆充币
             */

            private String currency;
            private String amount;
            @SerializedName("status")
            private String statusX;
            private String time;
            private String url;
            private String type;

            public String getCurrency() {
                return currency;
            }

            public void setCurrency(String currency) {
                this.currency = currency;
            }

            public String getAmount() {
                return amount;
            }

            public void setAmount(String amount) {
                this.amount = amount;
            }

            public String getStatusX() {
                return statusX;
            }

            public void setStatusX(String statusX) {
                this.statusX = statusX;
            }

            public String getTime() {
                return time;
            }

            public void setTime(String time) {
                this.time = time;
            }

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }
        }
    }
}
