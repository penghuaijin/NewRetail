package com.huijinlong.newretail.bean;

/**
 * Created by penghuaijin on 2018/10/10.
 */

public class SocketBeanLogin {
    String type;
    String token;

    public SocketBeanLogin(String type, String token) {
        this.type = type;
        this.token = token;
    }
}
