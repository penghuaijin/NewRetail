package com.huijinlong.newretail.bean;

import com.huijinlong.newretail.base.BaseBean;

import java.util.List;

/**
 * @author zsg
 * @date 2018/10/19 15:32
 * @Version 1.0
 * @Description: ()
 */
public class CurrenciesBean extends BaseBean {

    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * cid : 1
         * currency : BTC
         * currencyType : 0
         * name : Bitcoin
         * decimal : 8
         * feeType : 1
         * fee : 0.00010000
         * explorer : https://www.blockchain.com/btc/tx
         * limit : 0
         * transferLimit : 0
         * depositLimit : 0.001
         * balance
         */

        private int cid;
        private String currency;
        private int currencyType;
        private String name;
        private int decimal;
        private int feeType;
        private String fee;
        private String explorer;
        private String limit;
        private int transferLimit;
        private double depositLimit;
        private String type;
        private String balance;

        public String getBalance() {
            return balance;
        }

        public void setBalance(String balance) {
            this.balance = balance;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public int getCid() {
            return cid;
        }

        public void setCid(int cid) {
            this.cid = cid;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public int getCurrencyType() {
            return currencyType;
        }

        public void setCurrencyType(int currencyType) {
            this.currencyType = currencyType;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getDecimal() {
            return decimal;
        }

        public void setDecimal(int decimal) {
            this.decimal = decimal;
        }

        public int getFeeType() {
            return feeType;
        }

        public void setFeeType(int feeType) {
            this.feeType = feeType;
        }

        public String getFee() {
            return fee;
        }

        public void setFee(String fee) {
            this.fee = fee;
        }

        public String getExplorer() {
            return explorer;
        }

        public void setExplorer(String explorer) {
            this.explorer = explorer;
        }

        public String getLimit() {
            return limit;
        }

        public void setLimit(String limit) {
            this.limit = limit;
        }

        public int getTransferLimit() {
            return transferLimit;
        }

        public void setTransferLimit(int transferLimit) {
            this.transferLimit = transferLimit;
        }

        public double getDepositLimit() {
            return depositLimit;
        }

        public void setDepositLimit(double depositLimit) {
            this.depositLimit = depositLimit;
        }
    }
}
