package com.huijinlong.newretail.bean;

/**
 * Created by penghuaijin on 2018/10/10.
 */

public class SocketBeanJoin {

    private String instrument;
    private String type;

    public SocketBeanJoin(String instrument) {
        this.instrument = instrument;
    }

    public SocketBeanJoin(String instrument, String type) {
        this.instrument = instrument;
        this.type = type;
    }

    public String getInstrument() {
        return instrument;
    }

    public void setInstrument(String instrument) {
        this.instrument = instrument;
    }
}
