package com.huijinlong.newretail.bean;

/**
 * @author penghuaijin
 * @date 2018/10/24 3:12 PM
 * @Description: ()
 */
public class TickerSocketEntity {


    /**
     * last : 11.00
     * open : 11.00
     * high : 11.00
     * low : 5.00
     * vol : 5.1818
     * turnVol : 50.999800
     * close : 11.00
     * lastest_modify : 1540362964
     * date : 2018-10-24
     * type : ticker
     * instrument : BTCUSDT
     */

    private String last;
    private String open;
    private String high;
    private String low;
    private String vol;
    private String turnVol;
    private String close;
    private String lastest_modify;
    private String date;
    private String type;
    private String instrument;

    public String getLast() {
        return last;
    }

    public void setLast(String last) {
        this.last = last;
    }

    public String getOpen() {
        return open;
    }

    public void setOpen(String open) {
        this.open = open;
    }

    public String getHigh() {
        return high;
    }

    public void setHigh(String high) {
        this.high = high;
    }

    public String getLow() {
        return low;
    }

    public void setLow(String low) {
        this.low = low;
    }

    public String getVol() {
        return vol;
    }

    public void setVol(String vol) {
        this.vol = vol;
    }

    public String getTurnVol() {
        return turnVol;
    }

    public void setTurnVol(String turnVol) {
        this.turnVol = turnVol;
    }

    public String getClose() {
        return close;
    }

    public void setClose(String close) {
        this.close = close;
    }

    public String getLastest_modify() {
        return lastest_modify;
    }

    public void setLastest_modify(String lastest_modify) {
        this.lastest_modify = lastest_modify;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getInstrument() {
        return instrument;
    }

    public void setInstrument(String instrument) {
        this.instrument = instrument;
    }
}
