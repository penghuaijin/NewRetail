package com.huijinlong.newretail.bean;

import com.huijinlong.newretail.base.BaseBean;

/**
 * @author zsg
 * @date 2018/10/26 14:37
 * @Version 1.0
 * @Description: ()
 */
public class SecurityInfoBean extends BaseBean{


    /**
     * data : {"base":{"account":"139*****555","userId":4},"security":{"level":{"level":1,"text":"低","tip":"强烈建议开启2项双重身份验证"},"email":{"email":"555****77@qq.com","textBind":"修改","textVerify":"开启验证","textVerifyEnable":1,"warnVerify":1},"mobile":{"mobile":"139*****555","textBind":"修改","textVerify":"关闭验证","textVerifyEnable":0,"warnVerify":0},"googleAuthenticator":{"googleAuthenticator":"已绑定","textBind":"修改","textVerify":"开启验证","textVerifyEnable":1,"warnVerify":1}}}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * base : {"account":"139*****555","userId":4}
         * security : {"level":{"level":1,"text":"低","tip":"强烈建议开启2项双重身份验证"},"email":{"email":"555****77@qq.com","textBind":"修改","textVerify":"开启验证","textVerifyEnable":1,"warnVerify":1},"mobile":{"mobile":"139*****555","textBind":"修改","textVerify":"关闭验证","textVerifyEnable":0,"warnVerify":0},"googleAuthenticator":{"googleAuthenticator":"已绑定","textBind":"修改","textVerify":"开启验证","textVerifyEnable":1,"warnVerify":1}}
         */

        private BaseBean base;
        private SecurityBean security;

        public BaseBean getBase() {
            return base;
        }

        public void setBase(BaseBean base) {
            this.base = base;
        }

        public SecurityBean getSecurity() {
            return security;
        }

        public void setSecurity(SecurityBean security) {
            this.security = security;
        }

        public static class BaseBean {
            /**
             * account : 139*****555
             * userId : 4
             */

            private String account;
            private int userId;

            public String getAccount() {
                return account;
            }

            public void setAccount(String account) {
                this.account = account;
            }

            public int getUserId() {
                return userId;
            }

            public void setUserId(int userId) {
                this.userId = userId;
            }
        }

        public static class SecurityBean {
            /**
             * level : {"level":1,"text":"低","tip":"强烈建议开启2项双重身份验证"}
             * email : {"email":"555****77@qq.com","textBind":"修改","textVerify":"开启验证","textVerifyEnable":1,"warnVerify":1}
             * mobile : {"mobile":"139*****555","textBind":"修改","textVerify":"关闭验证","textVerifyEnable":0,"warnVerify":0}
             * googleAuthenticator : {"googleAuthenticator":"已绑定","textBind":"修改","textVerify":"开启验证","textVerifyEnable":1,"warnVerify":1}
             */

            private LevelBean level;
            private EmailBean email;
            private MobileBean mobile;
            private GoogleAuthenticatorBean googleAuthenticator;

            public LevelBean getLevel() {
                return level;
            }

            public void setLevel(LevelBean level) {
                this.level = level;
            }

            public EmailBean getEmail() {
                return email;
            }

            public void setEmail(EmailBean email) {
                this.email = email;
            }

            public MobileBean getMobile() {
                return mobile;
            }

            public void setMobile(MobileBean mobile) {
                this.mobile = mobile;
            }

            public GoogleAuthenticatorBean getGoogleAuthenticator() {
                return googleAuthenticator;
            }

            public void setGoogleAuthenticator(GoogleAuthenticatorBean googleAuthenticator) {
                this.googleAuthenticator = googleAuthenticator;
            }

            public static class LevelBean {
                /**
                 * level : 1
                 * text : 低
                 * tip : 强烈建议开启2项双重身份验证
                 */

                private int level;
                private String text;
                private String tip;

                public int getLevel() {
                    return level;
                }

                public void setLevel(int level) {
                    this.level = level;
                }

                public String getText() {
                    return text;
                }

                public void setText(String text) {
                    this.text = text;
                }

                public String getTip() {
                    return tip;
                }

                public void setTip(String tip) {
                    this.tip = tip;
                }
            }

            public static class EmailBean {
                /**
                 * email : 555****77@qq.com
                 * textBind : 修改
                 * textVerify : 开启验证
                 * textVerifyEnable : 1
                 * warnVerify : 1
                 */

                private String email;
                private String textBind;
                private String textVerify;
                private int textVerifyEnable;
                private int warnVerify;

                public String getEmail() {
                    return email;
                }

                public void setEmail(String email) {
                    this.email = email;
                }

                public String getTextBind() {
                    return textBind;
                }

                public void setTextBind(String textBind) {
                    this.textBind = textBind;
                }

                public String getTextVerify() {
                    return textVerify;
                }

                public void setTextVerify(String textVerify) {
                    this.textVerify = textVerify;
                }

                public int getTextVerifyEnable() {
                    return textVerifyEnable;
                }

                public void setTextVerifyEnable(int textVerifyEnable) {
                    this.textVerifyEnable = textVerifyEnable;
                }

                public int getWarnVerify() {
                    return warnVerify;
                }

                public void setWarnVerify(int warnVerify) {
                    this.warnVerify = warnVerify;
                }
            }

            public static class MobileBean {
                /**
                 * mobile : 139*****555
                 * textBind : 修改
                 * textVerify : 关闭验证
                 * textVerifyEnable : 0
                 * warnVerify : 0
                 */

                private String mobile;
                private String textBind;
                private String textVerify;
                private int textVerifyEnable;
                private int warnVerify;

                public String getMobile() {
                    return mobile;
                }

                public void setMobile(String mobile) {
                    this.mobile = mobile;
                }

                public String getTextBind() {
                    return textBind;
                }

                public void setTextBind(String textBind) {
                    this.textBind = textBind;
                }

                public String getTextVerify() {
                    return textVerify;
                }

                public void setTextVerify(String textVerify) {
                    this.textVerify = textVerify;
                }

                public int getTextVerifyEnable() {
                    return textVerifyEnable;
                }

                public void setTextVerifyEnable(int textVerifyEnable) {
                    this.textVerifyEnable = textVerifyEnable;
                }

                public int getWarnVerify() {
                    return warnVerify;
                }

                public void setWarnVerify(int warnVerify) {
                    this.warnVerify = warnVerify;
                }
            }

            public static class GoogleAuthenticatorBean {
                /**
                 * googleAuthenticator : 已绑定
                 * textBind : 修改
                 * textVerify : 开启验证
                 * textVerifyEnable : 1
                 * warnVerify : 1
                 */

                private String googleAuthenticator;
                private String textBind;
                private String textVerify;
                private int textVerifyEnable;
                private int warnVerify;

                public String getGoogleAuthenticator() {
                    return googleAuthenticator;
                }

                public void setGoogleAuthenticator(String googleAuthenticator) {
                    this.googleAuthenticator = googleAuthenticator;
                }

                public String getTextBind() {
                    return textBind;
                }

                public void setTextBind(String textBind) {
                    this.textBind = textBind;
                }

                public String getTextVerify() {
                    return textVerify;
                }

                public void setTextVerify(String textVerify) {
                    this.textVerify = textVerify;
                }

                public int getTextVerifyEnable() {
                    return textVerifyEnable;
                }

                public void setTextVerifyEnable(int textVerifyEnable) {
                    this.textVerifyEnable = textVerifyEnable;
                }

                public int getWarnVerify() {
                    return warnVerify;
                }

                public void setWarnVerify(int warnVerify) {
                    this.warnVerify = warnVerify;
                }
            }
        }
    }
}
