package com.huijinlong.newretail.bean;

import com.google.gson.annotations.SerializedName;
import com.huijinlong.newretail.base.BaseBean;

/**
 * @author zsg
 * @date 2018/11/29 12:06
 * @Version 1.0
 * @Description: ()
 */
public class CheckUpdateBean extends BaseBean {


    /**
     * data : {"version":"2.1.0","versionDesc":"1、添加新功能\\r\\n2、修改bug\\r\\n3、优化用户体验","status":1,"url":"https://dev.nr.exchange/download/android/apk_v1.0.apk"}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * version : 2.1.0
         * versionDesc : 1、添加新功能\r\n2、修改bug\r\n3、优化用户体验
         * status : 1
         * url : https://dev.nr.exchange/download/android/apk_v1.0.apk
         */

        private String version;
        private String versionDesc;
        @SerializedName("status")
        private int statusX;
        private String url;

        public String getVersion() {
            return version;
        }

        public void setVersion(String version) {
            this.version = version;
        }

        public String getVersionDesc() {
            return versionDesc;
        }

        public void setVersionDesc(String versionDesc) {
            this.versionDesc = versionDesc;
        }

        public int getStatusX() {
            return statusX;
        }

        public void setStatusX(int statusX) {
            this.statusX = statusX;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }
}
