package com.huijinlong.newretail.bean;

/**
 * Created by penghuaijin on 2018/9/30.
 */

public class IdCardBean {
    private int pic;
    private String status;
    private String description;

    public IdCardBean(int pic, String status, String description) {
        this.pic = pic;
        this.status = status;
        this.description = description;
    }

    public int getPic() {
        return pic;
    }

    public void setPic(int pic) {
        this.pic = pic;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
