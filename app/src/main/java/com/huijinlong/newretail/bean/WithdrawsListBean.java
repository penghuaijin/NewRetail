package com.huijinlong.newretail.bean;

import com.huijinlong.newretail.base.BaseBean;

import java.io.Serializable;
import java.util.List;

/**
 * @author zsg
 * @date 2018/10/22 15:04
 * @Version 1.0
 * @Description: ()
 */
public class WithdrawsListBean extends BaseBean  {


    /**
     * data : {"currentPage":1,"total":5,"pageSize":2,"list":[{"id":6,"currency":"BTC","channel":"钱包","address":"5h3jk5398jh345hj3k534","remark":"提币地址测试1"},{"id":5,"currency":"BTC","channel":"钱包","address":"231234564fsawr456r44q5rwq5","remark":"提币地址测试2"}]}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean implements Serializable{
        /**
         * currentPage : 1
         * total : 5
         * pageSize : 2
         * list : [{"id":6,"currency":"BTC","channel":"钱包","address":"5h3jk5398jh345hj3k534","remark":"提币地址测试1"},{"id":5,"currency":"BTC","channel":"钱包","address":"231234564fsawr456r44q5rwq5","remark":"提币地址测试2"}]
         */

        private int currentPage;
        private int total;
        private int pageSize;
        private List<ListBean> list;

        public int getCurrentPage() {
            return currentPage;
        }

        public void setCurrentPage(int currentPage) {
            this.currentPage = currentPage;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public static class ListBean implements Serializable{
            /**
             * id : 6
             * currency : BTC
             * channel : 钱包
             * address : 5h3jk5398jh345hj3k534
             * remark : 提币地址测试1
             */

            private int id;
            private String currency;
            private String channel;
            private String address;
            private String remark;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getCurrency() {
                return currency;
            }

            public void setCurrency(String currency) {
                this.currency = currency;
            }

            public String getChannel() {
                return channel;
            }

            public void setChannel(String channel) {
                this.channel = channel;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getRemark() {
                return remark;
            }

            public void setRemark(String remark) {
                this.remark = remark;
            }
        }
    }
}
