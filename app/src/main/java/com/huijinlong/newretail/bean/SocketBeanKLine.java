package com.huijinlong.newretail.bean;

/**
 * Created by penghuaijin on 2018/10/10.
 */

public class SocketBeanKLine {
    String type;
    String kline_type;

    public SocketBeanKLine(String type, String kline_type) {
        this.type = type;
        this.kline_type = kline_type;
    }
}
