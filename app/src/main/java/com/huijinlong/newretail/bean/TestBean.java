package com.huijinlong.newretail.bean;

import com.huijinlong.newretail.base.BaseBean;
import com.huijinlong.newretail.requestbean.DepthBean;

import java.util.List;

/**
 * Created by penghuaijin on 2018/9/19.
 */

public class TestBean extends BaseBean {



    /**
     * data : {"lastUpdateId":"72773","bids":[{"price":"1.12","quantity":"5.0000"},{"price":"1.12","quantity":"5.0000"},{"price":"1.12","quantity":"5.0000"}],"asks":[{"price":"1.12","quantity":"5.0000"},{"price":"1.12","quantity":"5.0000"},{"price":"1.12","quantity":"5.0000"}]}
     */

    private DepthBean.DataBean data;

    public DepthBean.DataBean getData() {
        return data;
    }

    public void setData(DepthBean.DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * lastUpdateId : 72773
         * bids : [{"price":"1.12","quantity":"5.0000"},{"price":"1.12","quantity":"5.0000"},{"price":"1.12","quantity":"5.0000"}]
         * asks : [{"price":"1.12","quantity":"5.0000"},{"price":"1.12","quantity":"5.0000"},{"price":"1.12","quantity":"5.0000"}]
         */

        private String lastUpdateId;
        private List<DepthBean.DataBean.BidsBean> bids;
        private List<DepthBean.DataBean.AsksBean> asks;

        public String getLastUpdateId() {
            return lastUpdateId;
        }

        public void setLastUpdateId(String lastUpdateId) {
            this.lastUpdateId = lastUpdateId;
        }

        public List<DepthBean.DataBean.BidsBean> getBids() {
            return bids;
        }

        public void setBids(List<DepthBean.DataBean.BidsBean> bids) {
            this.bids = bids;
        }

        public List<DepthBean.DataBean.AsksBean> getAsks() {
            return asks;
        }

        public void setAsks(List<DepthBean.DataBean.AsksBean> asks) {
            this.asks = asks;
        }

        public static class BidsBean {
            /**
             * price : 1.12
             * quantity : 5.0000
             */

            private String price;
            private String quantity;

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getQuantity() {
                return quantity;
            }

            public void setQuantity(String quantity) {
                this.quantity = quantity;
            }
        }

        public static class AsksBean {
            /**
             * price : 1.12
             * quantity : 5.0000
             */

            private String price;
            private String quantity;

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getQuantity() {
                return quantity;
            }

            public void setQuantity(String quantity) {
                this.quantity = quantity;
            }
        }
    }
}
