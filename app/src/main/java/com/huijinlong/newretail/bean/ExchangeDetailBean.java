package com.huijinlong.newretail.bean;

import com.google.gson.annotations.SerializedName;
import com.huijinlong.newretail.base.BaseBean;

import java.util.List;

/**
 * @author zsg
 * @date 2018/11/20 12:15
 * @Version 1.0
 * @Description: ()
 */
public class ExchangeDetailBean extends BaseBean {

    /**
     * data : {"currentPage":1,"total":43,"pageSize":2,"list":[{"currency":"RESS","amount":"112.000000000000","status":"已入账","time":"2018-11-02 19:08:21","receiveAddress":"聚宝盆 - LIBCPVDFWWDNIKAYDMBEFMIGUJ8SZQTG","sendAddress":"交易所 - 1HT953A33Q8fHXgJNsLSXmFfHTsYoqPqf2","type":"提币到聚宝盆"},{"currency":"RESS","amount":"112.000000000000","status":"已入账","time":"2018-11-02 19:03:27","receiveAddress":"聚宝盆 - LIBCPVDFWWDNIKAYDMBEFMIGUJ8SZQTG","sendAddress":"交易所 - 1HT953A33Q8fHXgJNsLSXmFfHTsYoqPqf2","type":"提币到聚宝盆"}]}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * currentPage : 1
         * total : 43
         * pageSize : 2
         * list : [{"currency":"RESS","amount":"112.000000000000","status":"已入账","time":"2018-11-02 19:08:21","receiveAddress":"聚宝盆 - LIBCPVDFWWDNIKAYDMBEFMIGUJ8SZQTG","sendAddress":"交易所 - 1HT953A33Q8fHXgJNsLSXmFfHTsYoqPqf2","type":"提币到聚宝盆"},{"currency":"RESS","amount":"112.000000000000","status":"已入账","time":"2018-11-02 19:03:27","receiveAddress":"聚宝盆 - LIBCPVDFWWDNIKAYDMBEFMIGUJ8SZQTG","sendAddress":"交易所 - 1HT953A33Q8fHXgJNsLSXmFfHTsYoqPqf2","type":"提币到聚宝盆"}]
         */

        private int currentPage;
        private int total;
        private int pageSize;
        private List<ListBean> list;

        public int getCurrentPage() {
            return currentPage;
        }

        public void setCurrentPage(int currentPage) {
            this.currentPage = currentPage;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public static class ListBean {
            /**
             * currency : RESS
             * amount : 112.000000000000
             * status : 已入账
             * time : 2018-11-02 19:08:21
             * receiveAddress : 聚宝盆 - LIBCPVDFWWDNIKAYDMBEFMIGUJ8SZQTG
             * sendAddress : 交易所 - 1HT953A33Q8fHXgJNsLSXmFfHTsYoqPqf2
             * type : 提币到聚宝盆
             */

            private String currency;
            private String amount;
            @SerializedName("status")
            private String statusX;
            private String time;
            private String receiveAddress;
            private String sendAddress;
            private String type;
            private boolean isshow=false;

            public boolean isIsshow() {
                return isshow;
            }

            public void setIsshow(boolean isshow) {
                this.isshow = isshow;
            }

            public String getCurrency() {
                return currency;
            }

            public void setCurrency(String currency) {
                this.currency = currency;
            }

            public String getAmount() {
                return amount;
            }

            public void setAmount(String amount) {
                this.amount = amount;
            }

            public String getStatusX() {
                return statusX;
            }

            public void setStatusX(String statusX) {
                this.statusX = statusX;
            }

            public String getTime() {
                return time;
            }

            public void setTime(String time) {
                this.time = time;
            }

            public String getReceiveAddress() {
                return receiveAddress;
            }

            public void setReceiveAddress(String receiveAddress) {
                this.receiveAddress = receiveAddress;
            }

            public String getSendAddress() {
                return sendAddress;
            }

            public void setSendAddress(String sendAddress) {
                this.sendAddress = sendAddress;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }
        }
    }
}
