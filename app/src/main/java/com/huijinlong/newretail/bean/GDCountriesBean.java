package com.huijinlong.newretail.bean;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by penghuaijin on 2018/9/27.
 */
@Entity
public class GDCountriesBean {
    @Id(autoincrement = true)
    private Long gdId;
    private int id;
    private String nameCn;
    private String nameEn;
    private String code;
    @Generated(hash = 1787884876)
    public GDCountriesBean(Long gdId, int id, String nameCn, String nameEn,
            String code) {
        this.gdId = gdId;
        this.id = id;
        this.nameCn = nameCn;
        this.nameEn = nameEn;
        this.code = code;
    }
    @Generated(hash = 659967294)
    public GDCountriesBean() {
    }
    public Long getGdId() {
        return this.gdId;
    }
    public void setGdId(Long gdId) {
        this.gdId = gdId;
    }
    public int getId() {
        return this.id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getNameCn() {
        return this.nameCn;
    }
    public void setNameCn(String nameCn) {
        this.nameCn = nameCn;
    }
    public String getNameEn() {
        return this.nameEn;
    }
    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }
    public String getCode() {
        return this.code;
    }
    public void setCode(String code) {
        this.code = code;
    }
}
