package com.huijinlong.newretail.bean;

import com.huijinlong.newretail.base.BaseBean;

import java.util.List;

/**
 * @author zsg
 * @date 2018/11/23 14:38
 * @Version 1.0
 * @Description: ()
 */
public class WthdrawChannelBean extends BaseBean {

    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 1
         * name : 钱包
         */

        private int id;
        private String name;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
