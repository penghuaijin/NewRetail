package com.huijinlong.newretail.bean;

import java.util.HashMap;

/**
 * Created by penghuaijin on 2018/10/12.
 */

public class OrderBookSocketEntity {


    /**
     * type : orderbook
     * instrument : BTCUSDT
     * lastUpdateId : 356762
     * orderbook : {"bid":{"1.75":"4.0000"}}
     * timestamp : 1539342655
     * seq : 1539342655000
     */

    private String type;
    private String instrument;
    private int lastUpdateId;
    private OrderBean order;
    private String timestamp;
    private String seq;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getInstrument() {
        return instrument;
    }

    public void setInstrument(String instrument) {
        this.instrument = instrument;
    }

    public int getLastUpdateId() {
        return lastUpdateId;
    }

    public void setLastUpdateId(int lastUpdateId) {
        this.lastUpdateId = lastUpdateId;
    }

    public OrderBean getOrderbook() {
        return order;
    }

    public void setOrderbook(OrderBean orderbook) {
        this.order = orderbook;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getSeq() {
        return seq;
    }

    public void setSeq(String seq) {
        this.seq = seq;
    }

    public static class OrderBean {
        /**
         * bid : {"1.75":"4.0000"}
         */

        private BidBean bid;
        private AskBean ask;

        public AskBean getAsk() {
            return ask;
        }

        public void setAsk(AskBean ask) {
            this.ask = ask;
        }

        public BidBean getBid() {
            return bid;
        }

        public void setBid(BidBean bid) {
            this.bid = bid;
        }

        public static class BidBean {
//            private double key;
//            private double value;
            private HashMap<Double,Double> hashMap;

            public HashMap<Double, Double> getHashMap() {

                return hashMap;
            }

            public void setHashMap(HashMap<Double, Double> hashMap) {
                this.hashMap = hashMap;
            }

        }

        public static class AskBean {

            private HashMap<Double,Double> hashMap;

            public HashMap<Double, Double> getHashMap() {

                return hashMap;
            }
            public void setHashMap(HashMap<Double, Double> hashMap) {
                this.hashMap = hashMap;
            }
        }
    }
}
