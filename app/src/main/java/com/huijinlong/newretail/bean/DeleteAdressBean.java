package com.huijinlong.newretail.bean;

import com.huijinlong.newretail.base.BaseBean;

/**
 * @author zsg
 * @date 2018/10/23 18:45
 * @Version 1.0
 * @Description: ()
 */
public class DeleteAdressBean extends BaseBean{


    /**
     * data : {"verifyMobile":1,"verifyEmail":1,"verifyGoogleAuthenticator":0}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * verifyMobile : 1
         * verifyEmail : 1
         * verifyGoogleAuthenticator : 0
         */

        private int verifyMobile;
        private int verifyEmail;
        private int verifyGoogleAuthenticator;

        public int getVerifyMobile() {
            return verifyMobile;
        }

        public void setVerifyMobile(int verifyMobile) {
            this.verifyMobile = verifyMobile;
        }

        public int getVerifyEmail() {
            return verifyEmail;
        }

        public void setVerifyEmail(int verifyEmail) {
            this.verifyEmail = verifyEmail;
        }

        public int getVerifyGoogleAuthenticator() {
            return verifyGoogleAuthenticator;
        }

        public void setVerifyGoogleAuthenticator(int verifyGoogleAuthenticator) {
            this.verifyGoogleAuthenticator = verifyGoogleAuthenticator;
        }
    }
}
