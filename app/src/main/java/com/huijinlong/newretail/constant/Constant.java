package com.huijinlong.newretail.constant;

import android.os.Environment;
import android.util.Log;

/**
 * @author phj
 * @date 15/12/23 3:07 PM
 * @Description: ()
 */
public class Constant {

    public static final String GESTURE_PASSWORD = "GesturePassword";

    public static final String BIND_MOBILE = "bindMobile";//首次绑定手机号
    public static final String REBIND_MOBILE = "reBindMobile";//修改绑定手机号
    public static final String BIND_EMAIL = "bindEmail";//首次绑定邮箱
    public static final String REBIND_EMAIL = "reBindEmail";//修改绑定邮箱
    public static final String BIND_GOOGLE_AUTHENTICATOR = "bindGoogleAuthenticator";//首次绑定google验证器
    public static final String REBIND_GOOGLE_AUTHENTICATOR = "reBindGoogleAuthenticator";//修改绑定google验证器
    public static final String VERIFY_MOBILE_ON = "verifyMobileOn";//开启手机安全认证
    public static final String VERIFY_MOBILE_OFF = "verifyMobileOff";//关闭手机安全认证
    public static final String VERIFY_EMAIL_ON = "verifyEmailOn";//开启邮箱安全认证
    public static final String VERIFY_EMAIL_OFF = "verifyEmailOff";//关闭邮箱安全认证
    public static final String VERIFY_GOOGLE_AUTHENTICATOR_ON = "verifyGoogleAuthenticatorOn";//开启google验证器安全认证
    public static final String VERIFY_GOOGLE_AUTHENTICATOR_OFF = "verifyGoogleAuthenticatorOff";//关闭google验证器安全认证
    public static final String CREATE_APIKEY = "createApiKey";//创建API Key
    public static final String EDIT_APIKEY = "editApiKey";//编辑Api Key
    public static final String DELETE_APIKEY = "deleteApiKey";//删除Api Key
    public static final String CREATE_ADDRESS = "createAddress";//创建提币地址
    public static final String EDIT_ADDRESS = "editAddress";//编辑提币地址
    public static final String DELETE_ADDRESS = "deleteAddress";//删除提币地址
    public static final String CFD_TRANSFER_IN = "cfdTransferIn";//cfd资金转入
    public static final String CFD_TRANSFER_OUT = "cfdTransferOut";//cfd资金转出

    // 文件的保存路徑
    public static final String DownloadPath = Environment.getExternalStorageDirectory().getAbsolutePath()
            + "/huijinlong/download/";
}
