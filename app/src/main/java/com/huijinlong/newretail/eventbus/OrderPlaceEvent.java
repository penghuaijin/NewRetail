package com.huijinlong.newretail.eventbus;

/**
 * @author penghuaijin
 * @date 2018/11/27 3:44 PM
 * @Description: ()
 */
public class OrderPlaceEvent {

    private String symbol;
    private String instrument;

    public OrderPlaceEvent(String symbol, String instrument) {
        this.symbol = symbol;
        this.instrument = instrument;
    }

    public String getInstrument() {
        return instrument;
    }

    public String getSymbol() {
        return symbol;
    }
}
