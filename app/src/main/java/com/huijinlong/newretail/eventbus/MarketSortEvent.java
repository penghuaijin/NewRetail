package com.huijinlong.newretail.eventbus;

/**
 * @author penghuaijin
 * @date 2018/11/7 4:24 PM
 * @Description: ()
 */
public class MarketSortEvent {
    private int typeCurrency;
    private int typeUpdatedPrice;
    private int typeUpsAndDowns;

    public MarketSortEvent(int typeCurrency, int typeUpdatedPrice, int typeUpsAndDowns) {
        this.typeCurrency = typeCurrency;
        this.typeUpdatedPrice = typeUpdatedPrice;
        this.typeUpsAndDowns = typeUpsAndDowns;
    }

    public int getTypeCurrency() {
        return typeCurrency;
    }

    public void setTypeCurrency(int typeCurrency) {
        this.typeCurrency = typeCurrency;
    }

    public int getTypeUpdatedPrice() {
        return typeUpdatedPrice;
    }

    public void setTypeUpdatedPrice(int typeUpdatedPrice) {
        this.typeUpdatedPrice = typeUpdatedPrice;
    }

    public int getTypeUpsAndDowns() {
        return typeUpsAndDowns;
    }

    public void setTypeUpsAndDowns(int typeUpsAndDowns) {
        this.typeUpsAndDowns = typeUpsAndDowns;
    }
}
