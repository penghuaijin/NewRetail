package com.huijinlong.newretail.eventbus;

/**
 * @author zsg
 * @date 2018/10/29 16:46
 * @Version 1.0
 * @Description: ()
 */
public class WalletEvent {
    private boolean isrefresh;
    public WalletEvent(boolean isrefresh) {
        this.isrefresh = isrefresh;
    }

    public boolean isIsrefresh() {
        return isrefresh;
    }

    public void setIsrefresh(boolean isrefresh) {
        this.isrefresh = isrefresh;
    }
}
