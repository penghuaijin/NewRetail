package com.huijinlong.newretail.manager;

import android.app.Activity;

import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.Stack;

/**
 * Created by shoulder on 16/6/1.
 */

public class MyActivityManager {
    private static MyActivityManager mManager = new MyActivityManager();
    private Stack<WeakReference<Activity>> mActivityStack = new Stack<>();

    private MyActivityManager() {

    }

    public static MyActivityManager getManager() {
        return mManager;
    }

    public void push(WeakReference<Activity> activity) {
        if (activity == null) {
            return;
        }
        mActivityStack.push(activity);
    }

    /**
     * @return 返回栈顶的Activity
     */
    public Activity pop() {
        if(mActivityStack.isEmpty()) {
            return null;
        }
        return mActivityStack.pop().get();
    }

    /**
     * @return 返回栈顶的Activity
     */
    public Activity getTop() {
        if(mActivityStack.isEmpty()) {
            return null;
        }
        return mActivityStack.lastElement().get();
    }

    /**
     * @param activity 移除栈中第一个与activity同名的
     */
    public void finishActivity(Activity activity) {
        Iterator<WeakReference<Activity>> it = mActivityStack.iterator();
        while (it.hasNext()) {
            WeakReference<Activity> act = it.next();
            if (act.get() == activity) {
                it.remove();
                act.get().finish();
            }
        }
    }

    /**
     * @param clazz 移除所有这一类的activity
     */
    public void finishAll(Class clazz) {
        Iterator<WeakReference<Activity>> it = mActivityStack.iterator();
        while (it.hasNext()) {
            WeakReference<Activity> act = it.next();
            if (act.getClass().getName().equals(clazz.getName())) {
                it.remove();
                act.get().finish();
            }
        }
    }

    /**
     * 移除所有activity
     */
    public void finishAll() {
        Iterator<WeakReference<Activity>> it = mActivityStack.iterator();
        while (it.hasNext()) {
            WeakReference<Activity> act = it.next();
            it.remove();
            act.get().finish();
        }
        mActivityStack.clear();
    }
}
