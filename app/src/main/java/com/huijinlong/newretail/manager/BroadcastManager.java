package com.huijinlong.newretail.manager;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;

import com.huijinlong.newretail.base.BaseApplication;
import com.huijinlong.newretail.util.LogUtils;


/**
 */

public class BroadcastManager {

    public static final String ACTION_EXIT_APP = "ACTION_EXIT_APP";
    public static final String WALLET_SHOW_NAVIGATION = "WALLET_SHOW_NAVIGATION";



    private static LocalBroadcastManager sManager = LocalBroadcastManager.getInstance(BaseApplication.getInstance());


//    public static LocalBroadcastManager getManager() {
//        return sManager;
//    }

    public static void sendBroadcast(Intent intent) {
        LogUtils.d("BroadcastManager---------sendBroadcast:"+intent.getAction());
        sManager.sendBroadcast(intent);
    }

    public static void registerReceiver(BroadcastReceiver receiver, IntentFilter filter) {
        sManager.registerReceiver(receiver, filter);
    }

    public static void unregisterReceiver(BroadcastReceiver receiver) {
        sManager.unregisterReceiver(receiver);
    }
}
