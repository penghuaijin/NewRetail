package com.huijinlong.newretail.ui.wallet.exchange;

import com.huijinlong.newretail.base.BasePresenter;
import com.huijinlong.newretail.base.BaseBean;
import com.huijinlong.newretail.net.MyCallback;
import com.huijinlong.newretail.net.RequestBuilder;
import com.huijinlong.newretail.util.MD5Util;
import com.huijinlong.newretail.util.SpUtil;

import java.util.TreeMap;

/**
 * @author zsg
 * @date 2018/11/20 15:04
 * @Version 1.0
 * @Description: ()
 */
public class TransferOutPresent extends BasePresenter<TransferOutlView>{
    private TransferOutlView transferOutlView;

    public TransferOutPresent(TransferOutlView mViews) {
        this.transferOutlView = mViews;
    }

    public void getwithdraws(String cid,String amount,String receiveAddress) {
        transferOutlView.showLoading();
        TreeMap<String, String> params = new TreeMap<>();
        params.put("deviceType", "android");
        params.put("language", SpUtil.getString(SpUtil.LANGUAGE,"zh-cn"));

        params.put("cid", cid);
        params.put("amount", amount);
        params.put("receiveAddress", receiveAddress);

        RequestBuilder.execute(RequestBuilder.getNetService(true).getTransferOut(MD5Util.getSignValue(params), params),
                "", new MyCallback<BaseBean>() {
            @Override
            public void onSuccess(BaseBean result) {
                super.onSuccess(result);
                transferOutlView.gettransferOut(result);
            }

            @Override
            public void onEmpty(BaseBean result) {
                super.onEmpty(result);

            }

            @Override
            public void onBusinessFailure(Exception e) {
                super.onBusinessFailure(e);
                transferOutlView.showError(e.getMessage());
            }

            @Override
            public void onFinalResponse(boolean isSuccess) {
                super.onFinalResponse(isSuccess);
                transferOutlView.hideLoading();
            }
        });
    }
}
