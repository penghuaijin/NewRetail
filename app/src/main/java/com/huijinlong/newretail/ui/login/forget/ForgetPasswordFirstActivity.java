package com.huijinlong.newretail.ui.login.forget;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.huijinlong.newretail.R;
import com.huijinlong.newretail.base.BaseActivity;
import com.huijinlong.newretail.base.BasePresenter;
import com.huijinlong.newretail.util.FontUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by penghuaijin on 2018/9/27.
 */

public class ForgetPasswordFirstActivity extends BaseActivity  {
    @BindView(R.id.tv_verify_identidy)
    TextView tvVerifyIdentidy;
    @BindView(R.id.tv_reset_password)
    TextView tvResetPassword;
    @BindView(R.id.et_account)
    EditText etAccount;
    @BindView(R.id.et_verification_code)
    EditText etVerificationCode;
    @BindView(R.id.btn_next)
    Button btnNext;
    private ArrayList<Integer> fontTextId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_forget_password_first);
        ButterKnife.bind(this);
//        initView();
//        initializeActionToolBar();
    }

    @Override
    public BasePresenter initPresenter() {
        return null;
    }


    @Override
    protected void initView() {
        setToolbarTitle(getString(R.string.zhdlmm));
        fontTextId = new ArrayList<>();
        fontTextId.add(R.id.tv_verify_identidy);
        fontTextId.add(R.id.tv_reset_password);
        initFont();
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_forget_password_first;
    }

    private void initFont() {
        for (int i = 0; i < fontTextId.size(); i++) {
            TextView textView = findViewById(fontTextId.get(i));
            FontUtil.injectFont(textView);
        }
    }


//    private void initializeActionToolBar() {
//        Toolbar mToolbar = findViewById(R.id.toolbar);
//        TextView tvTitle = findViewById(R.id.toolbar_title);
//        tvTitle.setText("找回登录密码");
//        initToolBar(mToolbar, true, "");
//    }

    @OnClick(R.id.btn_next)
    public void onViewClicked() {
        Intent intent = new Intent(this, ForgetPasswordSecondActivity.class);
        intent.putExtra("key", etAccount.getText().toString());
        intent.putExtra("resetType", etAccount.getText().toString().contains("@") ? "email" : "mobile");
        intent.putExtra("verificationCode", etVerificationCode.getText().toString());
        startActivity(intent);

    }
}
