package com.huijinlong.newretail.ui.mine.accountSecurity.binggooleauthenticator;

import com.huijinlong.newretail.base.BasePresenter;
import com.huijinlong.newretail.net.MyCallback;
import com.huijinlong.newretail.net.RequestBuilder;
import com.huijinlong.newretail.requestbean.GoogleAuthenticatorSecretBean;
import com.huijinlong.newretail.requestbean.VerifyPhone4BindBean;
import com.huijinlong.newretail.util.MD5Util;
import com.huijinlong.newretail.util.SpUtil;

import java.util.TreeMap;

/**
 * Created by penghuaijin on 2018/9/12.
 */

public class BindGooglePresenter extends BasePresenter<BindGoogleView> {

    private BindGoogleView bindGoogleView;

    public BindGooglePresenter(BindGoogleView mViews) {
        this.bindGoogleView = mViews;
    }

    public void verifyGoogleAuth4Bind(String verificationCode) {
        TreeMap<String, String> params = new TreeMap<>();
        params.put("verificationCode", verificationCode);
        params.put("deviceType", "android");
        params.put("language", SpUtil.getString(SpUtil.LANGUAGE, "zh-cn"));
        RequestBuilder.execute(RequestBuilder.getNetService().verifyGoogleAuth4Bind(MD5Util.getSignValue(params), params), "", new MyCallback<VerifyPhone4BindBean>() {
            @Override
            public void onSuccess(VerifyPhone4BindBean result) {
                super.onSuccess(result);
                bindGoogleView.bindGoogleSuccess(result);
            }

            @Override
            public void onEmpty(VerifyPhone4BindBean result) {
                super.onEmpty(result);

            }

            @Override
            public void onBusinessFailure(Exception e) {
                super.onBusinessFailure(e);
                bindGoogleView.showGetResult(e.getMessage());

            }

            @Override
            public void onFinalResponse(boolean isSuccess) {
                super.onFinalResponse(isSuccess);
                bindGoogleView.hideLoading();

            }
        });
    }

    public void getGoogleAuthenticatorSecret() {
        TreeMap<String, String> params = new TreeMap<>();

        params.put("deviceType", "android");
        params.put("language", SpUtil.getString(SpUtil.LANGUAGE, "zh-cn"));

        RequestBuilder.execute(RequestBuilder.getNetService(true).getGoogleAuthSecret(MD5Util.getSignValue(params), params), "", new MyCallback<GoogleAuthenticatorSecretBean>() {
            @Override
            public void onSuccess(GoogleAuthenticatorSecretBean result) {
                super.onSuccess(result);
                bindGoogleView.getGoogleAuthenticatorSecretSuccess(result);
            }

            @Override
            public void onEmpty(GoogleAuthenticatorSecretBean result) {
                super.onEmpty(result);

            }

            @Override
            public void onBusinessFailure(Exception e) {
                super.onBusinessFailure(e);
                bindGoogleView.showGetResult(e.getMessage());

            }

            @Override
            public void onFinalResponse(boolean isSuccess) {
                super.onFinalResponse(isSuccess);
                bindGoogleView.hideLoading();

            }
        });
    }


}
