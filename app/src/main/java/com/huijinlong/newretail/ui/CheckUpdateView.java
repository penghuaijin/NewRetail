package com.huijinlong.newretail.ui;

import com.huijinlong.newretail.base.BaseView;
import com.huijinlong.newretail.bean.CheckUpdateBean;

/**
 * @author zsg
 * @date 2018/11/29 14:55
 * @Version 1.0
 * @Description: ()
 */
public interface CheckUpdateView extends BaseView{
    void getCheck(CheckUpdateBean checkUpdateBean);
}
