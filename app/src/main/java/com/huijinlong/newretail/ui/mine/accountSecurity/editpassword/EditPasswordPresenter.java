package com.huijinlong.newretail.ui.mine.accountSecurity.editpassword;

import com.huijinlong.newretail.base.BasePresenter;
import com.huijinlong.newretail.base.BaseBean;
import com.huijinlong.newretail.net.MyCallback;
import com.huijinlong.newretail.net.RequestBuilder;
import com.huijinlong.newretail.util.MD5Util;
import com.huijinlong.newretail.util.SpUtil;

import java.util.TreeMap;

/**
 * Created by penghuaijin on 2018/9/12.
 */

public class EditPasswordPresenter extends BasePresenter<EditPasswordView> {

    private EditPasswordView editPasswordView;

    public EditPasswordPresenter(EditPasswordView mViews) {
        this.editPasswordView = mViews;
    }

    public void editPwd(String oldPassword,String password,String rePassword) {
        TreeMap<String, String> params = new TreeMap<>();
        params.put("oldPassword", oldPassword);
        params.put("password", password);
        params.put("rePassword", rePassword);
        params.put("deviceType", "android");
        params.put("language", SpUtil.getString(SpUtil.LANGUAGE,"zh-cn"));
        RequestBuilder.execute(RequestBuilder.getNetService(true).editPwd(MD5Util.getSignValue(params), params), "", new MyCallback<BaseBean>() {
            @Override
            public void onSuccess(BaseBean result) {
                super.onSuccess(result);
                editPasswordView.editPwdSuccess(result);
            }

            @Override
            public void onEmpty(BaseBean result) {
                super.onEmpty(result);

            }

            @Override
            public void onBusinessFailure(Exception e) {
                super.onBusinessFailure(e);
                editPasswordView.showError(e.getMessage());

            }

            @Override
            public void onFinalResponse(boolean isSuccess) {
                super.onFinalResponse(isSuccess);
                editPasswordView.hideLoading();
            }
        });
    }


}
