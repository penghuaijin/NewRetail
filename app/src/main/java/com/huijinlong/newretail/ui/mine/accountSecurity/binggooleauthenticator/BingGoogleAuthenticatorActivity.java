package com.huijinlong.newretail.ui.mine.accountSecurity.binggooleauthenticator;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.huijinlong.newretail.R;
import com.huijinlong.newretail.base.BaseActivity;
import com.huijinlong.newretail.requestbean.GoogleAuthenticatorSecretBean;
import com.huijinlong.newretail.requestbean.VerifyPhone4BindBean;
import com.huijinlong.newretail.util.FontUtil;
import com.huijinlong.newretail.util.ToastUtils;
import com.huijinlong.newretail.util.VerifySecurityUtil;
import com.uuzuche.lib_zxing.activity.CodeUtils;

import java.util.ArrayList;

/**
 * Created by penghuaijin on 2018/9/28.
 */

public class BingGoogleAuthenticatorActivity extends BaseActivity<BindGoogleView,BindGooglePresenter> implements View.OnClickListener, BindGoogleView {
    private ArrayList<Integer> fontTextId;

   // private BindGooglePresenter mPresenter;
    private ImageView qr_view;
    private TextView tv_secret;
    private TextView tv_copy_secret;
    private EditText et_verificationCode;
    private String action="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_bind_google_authenticator);
//        initView();
        mPresenter.getGoogleAuthenticatorSecret();
    }

    @Override
    public BindGooglePresenter initPresenter() {
        return new BindGooglePresenter(this);
    }

    @Override
    protected void initView() {
        setToolbarTitle(getString(R.string.bdggyzq));
        action=getIntent().getStringExtra("action");

        fontTextId = new ArrayList<>();
        fontTextId.add(R.id.tv_google_logo);
        initFont();

        findViewById(R.id.ll_google).setOnClickListener(this);
        findViewById(R.id.btn_bind_google).setOnClickListener(this);
        qr_view = findViewById(R.id.qr_view);
        tv_secret = findViewById(R.id.tv_secret);
        tv_copy_secret = findViewById(R.id.tv_copy_secret);
        tv_copy_secret.setOnClickListener(this);
        et_verificationCode = findViewById(R.id.et_verificationCode);

//        initializeActionToolBar();
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_bind_google_authenticator;
    }

    private void initFont() {
        for (int i = 0; i < fontTextId.size(); i++) {
            TextView textView = findViewById(fontTextId.get(i));
            FontUtil.injectFont(textView);
            textView.setOnClickListener(this);
        }
    }

//    private void initializeActionToolBar() {
//        Toolbar mToolbar = findViewById(R.id.toolbar);
//        TextView tvTitle = findViewById(R.id.toolbar_title);
//        tvTitle.setText("绑定谷歌验证器");
//        initToolBar(mToolbar, true, "");
//    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_google: {
                openThirdApk();
                break;
            }
            case R.id.tv_copy_secret: {
                if (!TextUtils.isEmpty(tv_secret.getText())) {

                    ClipboardManager clipboardmanager = (ClipboardManager)
                            getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("simple text", tv_secret.getText().toString());
                    assert clipboardmanager != null;
                    clipboardmanager.setPrimaryClip(clip);
                    ToastUtils.toast(getString(R.string.yicopy));
                }

                break;
            }
            case R.id.btn_bind_google: {
                if (!TextUtils.isEmpty(et_verificationCode.getText())) {
                    mPresenter.verifyGoogleAuth4Bind(et_verificationCode.getText().toString());
                } else {
                    ToastUtils.toast(getString(R.string.qsryzm));
                }
                break;
            }
        }
    }

    /**
     * 启动第三方apk
     */
    public void openThirdApk() {
        PackageManager packageManager = getPackageManager();//获取包名管理类
        Intent intent;
        //根据包名获取启动的intent 以微信为例子 注意包名之间不要有空格
        //微信包名:com.tencent.mm
        //QQ包名:com.tencent.mobileqq
        // 微博 com.sina.weibo com.sina.weibo.SplashActivity
        // 知乎 com.zhihu.android com.zhihu.android.ui.activity.GuideActivity
        // Google Play   com.android.vending
        // Google Authenticator   com.google.android.apps.authenticator2
        intent = packageManager.getLaunchIntentForPackage("com.android.vending");
        if (intent == null) {//根据intent判断应用是否安装
            ToastUtils.toast(getString(R.string.downshuoming));
        } else {
            startActivity(intent);
        }

    }


    @Override
    public void showGetResult(String resultMessage) {
        ToastUtils.toast(resultMessage);
    }

    @Override
    public void getGoogleAuthenticatorSecretSuccess(GoogleAuthenticatorSecretBean bean) {
        tv_secret.setText(bean.getData().getSecret());
        et_verificationCode.setEnabled(true);
        Bitmap bitmap = CodeUtils.createImage(bean.getData().getQrCodeContent(), 400, 400, null);
        qr_view.setImageBitmap(bitmap);
        qr_view.setVisibility(View.VISIBLE);

    }

    @Override
    public void bindGoogleSuccess(VerifyPhone4BindBean bean) {
        VerifySecurityUtil verifySecurityUtil = VerifySecurityUtil.getInstance();
        verifySecurityUtil.init(this, action,
                bean.getData().getVerifyMobile(),
                bean.getData().getVerifyEmail(),
                bean.getData().getVerifyGoogleAuthenticator());

        verifySecurityUtil.setListener(new VerifySecurityUtil.VerifySecurityUtilListener() {
            @Override
            public void onVerifySecuritySuccess() {
                ToastUtils.toast(getString(R.string.success));
                finish();
            }

            @Override
            public void onVerifySecurityFailed() {
                ToastUtils.toast(getString(R.string.faild));


            }

            @Override
            public void onCancelDialog() {


            }
        });
    }

    @Override
    public void showLoading() {
mLoadingDialog.show();
    }

    @Override
    public void hideLoading() {
mLoadingDialog.dismiss();
    }

    @Override
    public void showError(String error) {

    }
}
