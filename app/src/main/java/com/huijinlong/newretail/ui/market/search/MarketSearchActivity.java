package com.huijinlong.newretail.ui.market.search;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.huijinlong.newretail.R;
import com.huijinlong.newretail.base.BaseActivity;
import com.huijinlong.newretail.base.BaseBean;
import com.huijinlong.newretail.util.FontUtil;
import com.huijinlong.newretail.util.ToastUtils;
import com.huijinlong.newretail.view.TagCloudLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class MarketSearchActivity extends BaseActivity<MarketSearchView, MarketSearchPresenter> implements MarketSearchView {


    @BindView(R.id.toolbar_cancel)
    TextView toolbarCancel;
    @BindView(R.id.tv_search)
    TextView tvSearch;
    @BindView(R.id.et_search)
    EditText etSearch;
    @BindView(R.id.ll_search)
    LinearLayout llSearch;
    @BindView(R.id.rv_hot)
    RecyclerView rvHot;
    @BindView(R.id.toolbar_spot_search)
    Toolbar toolbarSpotSearch;
    @BindView(R.id.tcl_hot)
    TagCloudLayout tclHot;

    private HotSearchAdapter mAdapter;
    private List<String> mList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        mPresenter.getcurrencies();
    }


    @Override
    protected int getContentView() {
        return R.layout.activity_market_search;
    }

    @Override
    public MarketSearchPresenter initPresenter() {
        return new MarketSearchPresenter(this);
    }

    @Override
    protected void initView() {
       setToolbarVisibility(false);
        FontUtil.injectFont(tvSearch);

    }

    @Override
    public void showLoading() {
        mLoadingDialog.show();

    }

    @Override
    public void hideLoading() {
        mLoadingDialog.dismiss();

    }

    @Override
    public void showError(String error) {
        ToastUtils.toast(error);
    }

    @Override
    public void getHotSearch(BaseBean currenciesBean) {
        mList.clear();
        for (int i = 0; i < 6; i++) {
            mList.add("BTC" + i);
        }
        mAdapter = new HotSearchAdapter(this, mList);
        tclHot.setAdapter(mAdapter);
        tclHot.setItemClickListener(new TagCloudLayout.TagItemClickListener() {
            @Override
            public void itemClick(int position) {
                ToastUtils.toast(mList.get(position));
            }
        });
//        mAdapter.notifyDataSetChanged();

    }

    @OnClick({R.id.toolbar_cancel, R.id.tv_search, R.id.et_search})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.toolbar_cancel:
                finish();
                break;
            case R.id.tv_search:
                break;
            case R.id.et_search:
                break;
        }
    }

    @OnTextChanged(value = R.id.et_search, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    void afterPriceChange(Editable s) {

    }
}
