package com.huijinlong.newretail.ui.wallet;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.huijinlong.newretail.R;
import com.huijinlong.newretail.base.BaseActivity;
import com.huijinlong.newretail.base.BasePresenter;
import com.huijinlong.newretail.bean.AssetBean;
import com.huijinlong.newretail.bean.CurrenciesBean;
import com.huijinlong.newretail.ui.wallet.addressmanage.CurrenciesPresenter;
import com.huijinlong.newretail.ui.wallet.addressmanage.CurrenciesView;
import com.huijinlong.newretail.ui.wallet.addressmanage.EditAddressWithdrawBean;
import com.huijinlong.newretail.ui.wallet.addressmanage.withdraws.WithDrawsAdapter;
import com.huijinlong.newretail.util.FontUtil;
import com.huijinlong.newretail.util.ToastUtils;
import com.scwang.smartrefresh.header.MaterialHeader;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author zsg
 * @date 2018/11/22 20:06
 * @Version 1.0
 * @Description: (币种列表)
 */
public class CurrencyActivity extends BaseActivity<CurrenciesView, CurrenciesPresenter> implements CurrenciesView, AssetView {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.refresh_layout)
    SmartRefreshLayout refreshLayout;
    @BindView(R.id.tv_search)
    TextView tvSearch;
    @BindView(R.id.tv_feedback)
    EditText tvFeedback;
    @BindView(R.id.tv_feedback_right)
    CheckBox tvFeedbackRight;

    private CurrencyAdapter currencyAdapter;
    private static final int MESSAGE_SEARCH = 0x1;
    private static long INTERVAL = 1000;
    private AssetPresent present;
    private List<CurrenciesBean.DataBean> dataBeanList;

    @Override
    protected void initView() {
        setToolbarTitle("币种列表");
        refreshLayout.setRefreshHeader(new MaterialHeader(this));
        mPresenter.getcurrencies(0);
        FontUtil.injectFont(tvSearch);
        present = new AssetPresent(this);
        tvFeedback.addTextChangedListener(textWatcher);
    }

    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == MESSAGE_SEARCH) {
                present.getasset(0, tvFeedback.getText().toString() + "", 0);
            }
        }
    };

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (tvFeedback.getText().toString() != null) {
                if (mHandler.hasMessages(MESSAGE_SEARCH)) {
                    mHandler.removeMessages(MESSAGE_SEARCH);
                }
                mHandler.sendEmptyMessageDelayed(MESSAGE_SEARCH, INTERVAL);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_currency;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @Override
    public CurrenciesPresenter initPresenter() {
        return new CurrenciesPresenter(this);
    }

    @Override
    public void getCurrencise(CurrenciesBean currenciesBean) {
        currencyAdapter = new CurrencyAdapter(currenciesBean.getData());
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        currencyAdapter.bindToRecyclerView(recyclerView);
        currencyAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                switch (view.getId()) {
                    case R.id.tv_user_name:
                        currenciesBean.getData().get(position).setType(getIntent().getStringExtra("type"));
                        EventBus.getDefault().post(currenciesBean.getData().get(position));
                        finish();
                        break;
                }
            }
        });
    }

    @Override
    public void getEditAddressWithdraw(EditAddressWithdrawBean editAddressWithdrawBean) {

    }

    @Override
    public void showLoading() {
        mLoadingDialog.show();
    }

    @Override
    public void hideLoading() {
        mLoadingDialog.dismiss();
    }

    @Override
    public void showError(String error) {
        ToastUtils.toast(error);
    }

    @Override
    public void getAsset(AssetBean assetBean) {
        CurrenciesBean.DataBean dataBean;
        dataBeanList=new ArrayList<>();
        for (int x = 0; x < assetBean.getData().size(); x++) {
            dataBean = new CurrenciesBean.DataBean();
            dataBean.setType(getIntent().getStringExtra("type"));
            dataBean.setCurrency(assetBean.getData().get(x).getCurrency());
            dataBean.setCid(assetBean.getData().get(x).getCid());

            dataBeanList.add(dataBean);
        }

        currencyAdapter.setNewData(dataBeanList);
        currencyAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                switch (view.getId()) {
                    case R.id.tv_user_name:
                        EventBus.getDefault().post(dataBeanList.get(position));
                        finish();
                        break;
                }
            }
        });
    }
}
