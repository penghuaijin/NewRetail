package com.huijinlong.newretail.ui.wallet.addressmanage;

import com.huijinlong.newretail.base.BasePresenter;
import com.huijinlong.newretail.bean.CurrenciesBean;
import com.huijinlong.newretail.net.MyCallback;
import com.huijinlong.newretail.net.RequestBuilder;
import com.huijinlong.newretail.util.MD5Util;
import com.huijinlong.newretail.util.SpUtil;

import java.util.TreeMap;

/**
 * Created by penghuaijin on 2018/9/12.
 */

public class CurrenciesPresenter extends BasePresenter<CurrenciesView> {

    private CurrenciesView currenciesView;

    public CurrenciesPresenter(CurrenciesView mViews) {
        this.currenciesView = mViews;
    }

    public void getcurrencies(int channel) {
        currenciesView.showLoading();
        TreeMap<String, String> params = new TreeMap<>();
        params.put("deviceType", "android");
        params.put("language", SpUtil.getString(SpUtil.LANGUAGE,"zh-cn"));
        params.put("channel", String.valueOf(channel));

        RequestBuilder.execute(RequestBuilder.getNetService(true).getCurrencies(MD5Util.getSignValue(params), params), "", new MyCallback<CurrenciesBean>() {
            @Override
            public void onSuccess(CurrenciesBean result) {
                super.onSuccess(result);
                currenciesView.getCurrencise(result);
            }

            @Override
            public void onEmpty(CurrenciesBean result) {
                super.onEmpty(result);

            }

            @Override
            public void onBusinessFailure(Exception e) {
                super.onBusinessFailure(e);
                currenciesView.showError(e.getMessage());
            }

            @Override
            public void onFinalResponse(boolean isSuccess) {
                super.onFinalResponse(isSuccess);
                currenciesView.hideLoading();
            }
        });
    }

    public void geteditaddress(int cid,String address,String remark ,int channel) {
        currenciesView.showLoading();
        TreeMap<String, String> params = new TreeMap<>();
        params.put("deviceType", "android");
        params.put("language", SpUtil.getString(SpUtil.LANGUAGE,"zh-cn"));

        params.put("cid", String.valueOf(cid));
        params.put("address", address);
        params.put("remark", remark);
        params.put("channel",String.valueOf(channel));

        RequestBuilder.execute(RequestBuilder.getNetService(true).getEditAddressWithdraw(MD5Util.getSignValue(params), params), "", new MyCallback<EditAddressWithdrawBean>() {
            @Override
            public void onSuccess(EditAddressWithdrawBean result) {
                super.onSuccess(result);
                currenciesView.getEditAddressWithdraw(result);
            }

            @Override
            public void onEmpty(EditAddressWithdrawBean result) {
                super.onEmpty(result);

            }

            @Override
            public void onBusinessFailure(Exception e) {
                super.onBusinessFailure(e);
                currenciesView.showError(e.getMessage());
            }

            @Override
            public void onFinalResponse(boolean isSuccess) {
                super.onFinalResponse(isSuccess);
                currenciesView.hideLoading();
            }
        });
    }



}
