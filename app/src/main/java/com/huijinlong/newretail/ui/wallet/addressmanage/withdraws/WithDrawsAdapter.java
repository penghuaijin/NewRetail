package com.huijinlong.newretail.ui.wallet.addressmanage.withdraws;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.huijinlong.newretail.R;
import com.huijinlong.newretail.base.BaseBean;
import com.huijinlong.newretail.bean.CurrenciesBean;
import com.huijinlong.newretail.bean.DeleteAdressBean;
import com.huijinlong.newretail.bean.WithdrawsListBean;
import com.huijinlong.newretail.ui.wallet.addressmanage.CurrenciesPresenter;
import com.huijinlong.newretail.ui.wallet.addressmanage.CurrenciesView;
import com.huijinlong.newretail.ui.wallet.addressmanage.EditAddressWithdrawBean;
import com.huijinlong.newretail.util.ProgressDialogUtil;
import com.huijinlong.newretail.util.SpUtil;
import com.huijinlong.newretail.util.ToastUtils;
import com.huijinlong.newretail.util.VerifySecurityUtil;

import java.util.List;

/**
 * @author zsg
 * @date 2018/10/22 15:14
 * @Version 1.0
 * @Description: ()
 */
public class WithDrawsAdapter extends BaseQuickAdapter<WithdrawsListBean.DataBean.ListBean, BaseViewHolder> implements View.OnClickListener,
        WithDrawsGetCodeView, CurrenciesView {

    private Context mContext;
    private PopupWindow alarmWindow;
    private LinearLayout popupWindowView;
    private MyCount mc;
    private WithDrawsGetCodePresenter withDrawsGetCodePresenter;
    private CurrenciesPresenter currenciesPresenter;
    private static boolean issuccess;//验证码是否发送成功
    private int ids;//删除item的
    private RecyclerView recyclerView;
    private TextView nicespinner;
    private ImageView img;
    private CurrenciesAdapter currenciesAdapter;
    private boolean isshow = false;//币种列表是否显示
    private Dialog mLoadingDialog;
    private EditText edcode, edadresss, edremark;
    private View views;
    private int cid;
    private boolean isedit;

    public WithDrawsAdapter(@Nullable List<WithdrawsListBean.DataBean.ListBean> data, Context context) {
        super(R.layout.item_withdrawas, data);
        mContext = context;
        initWindow();
        withDrawsGetCodePresenter = new WithDrawsGetCodePresenter(this);
        currenciesPresenter = new CurrenciesPresenter(this);
        mLoadingDialog = ProgressDialogUtil.createLoadingDialog(mContext);
    }

    @Override
    protected void convert(BaseViewHolder helper, WithdrawsListBean.DataBean.ListBean item) {
        helper.setText(R.id.tv_currency, item.getCurrency());
        helper.setText(R.id.tv_address, item.getAddress());
        helper.setText(R.id.tv_remark, item.getRemark());

        helper.getView(R.id.tv_edit).setOnClickListener(this);
        helper.getView(R.id.tv_delete).setOnClickListener(this);

        helper.setTag(R.id.tv_delete, helper.getAdapterPosition());
        helper.setTag(R.id.tv_edit, helper.getAdapterPosition());
    }

    public void initWindow() {
        View nemuView = LayoutInflater.from(mContext).inflate(R.layout.popup_window_view, null, false);
        popupWindowView = (LinearLayout) nemuView.findViewById(R.id.ll_pop_item);

        alarmWindow = new PopupWindow(nemuView, RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT);
      //  alarmWindow.setAnimationStyle(R.style.AnimationFadeBottom);
// new ColorDrawable(Color.argb(0, 0, 0, 0))
        alarmWindow.setBackgroundDrawable(new ColorDrawable(0xb0000000));

        alarmWindow.setOutsideTouchable(false);
        alarmWindow.setFocusable(true);
    }

    public void showDeleWindow(View v) {
        if (!alarmWindow.isShowing()) {
            popupWindowView.removeAllViews();
//            for (CurrenciesBean.DataBean currenciesBean : currenciesBeans.getData()) {
            View popItem = LayoutInflater.from(mContext).inflate(R.layout.poppup_validate, null, false);
            TextView tvphone = (TextView) popItem.findViewById(R.id.tv_phonenum);
            TextView tvclose = (TextView) popItem.findViewById(R.id.tv_close);
            TextView tvcancle = (TextView) popItem.findViewById(R.id.tv_cancle);
            TextView tvsure = (TextView) popItem.findViewById(R.id.tv_sure);
            TextView tvsentcode = (TextView) popItem.findViewById(R.id.tv_sentcode);
            edcode = (EditText) popItem.findViewById(R.id.ed_code);

            mc = new MyCount(60000, 1000, tvsentcode);

            tvphone.setText(SpUtil.getString("mobile", "").replaceAll("(\\d{3})\\d{4}(\\d{4})", "$1****$2"));

            tvclose.setOnClickListener(this);
            tvcancle.setOnClickListener(this);
            tvsure.setOnClickListener(this);
            tvsentcode.setOnClickListener(this);

            popupWindowView.addView(popItem);

            alarmWindow.showAtLocation(v, Gravity.CENTER_VERTICAL, 0, 0);
            //alarmWindow.showAsDropDown(v, LinearLayout.LayoutParams.MATCH_PARENT, 1);

        } else {
            alarmWindow.dismiss();
        }
    }

    public void showEditWindow(View v) {
        if (!alarmWindow.isShowing()) {
            popupWindowView.removeAllViews();
//            for (CurrenciesBean.DataBean currenciesBean : currenciesBeans.getData()) {
            View popItem = LayoutInflater.from(mContext).inflate(R.layout.poppup_edit, null, false);

            TextView tvcancle = (TextView) popItem.findViewById(R.id.tv_edit_cancle);
            TextView tvsure = (TextView) popItem.findViewById(R.id.tv_edit_sure);
            TextView tvclose = (TextView) popItem.findViewById(R.id.tv_edit_close);
            RelativeLayout re_spinner = (RelativeLayout) popItem.findViewById(R.id.re_spinner);
            TextView tvtype=(TextView)popItem.findViewById(R.id.tv_type);
            edadresss = (EditText) popItem.findViewById(R.id.ed_adresss);
            edremark = (EditText) popItem.findViewById(R.id.ed_remark);
            recyclerView = (RecyclerView) popItem.findViewById(R.id.recyclerView);
            nicespinner = (TextView) popItem.findViewById(R.id.nice_spinner);
            img = (ImageView) popItem.findViewById(R.id.img);

            edadresss.setText(getData().get(ids).getAddress() + "");
            edadresss.setSelection(edadresss.getText().length());
            edremark.setText(getData().get(ids).getRemark() + "");
            edremark.setSelection(edremark.getText().length());
            nicespinner.setText(getData().get(ids).getCurrency() + "");
            tvtype.setText("钱包");

            tvclose.setOnClickListener(this);
            tvcancle.setOnClickListener(this);
            tvsure.setOnClickListener(this);
            re_spinner.setOnClickListener(this);
            tvtype.setOnClickListener(this);
            edadresss.setOnClickListener(this);

            popItem.setBackgroundColor(mContext.getResources().getColor(R.color.c_202));
            popupWindowView.addView(popItem);

            alarmWindow.showAtLocation(v, Gravity.CENTER, 0, 0);

        } else {
            alarmWindow.dismiss();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ed_adresss:
                ToastUtils.toast("仅可编辑备注信息");
                break;
            case R.id.tv_type:
                ToastUtils.toast("仅可编辑备注信息");
                break;
            case R.id.tv_edit:
                ids = (int) v.getTag();
                views = v;
                showEditWindow(v);
                isedit = true;
                isshow = false;
                break;
            case R.id.tv_delete:
                ids = (int) v.getTag();
                views = v;
                isedit = false;
                withDrawsGetCodePresenter.getDeleteAdress(getData().get(ids).getId());
                break;
            case R.id.tv_close:
                alarmWindow.dismiss();
                break;
            case R.id.tv_cancle:
                alarmWindow.dismiss();
                break;
            case R.id.tv_sure:
                //短信是否收到
                if (issuccess) {
                    if (isedit) {
                        withDrawsGetCodePresenter.verifySecurity(edcode.getText().toString(), "editAddress");
                    } else {
                        withDrawsGetCodePresenter.verifySecurity(edcode.getText().toString(), "deleteAddress");
                    }
                }
                break;
            case R.id.tv_sentcode:
                withDrawsGetCodePresenter.getCode();
                break;
            case R.id.tv_edit_cancle:
                alarmWindow.dismiss();
                break;
            case R.id.tv_edit_sure:
                alarmWindow.dismiss();
                views = v;
                withDrawsGetCodePresenter.getEditAddressWithdraw(cid, edadresss.getText().toString(),
                        edremark.getText().toString(), getData().get(ids).getId(),"1");//需更改：getData().get(ids).getChannel()
                break;
            case R.id.tv_edit_close:
                alarmWindow.dismiss();
                break;
            case R.id.re_spinner:
                ToastUtils.toast("仅可编辑备注信息");
//                if (isshow) {
//                    recyclerView.setVisibility(View.GONE);
//                    Drawable rightDrawable = mContext.getResources().getDrawable(R.mipmap.dropdown);// 获取res下的图片drawable
//                    rightDrawable.setBounds(0, 0, rightDrawable.getMinimumWidth(), rightDrawable.getMinimumHeight());// 一定要设置setBounds();
//                    img.setBackground(rightDrawable);
//                    isshow = false;
//                } else {
//                    currenciesPresenter.getcurrencies();
//                    isshow = true;
//                    Drawable rightDrawable = mContext.getResources().getDrawable(R.mipmap.dropup);// 获取res下的图片drawable
//                    rightDrawable.setBounds(0, 0, rightDrawable.getMinimumWidth(), rightDrawable.getMinimumHeight());// 一定要设置setBounds();
//                    img.setBackground(rightDrawable);
//                }

                break;

        }
    }

    @Override
    public void getCodeSuccess(BaseBean baseBean) {
        if (baseBean.getStatus() == 200) {
            mc.start();
            issuccess = true;
        } else
            issuccess = false;
    }

    @Override
    public void getDelete(DeleteAdressBean deleteAdressBean) {
        //  showDeleWindow(views);
        VerifySecurityUtil verifySecurityUtil = VerifySecurityUtil.getInstance();
        verifySecurityUtil.init((Activity) mContext, "deleteAddress",
                deleteAdressBean.getData().getVerifyMobile(),
                deleteAdressBean.getData().getVerifyEmail(),
                deleteAdressBean.getData().getVerifyGoogleAuthenticator());

        verifySecurityUtil.setListener(new VerifySecurityUtil.VerifySecurityUtilListener() {
            @Override
            public void onVerifySecuritySuccess() {
                ToastUtils.toast("删除成功");
                getData().remove(ids);
                notifyDataSetChanged();
            }

            @Override
            public void onVerifySecurityFailed() {
                //  ToastUtils.toast("编辑失败");

            }

            @Override
            public void onCancelDialog() {


            }
        });

    }

    @Override
    public void getPhoneCapt(BaseBean baseBean) {
        alarmWindow.dismiss();
        if (!isedit) {
            getData().remove(ids);
        } else {
            WithdrawsListBean.DataBean.ListBean listBean = new WithdrawsListBean.DataBean.ListBean();
            listBean.setAddress(edadresss.getText().toString());
            listBean.setRemark(edremark.getText().toString());
            listBean.setCurrency(nicespinner.getText().toString());
            listBean.setId(getData().get(ids).getId());
            getData().remove(ids);
            getData().add(ids, listBean);
        }
        notifyDataSetChanged();

    }

    @Override
    public void getEdit(EditAddressWithdrawBean bean) {
        //notifyDataSetChanged();
        // showDeleWindow(views);
        VerifySecurityUtil verifySecurityUtil = VerifySecurityUtil.getInstance();
        verifySecurityUtil.init((Activity) mContext, "editAddress",
                bean.getData().getVerifyMobile(),
                bean.getData().getVerifyEmail(),
                bean.getData().getVerifyGoogleAuthenticator());

        verifySecurityUtil.setListener(new VerifySecurityUtil.VerifySecurityUtilListener() {
            @Override
            public void onVerifySecuritySuccess() {
                ToastUtils.toast("编辑成功");

                WithdrawsListBean.DataBean.ListBean listBean = new WithdrawsListBean.DataBean.ListBean();
                listBean.setAddress(edadresss.getText().toString());
                listBean.setRemark(edremark.getText().toString());
                listBean.setCurrency(nicespinner.getText().toString());
                listBean.setId(getData().get(ids).getId());
                getData().remove(ids);
                getData().add(ids, listBean);

                notifyDataSetChanged();
            }

            @Override
            public void onVerifySecurityFailed() {
                // ToastUtils.toast("删除失败");


            }

            @Override
            public void onCancelDialog() {


            }
        });
    }

    @Override
    public void showLoading() {
        mLoadingDialog.show();
    }

    @Override
    public void hideLoading() {
        mLoadingDialog.dismiss();
    }

    @Override
    public void showError(String error) {
        ToastUtils.toast(error);
        isshow = false;
    }

    @Override
    public void getCurrencise(CurrenciesBean currenciesBean) {
        if (currenciesBean.getStatus() == 200) {
            recyclerView.setVisibility(View.VISIBLE);

            currenciesAdapter = new CurrenciesAdapter(currenciesBean.getData(), mContext);
            recyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
            currenciesAdapter.bindToRecyclerView(recyclerView);
            for (CurrenciesBean.DataBean dataBean : currenciesBean.getData()) {
                if (nicespinner.getText().toString().equals(String.valueOf(dataBean.getCid()))) {
                    cid = dataBean.getCid();
                }
            }
            currenciesAdapter.setOnItemChildClickListener(new OnItemChildClickListener() {
                @Override
                public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                    switch (view.getId()) {
                        case R.id.tv_user_name:
                            nicespinner.setText(currenciesBean.getData().get(position).getCurrency());
                            recyclerView.setVisibility(View.GONE);
                            Drawable rightDrawable = mContext.getResources().getDrawable(R.mipmap.dropdown);// 获取res下的图片drawable
                            rightDrawable.setBounds(0, 0, rightDrawable.getMinimumWidth(), rightDrawable.getMinimumHeight());// 一定要设置setBounds();
                            img.setBackground(rightDrawable);
                            isshow = false;
                            cid = currenciesBean.getData().get(position).getCid();

                            break;
                    }
                }
            });

        }
    }

    @Override
    public void getEditAddressWithdraw(EditAddressWithdrawBean editAddressWithdrawBean) {
    }


    /* 定义一个倒计时的内部类 */
    class MyCount extends CountDownTimer {
        TextView tvs;

        public MyCount(long millisInFuture, long countDownInterval, TextView tv) {
            super(millisInFuture, countDownInterval);
            tvs = tv;
        }

        // 获取时间60秒 重新获取（60）
        @Override
        public void onFinish() {
            tvs.setText("重新获取");
            tvs.setClickable(true);
        }

        @Override
        public void onTick(long millisUntilFinished) {

            if (issuccess) {
                tvs.setText(" 重新获取(" + millisUntilFinished / 1000 + ")");
                tvs.setClickable(false);
            }

        }
    }
}

