package com.huijinlong.newretail.ui.mine.accountSecurity.bindemail;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.huijinlong.newretail.R;
import com.huijinlong.newretail.base.BaseActivity;
import com.huijinlong.newretail.requestbean.GetPhoneCaptcha4BindBean;
import com.huijinlong.newretail.requestbean.VerifyPhone4BindBean;
import com.huijinlong.newretail.util.NRGeeTestUtils;
import com.huijinlong.newretail.util.TimeCountUtils;
import com.huijinlong.newretail.util.ToastUtils;
import com.huijinlong.newretail.util.VerifySecurityUtil;
import com.huijinlong.newretail.util.ViewUtil;

import java.util.Map;

/**
 *
 * Created by penghuaijin on 2018/9/26.
 */

public class BindEmailActivity extends BaseActivity<BindEmailView,BindEmailPresenter> implements BindEmailView, View.OnClickListener {

    private EditText et_email;
    private EditText et_verificationCode;
    private BindEmailPresenter mPresenter;
    private TimeCountUtils timeCountUtils;
    private NRGeeTestUtils utils;
    private String action="";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.acitvity_bind_email);
//        initializeActionToolBar();
//        initView();
//        initData();
    }

    @Override
    public BindEmailPresenter initPresenter() {
        return new BindEmailPresenter(this);
    }

    private void initData() {
        mPresenter = new BindEmailPresenter(this);
    }


    @Override
    protected void initView() {
        setToolbarTitle(getString(R.string.bdyx));
        action=getIntent().getStringExtra("action");

        et_email = findViewById(R.id.et_email);
        et_verificationCode = findViewById(R.id.et_verificationCode);
        TextView tv_send_verification = findViewById(R.id.tv_send_verification);


        ViewUtil.setEditTextHintSize(et_email, getString(R.string.yxcw), (int) getResources().getDimension(R.dimen.normal_text_size));
        ViewUtil.setEditTextHintSize(et_verificationCode, getString(R.string.qsryxyzm), (int) getResources().getDimension(R.dimen.normal_text_size));
        findViewById(R.id.btn_bind_email).setOnClickListener(this);
        findViewById(R.id.tv_send_verification).setOnClickListener(this);
        timeCountUtils = new TimeCountUtils(60000, 1000, tv_send_verification);
        utils = new NRGeeTestUtils(this);
        utils.setListener(new NRGeeTestUtils.MyGeeTestListener() {

            @Override
            public void onGeeTestSuccess(Map<String, String> map) {
                mPresenter.getEmailCaptcha4Bind(action, et_email.getText().toString(), map);
            }

        });
    }

    @Override
    protected int getContentView() {
        return R.layout.acitvity_bind_email;
    }

//    private void initializeActionToolBar() {
//        Toolbar mToolbar = findViewById(R.id.toolbar);
//        TextView tvTitle = findViewById(R.id.toolbar_title);
//        tvTitle.setText("绑定邮箱");
//        initToolBar(mToolbar, true, "");
//    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_send_verification: {
                utils.startTest();

                break;
            }
            case R.id.btn_bind_email: {
                mPresenter.verifyEmail4Bind(action, et_verificationCode.getText().toString());
                break;
            }

        }
    }


    @Override
    protected void onDestroy() {
        timeCountUtils.cancel();
        super.onDestroy();
    }

    @Override
    public void showGetResult(String resultMessage) {
        ToastUtils.toast(resultMessage);
    }

    @Override
    public void bindEmailSuccess(VerifyPhone4BindBean bean) {
        VerifySecurityUtil verifySecurityUtil = VerifySecurityUtil.getInstance();
        verifySecurityUtil.init(this, action,
                bean.getData().getVerifyMobile(),
                bean.getData().getVerifyEmail(),
                bean.getData().getVerifyGoogleAuthenticator());

        verifySecurityUtil.setListener(new VerifySecurityUtil.VerifySecurityUtilListener() {
            @Override
            public void onVerifySecuritySuccess() {
                ToastUtils.toast(getString(R.string.success));
                finish();
            }

            @Override
            public void onVerifySecurityFailed() {
                ToastUtils.toast(getString(R.string.faild));


            }

            @Override
            public void onCancelDialog() {


            }
        });
    }

    @Override
    public void getEmailCaptchaSuccess(GetPhoneCaptcha4BindBean bean) {
        utils.setCancelUtils();
        timeCountUtils.start();
    }


    @Override
    public void showLoading() {
        mLoadingDialog.show();
    }

    @Override
    public void hideLoading() {
        mLoadingDialog.dismiss();
    }

    @Override
    public void showError(String error) {

    }

}
