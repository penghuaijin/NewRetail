package com.huijinlong.newretail.ui.spotgoods.kline;

import com.huijinlong.newretail.base.BaseView;
import com.huijinlong.newretail.requestbean.KlineBean;

/**
 * @author zsg
 * @date 2018/10/19 15:52
 * @Version 1.0
 * @Description: ()
 */
public interface KlineView extends BaseView{
    void getKline(KlineBean klineBean);
}
