package com.huijinlong.newretail.ui.spotgoods.commission;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.huijinlong.newretail.R;
import com.huijinlong.newretail.base.BaseFragment;
import com.huijinlong.newretail.base.BaseBean;
import com.huijinlong.newretail.eventbus.OrderPlaceEvent;
import com.huijinlong.newretail.eventbus.SpotTitleEvent;
import com.huijinlong.newretail.requestbean.CurrentCommissionBean;
import com.huijinlong.newretail.requestbean.HistoryBean;
import com.huijinlong.newretail.util.LogUtils;
import com.huijinlong.newretail.util.ToastUtils;
import com.huijinlong.newretail.view.SimpleDividerDecoration;
import com.scwang.smartrefresh.header.MaterialHeader;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * @author penghuaijin
 * @date 2018/10/24 5:05 PM
 * @Description: (历史委托)
 */
public class HistoryCommissionFragment extends BaseFragment<CommissionView, CommissionPresenter> implements CommissionView {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.refresh_layout)
    SmartRefreshLayout refreshLayout;
    Unbinder unbinder;
    private HistoryCommissionAdapter mAdapter;
    private int page = 1;
    private List<HistoryBean.DataBean.ListBean> list = new ArrayList<>();
    private String instrument = "";

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_list;
    }

    @Override
    protected void initData() {
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.addItemDecoration(new SimpleDividerDecoration(getContext(), 0, 0, getResources().getColor(R.color.theme_color), getResources().getDimensionPixelSize(R.dimen.dp_10)));

        mAdapter = new HistoryCommissionAdapter(getContext(), list);
        recyclerView.setAdapter(mAdapter);
        refreshData();

        refreshLayout.setRefreshHeader(new MaterialHeader(getContext()));
        refreshLayout.setOnRefreshListener(refreshlayout -> {
            page = 1;
            refreshData();
            refreshLayout.setEnableLoadmore(true);

        });
        refreshLayout.setOnLoadmoreListener(refreshlayout -> {
            page += 1;
            refreshData();
        });

    }

    @Override
    protected CommissionPresenter initPresenter() {
        return new CommissionPresenter(this);
    }

    private void refreshData() {
        mPresenter.getHistory(instrument, page, 5);
    }

    @Override
    public void showLoading() {
        mLoading.show();
    }

    @Override
    public void hideLoading() {
        mLoading.dismiss();
    }

    @Override
    public void showError(String error) {
        ToastUtils.toast(error);
    }

    @Override
    public void getHistorySuccess(HistoryBean bean) {

        if (page == 1) {
            list.clear();
            refreshLayout.finishRefresh();
        } else {
            refreshLayout.finishLoadmore();
        }
        list.addAll(bean.getData().getList());

        if (list.size() == bean.getData().getTotal()) {
            refreshLayout.setEnableLoadmore(false);
        }
        if (mAdapter != null) {

            mAdapter.notifyDataSetChanged();
        }
        if (isFragmentVisible) {
        }

    }

    @Override
    public void getCurrentSuccess(CurrentCommissionBean bean) {

    }

    @Override
    public void orderSingleCancel(BaseBean bean) {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        EventBus.getDefault().register(this);
        return rootView;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);


    }

    @Subscribe()
    public void onEvent(SpotTitleEvent event) {
        LogUtils.e("SpotTitleEvent HistoryCommissionFragment:" + event.getSymbol());
        page = 1;
        this.instrument = event.getInstrument();
        refreshData();
    }

    @Subscribe()
    public void onEvent(OrderPlaceEvent event) {
        this.instrument = event.getInstrument();
        refreshData();

    }
    public void setInstrument(String instrument) {
        this.instrument = instrument;
    }
}
