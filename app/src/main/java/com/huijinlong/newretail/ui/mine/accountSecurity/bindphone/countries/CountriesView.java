package com.huijinlong.newretail.ui.mine.accountSecurity.bindphone.countries;

import com.huijinlong.newretail.base.BaseView;
import com.huijinlong.newretail.requestbean.CountriesBean;

/**
 * Created by penghuaijin on 2018/9/12.
 */

public interface CountriesView extends BaseView{
        void getCountriesSuccess(CountriesBean bean);
}
