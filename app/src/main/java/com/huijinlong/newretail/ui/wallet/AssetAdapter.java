package com.huijinlong.newretail.ui.wallet;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.huijinlong.newretail.R;
import com.huijinlong.newretail.base.BaseBean;
import com.huijinlong.newretail.bean.AssetBean;
import com.huijinlong.newretail.bean.WithdrawsListBean;
import com.huijinlong.newretail.ui.wallet.addressmanage.withdraws.WithDrawsPresenter;
import com.huijinlong.newretail.ui.wallet.addressmanage.withdraws.WithDrawsView;
import com.huijinlong.newretail.ui.wallet.exchange.ExchangeActivity;
import com.huijinlong.newretail.ui.wallet.recharge.RechargeActivity;
import com.huijinlong.newretail.ui.wallet.withdrawcash.RechargeAdressActivity;
import com.huijinlong.newretail.ui.wallet.withdrawcash.WithDrawMoneyActivity;
import com.huijinlong.newretail.ui.wallet.withdrawcash.WithDrawcashActivity;
import com.huijinlong.newretail.util.ToastUtils;

import java.io.Serializable;
import java.sql.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * @author zsg
 * @date 2018/11/14 20:32
 * @Version 1.0
 * @Description: ()
 */
public class AssetAdapter extends BaseQuickAdapter<AssetBean.DataBean, BaseViewHolder> implements View.OnClickListener, WithDrawsView {

    private WithDrawsPresenter withDrawsPresenter;
    private ArrayList<String> addresslist = new ArrayList<>();
    private int ids;

    public AssetAdapter(@Nullable List<AssetBean.DataBean> data) {
        super(R.layout.item_asset, data);
        withDrawsPresenter = new WithDrawsPresenter(this);
    }

    @Override
    protected void convert(BaseViewHolder helper, AssetBean.DataBean item) {
        helper.setText(R.id.tv_currency, item.getCurrency());
        helper.setText(R.id.tv_balance, mContext.getResources().getString(R.string.ze) + " " + item.getBalance());
        helper.setText(R.id.tv_frozen, mContext.getResources().getString(R.string.dongjie) + " " + item.getFrozen());
        helper.setText(R.id.tv_withdraw, mContext.getResources().getString(R.string.canuse) + " " + item.getWithdraw());

        helper.getView(R.id.lin_chargemoney).setOnClickListener(this);
        helper.getView(R.id.lin_withdraw).setOnClickListener(this);
        helper.getView(R.id.lin_exchange).setOnClickListener(this);

        helper.setTag(R.id.lin_chargemoney, helper.getAdapterPosition());
        helper.setTag(R.id.lin_withdraw, helper.getAdapterPosition());
        helper.setTag(R.id.lin_exchange, helper.getAdapterPosition());

        switch (item.getDepositStatus()) {
            case 1:
                helper.setGone(R.id.lin_chargemoney, true);
                helper.setGone(R.id.view_line_1, true);
                helper.setBackgroundRes(R.id.img_chargemoney, R.mipmap.chargemoney_blue);
                helper.setTextColor(R.id.tv_chargemoney, Color.parseColor("#56A7E6"));
                break;
            case 2:
                helper.setGone(R.id.lin_chargemoney, true);
                helper.setGone(R.id.view_line_1, true);
                helper.setBackgroundRes(R.id.img_chargemoney, R.mipmap.chargemoney);
                helper.setTextColor(R.id.tv_chargemoney, Color.parseColor("#919BC1"));
                break;
            case 3:
                helper.setGone(R.id.lin_chargemoney, false);
                helper.setGone(R.id.view_line_1, false);
                break;
        }
        switch (item.getWithdrawStatus()) {
            case 1:
                helper.setGone(R.id.lin_withdraw, true);
                helper.setGone(R.id.view_line_1, true);
                helper.setBackgroundRes(R.id.img_withdrawmoney, R.mipmap.withdrawmoney_blue);
                helper.setTextColor(R.id.tv_withdrawmoney, Color.parseColor("#56A7E6"));
                break;
            case 2:
                helper.setGone(R.id.lin_withdraw, true);
                helper.setGone(R.id.view_line_1, true);
                helper.setBackgroundRes(R.id.img_withdrawmoney, R.mipmap.withdrawmoney);
                helper.setTextColor(R.id.tv_withdrawmoney, Color.parseColor("#919BC1"));
                break;
            case 3:
                helper.setGone(R.id.lin_withdraw, false);
                helper.setGone(R.id.view_line_1, false);
                break;
        }
        switch (item.getTransferStatus()) {
            case 1:
                helper.setGone(R.id.lin_exchange, true);
                helper.setGone(R.id.view_line_2, true);
                helper.setBackgroundRes(R.id.img_exchange, R.mipmap.exchange_blue);
                helper.setTextColor(R.id.tv_exchange, Color.parseColor("#56A7E6"));
                break;
            case 2:
                helper.setGone(R.id.lin_exchange, true);
                helper.setGone(R.id.view_line_2, true);
                helper.setBackgroundRes(R.id.img_exchange, R.mipmap.exchange);
                helper.setTextColor(R.id.tv_exchange, Color.parseColor("#919BC1"));
                break;
            case 3:
                helper.setGone(R.id.lin_exchange, false);
                helper.setGone(R.id.view_line_2, false);
                break;
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lin_chargemoney:
                if (getData().get((int) v.getTag()).getDepositStatus() == 2) {
                    ToastUtils.toast(getData().get((int) v.getTag()).getDepositTip());
                } else {
                    Intent it = new Intent(mContext, RechargeAdressActivity.class);
                    it.putExtra("cid", String.valueOf(getData().get((int) v.getTag()).getCid()));
                    it.putExtra("currency", getData().get((int) v.getTag()).getCurrency());
                    mContext.startActivity(it);
                }
                break;
            case R.id.lin_withdraw:
                if (getData().get((int) v.getTag()).getWithdrawStatus() == 2) {
                    ToastUtils.toast(getData().get((int) v.getTag()).getWithdrawTip());
                } else {
                    ids = (int) v.getTag();
                    withDrawsPresenter.getwithdraws(1, 100, 1);
                }
                break;
            case R.id.lin_exchange:
                if (getData().get((int) v.getTag()).getTransferStatus() == 2) {
                    ToastUtils.toast(getData().get((int) v.getTag()).getTransferTip());
                } else {
                    Intent it = new Intent(mContext, ExchangeActivity.class);
                    it.putExtra("currency", getData().get((int) v.getTag()).getCurrency());
                    it.putExtra("transferFee", getData().get((int) v.getTag()).getFee());
                    it.putExtra("cid", getData().get((int) v.getTag()).getCid());
                    it.putExtra("limit", String.valueOf(getData().get((int) v.getTag()).getLimit()));
                    mContext.startActivity(it);
                }
                break;
        }
    }

    @Override
    public void getaddressWithdrawList(WithdrawsListBean addressWithdrawListBean) {
        addresslist=new ArrayList<>();
        for (int x = 0; x < addressWithdrawListBean.getData().getList().size(); x++) {
            if (getData().get(ids).getCurrency().equals(addressWithdrawListBean.getData().getList().get(x).getCurrency())) {
                addresslist.add(addressWithdrawListBean.getData().getList().get(x).getAddress());
            }
        }
        Log.e("2222",addresslist.toString());
        if (addresslist.size() == 0) {
            mContext.startActivity(new Intent(mContext, WithDrawcashActivity.class));
        } else {
            Intent it = new Intent(mContext, WithDrawMoneyActivity.class);
            it.putExtra("currency", getData().get(ids).getCurrency());
            it.putExtra("fee", getData().get(ids).getFee().toString());
            it.putExtra("cid", getData().get(ids).getCid());
            it.putStringArrayListExtra("addresslist", addresslist);
            it.putExtra("limit", String.valueOf(getData().get(ids).getLimit()));
            //  it.putExtra("type","only");
            mContext.startActivity(it);
        }
    }

    @Override
    public void getWithdraw(BaseBean baseBean) {

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showError(String error) {

    }
}
