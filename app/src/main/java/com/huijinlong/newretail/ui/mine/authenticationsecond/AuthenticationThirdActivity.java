package com.huijinlong.newretail.ui.mine.authenticationsecond;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.TextView;

import com.huijinlong.newretail.R;
import com.huijinlong.newretail.base.BaseActivity;
import com.huijinlong.newretail.base.BasePresenter;
import com.huijinlong.newretail.util.FontUtil;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by penghuaijin on 2018/9/29.
 */

public class AuthenticationThirdActivity extends BaseActivity {
    @BindView(R.id.tv_lv1_arrow)
    TextView tvLv1Arrow;
    @BindView(R.id.tv_lv2_arrow)
    TextView tvLv2Arrow;
    @BindView(R.id.tv_lv3_arrow)
    TextView tvLv3Arrow;
    @BindView(R.id.btn_ok)
    Button btnOk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_authentication_first);
        ButterKnife.bind(this);
//        initializeActionToolBar();
//        initView();
    }

    @Override
    public BasePresenter initPresenter() {
        return null;
    }

//    private void initializeActionToolBar() {
//        Toolbar mToolbar = findViewById(R.id.toolbar);
//        TextView tvTitle = findViewById(R.id.toolbar_title);
//        tvTitle.setText("身份认证");
//        initToolBar(mToolbar, true, "");
//    }

    @Override
    protected void initView() {
        setToolbarTitle(getString(R.string.sfrz));
        FontUtil.injectFont(tvLv1Arrow);
        FontUtil.injectFont(tvLv2Arrow);
        FontUtil.injectFont(tvLv3Arrow);
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_authentication_first;
    }
}
