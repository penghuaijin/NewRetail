package com.huijinlong.newretail.ui.wallet;

import android.support.annotation.Nullable;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.huijinlong.newretail.R;
import com.huijinlong.newretail.bean.CurrenciesBean;

import java.util.List;

/**
 * @author zsg
 * @date 2018/11/22 20:12
 * @Version 1.0
 * @Description: ()
 */
public class CurrencyAdapter extends BaseQuickAdapter<CurrenciesBean.DataBean, BaseViewHolder>{

    public CurrencyAdapter(@Nullable List<CurrenciesBean.DataBean> data) {
        super(R.layout.item_curreny, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, CurrenciesBean.DataBean item) {
        helper.setText(R.id.tv_user_name, item.getCurrency() + "");
        helper.addOnClickListener(R.id.tv_user_name);
    }

}
