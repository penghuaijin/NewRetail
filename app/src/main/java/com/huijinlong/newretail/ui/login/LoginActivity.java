package com.huijinlong.newretail.ui.login;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.text.method.TransformationMethod;
import android.view.View;

import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.huijinlong.newretail.MainActivity;
import com.huijinlong.newretail.R;
import com.huijinlong.newretail.base.BaseActivity;
import com.huijinlong.newretail.base.BaseActivityBefor;
import com.huijinlong.newretail.requestbean.LoginBean;
import com.huijinlong.newretail.ui.login.forget.ForgetPasswordFirstActivity;
import com.huijinlong.newretail.ui.login.register.RegisterActivity;
import com.huijinlong.newretail.util.FontUtil;
import com.huijinlong.newretail.util.LogUtils;
import com.huijinlong.newretail.util.NRGeeTestUtils;
import com.huijinlong.newretail.util.SpUtil;
import com.huijinlong.newretail.util.ToastUtils;

import java.util.Map;

/**
 * Created by penghuaijin on 2018/9/10.
 */

public class LoginActivity extends BaseActivityBefor<LoginView, LoginPresenter> implements View.OnClickListener, LoginView {
    // private LoginPresenter loginPresenter;
    private EditText account;
    private EditText password;
    private ImageView iv_login_eye;
    private boolean isHide = true;
    private boolean login_type = false;
    private TextView tv_login_type;
    private TextView tv_login_from_type;
    private NRGeeTestUtils utils;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initView();
        //loginPresenter = new LoginPresenter(this);
        utils = new NRGeeTestUtils(this);
        utils.setListener(new NRGeeTestUtils.MyGeeTestListener() {

            @Override
            public void onGeeTestSuccess(Map<String, String> map) {
                if (TextUtils.isEmpty(account.getText().toString())) {
                    mPresenter.doLogin("18813144059", "phj123456", map);
                } else {
                    mPresenter.doLogin(account.getText().toString(), password.getText().toString(), map);
                }
            }

        });

    }

    @Override
    public LoginPresenter initPresenter() {
        return new LoginPresenter(this);
    }

    private void initView() {

        Button btn_login = findViewById(R.id.btn_login);
        btn_login.setOnClickListener(this);
        TextView tv_login_back = findViewById(R.id.tv_login_back);
        FontUtil.injectFont(tv_login_back);
        tv_login_back.setOnClickListener(this);
        ImageView iv_login_account_clear = findViewById(R.id.iv_login_account_clear);
        iv_login_account_clear.setOnClickListener(this);
        TextView tv_login_register = findViewById(R.id.tv_login_register);
        tv_login_register.setOnClickListener(this);
        TextView tv_forget_password = findViewById(R.id.tv_forget_password);
        tv_forget_password.setOnClickListener(this);

        tv_login_from_type = findViewById(R.id.tv_login_from_type);
        tv_login_from_type.setOnClickListener(this);
        tv_login_type = findViewById(R.id.tv_login_account);
        account = findViewById(R.id.et_login_account);
        password = findViewById(R.id.et_login_password);
        iv_login_eye = findViewById(R.id.iv_login_eye);
        iv_login_eye.setOnClickListener(this);
        account.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (TextUtils.isEmpty(s.toString())){
                    iv_login_account_clear.setVisibility(View.GONE);
                }else {
                    iv_login_account_clear.setVisibility(View.VISIBLE);

                }
            }
        });

        initializeActionToolBar();
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_login: {

                utils.startTest();

                break;
            }
            case R.id.tv_login_back: {
                finish();
                break;
            }
            case R.id.iv_login_account_clear: {
                account.setText("");
                break;
            }
            case R.id.iv_login_eye: {
                if (!isHide) {
                    iv_login_eye.setImageResource(R.mipmap.ic_eye_close);
                    HideReturnsTransformationMethod method1 = HideReturnsTransformationMethod.getInstance();
                    password.setTransformationMethod(method1);
                    isHide = true;
                } else {
                    iv_login_eye.setImageResource(R.mipmap.ic_eye_open);
                    TransformationMethod method = PasswordTransformationMethod.getInstance();
                    password.setTransformationMethod(method);
                    isHide = false;
                }
                int index = password.getText().toString().length();
                password.setSelection(index);
                break;
            }
            case R.id.tv_login_register: {
                jump2Activity(RegisterActivity.class);
                break;
            }
            case R.id.tv_login_from_type: {
                login_type = !login_type;
                if (login_type) {
                    tv_login_type.setText(getResources().getString(R.string.yx));
                    tv_login_from_type.setText(getResources().getString(R.string.sysjdl));
                } else {
                    tv_login_type.setText(getResources().getString(R.string.sj));
                    tv_login_from_type.setText(getResources().getString(R.string.syyxdl));
                }
                break;
            }
            case R.id.tv_forget_password: {
                jump2Activity(ForgetPasswordFirstActivity.class);
                break;
            }
        }
    }


    @Override
    public void loginSuccess(LoginBean loginBean) {
        utils.setSuccess2(true);
        utils.getGt3GeetestUtils().cancelUtils();

        LogUtils.e("getAccessToken----------" + loginBean.getData().getAccessToken());
        SpUtil.putString(SpUtil.AUTHORIZATION, loginBean.getData().getTokenType() + " " + loginBean.getData().getAccessToken());
        SpUtil.putString(SpUtil.NICK_NAME, loginBean.getData().getNickName());
        SpUtil.putString(SpUtil.MOBILE, loginBean.getData().getMobile());
        SpUtil.putString(SpUtil.EMAIL, loginBean.getData().getEmail());
        SpUtil.putString(SpUtil.USER_ID, loginBean.getData().getUserId());
        jump2Activity(MainActivity.class);
        this.finish();
    }


    private void initializeActionToolBar() {
        Toolbar mToolbar = findViewById(R.id.toolbar);
        mToolbar.setVisibility(View.GONE);
        initToolBar(mToolbar, false, "");
    }

    @Override
    protected boolean translucentStatusBar() {
        return true;
    }


    @Override
    protected void onDestroy() {

        super.onDestroy();
    }

    @Override
    public void showLoading() {
        mLoadingDialog.show();
    }

    @Override
    public void hideLoading() {
        mLoadingDialog.dismiss();
    }

    @Override
    public void showError(String error) {
        ToastUtils.toast(error);
        utils.setSuccess2(false);
    }
}
