package com.huijinlong.newretail.ui.mine.accountSecurity.paypassword;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.EditText;
import android.widget.TextView;

import com.huijinlong.newretail.R;
import com.huijinlong.newretail.base.BaseActivity;
import com.huijinlong.newretail.base.BasePresenter;
import com.huijinlong.newretail.util.ViewUtil;

/**
 * Created by penghuaijin on 2018/9/28.
 */

public class PayPasswordActivity extends BaseActivity {
    private EditText et_pay_password;
    private EditText et_re_pay_password;
    private EditText et_verification_code;

//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_pay_password);
//        initView();
//        initializeActionToolBar();
//    }

    @Override
    public BasePresenter initPresenter() {
        return null;
    }

    @Override
    protected void initView() {
        setToolbarTitle(getString(R.string.szzfmm));
        et_pay_password = findViewById(R.id.et_pay_password);
        et_re_pay_password = findViewById(R.id.et_re_pay_password);
        et_verification_code = findViewById(R.id.et_verification_code);
        ViewUtil.setEditTextHintSize(et_pay_password, getString(R.string.szzfmmplace), (int) getResources().getDimension(R.dimen.normal_text_size));
        ViewUtil.setEditTextHintSize(et_re_pay_password, getString(R.string.szzfmmplace), (int) getResources().getDimension(R.dimen.normal_text_size));
        ViewUtil.setEditTextHintSize(et_verification_code, getString(R.string.qsrsjyzm), (int) getResources().getDimension(R.dimen.normal_text_size));

    }

    @Override
    protected int getContentView() {
        return R.layout.activity_pay_password;
    }

//    private void initializeActionToolBar() {
//        Toolbar mToolbar = findViewById(R.id.toolbar);
//        TextView tvTitle = findViewById(R.id.toolbar_title);
//        tvTitle.setText("支付密码");
//        initToolBar(mToolbar, true, "");
//    }
}
