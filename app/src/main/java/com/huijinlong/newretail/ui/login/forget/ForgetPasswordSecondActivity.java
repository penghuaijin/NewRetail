package com.huijinlong.newretail.ui.login.forget;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.huijinlong.newretail.R;
import com.huijinlong.newretail.base.BaseActivity;
import com.huijinlong.newretail.base.BaseBean;
import com.huijinlong.newretail.util.FontUtil;
import com.huijinlong.newretail.util.NRGeeTestUtils;
import com.huijinlong.newretail.util.TimeCountUtils;
import com.huijinlong.newretail.util.ToastUtils;
import com.huijinlong.newretail.util.ViewUtil;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by penghuaijin on 2018/9/27.
 */

public class ForgetPasswordSecondActivity extends BaseActivity<ForgetPasswordView, ForgetPasswordPresenter> implements View.OnClickListener, ForgetPasswordView {
    private ArrayList<Integer> fontTextId;
    private NRGeeTestUtils utils;
    private TimeCountUtils timeCountUtils;
    // private ForgetPasswordPresenter mPresenter;
    private String key = "";
    private String resetType = "";
    private TextView tv_send_verification;
    private EditText et_password;
    private EditText et_re_password;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_forget_password_second);
//        initView();
        initData();
//        initializeActionToolBar();
    }

    @Override
    public ForgetPasswordPresenter initPresenter() {
        return new ForgetPasswordPresenter(this);
    }

    private void initData() {
        key = getIntent().getStringExtra("key");
        resetType = getIntent().getStringExtra("resetType");
        // mPresenter = new ForgetPasswordPresenter(this);
        timeCountUtils = new TimeCountUtils(60000, 1000, tv_send_verification);
        utils = new NRGeeTestUtils(this);
        utils.setListener(new NRGeeTestUtils.MyGeeTestListener() {

            @Override
            public void onGeeTestSuccess(Map<String, String> map) {
                mPresenter.getPhoneCaptcha(key, map);
            }

        });
    }


    @Override
    protected void initView() {
        setToolbarTitle(getString(R.string.czmm));
        fontTextId = new ArrayList<>();
        fontTextId.add(R.id.tv_verify_identidy);
        fontTextId.add(R.id.tv_reset_password);
        initFont();
        findViewById(R.id.btn_ok).setOnClickListener(this);
        et_password = findViewById(R.id.et_password);
        et_re_password = findViewById(R.id.et_re_password);
        tv_send_verification = findViewById(R.id.tv_send_verification);
        tv_send_verification.setOnClickListener(this);
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_forget_password_second;
    }

    private void initFont() {
        for (int i = 0; i < fontTextId.size(); i++) {
            TextView textView = findViewById(fontTextId.get(i));
            FontUtil.injectFont(textView);
            textView.setOnClickListener(this);
        }
    }

    private boolean checkEditText() {

        return ViewUtil.checkEditTextForPassword(et_password, 12, getString(R.string.dlczmm))
                && ViewUtil.checkEditTextForPassword(et_re_password, 12, getString(R.string.qrczmm));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_ok: {
                if (checkEditText()) {
                    mPresenter.forgetPwd(key, et_password.getText().toString(), et_re_password.getText().toString(), tv_send_verification.getText().toString(), resetType);
                }
                break;
            }
            case R.id.tv_send_verification: {
                utils.startTest();
                break;
            }
        }
    }

//    private void initializeActionToolBar() {
//        Toolbar mToolbar = findViewById(R.id.toolbar);
//        TextView tvTitle = findViewById(R.id.toolbar_title);
//        tvTitle.setText("重置密码");
//        initToolBar(mToolbar, true, "");
//    }

    @Override
    public void forgetPwdSuccess(BaseBean bean) {
        ToastUtils.toast(bean.getMsg());
        finish();
    }

    @Override
    public void getCaptchaSuccess(BaseBean bean) {
        utils.setCancelUtils();
        timeCountUtils.start();
    }

    @Override
    protected void onDestroy() {
        timeCountUtils.cancel();
        super.onDestroy();
    }

    @Override
    public void showLoading() {
        mLoadingDialog.show();
    }

    @Override
    public void hideLoading() {
        mLoadingDialog.dismiss();
    }

    @Override
    public void showError(String error) {
        utils.setCancelUtils();
        ToastUtils.toast(error);
    }
}
