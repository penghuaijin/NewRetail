package com.huijinlong.newretail.ui.wallet.addressmanage.withdraws;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.huijinlong.newretail.R;
import com.huijinlong.newretail.base.BaseActivity;
import com.huijinlong.newretail.base.BaseBean;
import com.huijinlong.newretail.bean.WithdrawsListBean;
import com.huijinlong.newretail.util.ToastUtils;
import com.scwang.smartrefresh.header.MaterialHeader;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author zsg
 * @date 2018/10/19 18:07
 * @Version 1.0
 * @Description: (地址记录)
 */
public class WithDrawsActivity extends BaseActivity<WithDrawsView, WithDrawsPresenter> implements WithDrawsView {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.refresh_layout)
    SmartRefreshLayout refreshLayout;

    private List<WithdrawsListBean.DataBean.ListBean> list = new ArrayList<>();
    private WithDrawsAdapter withDrawsAdapter;
    private int page = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
    }

    @Override
    public WithDrawsPresenter initPresenter() {
        return new WithDrawsPresenter(this);
    }

    @Override
    protected void initView() {
        setToolbarTitle( getString(R.string.dzjl));

        withDrawsAdapter = new WithDrawsAdapter(list, this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        withDrawsAdapter.bindToRecyclerView(recyclerView);

        refreshData();

        refreshLayout.setRefreshHeader(new MaterialHeader(this));
        refreshLayout.setOnRefreshListener(refreshlayout -> {
            page = 1;
            refreshData();
            refreshLayout.setEnableLoadmore(true);

        });
        refreshLayout.setOnLoadmoreListener(refreshlayout -> {
            page+=1;
            refreshData();
        });
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_wallet_withdraws;
    }

    @Override
    public void getaddressWithdrawList(WithdrawsListBean addressWithdrawListBean) {

        if (page == 1) {
            list.clear();
            refreshLayout.finishRefresh();
        } else {
            refreshLayout.finishLoadmore();
        }
        list.addAll(addressWithdrawListBean.getData().getList());

        if (list.size() == addressWithdrawListBean.getData().getTotal()) {
            refreshLayout.setEnableLoadmore(false);
        }
        withDrawsAdapter.notifyDataSetChanged();
    }

    @Override
    public void getWithdraw(BaseBean baseBean) {

    }

    private void refreshData() {
        mPresenter.getwithdraws(page, 5,1);
    }

    @Override
    public void showLoading() {
        mLoadingDialog.show();
    }

    @Override
    public void hideLoading() {
        mLoadingDialog.dismiss();
    }

    @Override
    public void showError(String error) {
        ToastUtils.toast(error);
    }
}
