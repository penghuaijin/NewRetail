package com.huijinlong.newretail.ui.wallet.rechargeCapital;

import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.huijinlong.newretail.R;
import com.huijinlong.newretail.base.BaseActivity;
import com.huijinlong.newretail.base.BaseBean;
import com.huijinlong.newretail.bean.CurrenciesBean;
import com.huijinlong.newretail.bean.WithdrawsListBean;
import com.huijinlong.newretail.bean.WthdrawChannelBean;
import com.huijinlong.newretail.ui.wallet.CurrencyActivity;
import com.huijinlong.newretail.ui.wallet.addressmanage.withdraws.WithDrawsPresenter;
import com.huijinlong.newretail.ui.wallet.addressmanage.withdraws.WithDrawsView;
import com.huijinlong.newretail.ui.wallet.exchange.TransferOutPresent;
import com.huijinlong.newretail.ui.wallet.exchange.TransferOutlView;
import com.huijinlong.newretail.ui.wallet.walletDetail.WalletDetailActivity;
import com.huijinlong.newretail.util.FontUtil;
import com.huijinlong.newretail.util.ToastUtils;
import com.weigan.loopview.LoopView;
import com.weigan.loopview.OnItemSelectedListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author zsg
 * @date 2018/11/13 18:09
 * @Version 1.0
 * @Description: (资金划转)
 */
public class RechargeCapitalActivity extends BaseActivity<ChannelView, ChannelPresent> implements ChannelView, TransferOutlView, WithDrawsView {

    @BindView(R.id.tv)
    TextView tv;
    @BindView(R.id.tv_account)
    TextView tvAccount;
    @BindView(R.id.im_down)
    ImageView imDown;
    @BindView(R.id.tv_currency)
    TextView tvCurrency;
    @BindView(R.id.tv_chooses)
    TextView tvChooses;
    @BindView(R.id.lin_currency)
    LinearLayout linCurrency;
    @BindView(R.id.et_amount)
    EditText etAmount;
    @BindView(R.id.tv_address)
    TextView tvAddress;
    @BindView(R.id.tv_choose)
    ImageView tvChoose;
    @BindView(R.id.lin_address)
    LinearLayout linAddress;
    @BindView(R.id.bt_add)
    Button btAdd;
    @BindView(R.id.tv_balance)
    TextView tvBalance;
    private PopupWindow alarmWindow;
    private LinearLayout popupWindowView;
    private String type;
    private WthdrawChannelBean bean;
    private int cid;
    //private String address;

    private ArrayList<String> list = new ArrayList<String>();
    private TransferOutPresent transferOutPresent;
    private WithDrawsPresenter withDrawsPresenter;

    @Override
    protected void initView() {
        setToolbarTitle(getString(R.string.zjhz));
        setmToolbarRightBtnColor(R.color.c_55a);
        setToolbarRightBtn(true, false, getString(R.string.hzjl), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startToActivity(RechargeCapitalActivity.this, WalletDetailActivity.class);
            }
        });
        FontUtil.injectFont(tv);
        FontUtil.injectFont(tvChooses);
        mPresenter.getChannel();
        initWindow();

        transferOutPresent = new TransferOutPresent(this);
        withDrawsPresenter = new WithDrawsPresenter(this);


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public ChannelPresent initPresenter() {
        return new ChannelPresent(this);
    }

    @Override
    protected int getContentView() {
        return R.layout.fragment_rechargecapital;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        EventBus.getDefault().register(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getRechargeCapitalcurrency(CurrenciesBean.DataBean dataBean) {
        if (dataBean.getType().equals("RechargeCapital")) {
            tvCurrency.setText(dataBean.getCurrency() + "");
            cid = dataBean.getCid();
            //address = dataBean.getExplorer();
            tvBalance.setText(getString(R.string.zdkz)+" "+dataBean.getBalance()+" "+dataBean.getCurrency());
            tvBalance.setVisibility(View.VISIBLE);
        }

    }


    @OnClick({R.id.tv_choose, R.id.lin_currency, R.id.tv_account, R.id.bt_add, R.id.lin_address})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_choose:
                break;
            case R.id.lin_currency:
                if (!tvAccount.getText().toString().equals(getString(R.string.qianbaotd)) &&
                        !tvAccount.getText().toString().equals(getString(R.string.jbptd))) {
                    ToastUtils.toast(getString(R.string.qxxzhzfs));
                } else {
                    Intent it = new Intent(this, CurrencyActivity.class);
                    it.putExtra("type", "RechargeCapital");
                    startActivity(it);
                }

                break;
            case R.id.tv_account:
                if (bean != null) {
                    Drawable rightDrawable = getResources().getDrawable(R.mipmap.dropup);// 获取res下的图片drawable
                    rightDrawable.setBounds(0, 0, rightDrawable.getMinimumWidth(), rightDrawable.getMinimumHeight());// 一定要设置setBounds();
                    imDown.setBackground(rightDrawable);
                    showWindow(tvAccount, bean);
                }

                break;
            case R.id.bt_add:
                if (tvAccount.getText().toString().equals(getString(R.string.qianbaotd))) {
                    //钱包
                    withDrawsPresenter.getwithdraw(cid, etAmount.getText().toString(), type);
                } else {
                    //聚宝盆
                    transferOutPresent.getwithdraws(String.valueOf(cid), etAmount.getText().toString(), type);
                }
                break;

            case R.id.lin_address:
                Drawable rightDrawable = getResources().getDrawable(R.mipmap.dropup);// 获取res下的图片drawable
                rightDrawable.setBounds(0, 0, rightDrawable.getMinimumWidth(), rightDrawable.getMinimumHeight());// 一定要设置setBounds();
                tvChoose.setBackground(rightDrawable);

                if (tvAccount.getText().toString().equals(getString(R.string.qianbaotd))) {
                    withDrawsPresenter.getwithdraws(1, 100, 1);
                } else if (tvAccount.getText().toString().equals(getString(R.string.jbptd))) {
                    withDrawsPresenter.getwithdraws(1, 100, 2);
                } else {
                    ToastUtils.toast(getString(R.string.qxxzhzfs));
                    // Drawable rightDrawable = getResources().getDrawable(R.mipmap.dropdown);// 获取res下的图片drawable
                    rightDrawable.setBounds(0, 0, rightDrawable.getMinimumWidth(), rightDrawable.getMinimumHeight());// 一定要设置setBounds();
                    tvChoose.setBackground(rightDrawable);
                }
                break;
        }
    }

    public void initWindow() {
        View nemuView = LayoutInflater.from(this).inflate(R.layout.popup_window_view, null, false);
        popupWindowView = (LinearLayout) nemuView.findViewById(R.id.ll_pop_item);

        alarmWindow = new PopupWindow(nemuView, LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
//        alarmWindow.setAnimationStyle(R.style.popAnimationPreview);
        alarmWindow.setBackgroundDrawable(new BitmapDrawable());
        alarmWindow.setOutsideTouchable(false);
        alarmWindow.setFocusable(true);
        alarmWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                Drawable rightDrawable = getResources().getDrawable(R.mipmap.dropdown);// 获取res下的图片drawable
                rightDrawable.setBounds(0, 0, rightDrawable.getMinimumWidth(), rightDrawable.getMinimumHeight());// 一定要设置setBounds();
                imDown.setBackground(rightDrawable);
                tvChoose.setBackground(rightDrawable);
                alarmWindow.dismiss();
            }
        });
    }

    public void showWindow(View v, WthdrawChannelBean wthdrawChannelBean) {
        if (!alarmWindow.isShowing()) {
            popupWindowView.removeAllViews();

            View popItem = LayoutInflater.from(this).inflate(R.layout.dialog_wheelview, null, false);
            LoopView loopView = (LoopView) popItem.findViewById(R.id.loopView);
            TextView tv_cancle = (TextView) popItem.findViewById(R.id.tv_cancle);
            TextView tv_sure = (TextView) popItem.findViewById(R.id.tv_sure);

            ArrayList<String> list = new ArrayList<>();

            for (int x = 0; x < wthdrawChannelBean.getData().size(); x++) {
                list.add(String.valueOf(wthdrawChannelBean.getData().get(x).getName()));
            }
            type = list.get(0);

            loopView.setNotLoop();
            tv_cancle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alarmWindow.dismiss();
                }
            });
            tv_sure.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tvAccount.setText(type);
                    alarmWindow.dismiss();
                }
            });

            // 滚动监听
            loopView.setListener(new OnItemSelectedListener() {
                @Override
                public void onItemSelected(int index) {
                    type = String.valueOf(list.get(index));
                }
            });
            // 设置原始数据
            loopView.setItems(list);
            loopView.setTextSize(18);
            // loopView.setDividerColor(R.color.white);

            popupWindowView.addView(popItem);
            alarmWindow.showAtLocation(v, Gravity.BOTTOM, 0, 0);

        } else {
            alarmWindow.dismiss();
        }
    }

    public void showAddressWindow() {
        if (!alarmWindow.isShowing()) {
            popupWindowView.removeAllViews();
            // list.clear();
            View popItem = LayoutInflater.from(this).inflate(R.layout.dialog_wheelview, null, false);
            LoopView loopView = (LoopView) popItem.findViewById(R.id.loopView);
            TextView tv_cancle = (TextView) popItem.findViewById(R.id.tv_cancle);
            TextView tv_sure = (TextView) popItem.findViewById(R.id.tv_sure);

            type = list.get(0);

            loopView.setNotLoop();
            tv_cancle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alarmWindow.dismiss();
                }
            });
            tv_sure.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tvAddress.setText(type);
                    alarmWindow.dismiss();
                }
            });

            // 滚动监听
            loopView.setListener(new OnItemSelectedListener() {
                @Override
                public void onItemSelected(int index) {
                    type = String.valueOf(list.get(index));
                }
            });
            // 设置原始数据
            loopView.setItems(list);
            loopView.setTextSize(18);
            // loopView.setDividerColor(R.color.c_2f3);
            popupWindowView.addView(popItem);
            alarmWindow.showAtLocation(linAddress, Gravity.BOTTOM, 0, 0);

        } else {
            alarmWindow.dismiss();
        }
    }

    @Override
    public void getChannelSuccess(WthdrawChannelBean wthdrawChannelBean) {
        bean = wthdrawChannelBean;
    }

    @Override
    public void showLoading() {
        mLoadingDialog.show();
    }

    @Override
    public void hideLoading() {
        mLoadingDialog.dismiss();
    }

    @Override
    public void showError(String error) {
        ToastUtils.toast(error);
    }

    @Override
    public void getaddressWithdrawList(WithdrawsListBean addressWithdrawListBean) {

        list.clear();
        for (int x = 0; x < addressWithdrawListBean.getData().getList().size(); x++) {
            if (tvCurrency.getText().toString().equals(addressWithdrawListBean.getData().getList().get(x).getCurrency())) {
                list.add(addressWithdrawListBean.getData().getList().get(x).getAddress());
            }
        }
        if (list.size() == 0) {
            ToastUtils.toast("无地址");
            Drawable rightDrawable = getResources().getDrawable(R.mipmap.dropdown);// 获取res下的图片drawable
            rightDrawable.setBounds(0, 0, rightDrawable.getMinimumWidth(), rightDrawable.getMinimumHeight());// 一定要设置setBounds();
            tvChoose.setBackground(rightDrawable);
        } else {
            showAddressWindow();
        }

    }

    @Override
    public void getWithdraw(BaseBean baseBean) {
        ToastUtils.toast(getString(R.string.hzcg));
        finish();
    }

    @Override
    public void gettransferOut(BaseBean baseBean) {
        ToastUtils.toast(getString(R.string.hzcg));
        finish();
    }
}
