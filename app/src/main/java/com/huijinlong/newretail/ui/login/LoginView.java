package com.huijinlong.newretail.ui.login;


import com.huijinlong.newretail.base.BaseView;
import com.huijinlong.newretail.requestbean.LoginBean;

import java.util.Map;

public interface LoginView extends BaseView{
		void loginSuccess(LoginBean loginBean);
}
