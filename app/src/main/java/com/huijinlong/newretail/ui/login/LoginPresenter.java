package com.huijinlong.newretail.ui.login;


import com.huijinlong.newretail.base.BasePresenter;
import com.huijinlong.newretail.net.MyCallback;
import com.huijinlong.newretail.net.RequestBuilder;
import com.huijinlong.newretail.requestbean.LoginBean;
import com.huijinlong.newretail.util.MD5Util;

import java.util.Map;
import java.util.TreeMap;

public class LoginPresenter extends BasePresenter<LoginView> {
    private LoginView loginView;

    public LoginPresenter(LoginView views) {
        this.loginView = views;
    }

    public void doLogin(String userAccount, String password, Map<String, String> map) {
        TreeMap<String, String> params = new TreeMap<>();
        params.put("grant_type", "password");
        params.put("client_id", "3");
        params.put("client_secret", "uCGCHYmxfUEH1WNwTTiBrLkYU6qGXhHpyrgC8SID");
        params.put("key", userAccount);
        params.put("password", password);
        params.put("deviceType", "android");
        params.put("challenge",map.get("geetest_challenge") );
        params.put("validate", map.get("geetest_validate"));
        params.put("seccode", map.get("geetest_seccode"));
        RequestBuilder.execute(RequestBuilder.getNetService().login(MD5Util.getSignValue(params), params), "", new MyCallback<LoginBean>() {
            @Override
            public void onSuccess(LoginBean result) {
                super.onSuccess(result);
                loginView.loginSuccess(result);
            }

            @Override
            public void onEmpty(LoginBean result) {
                super.onEmpty(result);

            }

            @Override
            public void onBusinessFailure(Exception e) {
                super.onBusinessFailure(e);
                loginView.showError(e.getMessage());

            }

            @Override
            public void onFinalResponse(boolean isSuccess) {
                super.onFinalResponse(isSuccess);
                loginView.hideLoading();

            }
        });

    }

}
