package com.huijinlong.newretail.ui.mine.accountSecurity;

import com.huijinlong.newretail.base.BaseView;
import com.huijinlong.newretail.bean.SecurityInfoBean;

/**
 * @author zsg
 * @date 2018/10/26 14:39
 * @Version 1.0
 * @Description: ()
 */
public interface SecurityInfoView extends BaseView {

    void getSecurity(SecurityInfoBean securityInfoBean);
}
