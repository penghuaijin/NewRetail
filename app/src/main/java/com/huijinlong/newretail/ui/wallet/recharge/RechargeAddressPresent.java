package com.huijinlong.newretail.ui.wallet.recharge;

import com.huijinlong.newretail.base.BasePresenter;
import com.huijinlong.newretail.net.MyCallback;
import com.huijinlong.newretail.net.RequestBuilder;
import com.huijinlong.newretail.requestbean.WalletAddressBean;
import com.huijinlong.newretail.util.MD5Util;
import com.huijinlong.newretail.util.SpUtil;

import java.util.TreeMap;

/**
 * @author zsg
 * @date 2018/10/25 14:40
 * @Version 1.0
 * @Description: ()
 */
public class RechargeAddressPresent extends BasePresenter<RechargeAddressView> {
    private RechargeAddressView rechargeAddressView;

    public RechargeAddressPresent(RechargeAddressView mViews) {
        this.rechargeAddressView = mViews;
    }

    public void getRechargeAddress(int cid) {
        rechargeAddressView.showLoading();
        TreeMap<String, String> params = new TreeMap<>();
        params.put("deviceType", "android");
        params.put("language", SpUtil.getString(SpUtil.LANGUAGE,"zh-cn"));

        params.put("cid", String.valueOf(cid));
        
        RequestBuilder.execute(RequestBuilder.getNetService(true).getaddress(MD5Util.getSignValue(params), params),
                "", new MyCallback<WalletAddressBean>() {
            @Override
            public void onSuccess(WalletAddressBean result) {
                super.onSuccess(result);
                rechargeAddressView.getAddressSuccess(result);
            }

            @Override
            public void onEmpty(WalletAddressBean result) {
                super.onEmpty(result);

            }

            @Override
            public void onBusinessFailure(Exception e) {
                super.onBusinessFailure(e);
                rechargeAddressView.showError(e.getMessage());
            }

            @Override
            public void onFinalResponse(boolean isSuccess) {
                super.onFinalResponse(isSuccess);
                rechargeAddressView.hideLoading();
            }
        });
    }
}
