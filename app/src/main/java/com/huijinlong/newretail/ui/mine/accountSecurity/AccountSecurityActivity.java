package com.huijinlong.newretail.ui.mine.accountSecurity;

import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Switch;
import android.widget.TextView;

import com.huijinlong.newretail.R;
import com.huijinlong.newretail.base.BaseActivity;
import com.huijinlong.newretail.bean.SecurityInfoBean;
import com.huijinlong.newretail.constant.Constant;
import com.huijinlong.newretail.ui.login.CreateGestureActivity;
import com.huijinlong.newretail.ui.mine.accountSecurity.bindemail.BindEmailActivity;
import com.huijinlong.newretail.ui.mine.accountSecurity.bindphone.BindPhoneActivity;
import com.huijinlong.newretail.ui.mine.accountSecurity.binggooleauthenticator.BingGoogleAuthenticatorActivity;
import com.huijinlong.newretail.ui.mine.accountSecurity.editpassword.EditPasswordActivity;
import com.huijinlong.newretail.ui.mine.accountSecurity.paypassword.PayPasswordActivity;
import com.huijinlong.newretail.util.FontUtil;
import com.huijinlong.newretail.util.SpUtil;
import com.huijinlong.newretail.util.ToastUtils;
import com.huijinlong.newretail.util.VerifySecurityUtil;
import com.weigan.loopview.LoopView;
import com.weigan.loopview.OnItemSelectedListener;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by penghuaijin on 2018/9/21.
 */

public class AccountSecurityActivity extends BaseActivity<SecurityInfoView, SecurityInfoPresent> implements SecurityInfoView {
    @BindView(R.id.tv_need_gesture_password_arrow)
    TextView tvNeedGesturePasswordArrow;
    @BindView(R.id.tv_edit_gesture_password_arrow)
    TextView tvEditGesturePasswordArrow;
    @BindView(R.id.tv_edit_password_arrow)
    TextView tvEditPasswordArrow;
    @BindView(R.id.tv_phone_ic)
    TextView tvPhoneIc;
    @BindView(R.id.tv_bind_phone)
    TextView tvBindPhone;
    @BindView(R.id.tv_phone_arrow)
    TextView tvPhoneArrow;
    @BindView(R.id.tv_emile_ic)
    TextView tvEmileIc;
    @BindView(R.id.tv_bind_email)
    TextView tvBindEmail;
    @BindView(R.id.tv_emile_arrow)
    TextView tvEmileArrow;
    @BindView(R.id.tv_google_ic)
    TextView tvGoogleIc;
    @BindView(R.id.tv_google)
    TextView tvGoogle;
    @BindView(R.id.tv_google_arrow)
    TextView tvGoogleArrow;
    @BindView(R.id.tv_pay)
    TextView tvPay;
    @BindView(R.id.tv_pay_arrow)
    TextView tvPayArrow;
    @BindView(R.id.sw_gesture)
    Switch swGesture;
    @BindView(R.id.tv_need_gesture_password)
    TextView tvNeedGesturePassword;
    @BindView(R.id.ll_need_gesture_password)
    LinearLayout llNeedGesturePassword;
    @BindView(R.id.ll_edit_gesture_password)
    LinearLayout llEditGesturePassword;
    @BindView(R.id.ll_finger_password)
    LinearLayout llFingerPassword;
    @BindView(R.id.sw_finger)
    Switch swFinger;
    private ArrayList<Integer> fontTextId;
    private SecurityInfoBean.DataBean.SecurityBean securityBean;
    private String type;
    private long interval_time = 2 * 60 * 1000;
    private PopupWindow alarmWindow;
    private LinearLayout popupWindowView;

//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
////        setContentView(R.layout.activity_account_security);
////        initializeActionToolBar();
////        initView();
//    }

    @Override
    public SecurityInfoPresent initPresenter() {
        return new SecurityInfoPresent(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.getSecurityInfo();
    }

    @Override
    protected void initView() {
        ButterKnife.bind(this);

        setToolbarTitle(getResources().getString(R.string.zhaq));

        fontTextId = new ArrayList<>();
        fontTextId.add(R.id.tv_google_arrow);
        fontTextId.add(R.id.tv_google_ic);
        fontTextId.add(R.id.tv_emile_arrow);
        fontTextId.add(R.id.tv_emile_ic);
        fontTextId.add(R.id.tv_phone_arrow);
        fontTextId.add(R.id.tv_phone_ic);
        fontTextId.add(R.id.tv_edit_gesture_password_arrow);
        fontTextId.add(R.id.tv_need_gesture_password_arrow);
        fontTextId.add(R.id.tv_edit_password_arrow);
        fontTextId.add(R.id.tv_pay_arrow);
        initFont();

        swGesture.setChecked(SpUtil.getBoolean(SpUtil.LOGINGESTURE, true));
        swFinger.setChecked(SpUtil.getBoolean(SpUtil.FINGER, true));

        if (SpUtil.getBoolean(SpUtil.LOGINGESTURE, true)) {
            llEditGesturePassword.setVisibility(View.VISIBLE);
            llFingerPassword.setVisibility(View.VISIBLE);
            llNeedGesturePassword.setVisibility(View.VISIBLE);
            findViewById(R.id.line1).setVisibility(View.VISIBLE);
            findViewById(R.id.line2).setVisibility(View.VISIBLE);
            findViewById(R.id.line3).setVisibility(View.VISIBLE);
        } else {
            llEditGesturePassword.setVisibility(View.GONE);
            llFingerPassword.setVisibility(View.GONE);
            llNeedGesturePassword.setVisibility(View.GONE);
            findViewById(R.id.line1).setVisibility(View.GONE);
            findViewById(R.id.line2).setVisibility(View.GONE);
            findViewById(R.id.line3).setVisibility(View.GONE);

        }

        swGesture.setOnCheckedChangeListener((buttonView, isChecked) -> {

            if (isChecked) {
                llEditGesturePassword.setVisibility(View.VISIBLE);
                llFingerPassword.setVisibility(View.VISIBLE);
                llNeedGesturePassword.setVisibility(View.VISIBLE);
                findViewById(R.id.line1).setVisibility(View.VISIBLE);
                findViewById(R.id.line2).setVisibility(View.VISIBLE);
                findViewById(R.id.line3).setVisibility(View.VISIBLE);
            } else {
                llEditGesturePassword.setVisibility(View.GONE);
                llFingerPassword.setVisibility(View.GONE);
                llNeedGesturePassword.setVisibility(View.GONE);
                findViewById(R.id.line1).setVisibility(View.GONE);
                findViewById(R.id.line2).setVisibility(View.GONE);
                findViewById(R.id.line3).setVisibility(View.GONE);
                SpUtil.putBoolean(SpUtil.FINGER, false);

            }
            SpUtil.putBoolean(SpUtil.LOGINGESTURE, isChecked);
        });
        swFinger.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SpUtil.putBoolean(SpUtil.FINGER, isChecked);

            }
        });
        initWindow();
//        mPresenter.getSecurityInfo();
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_account_security;
    }


    private void initFont() {
        for (int i = 0; i < fontTextId.size(); i++) {
            TextView textView = findViewById(fontTextId.get(i));
            FontUtil.injectFont(textView);
        }
    }

//    private void initializeActionToolBar() {
//        Toolbar mToolbar = findViewById(R.id.toolbar);
//        TextView tvTitle = findViewById(R.id.toolbar_title);
//        tvTitle.setText("账户安全");
//        initToolBar(mToolbar, true, "");
//    }

    @OnClick({R.id.tv_edit_password_arrow, R.id.tv_bind_phone, R.id.tv_bind_email, R.id.tv_google, R.id.tv_pay, R.id.tv_edit_gesture_password_arrow, R.id.tv_phone_arrow, R.id.tv_emile_arrow, R.id.tv_google_arrow, R.id.ll_need_gesture_password})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_edit_password_arrow:
                startToActivity(this, EditPasswordActivity.class);
                break;

            case R.id.tv_bind_phone:
                Intent intent1 = new Intent(this, BindPhoneActivity.class);
                if (securityBean.getMobile().getTextBind().equals("绑定")) {
                    intent1.putExtra("action", Constant.BIND_MOBILE);
                } else {
                    intent1.putExtra("action", Constant.REBIND_MOBILE);
                }
                startActivity(intent1);
                break;

            case R.id.tv_phone_arrow:
                VerifySecurityUtil verifySecurityUtil = VerifySecurityUtil.getInstance();
                verifySecurityUtil.init(this,
                        securityBean.getMobile().getWarnVerify() == 0 ? Constant.VERIFY_MOBILE_OFF : Constant.VERIFY_MOBILE_ON,
                        1 - (securityBean.getMobile().getWarnVerify()),
                        1 - (securityBean.getEmail().getWarnVerify()),
                        1 - (securityBean.getGoogleAuthenticator().getWarnVerify()));

                verifySecurityUtil.setListener(new VerifySecurityUtil.VerifySecurityUtilListener() {
                    @Override
                    public void onVerifySecuritySuccess() {
                        ToastUtils.toast(getString(R.string.success));
                        mPresenter.getSecurityInfo();
                    }

                    @Override
                    public void onVerifySecurityFailed() {
                        ToastUtils.toast(getString(R.string.faild));


                    }

                    @Override
                    public void onCancelDialog() {


                    }
                });
                break;
            case R.id.tv_bind_email:
                Intent intent2 = new Intent(this, BindEmailActivity.class);
                if (securityBean.getMobile().getTextBind().equals("绑定")) {
                    intent2.putExtra("action", Constant.BIND_EMAIL);
                } else {
                    intent2.putExtra("action", Constant.REBIND_EMAIL);
                }
                startActivity(intent2);
                break;
            case R.id.tv_emile_arrow:
                VerifySecurityUtil verifySecurityUtil_email = VerifySecurityUtil.getInstance();
                verifySecurityUtil_email.init(this,
                        securityBean.getEmail().getWarnVerify() == 0 ? Constant.VERIFY_EMAIL_OFF : Constant.VERIFY_EMAIL_ON,
                        1 - (securityBean.getMobile().getWarnVerify()),
                        1 - (securityBean.getEmail().getWarnVerify()),
                        1 - (securityBean.getGoogleAuthenticator().getWarnVerify()));

                verifySecurityUtil_email.setListener(new VerifySecurityUtil.VerifySecurityUtilListener() {
                    @Override
                    public void onVerifySecuritySuccess() {
                        ToastUtils.toast(getString(R.string.success));
                        mPresenter.getSecurityInfo();
                    }

                    @Override
                    public void onVerifySecurityFailed() {
                        ToastUtils.toast(getString(R.string.faild));


                    }

                    @Override
                    public void onCancelDialog() {


                    }
                });
                break;
            case R.id.tv_google:
                Intent intent3 = new Intent(this, BingGoogleAuthenticatorActivity.class);
                if (securityBean.getMobile().getTextBind().equals("绑定")) {
                    intent3.putExtra("action", Constant.BIND_GOOGLE_AUTHENTICATOR);
                } else {
                    intent3.putExtra("action", Constant.REBIND_GOOGLE_AUTHENTICATOR);
                }
                startActivity(intent3);
                break;
            case R.id.tv_google_arrow:
                VerifySecurityUtil verifySecurityUtil_google = VerifySecurityUtil.getInstance();
                verifySecurityUtil_google.init(this,
                        securityBean.getGoogleAuthenticator().getWarnVerify() == 0 ? Constant.VERIFY_GOOGLE_AUTHENTICATOR_OFF : Constant.VERIFY_GOOGLE_AUTHENTICATOR_ON,
                        1 - (securityBean.getMobile().getWarnVerify()),
                        1 - (securityBean.getEmail().getWarnVerify()),
                        1 - (securityBean.getGoogleAuthenticator().getWarnVerify()));

                verifySecurityUtil_google.setListener(new VerifySecurityUtil.VerifySecurityUtilListener() {
                    @Override
                    public void onVerifySecuritySuccess() {
                        ToastUtils.toast(getString(R.string.success));
                        mPresenter.getSecurityInfo();
                    }

                    @Override
                    public void onVerifySecurityFailed() {
                        ToastUtils.toast(getString(R.string.faild));


                    }

                    @Override
                    public void onCancelDialog() {


                    }
                });
                break;
            case R.id.tv_pay:
                startToActivity(this, PayPasswordActivity.class);
                break;
            case R.id.ll_need_gesture_password:
                showWindow(view);
                break;
            case R.id.tv_edit_gesture_password_arrow:
                Intent intent = new Intent(this, CreateGestureActivity.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void getSecurity(SecurityInfoBean securityInfoBean) {

        securityBean = securityInfoBean.getData().getSecurity();

        tvBindPhone.setText(securityBean.getMobile().getTextBind());
        tvPhoneArrow.setText(securityBean.getMobile().getTextVerify());
        tvPhoneArrow.setVisibility(securityBean.getMobile().getTextVerifyEnable() == 1 ? View.VISIBLE : View.GONE);
        tvPhoneIc.setText(securityBean.getMobile().getWarnVerify() == 1 ? getResources().getString(R.string.xe6bf) :
                getResources().getString(R.string.xe613));
        tvPhoneIc.setTextColor(securityBean.getMobile().getWarnVerify() == 0 ? getResources().getColor(R.color.c_57b) : getResources().getColor(R.color.c_ff5));
        tvPhoneIc.setTextSize(TypedValue.COMPLEX_UNIT_SP, securityBean.getMobile().getWarnVerify() == 0 ? 15 : 17);


        tvBindEmail.setText(securityBean.getEmail().getTextBind());
        tvEmileArrow.setText(securityBean.getEmail().getTextVerify());
        tvEmileArrow.setVisibility(securityBean.getEmail().getTextVerifyEnable() == 1 ? View.VISIBLE : View.GONE);

        tvEmileIc.setText(securityBean.getEmail().getWarnVerify() == 1 ? getResources().getString(R.string.xe6bf) :
                getResources().getString(R.string.xe613));
        tvEmileIc.setTextColor(securityBean.getEmail().getWarnVerify() == 0 ? getResources().getColor(R.color.c_57b) : getResources().getColor(R.color.c_ff5));
        tvEmileIc.setTextSize(TypedValue.COMPLEX_UNIT_SP, securityBean.getEmail().getWarnVerify() == 0 ? 15 : 17);

        tvGoogle.setText(securityBean.getGoogleAuthenticator().getTextBind());
        tvGoogleArrow.setText(securityBean.getGoogleAuthenticator().getTextVerify());
        tvGoogleArrow.setVisibility(securityBean.getGoogleAuthenticator().getTextVerifyEnable() == 1 ? View.VISIBLE : View.GONE);
        tvGoogleIc.setText(securityBean.getGoogleAuthenticator().getWarnVerify() == 1 ? getResources().getString(R.string.xe6bf) :
                getResources().getString(R.string.xe613));
        tvGoogleIc.setTextColor(securityBean.getGoogleAuthenticator().getWarnVerify() == 0 ? getResources().getColor(R.color.c_57b) : getResources().getColor(R.color.c_ff5));
        tvGoogleIc.setTextSize(TypedValue.COMPLEX_UNIT_SP, securityBean.getGoogleAuthenticator().getWarnVerify() == 0 ? 15 : 17);


    }

    @Override
    public void showLoading() {
        mLoadingDialog.show();
    }

    @Override
    public void hideLoading() {
        mLoadingDialog.dismiss();
    }

    @Override
    public void showError(String error) {
        ToastUtils.toast(error);
    }


    public void initWindow() {
        View nemuView = LayoutInflater.from(this).inflate(R.layout.popup_window_view, null, false);
        popupWindowView = (LinearLayout) nemuView.findViewById(R.id.ll_pop_item);

        alarmWindow = new PopupWindow(nemuView, LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
//        alarmWindow.setAnimationStyle(R.style.popAnimationPreview);
        alarmWindow.setBackgroundDrawable(new BitmapDrawable());
        alarmWindow.setOutsideTouchable(false);
        alarmWindow.setFocusable(true);
        alarmWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
//                Drawable rightDrawable = getResources().getDrawable(R.mipmap.dropdown);// 获取res下的图片drawable
//                rightDrawable.setBounds(0, 0, rightDrawable.getMinimumWidth(), rightDrawable.getMinimumHeight());// 一定要设置setBounds();
//                tvNeedGesturePassword.setBackground(rightDrawable);
                alarmWindow.dismiss();
            }
        });
    }

    public void showWindow(View v) {

        if (!alarmWindow.isShowing()) {
            popupWindowView.removeAllViews();
            View popItem = LayoutInflater.from(this).inflate(R.layout.dialog_wheelview, null, false);
            LoopView loopView = (LoopView) popItem.findViewById(R.id.loopView);
            TextView tv_cancle = (TextView) popItem.findViewById(R.id.tv_cancle);
            TextView tv_sure = (TextView) popItem.findViewById(R.id.tv_sure);

            ArrayList<String> list = new ArrayList<>();
            list.clear();
            String[] array = getResources().getStringArray(R.array.interval_time);
            Collections.addAll(list, array);
            type = list.get(0);

            loopView.setNotLoop();
            tv_cancle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alarmWindow.dismiss();

                }
            });
            tv_sure.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SpUtil.putLong(SpUtil.INTERVAL_TIME, interval_time);
                    tvNeedGesturePassword.setText(type);
                    alarmWindow.dismiss();

                }
            });

            // 滚动监听
            loopView.setListener(new OnItemSelectedListener() {
                @Override
                public void onItemSelected(int index) {
                    type = String.valueOf(list.get(index));
                    switch (index) {
                        case 0:
                            interval_time = 2 * 60 * 1000;
                            break;

                        case 1:
                            interval_time = 5 * 60 * 1000;
                            break;

                        case 2:
                            interval_time = 10 * 60 * 1000;
                            break;

                        case 3:
                            interval_time = 30 * 60 * 1000;
                            break;
                    }
                }
            });
            // 设置原始数据
            loopView.setItems(list);
            loopView.setTextSize(18);
            // loopView.setDividerColor(R.color.c_2f3);
            popupWindowView.addView(popItem);
            alarmWindow.showAtLocation(v, Gravity.BOTTOM, 0, 0);
        } else {
            alarmWindow.dismiss();

        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
