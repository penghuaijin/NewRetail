package com.huijinlong.newretail.ui.mine.apimanger;

import com.huijinlong.newretail.base.BaseView;
import com.huijinlong.newretail.requestbean.EditApiBean;

/**
 * @author zsg
 * @date 2018/10/19 15:52
 * @Version 1.0
 * @Description: ()
 */
public interface ApiMangerView extends BaseView{
    void getEditApiSuccess(EditApiBean editApiBean);
}
