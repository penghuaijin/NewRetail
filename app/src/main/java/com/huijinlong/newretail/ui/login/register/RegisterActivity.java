package com.huijinlong.newretail.ui.login.register;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.text.method.TransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.huijinlong.newretail.MainActivity;
import com.huijinlong.newretail.R;
import com.huijinlong.newretail.base.BaseActivity;
import com.huijinlong.newretail.base.BaseActivityBefor;
import com.huijinlong.newretail.requestbean.LoginBean;
import com.huijinlong.newretail.ui.login.LoginActivity;
import com.huijinlong.newretail.ui.mine.accountSecurity.bindphone.countries.CountriesActivity;
import com.huijinlong.newretail.util.FontUtil;
import com.huijinlong.newretail.util.LogUtils;
import com.huijinlong.newretail.util.NRGeeTestUtils;
import com.huijinlong.newretail.util.SpUtil;
import com.huijinlong.newretail.util.TimeCountUtils;
import com.huijinlong.newretail.util.ToastUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterActivity extends BaseActivityBefor<RegisterView, RegisterPresenter> implements RegisterView {

    @BindView(R.id.tv_login_back)
    TextView tvLoginBack;
    @BindView(R.id.tv_have_account)
    TextView tvHaveAccount;
    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.tv_login_account)
    TextView tvLoginAccount;
    @BindView(R.id.et_login_account)
    EditText etLoginAccount;
    @BindView(R.id.iv_login_account_clear)
    ImageView ivLoginAccountClear;
    @BindView(R.id.tv_login_email_num)
    TextView tvLoginEmailNum;
    @BindView(R.id.et_login_num)
    EditText etLoginNum;
    @BindView(R.id.tv_send_verification)
    TextView tvSendVerification;
    @BindView(R.id.tv_login_password)
    TextView tvLoginPassword;
    @BindView(R.id.et_login_password)
    EditText etLoginPassword;
    @BindView(R.id.iv_login_eye)
    ImageView ivLoginEye;
    @BindView(R.id.btn_register)
    Button btnRegister;
    @BindView(R.id.tv_login_from_type)
    TextView tvLoginFromType;
    @BindView(R.id.iv_arrow)
    ImageView ivArrow;
    private TimeCountUtils timeCountUtils;

    private boolean isHide = true;
    private boolean login_type = false;
    private NRGeeTestUtils utils;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        initializeActionToolBar();
        initView();
//        utils = new NRGeeTestUtils(this);
//        utils.setListener(map -> {
//
//
//            if (login_type) {
//                mPresenter.getPhoneCaptcha(etLoginAccount.getText().toString(), map);
//            } else {
//                mPresenter.getEmailCaptcha(etLoginAccount.getText().toString(), map);
//            }
//        });

    }

    @Override
    public RegisterPresenter initPresenter() {
        return new RegisterPresenter(this);
    }

    protected void initView() {

//        setToolbarVisibility(false);

        utils = new NRGeeTestUtils(this);
        utils.setListener(map -> {


            if (login_type) {
                mPresenter.getPhoneCaptcha(etLoginAccount.getText().toString(), map);
            } else {
                mPresenter.getEmailCaptcha(etLoginAccount.getText().toString(), map);
            }
        });

        FontUtil.injectFont(tvLoginBack);
        timeCountUtils = new TimeCountUtils(60000, 1000, tvSendVerification);
        etLoginAccount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (TextUtils.isEmpty(s.toString())){
                    ivLoginAccountClear.setVisibility(View.GONE);
                }else {
                    ivLoginAccountClear.setVisibility(View.VISIBLE);

                }
            }
        });
    }

//    @Override
//    protected int getContentView() {
//        return R.layout.activity_register;
//    }

    private void initializeActionToolBar() {
        Toolbar mToolbar = findViewById(R.id.toolbar);
        mToolbar.setVisibility(View.GONE);
        initToolBar(mToolbar, false, "");
    }

    @Override
    public void registerSuccess(LoginBean loginBean) {
        utils.setSuccess2(true);
        utils.getGt3GeetestUtils().cancelUtils();
        LogUtils.e("getAccessToken----------" + loginBean.getData().getAccessToken());
        SpUtil.putString(SpUtil.AUTHORIZATION, loginBean.getData().getTokenType() + " " + loginBean.getData().getAccessToken());
        SpUtil.putString(SpUtil.NICK_NAME, loginBean.getData().getNickName());
        SpUtil.putString(SpUtil.MOBILE, loginBean.getData().getMobile());
        SpUtil.putString(SpUtil.EMAIL, loginBean.getData().getEmail());
        SpUtil.putString(SpUtil.USER_ID, loginBean.getData().getUserId());
        jump2Activity(MainActivity.class);
        finish();


    }

    @Override
    public void getCaptchaSuccess() {
        timeCountUtils.start();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        timeCountUtils.cancel();
    }

    @Override
    protected boolean translucentStatusBar() {
        return true;
    }

    @Override
    public void showLoading() {
        mLoadingDialog.show();
    }

    @Override
    public void hideLoading() {
        mLoadingDialog.dismiss();
    }

    @Override
    public void showError(String error) {
        ToastUtils.toast(error);
        utils.setSuccess2(false);
    }

    @OnClick({R.id.tv_login_back, R.id.tv_have_account, R.id.iv_login_account_clear, R.id.tv_send_verification, R.id.iv_login_eye, R.id.btn_register, R.id.tv_login_from_type, R.id.tv_login_account})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_login_back:
                finish();

                break;
            case R.id.tv_have_account:
                jump2Activity(LoginActivity.class);
                finish();
                break;
            case R.id.iv_login_account_clear:
                etLoginAccount.setText("");

                break;
            case R.id.tv_send_verification:
                utils.startTest();
                break;
            case R.id.iv_login_eye:
                if (!isHide) {
                    ivLoginEye.setImageResource(R.mipmap.ic_eye_close);
                    HideReturnsTransformationMethod method1 = HideReturnsTransformationMethod.getInstance();
                    etLoginPassword.setTransformationMethod(method1);
                    isHide = true;
                } else {
                    ivLoginEye.setImageResource(R.mipmap.ic_eye_open);
                    TransformationMethod method = PasswordTransformationMethod.getInstance();
                    etLoginPassword.setTransformationMethod(method);
                    isHide = false;
                }
                int index = etLoginPassword.getText().toString().length();
                etLoginPassword.setSelection(index);
                break;
            case R.id.btn_register:
                mPresenter.doRegister(etLoginAccount.getText().toString(), etLoginPassword.getText().toString(), login_type, etLoginNum.getText().toString());

                break;
            case R.id.tv_login_from_type:
                login_type = !login_type;
                if (login_type) {
                    tvLoginAccount.setText(getResources().getString(R.string.yx));
                    tvLoginEmailNum.setText(getResources().getString(R.string.yxyzm));
                    tvLoginFromType.setText(getResources().getString(R.string.sysjzc));
                    ivArrow.setVisibility(View.INVISIBLE);
                } else {
                    ivArrow.setVisibility(View.VISIBLE);
                    tvLoginAccount.setText(getResources().getString(R.string.sj));
                    tvLoginEmailNum.setText(getResources().getString(R.string.sjyzm));
                    tvLoginFromType.setText(getResources().getString(R.string.syyxzc));
                }

                break;
            case R.id.tv_login_account:
                if (!login_type) {
                    Intent intent = new Intent(this, CountriesActivity.class);
                    startActivity(intent);
                }
                break;
        }
    }
}
