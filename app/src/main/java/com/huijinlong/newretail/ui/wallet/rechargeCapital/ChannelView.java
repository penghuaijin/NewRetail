package com.huijinlong.newretail.ui.wallet.rechargeCapital;

import com.huijinlong.newretail.base.BaseView;
import com.huijinlong.newretail.bean.WthdrawChannelBean;
import com.huijinlong.newretail.requestbean.WalletAddressBean;

/**
 * @author zsg
 * @date 2018/10/19 15:52
 * @Version 1.0
 * @Description: ()
 */
public interface ChannelView extends BaseView{
    void getChannelSuccess(WthdrawChannelBean wthdrawChannelBean);
}
