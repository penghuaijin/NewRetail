package com.huijinlong.newretail.ui.wallet.addressmanage;

import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.huijinlong.newretail.R;
import com.huijinlong.newretail.base.BaseActivity;
import com.huijinlong.newretail.base.BaseBean;
import com.huijinlong.newretail.bean.CurrenciesBean;
import com.huijinlong.newretail.bean.DeleteAdressBean;
import com.huijinlong.newretail.bean.WthdrawChannelBean;
import com.huijinlong.newretail.ui.wallet.addressmanage.withdraws.WithDrawsActivity;
import com.huijinlong.newretail.ui.wallet.addressmanage.withdraws.WithDrawsGetCodePresenter;
import com.huijinlong.newretail.ui.wallet.addressmanage.withdraws.WithDrawsGetCodeView;
import com.huijinlong.newretail.ui.wallet.rechargeCapital.ChannelPresent;
import com.huijinlong.newretail.ui.wallet.rechargeCapital.ChannelView;
import com.huijinlong.newretail.util.FontUtil;
import com.huijinlong.newretail.util.SpUtil;
import com.huijinlong.newretail.util.ToastUtils;
import com.huijinlong.newretail.util.VerifySecurityUtil;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author zsg
 * @date 2018/10/19 14:48
 * @Version 1.0
 * @Description: (地址管理)
 */
public class AddressManageActivity extends BaseActivity<CurrenciesView, CurrenciesPresenter> implements CurrenciesView,ChannelView, View.OnClickListener
        , WithDrawsGetCodeView {

    @BindView(R.id.tv_select)
    TextView tvSelect;
    @BindView(R.id.tv_currency)
    TextView tvCurrency;
    @BindView(R.id.re_spinner)
    RelativeLayout reSpinner;
    @BindView(R.id.nice_spinner)
    TextView niceSpinner;
    @BindView(R.id.img)
    ImageView img;
    @BindView(R.id.bt_add)
    Button btAdd;
    @BindView(R.id.ed_adresss)
    EditText edAdresss;
    @BindView(R.id.ed_remark)
    EditText edRemark;
    @BindView(R.id.nice_spinners)
    TextView niceSpinners;
    @BindView(R.id.imgs)
    ImageView imgs;
    @BindView(R.id.re_spinners)
    RelativeLayout reSpinners;

    private PopupWindow alarmWindow;
    private LinearLayout popupWindowView;
    private CurrenciesBean bean;
    private int mycid = 0;
    private MyCount mc;
    private static boolean issuccess;//验证码是否发送成功
    private WithDrawsGetCodePresenter drawsGetCodePresenter;
    private EditText edcode;
    private ChannelPresent channelPresent;
    private WthdrawChannelBean wthdrawChannelBean;
    private int channelcid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        FontUtil.injectFont(tvSelect);
        initWindow();
        mPresenter.getcurrencies(0);
        drawsGetCodePresenter = new WithDrawsGetCodePresenter(this);

        tvSelect.setOnClickListener(this);
        niceSpinner.setOnClickListener(this);
        niceSpinners.setOnClickListener(this);
        btAdd.setOnClickListener(this);

        channelPresent=new ChannelPresent(this);
        channelPresent.getChannel();
    }

    @Override
    public CurrenciesPresenter initPresenter() {
        return new CurrenciesPresenter(this);
    }

    @Override
    protected void initView() {
        setToolbarTitle(getString(R.string.dzgl));
        setToolbarRightBtn(true, false, getString(R.string.dzjl), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startToActivity(AddressManageActivity.this, WithDrawsActivity.class);

            }
        });
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_wallet_address;
    }

    public void initWindow() {
        View nemuView = LayoutInflater.from(this).inflate(R.layout.popup_window_view, null, false);
        popupWindowView = (LinearLayout) nemuView.findViewById(R.id.ll_pop_item);

        alarmWindow = new PopupWindow(nemuView, LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
//        alarmWindow.setAnimationStyle(R.style.popAnimationPreview);
        alarmWindow.setBackgroundDrawable(new BitmapDrawable());
        alarmWindow.setOutsideTouchable(false);
        alarmWindow.setFocusable(true);
        alarmWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                Drawable rightDrawable = getResources().getDrawable(R.mipmap.dropdown);// 获取res下的图片drawable
                rightDrawable.setBounds(0, 0, rightDrawable.getMinimumWidth(), rightDrawable.getMinimumHeight());// 一定要设置setBounds();
                img.setBackground(rightDrawable);
                imgs.setBackground(rightDrawable);
                alarmWindow.dismiss();
            }
        });
    }

    public void showWindows(View v, WthdrawChannelBean wthdrawChannelBeans) {
        if (!alarmWindow.isShowing()) {
            popupWindowView.removeAllViews();
            for (WthdrawChannelBean.DataBean  currenciesBean : wthdrawChannelBeans.getData()) {
                View popItem = LayoutInflater.from(this).inflate(R.layout.popup_window_item, null, false);
                TextView tvDateName = (TextView) popItem.findViewById(R.id.tv_user_name);

                tvDateName.setText(currenciesBean.getName());

                tvDateName.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        niceSpinners.setText(currenciesBean.getName());
                        channelcid=currenciesBean.getId();
                        alarmWindow.dismiss();
                    }
                });
                popupWindowView.addView(popItem);
            }
            alarmWindow.showAsDropDown(v, LinearLayout.LayoutParams.MATCH_PARENT, 1);
        } else {
            alarmWindow.dismiss();
        }
    }

    public void showWindow(View v, CurrenciesBean currenciesBeans) {
        if (!alarmWindow.isShowing()) {
            popupWindowView.removeAllViews();
            for (CurrenciesBean.DataBean currenciesBean : currenciesBeans.getData()) {
                View popItem = LayoutInflater.from(this).inflate(R.layout.popup_window_item, null, false);
                TextView tvDateName = (TextView) popItem.findViewById(R.id.tv_user_name);

                tvDateName.setText(currenciesBean.getCurrency());

                tvDateName.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        niceSpinner.setText(currenciesBean.getCurrency() + "");
                        mycid = currenciesBean.getCid();
                        alarmWindow.dismiss();
                    }
                });
                popupWindowView.addView(popItem);
            }
            alarmWindow.showAsDropDown(v, LinearLayout.LayoutParams.MATCH_PARENT, 1);
        } else {
            alarmWindow.dismiss();
        }
    }

    public void showCodeWindow(View v) {
        if (!alarmWindow.isShowing()) {
            popupWindowView.removeAllViews();
//            for (CurrenciesBean.DataBean currenciesBean : currenciesBeans.getData()) {
            View popItem = LayoutInflater.from(this).inflate(R.layout.poppup_validate, null, false);
            TextView tvphone = (TextView) popItem.findViewById(R.id.tv_phonenum);
            TextView tvclose = (TextView) popItem.findViewById(R.id.tv_close);
            TextView tvcancle = (TextView) popItem.findViewById(R.id.tv_cancle);
            TextView tvsure = (TextView) popItem.findViewById(R.id.tv_sure);
            TextView tvsentcode = (TextView) popItem.findViewById(R.id.tv_sentcode);
            edcode = (EditText) popItem.findViewById(R.id.ed_code);

            mc = new MyCount(60000, 1000, tvsentcode);

            tvphone.setText(SpUtil.getString("mobile", "").replaceAll("(\\d{3})\\d{4}(\\d{4})", "$1****$2"));

            tvclose.setOnClickListener(this);
            tvcancle.setOnClickListener(this);
            tvsure.setOnClickListener(this);
            tvsentcode.setOnClickListener(this);
            tvsentcode.setOnClickListener(this);

            popupWindowView.addView(popItem);

            alarmWindow.showAtLocation(v, Gravity.CENTER_VERTICAL, 0, 0);
            //alarmWindow.showAsDropDown(v, LinearLayout.LayoutParams.MATCH_PARENT, 1);

        } else {
            alarmWindow.dismiss();
        }
    }

    @Override
    public void getCurrencise(CurrenciesBean currenciesBean) {
        bean = currenciesBean;
        mycid = currenciesBean.getData().get(0).getCid();
    }

    @Override
    public void getEditAddressWithdraw(EditAddressWithdrawBean editAddressWithdrawBean) {

        //   showCodeWindow(niceSpinner);
        mLoadingDialog.show();
        VerifySecurityUtil verifySecurityUtil = VerifySecurityUtil.getInstance();
        verifySecurityUtil.init(this, "createAddress",
                editAddressWithdrawBean.getData().getVerifyMobile(),
                editAddressWithdrawBean.getData().getVerifyEmail(),
                editAddressWithdrawBean.getData().getVerifyGoogleAuthenticator());

        verifySecurityUtil.setListener(new VerifySecurityUtil.VerifySecurityUtilListener() {
            @Override
            public void onVerifySecuritySuccess() {
                mLoadingDialog.dismiss();
                ToastUtils.toast("添加成功");
                finish();
            }

            @Override
            public void onVerifySecurityFailed() {
                mLoadingDialog.dismiss();
                ToastUtils.toast("添加失败");
            }

            @Override
            public void onCancelDialog() {

            }
        });


    }

    @Override
    public void showLoading() {
        mLoadingDialog.show();
    }

    @Override
    public void hideLoading() {
        mLoadingDialog.dismiss();
    }

    @Override
    public void showError(String error) {
        ToastUtils.toast(error);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_select:
                break;
            case R.id.toolbar_func:
                startActivity(new Intent(this, WithDrawsActivity.class));
                break;
            case R.id.nice_spinner:
                if (bean != null) {
                    Drawable rightDrawable = getResources().getDrawable(R.mipmap.dropup);// 获取res下的图片drawable
                    rightDrawable.setBounds(0, 0, rightDrawable.getMinimumWidth(), rightDrawable.getMinimumHeight());// 一定要设置setBounds();
                    img.setBackground(rightDrawable);
                    showWindow(niceSpinner, bean);
                }
                break;
            case R.id.bt_add:
                if (mycid != 0 && !edAdresss.getText().toString().equals("") && !edRemark.getText().toString().equals("")) {
                    mPresenter.geteditaddress(mycid, edAdresss.getText().toString(), edRemark.getText().toString(), 1);//需更改：1为默认，通过withdrawChannel中得id获取
                } else
                    ToastUtils.toast("请完善信息");
                break;
            case R.id.tv_phonenum:
                alarmWindow.dismiss();
                break;
            case R.id.tv_close:
                alarmWindow.dismiss();
                break;
            case R.id.tv_cancle:
                alarmWindow.dismiss();
                break;
            case R.id.tv_sure:
                if (issuccess) {
                    drawsGetCodePresenter.verifySecurity(edcode.getText().toString(), "createAddress");
                }
                break;
            case R.id.tv_sentcode:
                drawsGetCodePresenter.getCode();
                break;
            case R.id.nice_spinners:
                if (wthdrawChannelBean != null) {
                    Drawable rightDrawable = getResources().getDrawable(R.mipmap.dropup);// 获取res下的图片drawable
                    rightDrawable.setBounds(0, 0, rightDrawable.getMinimumWidth(), rightDrawable.getMinimumHeight());// 一定要设置setBounds();
                    imgs.setBackground(rightDrawable);
                    showWindows(niceSpinners, wthdrawChannelBean);;
                }
                break;
        }
    }

    @Override
    public void getCodeSuccess(BaseBean baseBean) {
        if (baseBean.getStatus() == 200) {
            mc.start();
            issuccess = true;
        } else
            issuccess = false;
    }

    @Override
    public void getDelete(DeleteAdressBean deleteAdressBean) {

    }

    @Override
    public void getPhoneCapt(BaseBean baseBean) {
        if (baseBean.getStatus() == 200) {
            alarmWindow.dismiss();
            ToastUtils.toast("添加成功");
        }
    }

    @Override
    public void getEdit(EditAddressWithdrawBean bean) {

    }

    @Override
    public void getChannelSuccess(WthdrawChannelBean wthdrawChannelBeans) {
        wthdrawChannelBean=wthdrawChannelBeans;
    }

    class MyCount extends CountDownTimer {
        TextView tvs;

        public MyCount(long millisInFuture, long countDownInterval, TextView tv) {
            super(millisInFuture, countDownInterval);
            tvs = tv;
        }

        // 获取时间60秒 重新获取（60）
        @Override
        public void onFinish() {
            tvs.setText("重新获取");
            tvs.setClickable(true);
        }

        @Override
        public void onTick(long millisUntilFinished) {

            if (issuccess) {
                tvs.setText(" 重新获取(" + millisUntilFinished / 1000 + ")");
                tvs.setClickable(false);
            }

        }
    }
}

