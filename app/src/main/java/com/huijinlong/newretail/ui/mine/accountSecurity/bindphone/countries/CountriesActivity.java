package com.huijinlong.newretail.ui.mine.accountSecurity.bindphone.countries;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.com.huijinlong.greendao.DaoSession;
import com.com.huijinlong.greendao.GDCountriesBeanDao;
import com.huijinlong.newretail.R;
import com.huijinlong.newretail.base.BaseActivity;
import com.huijinlong.newretail.base.BaseApplication;
import com.huijinlong.newretail.bean.GDCountriesBean;
import com.huijinlong.newretail.requestbean.CountriesBean;
import com.huijinlong.newretail.util.FontUtil;
import com.huijinlong.newretail.util.SpUtil;
import com.huijinlong.newretail.util.ToastUtils;
import com.huijinlong.newretail.view.SimpleDividerDecoration;

import org.greenrobot.greendao.query.Query;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTextChanged;

/**
 * Created by penghuaijin on 2018/9/27.
 */

public class CountriesActivity extends BaseActivity<CountriesView, CountriesPresenter> implements CountriesView {

    @BindView(R.id.toolbar_cancel)
    TextView toolbarCancel;
    @BindView(R.id.tv_search)
    TextView tvSearch;
    @BindView(R.id.et_search)
    EditText etSearch;
    @BindView(R.id.ll_search)
    LinearLayout llSearch;
    @BindView(R.id.toolbar_spot_search)
    Toolbar toolbarSpotSearch;
    @BindView(R.id.rv_search_countries)
    RecyclerView rvSearchCountries;
    private GDCountriesBeanDao gdCountriesBeanDao;
    private CountriesAdapter mAdapter;
    private List<GDCountriesBean> list;
    private ArrayList<GDCountriesBean> searchList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initData();
    }

    @Override
    public CountriesPresenter initPresenter() {
        return new CountriesPresenter(this);
    }

    @Override
    protected void initView() {
        setToolbarVisibility(false);
        FontUtil.injectFont(tvSearch);



//        etSearch.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                if (TextUtils.isEmpty(s.toString())) {
//                    mAdapter.setData(list);
//                    mAdapter.notifyDataSetChanged();
//                } else {
////                    Query<GDCountriesBean> query = gdCountriesBeanDao.queryBuilder().whereOr(GDCountriesBeanDao.Properties.NameCn.like(s.toString().trim()), GDCountriesBeanDao.Properties.NameEn.like(s.toString().trim())).build();
//                    searchList.clear();
//                    for (int i = 0; i < list.size(); i++) {
//                        if (list.get(i).getNameCn().contains(s.toString().trim()) ||
//                                list.get(i).getNameEn().toLowerCase().contains(s.toString().trim().toLowerCase())
//                                ) {
//                            searchList.add(list.get(i));
//                        }
//                    }
//                    mAdapter.setData(searchList);
//                    mAdapter.notifyDataSetChanged();
//
//                }
//            }
//        });
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_countries_search;
    }

    private void initData() {

        rvSearchCountries.setLayoutManager(new LinearLayoutManager(this));
        rvSearchCountries.addItemDecoration(new SimpleDividerDecoration(this, 10, 10, getResources().getColor(R.color.c_687), getResources().getDimensionPixelSize(R.dimen.dp_0)));
        list = new ArrayList<>();
        mAdapter = new CountriesAdapter(this, list);
        rvSearchCountries.setAdapter(mAdapter);


        DaoSession daoSession = BaseApplication.getInstance().getDaoSession();
        gdCountriesBeanDao = daoSession.getGDCountriesBeanDao();
        Query<GDCountriesBean> gdCountriesBeanQuery = gdCountriesBeanDao.queryBuilder().orderAsc(GDCountriesBeanDao.Properties.NameEn).build();
        list = gdCountriesBeanQuery.list();
        if (list.size() == 0) {
//            CountriesPresenter mPresenter = new CountriesPresenter(this);
            mPresenter.getCountries();
        } else {
            mAdapter.setData(list);
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void getCountriesSuccess(CountriesBean bean) {

        for (int i = 0; i < bean.getData().size(); i++) {
            GDCountriesBean gdCountriesBean = new GDCountriesBean();
            gdCountriesBean.setCode(bean.getData().get(i).getCode());
            gdCountriesBean.setId(bean.getData().get(i).getId());
            gdCountriesBean.setNameCn(bean.getData().get(i).getNameCn());
            gdCountriesBean.setNameEn(bean.getData().get(i).getNameEn());
            gdCountriesBeanDao.insertOrReplace(gdCountriesBean);

            if (SpUtil.getString(SpUtil.NATIONALITY, "86").equals(bean.getData().get(i).getId() + "")) {
                SpUtil.putString(SpUtil.MOBILE_CODE, bean.getData().get(i).getCode());
            }
        }

        initData();

    }


//    private void initializeActionToolBar() {
//        Toolbar mToolbar = findViewById(R.id.toolbar);
//        TextView tvTitle = findViewById(R.id.toolbar_title);
//        tvTitle.setText("国家/地区");
//        initToolBar(mToolbar, true, "");
//    }

    @Override
    public void showLoading() {
        mLoadingDialog.show();
    }

    @Override
    public void hideLoading() {
        mLoadingDialog.dismiss();
    }

    @Override
    public void showError(String error) {
        ToastUtils.toast(error);
    }

    @OnClick(R.id.toolbar_cancel)
    public void onViewClicked() {
        finish();
    }

    @OnTextChanged(value = R.id.et_search, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    void afterPriceChange(Editable s) {
        if (TextUtils.isEmpty(s.toString())) {
            mAdapter.setData(list);
            mAdapter.notifyDataSetChanged();
        } else {
//                    Query<GDCountriesBean> query = gdCountriesBeanDao.queryBuilder().whereOr(GDCountriesBeanDao.Properties.NameCn.like(s.toString().trim()), GDCountriesBeanDao.Properties.NameEn.like(s.toString().trim())).build();
            searchList.clear();
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getNameCn().contains(s.toString().trim()) ||
                        list.get(i).getNameEn().toLowerCase().contains(s.toString().trim().toLowerCase())
                        ) {
                    searchList.add(list.get(i));
                }
            }
            mAdapter.setData(searchList);
            mAdapter.notifyDataSetChanged();

        }
    }
}
