package com.huijinlong.newretail.ui.wallet.exchange;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.huijinlong.newretail.R;
import com.huijinlong.newretail.base.BaseActivity;
import com.huijinlong.newretail.base.BasePresenter;
import com.huijinlong.newretail.bean.ExchangeDetailBean;
import com.huijinlong.newretail.util.ToastUtils;
import com.huijinlong.newretail.view.SimpleDividerDecoration;
import com.scwang.smartrefresh.header.MaterialHeader;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author zsg
 * @date 2018/11/20 11:54
 * @Version 1.0
 * @Description: (兑换记录)
 */
public class ExchangeDetailActivity extends BaseActivity<ExchangeDetailView, ExchangeDetailPresent> implements ExchangeDetailView {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.refresh_layout)
    SmartRefreshLayout refreshLayout;

    private ExchangeDetailAdapter exchangeDetailAdapter;
    private int page = 1;
    private List<ExchangeDetailBean.DataBean.ListBean> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
    }

    @Override
    public ExchangeDetailPresent initPresenter() {
        return new ExchangeDetailPresent(this);
    }

    @Override
    protected void initView() {
        setToolbarTitle("兑换记录");
        mPresenter.getwithdraws(page,10,"");

        exchangeDetailAdapter = new ExchangeDetailAdapter(list);

        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        exchangeDetailAdapter.bindToRecyclerView(recyclerView);

        refreshLayout.setRefreshHeader(new MaterialHeader(this));
        refreshLayout.setOnRefreshListener(refreshlayout -> {
            page = 1;
            mPresenter.getwithdraws(page,10,"");
            refreshLayout.setEnableLoadmore(true);

        });
        refreshLayout.setOnLoadmoreListener(refreshlayout -> {
            page+=1;
            mPresenter.getwithdraws(page,10,"");
        });

    }

    @Override
    protected int getContentView() {
        return R.layout.activity_exchangedetail;
    }

    @Override
    public void getexchangdetail(ExchangeDetailBean exchangeDetailBean) {
        if (page == 1) {
            list.clear();
            refreshLayout.finishRefresh();
        } else {
            refreshLayout.finishLoadmore();
        }
        list.addAll(exchangeDetailBean.getData().getList());

        if (list.size() == exchangeDetailBean.getData().getTotal()) {
            refreshLayout.setEnableLoadmore(false);
        }
        exchangeDetailAdapter.notifyDataSetChanged();
    }

    @Override
    public void showLoading() {
        mLoadingDialog.show();
    }

    @Override
    public void hideLoading() {
        mLoadingDialog.dismiss();
    }

    @Override
    public void showError(String error) {
        ToastUtils.toast(error);
    }
}
