package com.huijinlong.newretail.ui.wallet.exchange;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.huijinlong.newretail.R;
import com.huijinlong.newretail.bean.ExchangeDetailBean;

import java.util.List;

/**
 * @author zsg
 * @date 2018/11/20 12:01
 * @Version 1.0
 * @Description: ()
 */
public class ExchangeDetailAdapter extends BaseQuickAdapter<ExchangeDetailBean.DataBean.ListBean, BaseViewHolder> implements View.OnClickListener{


    public ExchangeDetailAdapter(@Nullable List<ExchangeDetailBean.DataBean.ListBean> data) {
        super(R.layout.item_exchangedetail, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, ExchangeDetailBean.DataBean.ListBean item) {
        helper.setText(R.id.tv_time, item.getTime().substring(5,item.getTime().toString().length()));
        helper.setText(R.id.tv_currency, item.getCurrency());
        helper.setText(R.id.tv_type, item.getType());
        helper.setText(R.id.tv_amount, item.getAmount());
        helper.setText(R.id.tv_status, item.getStatusX());

        helper.setTag(R.id.tv_detail, helper.getAdapterPosition());
        helper.getView(R.id.tv_detail).setOnClickListener(this);

        helper.setGone(R.id.lin_detail,item.isIsshow());

    }


    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.tv_detail:
                getData().get((int)v.getTag()).setIsshow(!getData().get((int)v.getTag()).isIsshow());
                notifyDataSetChanged();
                break;
        }
    }
}
