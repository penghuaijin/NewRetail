package com.huijinlong.newretail.ui;

import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.huijinlong.newretail.R;
import com.huijinlong.newretail.base.BaseActivity;
import com.huijinlong.newretail.base.BasePresenter;
import com.huijinlong.newretail.util.FontUtil;
import com.huijinlong.newretail.util.LogUtils;
import com.huijinlong.newretail.view.RootLoadingLayout;


/**
 */

public class WebViewActivity extends BaseActivity {


    private View mRootLayout;
    private RootLoadingLayout mLoadingLayout;
    private WebView mWebView;
    private String mUrl = "";
    private String webTitle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_webview);
        mUrl = getIntent().getStringExtra("webUrl");
        webTitle = getIntent().getStringExtra("webTitle");
//        initializeActionToolBar();
//        initView();

    }

    @Override
    public BasePresenter initPresenter() {
        return null;
    }

    private void initializeActionToolBar() {
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView tvTitle = (TextView) findViewById(R.id.toolbar_title);
        webTitle = webTitle.length() > 8 ? webTitle.substring(0, 8) + "..." : webTitle;
        tvTitle.setText(TextUtils.isEmpty(webTitle) ? "详情" : webTitle);
        TextView rightIcon = (TextView) findViewById(R.id.toolbar_func);
        rightIcon.setVisibility(View.VISIBLE);
        rightIcon.setText(R.string.xe644);
        FontUtil.injectFont(rightIcon);
        rightIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mWebView.reload();
            }
        });
//        initToolBar(mToolbar, true, "");
    }

    @Override
    protected void initView() {

        webTitle = webTitle.length() > 8 ? webTitle.substring(0, 8) + "..." : webTitle;
        setToolbarTitle(webTitle);
        setToolbarRightBtn(true, true,
                getResources().getString(R.string.xe644)
                , new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mWebView.reload();
                    }
                });

        mRootLayout = findViewById(R.id.rootLayout);
        mLoadingLayout = (RootLoadingLayout) findViewById(R.id.loadingLayout);
        mRootLayout.setVisibility(View.INVISIBLE);
        mLoadingLayout.setVisibility(View.VISIBLE);


        mWebView = (WebView) findViewById(R.id.webView);

        WebSettings s = mWebView.getSettings();
        s.setBuiltInZoomControls(true);
        s.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
        s.setUseWideViewPort(true);
        s.setLoadWithOverviewMode(true);
        s.setSaveFormData(true);
        s.setAppCacheEnabled(true);
        s.setAppCacheEnabled(true);
        s.setLoadWithOverviewMode(true);
        s.setDefaultTextEncodingName("utf-8");
        s.setJavaScriptEnabled(true);
        s.setJavaScriptCanOpenWindowsAutomatically(true);
        s.setDomStorageEnabled(true);
        mWebView.requestFocus();

        mWebView.addJavascriptInterface(new JSInterface(), "Android");
        mWebView.setWebChromeClient(new WebChromeClient() {

            @Override
            public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
                return super.onJsAlert(view, url, message, result);
            }

            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);
                LogUtils.e("title == " + title);
            }

        });

        mWebView.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                LogUtils.e("url == " + url);
                return super.shouldOverrideUrlLoading(view, url);

            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                mRootLayout.setVisibility(View.VISIBLE);
                mLoadingLayout.setVisibility(View.GONE);
                LogUtils.e("1111 == " + url);
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                mRootLayout.setVisibility(View.INVISIBLE);
                mLoadingLayout.setVisibility(View.VISIBLE);
            }

            @Override
            public void onReceivedSslError(WebView view,
                                           SslErrorHandler handler, SslError error) {
                LogUtils.e("error...");
            }
        });


        LogUtils.e("mUrl == " + mUrl);
        mWebView.loadUrl(mUrl);


    }

    @Override
    protected int getContentView() {
        return R.layout.activity_webview;
    }


    private class JSInterface {
        //调用JS的方法
        @JavascriptInterface
        public void paySuc(String url) {
            LogUtils.e("url == " + url);
        }
    }


}
