package com.huijinlong.newretail.ui.mine;

import com.huijinlong.newretail.base.BasePresenter;
import com.huijinlong.newretail.base.BaseBean;
import com.huijinlong.newretail.net.MyCallback;
import com.huijinlong.newretail.net.RequestBuilder;
import com.huijinlong.newretail.util.MD5Util;
import com.huijinlong.newretail.util.SpUtil;

import java.util.TreeMap;

/**
 * @author zsg
 * @date 2018/10/25 14:40
 * @Version 1.0
 * @Description: ()
 */
public class LoginOutPresent extends BasePresenter<LoginOutView> {
    private LoginOutView loginOutView;

    public LoginOutPresent(LoginOutView mViews) {
        this.loginOutView = mViews;
    }

    public void getLoginOut() {
        loginOutView.showLoading();
        TreeMap<String, String> params = new TreeMap<>();
        params.put("deviceType", "android");
        params.put("language", SpUtil.getString(SpUtil.LANGUAGE,"zh-cn"));
        RequestBuilder.execute(RequestBuilder.getNetService(true).getLoginOut(MD5Util.getSignValue(params), params), "", new MyCallback<BaseBean>() {
            @Override
            public void onSuccess(BaseBean result) {
                super.onSuccess(result);
                loginOutView.getOutSuccess(result);
            }

            @Override
            public void onEmpty(BaseBean result) {
                super.onEmpty(result);

            }

            @Override
            public void onBusinessFailure(Exception e) {
                super.onBusinessFailure(e);
                loginOutView.showError(e.getMessage());
            }

            @Override
            public void onFinalResponse(boolean isSuccess) {
                super.onFinalResponse(isSuccess);
                loginOutView.hideLoading();
            }
        });
    }
}
