package com.huijinlong.newretail.ui.mine.accountSecurity;

import com.huijinlong.newretail.base.BasePresenter;
import com.huijinlong.newretail.bean.SecurityInfoBean;
import com.huijinlong.newretail.net.MyCallback;
import com.huijinlong.newretail.net.RequestBuilder;
import com.huijinlong.newretail.util.MD5Util;
import com.huijinlong.newretail.util.SpUtil;

import java.util.TreeMap;

/**
 * @author zsg
 * @date 2018/10/26 14:40
 * @Version 1.0
 * @Description: ()
 */
public class SecurityInfoPresent extends BasePresenter<SecurityInfoView> {
    private SecurityInfoView securityInfoView;

    public SecurityInfoPresent(SecurityInfoView mViews) {
        this.securityInfoView = mViews;
    }

    public void getSecurityInfo() {
        securityInfoView.showLoading();
        TreeMap<String, String> params = new TreeMap<>();
        params.put("deviceType", "android");
        params.put("language", SpUtil.getString(SpUtil.LANGUAGE,"zh-cn"));

        RequestBuilder.execute(RequestBuilder.getNetService(true).getSecurity(MD5Util.getSignValue(params), params), "", new MyCallback<SecurityInfoBean>() {
            @Override
            public void onSuccess(SecurityInfoBean result) {
                super.onSuccess(result);
                securityInfoView.getSecurity(result);
            }

            @Override
            public void onEmpty(SecurityInfoBean result) {
                super.onEmpty(result);

            }

            @Override
            public void onBusinessFailure(Exception e) {
                super.onBusinessFailure(e);
                securityInfoView.showError(e.getMessage());
            }

            @Override
            public void onFinalResponse(boolean isSuccess) {
                super.onFinalResponse(isSuccess);
                securityInfoView.hideLoading();
            }
        });
    }

}
