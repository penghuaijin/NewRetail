package com.huijinlong.newretail.ui.mine;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.finddreams.languagelib.LanguageType;
import com.finddreams.languagelib.MultiLanguageUtil;
import com.huijinlong.newretail.MainActivity;
import com.huijinlong.newretail.R;
import com.huijinlong.newretail.base.BaseApplication;
import com.huijinlong.newretail.base.BaseFragment;
import com.huijinlong.newretail.base.BaseBean;
import com.huijinlong.newretail.manager.MyActivityManager;
import com.huijinlong.newretail.ui.login.LoginActivity;
import com.huijinlong.newretail.ui.mine.accountSecurity.AccountSecurityActivity;
import com.huijinlong.newretail.ui.mine.apimanger.ApiMangerActivity;
import com.huijinlong.newretail.ui.mine.authenticationsecond.AuthenticationActivity;
import com.huijinlong.newretail.ui.mine.setting.UserSettingActivity;
import com.huijinlong.newretail.util.FontUtil;
import com.huijinlong.newretail.util.SocketUtil;
import com.huijinlong.newretail.util.SpUtil;
import com.huijinlong.newretail.util.ToastUtils;
import com.huijinlong.newretail.view.SelectLanguagePopupWindow;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * @author zsg
 * @date 2018/10/25 14:40
 * @Version 1.0
 * @Description: (我的)
 */

public class UserFragment extends BaseFragment<LoginOutView, LoginOutPresent> implements LoginOutView {


    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar_func1)
    TextView toolbarFunc1;
    @BindView(R.id.toolbar_func)
    TextView toolbarFunc;
    @BindView(R.id.ll_right_func)
    LinearLayout llRightFunc;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.iv_user_avatar)
    ImageView ivUserAvatar;
    @BindView(R.id.tv_user_account)
    TextView tvUserAccount;
    @BindView(R.id.tv_account_security)
    TextView tvAccountSecurity;
    @BindView(R.id.tv_authentication)
    TextView tvAuthentication;
    @BindView(R.id.tv_account_balance)
    TextView tvAccountBalance;
    @BindView(R.id.tv_account_balance_right)
    TextView tvAccountBalanceRight;
    @BindView(R.id.tv_user_setting)
    TextView tvUserSetting;
    @BindView(R.id.tv_personal_setting)
    TextView tvPersonalSetting;
    @BindView(R.id.tv_user_setting_right)
    TextView tvUserSettingRight;
    @BindView(R.id.tv_api_manager)
    TextView tvApiManager;
    @BindView(R.id.tv_api_manager_right)
    TextView tvApiManagerRight;
    @BindView(R.id.tv_customer_service)
    TextView tvCustomerService;
    @BindView(R.id.tv_customer_service_right)
    TextView tvCustomerServiceRight;
    @BindView(R.id.tv_feedback)
    TextView tvFeedback;
    @BindView(R.id.tv_feedback_right)
    TextView tvFeedbackRight;
    @BindView(R.id.tv_help_center)
    TextView tvHelpCenter;
    @BindView(R.id.tv_help_center_right)
    TextView tvHelpCenterRight;
    @BindView(R.id.user_login_out)
    TextView userLoginOut;
    @BindView(R.id.ll_account_security)
    LinearLayout llAccountSecurity;
    @BindView(R.id.ll_authentication)
    LinearLayout llAuthentication;
    private Unbinder unbinder;
    private SelectLanguagePopupWindow selectLanguagePopupWindow;

    private void initView() {
        FontUtil.injectFont(tvAccountSecurity);
        FontUtil.injectFont(tvAuthentication);
        FontUtil.injectFont(tvAccountBalance);
        FontUtil.injectFont(tvAccountBalanceRight);
        FontUtil.injectFont(tvUserSetting);
        FontUtil.injectFont(tvUserSettingRight);
        FontUtil.injectFont(tvApiManager);
        FontUtil.injectFont(tvApiManagerRight);
        FontUtil.injectFont(tvCustomerService);
        FontUtil.injectFont(tvCustomerServiceRight);
        FontUtil.injectFont(tvFeedback);
        FontUtil.injectFont(tvFeedbackRight);
        FontUtil.injectFont(tvHelpCenter);
        FontUtil.injectFont(tvHelpCenterRight);
        tvUserAccount.setText(SpUtil.getString(SpUtil.EMAIL, ""));
        toolbarFunc.setText(getString(R.string.yy));
        toolbarFunc.setTextColor(Color.WHITE);
        toolbarFunc.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);

        selectLanguagePopupWindow = new SelectLanguagePopupWindow(getContext(), new SelectLanguagePopupWindow.OnLanguageSelectListener() {
            @Override
            public void OnSelectChinese() {
                SpUtil.putString(SpUtil.LANGUAGE, "zh-cn");
                MyActivityManager.getManager().finishAll();

                int selectedLanguage = LanguageType.LANGUAGE_CHINESE_SIMPLIFIED;
                MultiLanguageUtil.getInstance().updateLanguage(selectedLanguage);
                SocketUtil.getInstance().destroy();
                Intent intent = new Intent(getActivity(), MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);

            }

            @Override
            public void OnSelectChineseX() {
                SpUtil.putString(SpUtil.LANGUAGE, "zh-tw");
                MyActivityManager.getManager().finishAll();

                int selectedLanguage = LanguageType.LANGUAGE_CHINESE_TRADITIONAL;
                MultiLanguageUtil.getInstance().updateLanguage(selectedLanguage);
                SocketUtil.getInstance().destroy();

                Intent intent = new Intent(getActivity(), MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);

            }

            @Override
            public void OnSelectEnglish() {
                SpUtil.putString(SpUtil.LANGUAGE, "en-us");

                MyActivityManager.getManager().finishAll();
                int selectedLanguage = LanguageType.LANGUAGE_EN;
                MultiLanguageUtil.getInstance().updateLanguage(selectedLanguage);
                SocketUtil.getInstance().destroy();

                Intent intent = new Intent(getActivity(), MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);


            }
        });
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();

        unbinder.unbind();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.user_fragment;
    }

    @Override
    protected void initData() {
        toolbarTitle.setText(getResources().getString(R.string.user));
        initView();

    }

    @Override
    protected LoginOutPresent initPresenter() {
        return new LoginOutPresent(this);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void getOutSuccess(BaseBean baseBean) {

        SocketUtil.getInstance().destroy();
        SpUtil.clear();
        MyActivityManager.getManager().finishAll();
        Intent intent = new Intent(getContext(), LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void showLoading() {
        mLoading.show();
    }

    @Override
    public void hideLoading() {
        mLoading.dismiss();
    }

    @Override
    public void showError(String error) {
        ToastUtils.toast(error);
    }

    @OnClick({R.id.tv_user_setting_right, R.id.tv_api_manager_right,
            R.id.tv_customer_service_right, R.id.tv_feedback_right, R.id.tv_help_center_right, R.id.user_login_out, R.id.iv_user_avatar, R.id.ll_account_security, R.id.ll_authentication, R.id.toolbar_func})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_user_avatar:

                break;

//            case R.id.tv_account_security:
//                Intent intent1 = new Intent(getContext(), AccountSecurityActivity.class);
//                startActivity(intent1);
//                break;
//            case R.id.tv_authentication:
//                Intent intent2 = new Intent(getContext(), AuthenticationActivity.class);
//                startActivity(intent2);
//                break;
            case R.id.tv_user_setting_right:
                Intent intent3 = new Intent(getContext(), UserSettingActivity.class);
                startActivity(intent3);
                break;
            case R.id.tv_api_manager_right:
                Intent intent4 = new Intent(getContext(), ApiMangerActivity.class);
                startActivity(intent4);
                break;
            case R.id.tv_customer_service_right:
//                Intent intent5 = new Intent(getContext(), Kefu.class);
//                startActivity(intent5);
                break;
            case R.id.tv_feedback_right:
                break;
            case R.id.tv_help_center_right:
                break;
            case R.id.user_login_out:
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle(getString(R.string.qdtc));
//                    builder.setMessage(result.getMsg());
                builder.setNegativeButton(getString(R.string.qx), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                });
                builder.setPositiveButton(getString(R.string.queren), new DialogInterface.OnClickListener() {


                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mPresenter.getLoginOut();

                    }
                });

                builder.show();
                break;
            case R.id.ll_account_security:
                Intent intent1 = new Intent(getContext(), AccountSecurityActivity.class);
                startActivity(intent1);
                break;
            case R.id.ll_authentication:
                Intent intent2 = new Intent(getContext(), AuthenticationActivity.class);
                startActivity(intent2);
                break;
            case R.id.toolbar_func:
                if (selectLanguagePopupWindow != null && !selectLanguagePopupWindow.isShowing()) {
                    selectLanguagePopupWindow.showAtLocation(view, Gravity.BOTTOM, 0, 0);
                }
                break;
        }
    }


}
