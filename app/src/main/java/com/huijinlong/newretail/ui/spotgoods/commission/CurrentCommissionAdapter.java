package com.huijinlong.newretail.ui.spotgoods.commission;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.huijinlong.newretail.R;
import com.huijinlong.newretail.requestbean.CurrentCommissionBean;
import com.huijinlong.newretail.util.ToastUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author penghuaijin
 * @date 2018/10/16 8:16 PM
 * @Description: ()
 */

public class CurrentCommissionAdapter extends RecyclerView.Adapter<CurrentCommissionAdapter.ViewHolder> {

    private Context context;
    private List<CurrentCommissionBean.DataBean> list;
    private ItemClickListener listener;

    public ItemClickListener getListener() {
        return listener;
    }

    public void setListener(ItemClickListener listener) {
        this.listener = listener;
    }

    public CurrentCommissionAdapter(Context context, List<CurrentCommissionBean.DataBean> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_current_commission, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        CurrentCommissionBean.DataBean bean = list.get(position);
        holder.tvSide.setText(bean.getSide());
        holder.tvSymbol.setText(bean.getSymbol());

        holder.tvTime.setText(bean.getTime());
        holder.tvPriceName.setText(String.format(context.getString(R.string.jgzw), bean.getSymbol().split("/")[1]));
        holder.tvPrice.setText(bean.getPrice());
        holder.tvQtyName.setText(String.format(context.getString(R.string.slzw), bean.getSymbol().split("/")[0]));
        holder.tvQty.setText(bean.getQty());


        holder.tvFilled.setText(bean.getFilled());
        holder.tvFilledName.setText(String.format(context.getString(R.string.sjcjzw), bean.getSymbol().split("/")[0]));

        if (bean.getSide().equals(context.getString(R.string.mc))) {
            holder.tvSide.setTextColor(context.getResources().getColor(R.color.main_red));
        } else {
            holder.tvSide.setTextColor(context.getResources().getColor(R.color.main_blue));
        }

        holder.tvCancel.setOnClickListener(v -> listener.onCancelOrder(bean.getId()+"",position));

    }

    @Override
    public int getItemCount() {
        return list != null ? list.size() : 0;
    }

     interface ItemClickListener{
        void onCancelOrder(String id, int position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_side)
        TextView tvSide;
        @BindView(R.id.tv_symbol)
        TextView tvSymbol;
        @BindView(R.id.tv_time)
        TextView tvTime;
        @BindView(R.id.tv_cancel)
        TextView tvCancel;
        @BindView(R.id.tv_price_name)
        TextView tvPriceName;
        @BindView(R.id.tv_qty_name)
        TextView tvQtyName;
        @BindView(R.id.tv_filled_name)
        TextView tvFilledName;
        @BindView(R.id.tv_price)
        TextView tvPrice;
        @BindView(R.id.tv_qty)
        TextView tvQty;
        @BindView(R.id.tv_filled)
        TextView tvFilled;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
