package com.huijinlong.newretail.ui.wallet;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.huijinlong.newretail.R;
import com.huijinlong.newretail.bean.WithDrawsBean;

import java.util.List;

/**
 * @author zsg
 * @date 2018/10/25 16:52
 * @Version 1.0
 * @Description: ()
 */
public class WalletDetailAdapter extends BaseQuickAdapter<WithDrawsBean.DataBean.ListBean,BaseViewHolder>{


    public WalletDetailAdapter(@Nullable List<WithDrawsBean.DataBean.ListBean> data) {
        super(R.layout.item_walletdetail, data);
    }


    @Override
    protected void convert(BaseViewHolder helper, WithDrawsBean.DataBean.ListBean item) {

        helper.setText(R.id.tv_time,item.getTime().substring(0,10));
        helper.setText(R.id.tv_currency,item.getCurrency());
        helper.setText(R.id.tv_type,item.getType());
        if(item.getAmount().length()>15)
        {
            helper.setText(R.id.tv_count,item.getAmount().substring(0,15));
        }else
            helper.setText(R.id.tv_count,item.getAmount());

        helper.setText(R.id.tv_opreat,item.getStatusX());
    }
}
