package com.huijinlong.newretail.ui.wallet.walletDetail;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.huijinlong.newretail.R;
import com.huijinlong.newretail.base.BaseActivity;
import com.huijinlong.newretail.base.BasePresenter;
import com.huijinlong.newretail.base.PagerAdapter;
import com.huijinlong.newretail.ui.wallet.WalletDetailFragment;
import com.huijinlong.newretail.ui.wallet.exchange.ExchangeDetailFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author zsg
 * @date 2018/10/26 15:53
 * @Version 1.0
 * @Description: (财务记录)
 */
public class WalletDetailActivity extends BaseActivity {

    @BindView(R.id.tabs)
    TabLayout tabs;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.tv_currency)
    TextView tvCurrency;
    @BindView(R.id.tv_type)
    TextView tvType;
    @BindView(R.id.tv_count)
    TextView tvCount;
    @BindView(R.id.tv_opreat)
    TextView tvOpreat;
    @BindView(R.id.content)
    LinearLayout content;
    @BindView(R.id.vp_view)
    ViewPager vpView;
    private List<String> titles = new ArrayList<>();
    private List<Fragment> fragments = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
    }

    @Override
    public BasePresenter initPresenter() {
        return null;
    }

    @Override
    protected void initView() {
        setToolbarTitle(getString(R.string.cwjl));

        titles.add(getString(R.string.cbjl));
        titles.add(getString(R.string.tbjl));
        titles.add(getString(R.string.dhjl));
        titles.add(getString(R.string.hzjl));

        fragments.add(WalletDetailFragment.newInstance(getString(R.string.chongbi),""));
        fragments.add(WalletDetailFragment.newInstance(getString(R.string.tixian),""));
        fragments.add(ExchangeDetailFragment.newInstance());
        fragments.add(WalletDetailFragment.newInstance(getString(R.string.chongbi),""));

        vpView.setAdapter(new PagerAdapter(getSupportFragmentManager(), titles, fragments));
        tabs.setupWithViewPager(vpView);//将导航栏和viewpager进行关联
        tabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                vpView.setCurrentItem(tab.getPosition());//联动
                if(tab.getPosition()==2)
                {
                    content.setVisibility(View.GONE);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_walletdetail;
    }


}
