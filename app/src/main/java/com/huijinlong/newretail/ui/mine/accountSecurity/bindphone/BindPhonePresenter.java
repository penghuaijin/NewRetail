package com.huijinlong.newretail.ui.mine.accountSecurity.bindphone;

import com.huijinlong.newretail.base.BasePresenter;
import com.huijinlong.newretail.net.MyCallback;
import com.huijinlong.newretail.net.RequestBuilder;
import com.huijinlong.newretail.requestbean.GetPhoneCaptcha4BindBean;
import com.huijinlong.newretail.requestbean.VerifyPhone4BindBean;
import com.huijinlong.newretail.util.MD5Util;
import com.huijinlong.newretail.util.SpUtil;

import java.util.Map;
import java.util.TreeMap;

/**
 * Created by penghuaijin on 2018/9/12.
 */

public class BindPhonePresenter extends BasePresenter<BindPhoneView> {

    private BindPhoneView bindPhoneView;

    public BindPhonePresenter(BindPhoneView mViews) {
        this.bindPhoneView = mViews;
    }

    public void verifyPhone4Bind(String action, String verificationCode) {
        TreeMap<String, String> params = new TreeMap<>();
        params.put("action", action);
        params.put("verificationCode", verificationCode);
        params.put("deviceType", "android");
        params.put("language", SpUtil.getString(SpUtil.LANGUAGE,"zh-cn"));
        RequestBuilder.execute(RequestBuilder.getNetService(true).verifyPhone4Bind(MD5Util.getSignValue(params), params), "", new MyCallback<VerifyPhone4BindBean>() {
            @Override
            public void onSuccess(VerifyPhone4BindBean result) {
                super.onSuccess(result);
                bindPhoneView.bindPhoneSuccess(result);
            }

            @Override
            public void onEmpty(VerifyPhone4BindBean result) {
                super.onEmpty(result);

            }

            @Override
            public void onBusinessFailure(Exception e) {
                super.onBusinessFailure(e);
                bindPhoneView.showGetResult(e.getMessage());

            }

            @Override
            public void onFinalResponse(boolean isSuccess) {
                super.onFinalResponse(isSuccess);
                bindPhoneView.hideLoading();

            }
        });
    }

    public void getPhoneCaptcha4Bind(String action, String mobileCode, String mobile, Map<String, String> map) {
        TreeMap<String, String> params = new TreeMap<>();
        params.put("action",action);
        params.put("mobileCode", mobileCode);
        params.put("mobile", mobile);
        params.put("deviceType", "android");
        params.put("language", SpUtil.getString(SpUtil.LANGUAGE, "zh-cn"));
        params.put("challenge", map.get("geetest_challenge"));
        params.put("validate", map.get("geetest_validate"));
        params.put("seccode", map.get("geetest_seccode"));
        RequestBuilder.execute(RequestBuilder.getNetService(true).getPhoneCaptcha4Bind(MD5Util.getSignValue(params), params), "", new MyCallback<GetPhoneCaptcha4BindBean>() {
            @Override
            public void onSuccess(GetPhoneCaptcha4BindBean result) {
                super.onSuccess(result);
                bindPhoneView.getPhoneCaptchaSuccess(result);
            }

            @Override
            public void onEmpty(GetPhoneCaptcha4BindBean result) {
                super.onEmpty(result);

            }

            @Override
            public void onBusinessFailure(Exception e) {
                super.onBusinessFailure(e);
                bindPhoneView.showGetResult(e.getMessage());

            }

            @Override
            public void onFinalResponse(boolean isSuccess) {
                super.onFinalResponse(isSuccess);
                bindPhoneView.hideLoading();

            }
        });
    }


}
