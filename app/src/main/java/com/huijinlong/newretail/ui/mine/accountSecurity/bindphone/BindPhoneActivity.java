package com.huijinlong.newretail.ui.mine.accountSecurity.bindphone;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.huijinlong.newretail.R;
import com.huijinlong.newretail.base.BaseActivity;
import com.huijinlong.newretail.requestbean.GetPhoneCaptcha4BindBean;
import com.huijinlong.newretail.requestbean.VerifyPhone4BindBean;
import com.huijinlong.newretail.ui.mine.accountSecurity.bindphone.countries.CountriesActivity;
import com.huijinlong.newretail.util.NRGeeTestUtils;
import com.huijinlong.newretail.util.TimeCountUtils;
import com.huijinlong.newretail.util.ToastUtils;
import com.huijinlong.newretail.util.VerifySecurityUtil;
import com.huijinlong.newretail.util.ViewUtil;

import java.util.Map;

/**
 * Created by penghuaijin on 2018/9/26.
 */

public class BindPhoneActivity extends BaseActivity<BindPhoneView,BindPhonePresenter> implements BindPhoneView, View.OnClickListener {

    private TextView tv_mobileCode;
    private EditText et_mobileCode;
    private EditText et_verificationCode;
    private BindPhonePresenter mPresenter;
    private TimeCountUtils timeCountUtils;
    private NRGeeTestUtils utils;
    private final int GET_CODE = 0x105;
    private String action="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.acitvity_bind_phone);
//        initializeActionToolBar();
//        initView();
//        initData();
    }

    @Override
    public BindPhonePresenter initPresenter() {
        return new BindPhonePresenter(this);
    }

//    private void initData() {
//        mPresenter = new BindPhonePresenter(this);
//    }


    @Override
    protected void initView() {
        setToolbarTitle(getString(R.string.bdsj));
        action=getIntent().getStringExtra("action");
        et_mobileCode = findViewById(R.id.et_mobileCode);
        et_verificationCode = findViewById(R.id.et_verificationCode);
        TextView tv_send_verification = findViewById(R.id.tv_send_verification);
        tv_mobileCode = findViewById(R.id.tv_mobileCode);
        tv_mobileCode.setOnClickListener(this);

        ViewUtil.setEditTextHintSize(et_mobileCode, getString(R.string.qsrsj), (int) getResources().getDimension(R.dimen.normal_text_size));
        ViewUtil.setEditTextHintSize(et_verificationCode, getString(R.string.qsrsjyzm), (int) getResources().getDimension(R.dimen.normal_text_size));
        findViewById(R.id.btn_bind_phone).setOnClickListener(this);
        findViewById(R.id.tv_send_verification).setOnClickListener(this);
        timeCountUtils = new TimeCountUtils(60000, 1000, tv_send_verification);
        utils = new NRGeeTestUtils(this);
        utils.setListener(new NRGeeTestUtils.MyGeeTestListener() {

            @Override
            public void onGeeTestSuccess(Map<String, String> map) {
                mPresenter.getPhoneCaptcha4Bind(action, tv_mobileCode.getText().toString(), et_mobileCode.getText().toString(), map);
            }

        });
    }

    @Override
    protected int getContentView() {
        return R.layout.acitvity_bind_phone;
    }

//    private void initializeActionToolBar() {
//        Toolbar mToolbar = findViewById(R.id.toolbar);
//        TextView tvTitle = findViewById(R.id.toolbar_title);
//        tvTitle.setText("绑定手机");
//        initToolBar(mToolbar, true, "");
//    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_send_verification: {
                utils.startTest();

                break;
            }
            case R.id.btn_bind_phone: {
                mPresenter.verifyPhone4Bind(action, et_verificationCode.getText().toString());
                break;
            }
            case R.id.tv_mobileCode: {
                Intent intent = new Intent(this, CountriesActivity.class);
                startActivityForResult(intent, GET_CODE);
                break;
            }
        }
    }


    @Override
    protected void onDestroy() {
        timeCountUtils.cancel();
        super.onDestroy();
    }

    @Override
    public void showGetResult(String resultMessage) {
        ToastUtils.toast(resultMessage);
    }

    @Override
    public void bindPhoneSuccess(VerifyPhone4BindBean bean) {


        VerifySecurityUtil verifySecurityUtil = VerifySecurityUtil.getInstance();
        verifySecurityUtil.init(this, action,
                bean.getData().getVerifyMobile(),
                bean.getData().getVerifyEmail(),
                bean.getData().getVerifyGoogleAuthenticator());

        verifySecurityUtil.setListener(new VerifySecurityUtil.VerifySecurityUtilListener() {
            @Override
            public void onVerifySecuritySuccess() {
                ToastUtils.toast(getString(R.string.bdcg));
                finish();
            }

            @Override
            public void onVerifySecurityFailed() {
                ToastUtils.toast(getString(R.string.faild));


            }

            @Override
            public void onCancelDialog() {


            }
        });
    }


    @Override
    public void getPhoneCaptchaSuccess(GetPhoneCaptcha4BindBean bean) {
        utils.setCancelUtils();
        timeCountUtils.start();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK){
            switch (requestCode) {
                case GET_CODE:{
                    assert data != null;
                    tv_mobileCode.setText(data.getStringExtra("code"));
                }
            }
        }
    }

    @Override
    public void showLoading() {
        mLoadingDialog.show();
    }

    @Override
    public void hideLoading() {
        mLoadingDialog.dismiss();
    }

    @Override
    public void showError(String error) {
    }
}
