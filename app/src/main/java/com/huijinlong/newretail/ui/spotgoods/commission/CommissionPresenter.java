package com.huijinlong.newretail.ui.spotgoods.commission;

import com.huijinlong.newretail.base.BasePresenter;
import com.huijinlong.newretail.base.BaseBean;
import com.huijinlong.newretail.net.MyCallback;
import com.huijinlong.newretail.net.RequestBuilder;
import com.huijinlong.newretail.requestbean.CurrentCommissionBean;
import com.huijinlong.newretail.requestbean.HistoryBean;
import com.huijinlong.newretail.util.MD5Util;
import com.huijinlong.newretail.util.SpUtil;

import java.util.TreeMap;

/**
 * @author penghuaijin
 * @date 2018/9/12 5:24 PM
 * @Description: ()
 */

public class CommissionPresenter extends BasePresenter<CommissionView> {

    private CommissionView commissionView;

    public CommissionPresenter(CommissionView depthlViews) {
        commissionView = depthlViews;
    }

    public void getHistory(String instrument, int currentPage, int pageSize) {
        commissionView.showLoading();
        TreeMap<String, String> params = new TreeMap<>();
        params.put("instrument", instrument);
        params.put("currentPage", currentPage+"");
        params.put("pageSize", pageSize+"");
        params.put("deviceType", "android");
        params.put("language", SpUtil.getString(SpUtil.LANGUAGE, "zh-cn"));
        RequestBuilder.execute(RequestBuilder.getNetService(true).getHistory(MD5Util.getSignValue(params), params), this.getClass().getSimpleName(), new MyCallback<HistoryBean>() {
            @Override
            public void onSuccess(HistoryBean result) {
                super.onSuccess(result);
                commissionView.getHistorySuccess(result);
            }

            @Override
            public void onEmpty(HistoryBean result) {
                super.onEmpty(result);

            }

            @Override
            public void onBusinessFailure(Exception e) {
                super.onBusinessFailure(e);
                commissionView.showError(e.getMessage());

            }

            @Override
            public void onFinalResponse(boolean isSuccess) {
                super.onFinalResponse(isSuccess);
                commissionView.hideLoading();
            }
        });
    }

    public void getCurrent(String instrument) {
        commissionView.showLoading();
        TreeMap<String, String> params = new TreeMap<>();
        params.put("instrument", instrument);
        params.put("deviceType", "android");
        params.put("language", SpUtil.getString(SpUtil.LANGUAGE, "zh-cn"));
        RequestBuilder.execute(RequestBuilder.getNetService(true).getCurrent(MD5Util.getSignValue(params), params), this.getClass().getSimpleName(), new MyCallback<CurrentCommissionBean>() {
            @Override
            public void onSuccess(CurrentCommissionBean result) {
                super.onSuccess(result);
                commissionView.getCurrentSuccess(result);
            }

            @Override
            public void onEmpty(CurrentCommissionBean result) {
                super.onEmpty(result);

            }

            @Override
            public void onBusinessFailure(Exception e) {
                super.onBusinessFailure(e);
                commissionView.showError(e.getMessage());

            }

            @Override
            public void onFinalResponse(boolean isSuccess) {
                super.onFinalResponse(isSuccess);
                commissionView.hideLoading();
            }
        });
    }

    public void orderSingleCancel(String order_id) {
        commissionView.showLoading();
        TreeMap<String, String> params = new TreeMap<>();
        params.put("order_id", order_id);
        params.put("deviceType", "android");
        params.put("language", SpUtil.getString(SpUtil.LANGUAGE, "zh-cn"));
        RequestBuilder.execute(RequestBuilder.getNetService(true).orderSingleCancel(MD5Util.getSignValue(params), params), this.getClass().getSimpleName(), new MyCallback<BaseBean>() {
            @Override
            public void onSuccess(BaseBean result) {
                super.onSuccess(result);
                commissionView.orderSingleCancel(result);
            }

            @Override
            public void onEmpty(BaseBean result) {
                super.onEmpty(result);

            }

            @Override
            public void onBusinessFailure(Exception e) {
                super.onBusinessFailure(e);
                commissionView.showError(e.getMessage());

            }

            @Override
            public void onFinalResponse(boolean isSuccess) {
                super.onFinalResponse(isSuccess);
                commissionView.hideLoading();
            }
        });
    }


}
