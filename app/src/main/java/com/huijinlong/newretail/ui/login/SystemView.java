package com.huijinlong.newretail.ui.login;

import com.huijinlong.newretail.base.BaseView;
import com.huijinlong.newretail.requestbean.CountriesBean;
import com.huijinlong.newretail.requestbean.CurrentCountryIdBean;

/**
 * Created by penghuaijin on 2018/9/12.
 */

public interface SystemView extends BaseView{
        void getCountriesSuccess(CountriesBean bean);
        void getCurrentCountryIdSuccess(CurrentCountryIdBean bean);

}
