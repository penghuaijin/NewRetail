package com.huijinlong.newretail.ui.market;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.huijinlong.newretail.R;
import com.huijinlong.newretail.base.BaseFragment;
import com.huijinlong.newretail.bean.TickerSocketEntity;
import com.huijinlong.newretail.eventbus.MarketSortEvent;
import com.huijinlong.newretail.requestbean.SpotProductsBean;
import com.huijinlong.newretail.ui.market.search.MarketSearchActivity;
import com.huijinlong.newretail.util.FontUtil;
import com.huijinlong.newretail.util.SocketUtil;
import com.huijinlong.newretail.util.ThreadPool;
import com.huijinlong.newretail.util.ToastUtils;

import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.ViewPagerHelper;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.CommonPagerTitleView;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by penghuaijin on 2018/9/10.
 */

public class MarketFragment extends BaseFragment<MarketView, MarketPresenter> implements View.OnClickListener, MarketView {


    @BindView(R.id.toolbar_func_left)
    TextView toolbarFuncLeft;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar_func_fight)
    TextView toolbarFuncFight;
    @BindView(R.id.ll_right_func)
    LinearLayout llRightFunc;
    @BindView(R.id.toolbar_market)
    Toolbar toolbarMarket;
    @BindView(R.id.magic_indicator_market)
    MagicIndicator magicIndicatorMarket;
    @BindView(R.id.tv_currency)
    TextView tvCurrency;
    @BindView(R.id.iv_ic_currency)
    ImageView ivIcCurrency;
    @BindView(R.id.tv_updatedPrice)
    TextView tvUpdatedPrice;
    @BindView(R.id.iv_ic_updatedPrice)
    ImageView ivIcUpdatedPrice;
    @BindView(R.id.tv_UpsAndDowns)
    TextView tvUpsAndDowns;
    @BindView(R.id.iv_ic_UpsAndDowns)
    ImageView ivIcUpsAndDowns;
    @BindView(R.id.view_pager_market)
    ViewPager viewPagerMarket;
    Unbinder unbinder;
    private ArrayList<Fragment> fragmentList = new ArrayList<>();
    private ProjectInfoPagerAdapter projectInfoPagerAdapter;
    private List<String> stringList = new ArrayList<>();
    private CommonNavigator commonNavigator;
    private int type_currency, type_updatedPrice, type_UpsAndDowns;


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.market_fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    protected void initData() {

        toolbarTitle.setText(getResources().getString(R.string.hangqing));
        FontUtil.injectFont(toolbarFuncFight);
        FontUtil.injectFont(toolbarFuncLeft);
        toolbarFuncFight.setOnClickListener(this);
        projectInfoPagerAdapter = new ProjectInfoPagerAdapter(getFragmentManager(), fragmentList);
        viewPagerMarket.setAdapter(projectInfoPagerAdapter);

        initMagicIndicator();

        mPresenter.getsSpotProducts();
    }

    @Override
    protected MarketPresenter initPresenter() {
        return new MarketPresenter(this);
    }

    private void initMagicIndicator() {
        magicIndicatorMarket.setBackgroundColor(getResources().getColor(R.color.theme_color));
        commonNavigator = new CommonNavigator(getContext());
        commonNavigator.setAdjustMode(false);

        commonNavigator.setAdapter(new CommonNavigatorAdapter() {
            @Override
            public int getCount() {
                return stringList.size();

            }

            @Override
            public IPagerTitleView getTitleView(Context context, final int i) {
                final CommonPagerTitleView commonPagerTitleView = new CommonPagerTitleView(context);
                View inflate = LayoutInflater.from(context).inflate(R.layout.simple_pager_title_layout, null);
                final TextView textView = inflate.findViewById(R.id.title_text);
                textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                textView.setText(stringList.get(i));
                final ImageView icon = inflate.findViewById(R.id.title_icon);

                icon.setVisibility(View.GONE);
                commonPagerTitleView.setContentView(inflate);
                commonPagerTitleView.setOnPagerTitleChangeListener(new CommonPagerTitleView.OnPagerTitleChangeListener() {
                    @Override
                    public void onSelected(int i, int i1) {
                        textView.setTextColor(getResources().getColor(R.color.c_55a));

                    }

                    @Override
                    public void onDeselected(int i, int i1) {
                        textView.setTextColor(getResources().getColor(R.color.white));

                    }

                    @Override
                    public void onLeave(int i, int i1, float v, boolean b) {

                    }

                    @Override
                    public void onEnter(int i, int i1, float v, boolean b) {

                    }
                });
                commonPagerTitleView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        viewPagerMarket.setCurrentItem(i);
                    }
                });

                return commonPagerTitleView;
            }

            @Override
            public IPagerIndicator getIndicator(Context context) {
                LinePagerIndicator linePagerIndicator = new LinePagerIndicator(context);
                linePagerIndicator.setMode(LinePagerIndicator.MODE_WRAP_CONTENT);
                linePagerIndicator.setColors(getResources().getColor(R.color.c_55a));
                return linePagerIndicator;
            }
        });

        magicIndicatorMarket.setNavigator(commonNavigator);
        commonNavigator.notifyDataSetChanged();
        ViewPagerHelper.bind(magicIndicatorMarket, viewPagerMarket);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void spotProductsSuccess(SpotProductsBean spotProductsBean) {
        stringList.clear();
        fragmentList.clear();

        List<SpotProductsBean.DataBean.ProductsBean> products = spotProductsBean.getData().getProducts();
        for (SpotProductsBean.DataBean.ProductsBean productListBean : products) {
            stringList.add(productListBean.getName());
        }

        for (int i = 0; i < products.size(); i++) {
            MarketListFragment marketListFragment = new MarketListFragment();
            marketListFragment.setKey(products.get(i).getName());
            SpotProductsBean.DataBean.ProductsBean listBean = products.get(i);
            marketListFragment.setData(listBean);
            fragmentList.add(marketListFragment);

        }
        ThreadPool.newUITask(new Runnable() {
            @Override
            public void run() {
                commonNavigator.notifyDataSetChanged();
                projectInfoPagerAdapter.notifyDataSetChanged();
            }
        }, 100);


        initSocket(spotProductsBean);
        SocketUtil.getInstance().setTickerListener(new SocketUtil.TickerListener() {
            @Override
            public void onTickerMsg(TickerSocketEntity tickerSocketEntity) {
                if (tickerSocketEntity != null) {
                    ThreadPool.newUITask(new Runnable() {
                        @Override
                        public void run() {
                            EventBus.getDefault().post(tickerSocketEntity);
                        }
                    });
                }
            }
        });


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.toolbar_func_fight:
                startToActivity(getActivity(), MarketSearchActivity.class);
                break;
        }
    }

    @Override
    public void showLoading() {
        mLoading.show();
    }

    @Override
    public void hideLoading() {
        mLoading.dismiss();
    }

    @Override
    public void showError(String error) {
        ToastUtils.toast(error);
    }

    private void initSocket(SpotProductsBean spotProductsBean) {
        ThreadPool.newTask(new Runnable() {
            @Override
            public void run() {
                SocketUtil.getInstance().destroy();
                SocketUtil.getInstance().init5000(spotProductsBean.getData().getProducts().get(0).getArea().getMain().get(0).getInstrument(), true, false);
//                SocketUtil.getInstance().init5005("FERUSDT", "1min");
            }
        });

    }

    @OnClick({R.id.tv_currency, R.id.tv_updatedPrice, R.id.tv_UpsAndDowns, R.id.iv_ic_currency, R.id.iv_ic_updatedPrice, R.id.iv_ic_UpsAndDowns})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_ic_currency:
                if (type_currency == 1) {
                    type_currency = -1;
                    ivIcCurrency.setImageResource(R.mipmap.ic_down_blue);
                } else {
                    type_currency++;
                    ivIcCurrency.setImageResource(type_currency == 0 ? R.mipmap.ic_no_blue : R.mipmap.ic_up_blue);
                }
                type_UpsAndDowns = 0;
                type_updatedPrice = 0;
                ivIcUpdatedPrice.setImageResource(R.mipmap.ic_no_blue);
                ivIcUpsAndDowns.setImageResource(R.mipmap.ic_no_blue);
                break;
            case R.id.iv_ic_updatedPrice:
                if (type_updatedPrice == 1) {
                    type_updatedPrice = -1;
                    ivIcUpdatedPrice.setImageResource(R.mipmap.ic_down_blue);
                } else {
                    type_updatedPrice++;
                    ivIcUpdatedPrice.setImageResource(type_updatedPrice == 0 ? R.mipmap.ic_no_blue : R.mipmap.ic_up_blue);
                }
                type_currency = 0;
                type_UpsAndDowns = 0;
                ivIcCurrency.setImageResource(R.mipmap.ic_no_blue);
                ivIcUpsAndDowns.setImageResource(R.mipmap.ic_no_blue);
                break;
            case R.id.iv_ic_UpsAndDowns:
                if (type_UpsAndDowns == 1) {
                    type_UpsAndDowns = -1;
                    ivIcUpsAndDowns.setImageResource(R.mipmap.ic_down_blue);
                } else {
                    type_UpsAndDowns++;
                    ivIcUpsAndDowns.setImageResource(type_UpsAndDowns == 0 ? R.mipmap.ic_no_blue : R.mipmap.ic_up_blue);
                }
                type_currency = 0;
                type_updatedPrice = 0;
                ivIcCurrency.setImageResource(R.mipmap.ic_no_blue);
                ivIcUpdatedPrice.setImageResource(R.mipmap.ic_no_blue);
                break;

            case R.id.tv_currency:
                if (type_currency == 1) {
                    type_currency = -1;
                    ivIcCurrency.setImageResource(R.mipmap.ic_down_blue);
                } else {
                    type_currency++;
                    ivIcCurrency.setImageResource(type_currency == 0 ? R.mipmap.ic_no_blue : R.mipmap.ic_up_blue);
                }
                type_UpsAndDowns = 0;
                type_updatedPrice = 0;
                ivIcUpdatedPrice.setImageResource(R.mipmap.ic_no_blue);
                ivIcUpsAndDowns.setImageResource(R.mipmap.ic_no_blue);
                break;
            case R.id.tv_updatedPrice:
                if (type_updatedPrice == 1) {
                    type_updatedPrice = -1;
                    ivIcUpdatedPrice.setImageResource(R.mipmap.ic_down_blue);
                } else {
                    type_updatedPrice++;
                    ivIcUpdatedPrice.setImageResource(type_updatedPrice == 0 ? R.mipmap.ic_no_blue : R.mipmap.ic_up_blue);
                }
                type_currency = 0;
                type_UpsAndDowns = 0;
                ivIcCurrency.setImageResource(R.mipmap.ic_no_blue);
                ivIcUpsAndDowns.setImageResource(R.mipmap.ic_no_blue);
                break;
            case R.id.tv_UpsAndDowns:
                if (type_UpsAndDowns == 1) {
                    type_UpsAndDowns = -1;
                    ivIcUpsAndDowns.setImageResource(R.mipmap.ic_down_blue);
                } else {
                    type_UpsAndDowns++;
                    ivIcUpsAndDowns.setImageResource(type_UpsAndDowns == 0 ? R.mipmap.ic_no_blue : R.mipmap.ic_up_blue);
                }
                type_currency = 0;
                type_updatedPrice = 0;
                ivIcCurrency.setImageResource(R.mipmap.ic_no_blue);
                ivIcUpdatedPrice.setImageResource(R.mipmap.ic_no_blue);
                break;

        }
        EventBus.getDefault().post(new MarketSortEvent(type_currency, type_updatedPrice, type_UpsAndDowns));

    }


}
