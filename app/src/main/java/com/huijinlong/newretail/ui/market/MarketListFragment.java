package com.huijinlong.newretail.ui.market;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.google.gson.Gson;
import com.huijinlong.newretail.R;
import com.huijinlong.newretail.base.BaseRecyclerAdapter;
import com.huijinlong.newretail.base.SmartViewHolder;
import com.huijinlong.newretail.base.BaseFragment;
import com.huijinlong.newretail.base.BasePresenter;
import com.huijinlong.newretail.bean.TickerSocketEntity;
import com.huijinlong.newretail.eventbus.MarketItemCLickEvent;
import com.huijinlong.newretail.eventbus.MarketSortEvent;
import com.huijinlong.newretail.requestbean.SpotProductsBean;
import com.huijinlong.newretail.util.ListUtil;
import com.huijinlong.newretail.util.LogUtils;
import com.huijinlong.newretail.util.ToastUtils;
import com.huijinlong.newretail.view.SimpleDividerDecoration;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.DecimalFormat;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


/**
 * Created by penghuaijin on 2018/9/10.
 */

public class MarketListFragment extends BaseFragment  {


    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    Unbinder unbinder;
    private String key = "";
    private SpotProductsBean.DataBean.ProductsBean mlistBean;
    private MarketListAdapter mAdapter;
    private LinearLayoutManager layoutManager;


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    protected int getLayoutId() {
        EventBus.getDefault().register(this);
        return R.layout.market_list_fragment;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);

    }

    @Override
    protected void initData() {
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new SimpleDividerDecoration(getContext(), 10, 10, getResources().getColor(R.color.c_line), getResources().getDimensionPixelSize(R.dimen.divider_height)));
        refreshData();

    }

    @Override
    protected BasePresenter initPresenter() {
        return null;
    }


    private void refreshData() {

        mAdapter=new MarketListAdapter(mlistBean.getArea().getMain(),getContext());
        mAdapter.setHasStableIds(true);
        mAdapter.setOnItemClickListener((adapter, view, position) -> {

            String json = new Gson().toJson(mlistBean.getArea().getMain().get(position));
            MarketItemCLickEvent event=new Gson().fromJson(json,MarketItemCLickEvent.class);
            EventBus.getDefault().post(event);
        });

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mAdapter);

    }

    public void setKey(String key) {
        this.key = key;
    }


    public void setData(SpotProductsBean.DataBean.ProductsBean listBean) {
        mlistBean = listBean;
    }



    @Subscribe()
    public void onEvent(TickerSocketEntity event) {
        LogUtils.e("TickerSocketEntity----------" + new Gson().toJson(event));
        List<SpotProductsBean.DataBean.ProductsBean.AreaBean.MainBean> main = mlistBean.getArea().getMain();
        for (int i = 0; i < main.size(); i++) {
            if (main.get(i).getInstrument().equals(event.getInstrument())) {
                SpotProductsBean.DataBean.ProductsBean.AreaBean.MainBean entity;
                entity = main.get(i);
                entity.setLast(event.getLast());
                double last = Double.parseDouble(event.getLast());
                double open = Double.parseDouble(event.getOpen());
                DecimalFormat decimalFormat = new DecimalFormat("#.00");
                String s = decimalFormat.format((last - open) / open * 100);
                if (s.startsWith(".")){
                    s="0"+s;
                }
                entity.setRate(s + "%");
                mlistBean.getArea().getMain().set(i, entity);
                if (mAdapter!=null){
                    recyclerView.setLayoutManager(layoutManager);
                    mAdapter.replaceData(mlistBean.getArea().getMain());
                }

                break;

            }

        }
    }

    @Subscribe()
    public void onEvent(MarketSortEvent event) {
        ListUtil.getMarketList(mlistBean.getArea().getMain()
                ,event.getTypeCurrency(),event.getTypeUpdatedPrice(),
                event.getTypeUpsAndDowns());
        if (mAdapter!=null) {
            recyclerView.setLayoutManager(layoutManager);
            mAdapter.replaceData(mlistBean.getArea().getMain());

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }
}
