package com.huijinlong.newretail.ui.mine.apimanger;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.huijinlong.newretail.R;
import com.huijinlong.newretail.base.BaseActivity;
import com.huijinlong.newretail.constant.Constant;
import com.huijinlong.newretail.requestbean.EditApiBean;
import com.huijinlong.newretail.util.ToastUtils;
import com.huijinlong.newretail.util.VerifySecurityUtil;
import com.huijinlong.newretail.view.IPEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author penghuaijin
 * @date 2018-10-26
 * @Description: (API管理)
 */

public class ApiMangerActivity extends BaseActivity<ApiMangerView, ApiMangerPresent> implements ApiMangerView {

//    @BindView(R.id.toolbar_title)
//    TextView toolbarTitle;
//    @BindView(R.id.toolbar_func1)
//    TextView toolbarFunc1;
//    @BindView(R.id.toolbar_func)
//    TextView toolbarFunc;
//    @BindView(R.id.ll_right_func)
//    LinearLayout llRightFunc;
//    @BindView(R.id.toolbar)
//    Toolbar toolbar;
    @BindView(R.id.ed_remake)
    EditText edRemake;

    @BindView(R.id.tv_third_authentication)
    TextView tvThirdAuthentication;
    @BindView(R.id.bt_add)
    Button btAdd;
    @BindView(R.id.ip_text)
    IPEditText ipText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_api_manger);
        ButterKnife.bind(this);
//        initView();
    }

    @Override
    public ApiMangerPresent initPresenter() {
        return new ApiMangerPresent(this);
    }

    @Override
    protected void initView() {

        setToolbarTitle(getString(R.string.apigl));
        setToolbarRightBtn(true, false, getString(R.string.apilist), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ToastUtils.toast(getString(R.string.apilist));
            }
        });

//        toolbarTitle.setText("API管理");
//        toolbarFunc.setText("api列表");
//        toolbarFunc.setTextSize(13);
//        initToolBar(toolbar, true, "");
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_api_manger;
    }


    @Override
    public void showLoading() {

        mLoadingDialog.show();
    }

    @Override
    public void hideLoading() {
        mLoadingDialog.dismiss();
    }

    @Override
    public void showError(String error) {
        ToastUtils.toast(error);
    }

    @Override
    public void getEditApiSuccess(EditApiBean editApiBean) {

        EditApiBean.DataBean bean = editApiBean.getData();

        VerifySecurityUtil verifySecurityUtil = VerifySecurityUtil.getInstance();
        verifySecurityUtil.init(this, Constant.CREATE_APIKEY,
                bean.getVerifyMobile(),
                bean.getVerifyEmail(),
                bean.getVerifyGoogleAuthenticator());

        verifySecurityUtil.setListener(new VerifySecurityUtil.VerifySecurityUtilListener() {
            @Override
            public void onVerifySecuritySuccess() {
                ToastUtils.toast(getString(R.string.cjcg));

            }

            @Override
            public void onVerifySecurityFailed() {
                ToastUtils.toast(getString(R.string.faild));


            }

            @Override
            public void onCancelDialog() {


            }
        });
    }

    @OnClick({R.id.tv_third_authentication, R.id.bt_add})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_third_authentication:
                break;
            case R.id.bt_add:
                if (TextUtils.isEmpty(edRemake.getText().toString())) {
                    ToastUtils.toast(getString(R.string.qsrbz));
                    return;
                }
                if (ipText.isEmpty()) {
                    ToastUtils.toast(getString(R.string.qsrhfip));
                    return;
                }

                mPresenter.editApiKey(edRemake.getText().toString(), ipText.getText(this));
                break;

        }
    }
}
