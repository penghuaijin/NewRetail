package com.huijinlong.newretail.ui.wallet;

import com.huijinlong.newretail.base.BaseView;
import com.huijinlong.newretail.bean.AssetBean;

/**
 * @author zsg
 * @date 2018/10/26 11:13
 * @Version 1.0
 * @Description: ()
 */
public interface AssetView extends BaseView {
    void getAsset(AssetBean assetBean);
}
