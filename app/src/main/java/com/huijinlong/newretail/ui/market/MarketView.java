package com.huijinlong.newretail.ui.market;

import com.huijinlong.newretail.base.BaseView;
import com.huijinlong.newretail.requestbean.SpotProductsBean;

public interface MarketView extends BaseView{
		void spotProductsSuccess(SpotProductsBean spotProductsBean);
}
