package com.huijinlong.newretail.ui.market.search;

import com.huijinlong.newretail.base.BaseView;
import com.huijinlong.newretail.base.BaseBean;

/**
 * @author zsg
 * @date 2018/10/19 15:52
 * @Version 1.0
 * @Description: ()
 */
public interface MarketSearchView extends BaseView{
    void getHotSearch(BaseBean currenciesBean);
}
