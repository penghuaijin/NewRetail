package com.huijinlong.newretail.ui;

import android.util.Log;

import com.huijinlong.newretail.base.BaseApplication;
import com.huijinlong.newretail.base.BaseBean;
import com.huijinlong.newretail.base.BasePresenter;
import com.huijinlong.newretail.bean.CheckUpdateBean;
import com.huijinlong.newretail.net.MyCallback;
import com.huijinlong.newretail.net.RequestBuilder;
import com.huijinlong.newretail.ui.mine.LoginOutView;
import com.huijinlong.newretail.util.MD5Util;
import com.huijinlong.newretail.util.SpUtil;

import java.util.TreeMap;

/**
 * @author zsg
 * @date 2018/10/25 14:40
 * @Version 1.0
 * @Description: ()
 */
public class CheckUpdatePresent extends BasePresenter<CheckUpdateView> {
    private CheckUpdateView checkUpdateView;

    public CheckUpdatePresent(CheckUpdateView mViews) {
        this.checkUpdateView = mViews;
    }

    public void getCheckUpdate() {
        checkUpdateView.showLoading();
        TreeMap<String, String> params = new TreeMap<>();
        params.put("deviceType", "android");
        params.put("language", SpUtil.getString(SpUtil.LANGUAGE,"zh-cn"));
        params.put("appVersion", BaseApplication.GetVersion());
        RequestBuilder.execute(RequestBuilder.getNetService(false).getcheckUpdate(MD5Util.getSignValue(params), params), "", new MyCallback<CheckUpdateBean>() {
            @Override
            public void onSuccess(CheckUpdateBean result) {
                super.onSuccess(result);
                checkUpdateView.getCheck(result);
            }

            @Override
            public void onEmpty(CheckUpdateBean result) {
                super.onEmpty(result);

            }

            @Override
            public void onBusinessFailure(Exception e) {
                super.onBusinessFailure(e);
                checkUpdateView.showError(e.getMessage());
            }

            @Override
            public void onFinalResponse(boolean isSuccess) {
                super.onFinalResponse(isSuccess);
                checkUpdateView.hideLoading();
            }
        });
    }
}
