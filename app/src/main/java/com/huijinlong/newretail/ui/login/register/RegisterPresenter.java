package com.huijinlong.newretail.ui.login.register;

import com.huijinlong.newretail.base.BasePresenter;
import com.huijinlong.newretail.base.BaseBean;
import com.huijinlong.newretail.net.MyCallback;
import com.huijinlong.newretail.net.RequestBuilder;
import com.huijinlong.newretail.requestbean.LoginBean;
import com.huijinlong.newretail.util.MD5Util;
import com.huijinlong.newretail.util.SpUtil;

import java.util.Map;
import java.util.TreeMap;

//import me.jessyan.autosize.utils.LogUtils;

/**
 * Created by penghuaijin on 2018/9/12.
 */

public class RegisterPresenter extends BasePresenter<RegisterView> {

    private RegisterView registerView;

    public RegisterPresenter(RegisterView views) {
        this.registerView = views;
    }
    public void doRegister(String userAccount, String password, boolean type, String captcha) {
        TreeMap<String, String> params = new TreeMap<>();
        params.put("grant_type", "password");
        params.put("client_id", "3");
        params.put("client_secret", "uCGCHYmxfUEH1WNwTTiBrLkYU6qGXhHpyrgC8SID");
        params.put("nationality", SpUtil.getString(SpUtil.NATIONALITY, ""));
        params.put("registerType", type ? "email" : "mobile");
        params.put("key", userAccount);
        params.put("verificationCode", captcha);
        params.put("mobileCode", SpUtil.getString(SpUtil.MOBILE_CODE, "86"));
        params.put("password", password);
        params.put("rePassword", password);
        params.put("deviceType", "android");
        params.put("language", SpUtil.getString(SpUtil.LANGUAGE, "zh-cn"));

        RequestBuilder.execute(RequestBuilder.getNetService().register(MD5Util.getSignValue(params), params), "", new MyCallback<LoginBean>() {
            @Override
            public void onSuccess(LoginBean result) {
                super.onSuccess(result);
                registerView.showError(result.getMsg());
                registerView.registerSuccess(result);
            }

            @Override
            public void onEmpty(LoginBean result) {
                super.onEmpty(result);

            }

            @Override
            public void onBusinessFailure(Exception e) {
                super.onBusinessFailure(e);
                registerView.showError(e.getMessage());

            }

            @Override
            public void onFinalResponse(boolean isSuccess) {
                super.onFinalResponse(isSuccess);
                registerView.hideLoading();

            }
        });
    }
    public void getEmailCaptcha( String email, Map<String, String> map) {
        TreeMap<String, String> params = new TreeMap<>();
        params.put("email", email);
        params.put("action", "register");
        params.put("deviceType", "android");
        params.put("challenge", map.get("geetest_challenge"));
        params.put("validate", map.get("geetest_validate"));
        params.put("seccode", map.get("geetest_seccode"));
        params.put("language", SpUtil.getString(SpUtil.LANGUAGE, "zh-cn"));
        RequestBuilder.execute(RequestBuilder.getNetService().getEmailCaptcha(MD5Util.getSignValue(params), params), "", new MyCallback<LoginBean>() {
            @Override
            public void onSuccess(LoginBean result) {
                super.onSuccess(result);
                registerView.showError(result.getMsg());
                registerView.getCaptchaSuccess();
            }

            @Override
            public void onEmpty(LoginBean result) {
                super.onEmpty(result);

            }

            @Override
            public void onBusinessFailure(Exception e) {
                super.onBusinessFailure(e);
                registerView.showError(e.getMessage());

            }

            @Override
            public void onFinalResponse(boolean isSuccess) {
                super.onFinalResponse(isSuccess);
                registerView.hideLoading();

            }
        });
    }
    public void getPhoneCaptcha( String mobile, Map<String, String> map) {
        TreeMap<String, String> params = new TreeMap<>();
        params.put("mobile", mobile);
        params.put("action", "register");
        params.put("deviceType", "android");
        params.put("challenge", map.get("geetest_challenge"));
        params.put("validate", map.get("geetest_validate"));
        params.put("seccode", map.get("geetest_seccode"));
        params.put("language", SpUtil.getString(SpUtil.LANGUAGE, "zh-cn"));
        RequestBuilder.execute(RequestBuilder.getNetService().getPhoneCaptcha(MD5Util.getSignValue(params), params), "", new MyCallback<BaseBean>() {
            @Override
            public void onSuccess(BaseBean result) {
                super.onSuccess(result);
                registerView.showError(result.getMsg());
                registerView.getCaptchaSuccess();
            }

            @Override
            public void onEmpty(BaseBean result) {
                super.onEmpty(result);

            }

            @Override
            public void onBusinessFailure(Exception e) {
                super.onBusinessFailure(e);
                registerView.showError(e.getMessage());

            }

            @Override
            public void onFinalResponse(boolean isSuccess) {
                super.onFinalResponse(isSuccess);
                registerView.hideLoading();

            }
        });
    }

}
