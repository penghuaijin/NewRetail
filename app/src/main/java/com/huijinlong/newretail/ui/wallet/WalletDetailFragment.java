package com.huijinlong.newretail.ui.wallet;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.huijinlong.newretail.R;
import com.huijinlong.newretail.base.BaseFragment;
import com.huijinlong.newretail.bean.WithDrawsBean;
import com.huijinlong.newretail.util.ToastUtils;
import com.huijinlong.newretail.view.SimpleDividerDecoration;
import com.scwang.smartrefresh.header.MaterialHeader;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * @author zsg
 * @date 2018/10/25 16:47
 * @Version 1.0
 * @Description: (充币记录, 提币记录)
 */
public class WalletDetailFragment extends BaseFragment<WalletDetailView, WithDrawsPresent> implements WalletDetailView {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.refresh_layout)
    SmartRefreshLayout refreshLayout;
    Unbinder unbinder;
    @BindView(R.id.lin_nocontent)
    LinearLayout linNocontent;

    private WalletDetailAdapter walletDetailAdapter;
    private int page = 1;
    private static final String ARG_PARAM = "param";
    private static final String CURRENCY = "currency";
    private String mParam;
    private String currency;
    private List<WithDrawsBean.DataBean.ListBean> list = new ArrayList<>();

    public static WalletDetailFragment newInstance(String types, String currencys) {
        WalletDetailFragment fragment = new WalletDetailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM, types);
        args.putString(CURRENCY, currencys);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam = getArguments().getString(ARG_PARAM);
            currency = getArguments().getString(CURRENCY);
        }

    }


    @Override
    protected int getLayoutId() {
        return R.layout.fragment_list;
    }

    private void refreshData() {
        if (mParam.equals(getString(R.string.tixian))) {
            mPresenter.getwithdraws(page, 10, currency);
        } else {
            mPresenter.getdeposits(page, 10, currency);
        }

    }

    @Override
    protected void initData() {
        refreshData();
//        for(int x=0;x<list.size();x++)
//        {
//            list.get(x).setType(mParam);
//        }
        walletDetailAdapter = new WalletDetailAdapter(list);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.addItemDecoration(new SimpleDividerDecoration(getContext(), 10, 10, getResources().getColor(R.color.c_687), getResources().getDimensionPixelSize(R.dimen.divider_height)));
        walletDetailAdapter.bindToRecyclerView(recyclerView);

        refreshLayout.setRefreshHeader(new MaterialHeader(getContext()));
        refreshLayout.setOnRefreshListener(refreshlayout -> {
            page = 1;
            refreshData();
            refreshLayout.setEnableLoadmore(true);

        });
        refreshLayout.setOnLoadmoreListener(refreshlayout -> {
            page += 1;
            refreshData();
        });


    }

    @Override
    protected WithDrawsPresent initPresenter() {
        return new WithDrawsPresent(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void getWithDraw(WithDrawsBean withDrawsBean) {
        if(withDrawsBean.getData().getList().size()==0)
        {
            linNocontent.setVisibility(View.VISIBLE);
        }else
        {
            linNocontent.setVisibility(View.GONE);
            if (page == 1) {
                list.clear();
                refreshLayout.finishRefresh();
            } else {
                refreshLayout.finishLoadmore();
            }
            list.addAll(withDrawsBean.getData().getList());

            if (list.size() == withDrawsBean.getData().getTotal()) {
                refreshLayout.setEnableLoadmore(false);
            }
            walletDetailAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void showLoading() {
        mLoading.show();
    }

    @Override
    public void hideLoading() {
        mLoading.dismiss();
    }

    @Override
    public void showError(String error) {
        ToastUtils.toast(error);
    }
}
