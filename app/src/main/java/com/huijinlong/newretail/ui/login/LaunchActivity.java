package com.huijinlong.newretail.ui.login;

import android.app.Application;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;


import com.huijinlong.newretail.MainActivity;
import com.huijinlong.newretail.R;
import com.huijinlong.newretail.base.BaseActivity;
import com.huijinlong.newretail.cache.ACache;
import com.huijinlong.newretail.constant.Constant;
import com.huijinlong.newretail.requestbean.CountriesBean;
import com.huijinlong.newretail.requestbean.CurrentCountryIdBean;
import com.huijinlong.newretail.util.SpUtil;
import com.huijinlong.newretail.util.ToastUtils;

import java.util.Locale;

public class LaunchActivity extends BaseActivity<SystemView, SystemPresenter> implements SystemView {

    //   private IdentityAuthPresenter mPresenter;
    private int mCurrentCountryId;
    private ACache aCache;

//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
////        setContentView(R.layout.activity_launch);
//        getLanguage();
//
//        aCache = ACache.get(this);
//
//        if (TextUtils.isEmpty(SpUtil.getString(SpUtil.NATIONALITY, ""))) {
//            // mPresenter = new IdentityAuthPresenter(this);
//            mPresenter.getCurrentCountryId();
//        }
//
//
//        if (TextUtils.isEmpty(SpUtil.getString(SpUtil.AUTHORIZATION, ""))) {
//            startToActivity(this,LoginActivity.class);
//        } else {
//
//            if (SpUtil.getBoolean(SpUtil.LOGINGESTURE, true)) {
//                String gesturePassword = aCache.getAsString(Constant.GESTURE_PASSWORD);
//                if (gesturePassword == null || "".equals(gesturePassword)) {
//                    Intent intent = new Intent(LaunchActivity.this, CreateGestureActivity.class);
//                    startActivity(intent);
//                } else {
//                    Intent intent = new Intent(LaunchActivity.this, GestureLoginActivity.class);
//                    startActivity(intent);
//                }
//            } else {
//                Intent intent = new Intent(LaunchActivity.this, MainActivity.class);
//                startActivity(intent);
//
//            }
//
//        }
//        finish();
//    }

    @Override
    public SystemPresenter initPresenter() {
        return new SystemPresenter(this);
    }

    @Override
    protected void initView() {
        getWindow().setBackgroundDrawable(null);

        if (TextUtils.isEmpty(SpUtil.getString(SpUtil.LANGUAGE,""))){
            getLanguage();
        }else {

        }

        aCache = ACache.get(this);

        if (TextUtils.isEmpty(SpUtil.getString(SpUtil.NATIONALITY, ""))) {
            // mPresenter = new IdentityAuthPresenter(this);
            mPresenter.getCurrentCountryId();
        }


        if (TextUtils.isEmpty(SpUtil.getString(SpUtil.AUTHORIZATION, ""))) {
            startToActivity(this,LoginActivity.class);
        } else {

            if (SpUtil.getBoolean(SpUtil.LOGINGESTURE, true)) {
                String gesturePassword = aCache.getAsString(Constant.GESTURE_PASSWORD);
                if (gesturePassword == null || "".equals(gesturePassword)) {
                    Intent intent = new Intent(LaunchActivity.this, CreateGestureActivity.class);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(LaunchActivity.this, GestureLoginActivity.class);
                    startActivity(intent);
                }
            } else {
                Intent intent = new Intent(LaunchActivity.this, MainActivity.class);
                startActivity(intent);

            }

        }
        finish();
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_launch;
    }

    private void getLanguage() {
        Locale language;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            language = getResources().getConfiguration().getLocales().get(0);
        } else {
            language = getResources().getConfiguration().locale;
        }
        switch (language.getLanguage()) {
            case "en_US": {
                SpUtil.putString(SpUtil.LANGUAGE, "en-us");
                break;
            }
            case "zh_TW": {
                SpUtil.putString(SpUtil.LANGUAGE, "zh-tw");
                break;
            }
            default: {
                SpUtil.putString(SpUtil.LANGUAGE, "zh-cn");
                break;
            }
        }
    }



    @Override
    public void getCountriesSuccess(CountriesBean bean) {


        for (int i = 0; i < bean.getData().size(); i++) {

            if (mCurrentCountryId == bean.getData().get(i).getId()) {
                SpUtil.putString(SpUtil.MOBILE_CODE, bean.getData().get(i).getCode());
            }
        }
    }

    @Override
    public void getCurrentCountryIdSuccess(CurrentCountryIdBean bean) {
        mCurrentCountryId = bean.getData().getId();
        SpUtil.putString(SpUtil.NATIONALITY, mCurrentCountryId + "");
        mPresenter.getCountries();
    }

//    @Override
//    protected boolean translucentStatusBar() {
//        return true;
//    }

    @Override
    public void showLoading() {
        mLoadingDialog.show();
    }

    @Override
    public void hideLoading() {
        mLoadingDialog.dismiss();
    }

    @Override
    public void showError(String error) {
        ToastUtils.toast(error);
    }
}

