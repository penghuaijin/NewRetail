package com.huijinlong.newretail.ui.login.forget;

import com.huijinlong.newretail.base.BaseView;
import com.huijinlong.newretail.base.BaseBean;

/**
 *
 * Created by penghuaijin on 2018/9/12.
 */

public interface ForgetPasswordView extends BaseView{
        void forgetPwdSuccess(BaseBean bean);
        void getCaptchaSuccess(BaseBean bean);
}
