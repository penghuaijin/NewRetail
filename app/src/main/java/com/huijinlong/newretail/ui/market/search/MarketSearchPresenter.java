package com.huijinlong.newretail.ui.market.search;

import com.huijinlong.newretail.base.BasePresenter;
import com.huijinlong.newretail.bean.CurrenciesBean;
import com.huijinlong.newretail.net.MyCallback;
import com.huijinlong.newretail.net.RequestBuilder;
import com.huijinlong.newretail.util.MD5Util;
import com.huijinlong.newretail.util.SpUtil;

import java.util.TreeMap;

/**
 * Created by penghuaijin on 2018/9/12.
 */

public class MarketSearchPresenter extends BasePresenter<MarketSearchView> {

    private MarketSearchView marketSearchView;

    public MarketSearchPresenter(MarketSearchView mViews) {
        this.marketSearchView = mViews;
    }

    public void getcurrencies() {
        marketSearchView.showLoading();
        TreeMap<String, String> params = new TreeMap<>();
        params.put("deviceType", "android");
        params.put("language", SpUtil.getString(SpUtil.LANGUAGE, "zh-cn"));
        RequestBuilder.execute(RequestBuilder.getNetService(true).getCurrencies(MD5Util.getSignValue(params), params), "", new MyCallback<CurrenciesBean>() {
            @Override
            public void onSuccess(CurrenciesBean result) {
                super.onSuccess(result);
                marketSearchView.getHotSearch(result);
            }

            @Override
            public void onEmpty(CurrenciesBean result) {
                super.onEmpty(result);

            }

            @Override
            public void onBusinessFailure(Exception e) {
                super.onBusinessFailure(e);
                marketSearchView.showError(e.getMessage());
            }

            @Override
            public void onFinalResponse(boolean isSuccess) {
                super.onFinalResponse(isSuccess);
                marketSearchView.hideLoading();
            }
        });
    }


}
