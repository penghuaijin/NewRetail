package com.huijinlong.newretail.ui.wallet.withdrawcash;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.huijinlong.newretail.R;
import com.huijinlong.newretail.base.BaseActivity;
import com.huijinlong.newretail.base.BaseBean;
import com.huijinlong.newretail.bean.CurrenciesBean;
import com.huijinlong.newretail.bean.WithdrawsListBean;
import com.huijinlong.newretail.ui.wallet.CurrencyActivity;
import com.huijinlong.newretail.ui.wallet.addressmanage.AddressManageActivity;
import com.huijinlong.newretail.ui.wallet.addressmanage.withdraws.WithDrawsPresenter;
import com.huijinlong.newretail.ui.wallet.addressmanage.withdraws.WithDrawsView;
import com.huijinlong.newretail.ui.wallet.walletDetail.WalletDetailActivity;
import com.huijinlong.newretail.util.FontUtil;
import com.huijinlong.newretail.util.ToastUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.Serializable;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author zsg
 * @date 2018/11/13 17:21
 * @Version 1.0
 * @Description: (提现)
 */
public class WithDrawcashActivity extends BaseActivity<WithDrawsView, WithDrawsPresenter> implements WithDrawsView {

    @BindView(R.id.tv_choose)
    TextView tvChoose;
    @BindView(R.id.bt_add)
    Button btAdd;
    @BindView(R.id.tv_currency)
    TextView tvCurrency;
    @BindView(R.id.lin_currency)
    LinearLayout linCurrency;
    @BindView(R.id.tv_zfje)
    TextView tvZfje;
    @BindView(R.id.tv_txxe)
    TextView tvTxxe;
    @BindView(R.id.tv_txsxf)
    TextView tvTxsxf;

    private ArrayList<String> addresslist=new ArrayList<>();
    private int cid;//币种cid
    private String fee;//币种汇率
    private String limit;

    @Override
    protected void initView() {
        setToolbarTitle(getString(R.string.tixian));
        setmToolbarRightBtnColor(R.color.c_55a);
        setToolbarRightBtn(true, false, getString(R.string.txjl), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(WithDrawcashActivity.this, WalletDetailActivity.class);
                it.putExtra("title", getString(R.string.txjl));
                startActivity(it);
            }
        });
        FontUtil.injectFont(tvChoose);
        EventBus.getDefault().register(this);

        tvZfje.setText(String.format(getString(R.string.zfje), "0.01", "BTC"));
        tvTxxe.setText(String.format(getString(R.string.txxe), "2", "BTC"));
        tvTxsxf.setText(String.format(getString(R.string.txsxf),"0.005","0.02","BTC"));

    }

    @Override
    protected int getContentView() {
        return R.layout.activity_withdrawcash;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public WithDrawsPresenter initPresenter() {
        return new WithDrawsPresenter(this);
    }

    @SuppressLint("StringFormatMatches")
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getwithcurrency(CurrenciesBean.DataBean dataBean) {
        if (dataBean.getType().equals("WithDrawcash")) {
            tvCurrency.setText(dataBean.getCurrency() + "");
            mPresenter.getwithdraws(1, 100, 0);
            cid = dataBean.getCid();
            fee = dataBean.getFee();
            limit = dataBean.getLimit();
            tvZfje.setText(String.format(getString(R.string.zfje), limit, dataBean.getCurrency()));
            tvTxxe.setText(String.format(getString(R.string.txxe), "100000.00000", dataBean.getCurrency()));
            tvTxsxf.setText(String.format(getString(R.string.txsxf),"0",fee,dataBean.getCurrency()));
        }

    }

    @OnClick({R.id.tv_choose, R.id.bt_add, R.id.lin_currency})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_choose:
                break;
            case R.id.bt_add:
                startActivity(new Intent(this, AddressManageActivity.class));
                break;
            case R.id.lin_currency:
                Intent it = new Intent(this, CurrencyActivity.class);
                it.putExtra("type", "WithDrawcash");
                startActivity(it);
                break;
        }
    }

    @Override
    public void getaddressWithdrawList(WithdrawsListBean addressWithdrawListBean) {
        for (int x = 0; x < addressWithdrawListBean.getData().getList().size(); x++) {
            if (tvCurrency.getText().toString().equals(addressWithdrawListBean.getData().getList().get(x).getCurrency())) {
                if (TextUtils.isEmpty(addressWithdrawListBean.getData().getList().get(x).getAddress())) {
                } else {
                    addresslist.add(addressWithdrawListBean.getData().getList().get(x).getAddress());
                }
            }
        }
        if(addresslist.size()>0)
        {
            Intent it = new Intent(WithDrawcashActivity.this, WithDrawMoneyActivity.class);
            it.putExtra("currency", tvCurrency.getText().toString());
            it.putExtra("addresslist", addresslist);
            it.putExtra("cid", cid);
            it.putExtra("fee", fee);
            it.putExtra("limit", limit);
            // it.putExtra("type","all");
            startActivity(it);
            finish();
        }

    }

    @Override
    public void getWithdraw(BaseBean baseBean) {

    }

    @Override
    public void showLoading() {
        mLoadingDialog.show();
    }

    @Override
    public void hideLoading() {
        mLoadingDialog.dismiss();
    }

    @Override
    public void showError(String error) {
        ToastUtils.toast(error);
    }
}
