package com.huijinlong.newretail.ui.login;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.huijinlong.newretail.MainActivity;
import com.huijinlong.newretail.R;
import com.huijinlong.newretail.cache.ACache;
import com.huijinlong.newretail.constant.Constant;
import com.huijinlong.newretail.finger.FPerException;
import com.huijinlong.newretail.finger.FingerPrinterView;
import com.huijinlong.newretail.finger.IdentificationInfo;
import com.huijinlong.newretail.finger.RxFingerPrinter;
import com.huijinlong.newretail.util.SpUtil;
import com.huijinlong.newretail.util.ToastUtils;
import com.star.lockpattern.util.LockPatternUtil;
import com.star.lockpattern.widget.LockPatternView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.observers.DisposableObserver;

/**
 * Created by Sym on 2015/12/24.
 */
public class GestureLoginActivity extends AppCompatActivity {

    private static final String TAG = "LoginGestureActivity";
    @BindView(R.id.messageTv)
    TextView messageTv;
    @BindView(R.id.lockPatternView)
    LockPatternView lockPatternView;
    @BindView(R.id.forgetGestureBtn)
    Button forgetGestureBtn;
    @BindView(R.id.useFinger)
    Button useFinger;


    private ACache aCache;
    private static final long DELAYTIME = 600l;
    private byte[] gesturePassword;
    private FingerPrinterView fingerPrinterView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gesture_login);
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        aCache = ACache.get(GestureLoginActivity.this);
        //得到当前用户的手势密码
        gesturePassword = aCache.getAsBinary(Constant.GESTURE_PASSWORD);
        lockPatternView.setOnPatternListener(patternListener);
        updateStatus(Status.DEFAULT);
        if (!SpUtil.getBoolean(SpUtil.FINGER,false)) {
            useFinger.setVisibility(View.GONE);
        }
    }

    private LockPatternView.OnPatternListener patternListener = new LockPatternView.OnPatternListener() {

        @Override
        public void onPatternStart() {
            lockPatternView.removePostClearPatternRunnable();
        }

        @Override
        public void onPatternComplete(List<LockPatternView.Cell> pattern) {
            if (pattern != null) {
                if (LockPatternUtil.checkPattern(pattern, gesturePassword)) {
                    updateStatus(Status.CORRECT);
                } else {
                    updateStatus(Status.ERROR);
                }
            }
        }
    };

    /**
     * 更新状态
     *
     * @param status
     */
    private void updateStatus(Status status) {
        messageTv.setText(status.strId);
        messageTv.setTextColor(getResources().getColor(status.colorId));
        switch (status) {
            case DEFAULT:
                lockPatternView.setPattern(LockPatternView.DisplayMode.DEFAULT);
                break;
            case ERROR:
                lockPatternView.setPattern(LockPatternView.DisplayMode.ERROR);
                lockPatternView.postClearPatternRunnable(DELAYTIME);
                break;
            case CORRECT:
                lockPatternView.setPattern(LockPatternView.DisplayMode.DEFAULT);
                loginGestureSuccess();
                break;
        }
    }

    /**
     * 手势登录成功（去首页）
     */
    private void loginGestureSuccess() {
        SpUtil.putLong(SpUtil.OUT_TIME,System.currentTimeMillis());
        finish();
        startActivity(new Intent(this, MainActivity.class));
    }

    /**
     * 忘记手势密码（去账号登录界面）
     */
    @OnClick(R.id.forgetGestureBtn)
    void forgetGesturePasswrod() {
        Intent intent = new Intent(GestureLoginActivity.this, CreateGestureActivity.class);
        startActivity(intent);
        this.finish();
    }

    @OnClick(R.id.useFinger)
    public void onViewClicked() {
        // where this is an Activity instance
        RxFingerPrinter rxFingerPrinter = new RxFingerPrinter(this);
        AlertDialog dialog;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_finger, null);
        fingerPrinterView = view.findViewById(R.id.fpv);
        builder.setView(view);
        builder.setCancelable(false);
        builder.setMessage(getString(R.string.zwyz));
        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        fingerPrinterView.setOnStateChangedListener(new FingerPrinterView.OnStateChangedListener() {
            @Override
            public void onChange(int state) {
                if (state == FingerPrinterView.STATE_WRONG_PWD) {
                    fingerPrinterView.setState(FingerPrinterView.STATE_NO_SCANING);
                }
            }
        });

        dialog = builder.create();
        dialog.show();
        // 可以在oncreat方法中执行
        DisposableObserver<IdentificationInfo> observer =
                new DisposableObserver<IdentificationInfo>() {
                    @Override
                    protected void onStart() {
                        if (fingerPrinterView.getState() == FingerPrinterView.STATE_SCANING) {
                            return;
                        } else if (fingerPrinterView.getState() == FingerPrinterView.STATE_CORRECT_PWD
                                || fingerPrinterView.getState() == FingerPrinterView.STATE_WRONG_PWD) {
                            fingerPrinterView.setState(FingerPrinterView.STATE_NO_SCANING);
                        } else {
                            fingerPrinterView.setState(FingerPrinterView.STATE_SCANING);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }

                    @Override
                    public void onNext(IdentificationInfo info) {
                        if (info.isSuccessful()) {//识别成功
                            SpUtil.putLong(SpUtil.OUT_TIME,System.currentTimeMillis());

                            fingerPrinterView.setState(FingerPrinterView.STATE_CORRECT_PWD);
                            dialog.dismiss();
                            finish();
                            startActivity(new Intent(GestureLoginActivity.this, MainActivity.class));
                        } else {//识别失败 获取错误信息
                            FPerException exception = info.getException();
                            if (exception != null) {
                                ToastUtils.toast(exception.getDisplayMessage());
                            }
                            fingerPrinterView.setState(FingerPrinterView.STATE_WRONG_PWD);

                        }
                    }
                };
        rxFingerPrinter.begin().subscribe(observer);//RxfingerPrinter会自动在onPause()时暂停指纹监听，onResume()时恢复指纹监听)

    }

    private enum Status {
        //默认的状态
        DEFAULT(R.string.gesture_default, R.color.c_687),
        //密码输入错误
        ERROR(R.string.gesture_error, R.color.red_f3323b),
        //密码输入正确
        CORRECT(R.string.gesture_correct, R.color.c_687);

        private Status(int strId, int colorId) {
            this.strId = strId;
            this.colorId = colorId;
        }

        private int strId;
        private int colorId;
    }

    private long exitTime = 0;

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            exit();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }


    public void exit() {
        if ((System.currentTimeMillis() - exitTime) > 2000) {

            ToastUtils.toast("再按一次退出程序");
            exitTime = System.currentTimeMillis();
        } else {
            finish();
            System.exit(0);
        }
    }
}
