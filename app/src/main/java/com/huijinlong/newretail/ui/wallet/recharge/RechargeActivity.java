package com.huijinlong.newretail.ui.wallet.recharge;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.huijinlong.newretail.R;
import com.huijinlong.newretail.base.BaseActivity;
import com.huijinlong.newretail.base.BasePresenter;
import com.huijinlong.newretail.base.PagerAdapter;
import com.huijinlong.newretail.ui.wallet.exchange.ExchangeDetailActivity;
import com.huijinlong.newretail.ui.wallet.walletDetail.WalletDetailActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.support.design.widget.TabLayout.MODE_FIXED;

/**
 * @author zsg
 * @date 2018/10/26 16:20
 * @Version 1.0
 * @Description: (我的钱包充值)
 */
public class RechargeActivity extends BaseActivity {

    @BindView(R.id.tabs)
    TabLayout tabs;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.tv_currency)
    TextView tvCurrency;
    @BindView(R.id.tv_type)
    TextView tvType;
    @BindView(R.id.tv_count)
    TextView tvCount;
    @BindView(R.id.tv_opreat)
    TextView tvOpreat;
    @BindView(R.id.content)
    LinearLayout content;
    @BindView(R.id.vp_view)
    ViewPager vpView;
    private List<String> titles = new ArrayList<>();
    private List<Fragment> fragments = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
    }

    @Override
    public BasePresenter initPresenter() {
        return null;
    }

    @Override
    protected void initView() {
        setToolbarTitle(getString(R.string.wdqbcz));
        setmToolbarRightBtnColor(R.color.c_55a);
        setToolbarRightBtn(true, false, getString(R.string.czjl), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startToActivity(RechargeActivity.this, WalletDetailActivity.class);
            }
        });

        content.setVisibility(View.GONE);
        tabs.setTabMode(MODE_FIXED);

        titles.add(getString(R.string.dzcz));
        titles.add(getString(R.string.zjhz));

        fragments.add(RechargeAdressFragment.newInstance("1","BTC"));
        fragments.add(RechargeCapitalFragment.newInstance());

        vpView.setAdapter(new PagerAdapter(getSupportFragmentManager(), titles, fragments));
        tabs.setupWithViewPager(vpView);//将导航栏和viewpager进行关联
        tabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                vpView.setCurrentItem(tab.getPosition());//联动
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_walletdetail;
    }


}
