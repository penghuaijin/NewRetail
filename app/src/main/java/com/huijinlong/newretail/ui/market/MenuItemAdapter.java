package com.huijinlong.newretail.ui.market;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.widget.DrawerLayout;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.huijinlong.newretail.R;
import com.huijinlong.newretail.bean.LvMenuItem;
import com.huijinlong.newretail.ui.wallet.transactionDetails.TransactionDetailsActivity;
import com.huijinlong.newretail.ui.wallet.walletDetail.WalletDetailActivity;
import com.huijinlong.newretail.ui.wallet.addressmanage.AddressManageActivity;
import com.huijinlong.newretail.util.FontUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by penghuaijin on 2018/9/21.
 */


public class MenuItemAdapter extends BaseAdapter {
    private final int mIconSize;
    private LayoutInflater mInflater;
    private Context mContext;
    private List<LvMenuItem> mItems;
    private final DrawerLayout drawerLayout;


    public MenuItemAdapter(Context context, DrawerLayout drawerLayout) {
        mInflater = LayoutInflater.from(context);
        mContext = context;
        mIconSize = 24;
        initData(mContext);
        this.drawerLayout = drawerLayout;
    }

    private void initData(Context mContext) {
        mItems = new ArrayList<>(
                Arrays.asList(

                        new LvMenuItem(0, ""),
                        new LvMenuItem(0, "资产"),
                        new LvMenuItem(R.mipmap.detail, "交易账户"),
                        new LvMenuItem(R.mipmap.adress, "地址管理"),
                        new LvMenuItem(R.mipmap.finance, " 财务记录")
//                        new LvMenuItem()

                ));
    }


    @Override
    public int getCount() {
        return mItems.size();
    }


    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 3;
    }

    @Override
    public int getItemViewType(int position) {
        return mItems.get(position).type;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LvMenuItem item = mItems.get(position);
        switch (item.type) {
            case LvMenuItem.TYPE_NORMAL:
                if (convertView == null) {
                    convertView = mInflater.inflate(R.layout.design_drawer_item, parent,
                            false);
                }
                ImageView icon = convertView.findViewById(R.id.iv_icon);
                TextView text = convertView.findViewById(R.id.tv_text);
                text.setText(item.name);
                icon.setImageResource(item.icon);
                FontUtil.injectFont(icon);

                break;
            case LvMenuItem.TYPE_NO_ICON:
                if (convertView == null) {
                    convertView = mInflater.inflate(R.layout.design_drawer_item_subheader,
                            parent, false);
                }
                TextView subHeader = convertView.findViewById(R.id.tv_text);
                subHeader.setText(item.name);
                break;
            case LvMenuItem.TYPE_SEPARATOR:
                if (convertView == null) {
                    convertView = mInflater.inflate(R.layout.design_drawer_item_separator,
                            parent, false);
                    ImageView iconClose = convertView.findViewById(R.id.iv_icon);
                    iconClose.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            drawerLayout.closeDrawer(Gravity.START);
                        }
                    });

                }
                break;
        }
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (position) {
                    case 0:
                        break;
                    case 1:
                        break;
                    case 2:
                        mContext.startActivity(new Intent(mContext, TransactionDetailsActivity.class));
                        break;
                    case 3:
                        mContext.startActivity(new Intent(mContext, AddressManageActivity.class));
                        break;
                    case 4:
                        mContext.startActivity(new Intent(mContext, WalletDetailActivity.class));
                        break;
                }
            }
        });

        return convertView;
    }

    public void setIconColor(Drawable icon) {
        int textColorSecondary = android.R.attr.textColorSecondary;
        TypedValue value = new TypedValue();
        if (!mContext.getTheme().resolveAttribute(textColorSecondary, value, true)) {
            return;
        }
        int baseColor = mContext.getResources().getColor(value.resourceId);
        icon.setColorFilter(baseColor, PorterDuff.Mode.MULTIPLY);
    }
}
