package com.huijinlong.newretail.ui.wallet.addressmanage.withdraws;

import com.huijinlong.newretail.base.BaseView;
import com.huijinlong.newretail.base.BaseBean;
import com.huijinlong.newretail.bean.DeleteAdressBean;
import com.huijinlong.newretail.ui.wallet.addressmanage.EditAddressWithdrawBean;

/**
 * @author zsg
 * @date 2018/10/23 18:33
 * @Version 1.0
 * @Description: ()
 */
public interface WithDrawsGetCodeView extends BaseView{

    void getCodeSuccess(BaseBean baseBean);
    void getDelete(DeleteAdressBean deleteAdressBean);
    void getPhoneCapt(BaseBean baseBean);
    void getEdit(EditAddressWithdrawBean bean);
}
