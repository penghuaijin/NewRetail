package com.huijinlong.newretail.ui.mine.accountSecurity.bindphone;

import com.huijinlong.newretail.base.BaseView;
import com.huijinlong.newretail.requestbean.GetPhoneCaptcha4BindBean;
import com.huijinlong.newretail.requestbean.VerifyPhone4BindBean;

/**
 * Created by penghuaijin on 2018/9/12.
 */

public interface BindPhoneView extends BaseView{
        void showGetResult(String resultMessage);
        void getPhoneCaptchaSuccess(GetPhoneCaptcha4BindBean bean);
        void bindPhoneSuccess(VerifyPhone4BindBean bean);


}
