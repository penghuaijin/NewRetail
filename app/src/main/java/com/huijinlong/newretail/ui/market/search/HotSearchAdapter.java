package com.huijinlong.newretail.ui.market.search;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.huijinlong.newretail.R;

import java.util.List;

/**
 * @author fyales
 * @since 8/26/15.
 */
public class HotSearchAdapter extends BaseAdapter {

    private Context mContext;
    private List<String> mList;

    public HotSearchAdapter(Context context, List<String> list) {
        mContext = context;
        mList = list;

    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public String getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_hot_search_view, null);
            holder = new ViewHolder();
            holder.textView =convertView.findViewById(R.id.tv_hot);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder)convertView.getTag();
        }
        final String text = getItem(position);
        holder.textView.setText(text);

        return convertView;
    }

    static class ViewHolder {
        TextView textView;
    }

}
