package com.huijinlong.newretail.ui.spotgoods.kline;

import com.huijinlong.newretail.base.BasePresenter;
import com.huijinlong.newretail.net.MyCallback;
import com.huijinlong.newretail.net.OpenRequestBuilder;
import com.huijinlong.newretail.requestbean.KlineBean;
import com.huijinlong.newretail.util.SpUtil;

import java.util.TreeMap;

/**
 * @author zsg
 * @date 2018/10/25 14:40
 * @Version 1.0
 * @Description: ()
 */
public class KlinePresent extends BasePresenter<KlineView> {
    private KlineView klineView;

    public KlinePresent(KlineView mViews) {
        this.klineView = mViews;
    }


    //span  默认值: 1min
    //允许值: "1min", "3min", "5min", "15min", "30min", "1h", "2h", "4h", "6h", "12h", "1d", "1w", "1m"
    public void getKline(String  instrument,String span) {
        klineView.showLoading();
        TreeMap<String, String> params = new TreeMap<>();
        params.put("deviceType", "android");
        params.put("language", SpUtil.getString(SpUtil.LANGUAGE,"zh-cn"));
        params.put("instrument", instrument);
        params.put("span", span);

        OpenRequestBuilder.execute(OpenRequestBuilder.getNetService().getKline(instrument,span), "", new MyCallback<KlineBean>() {
            @Override
            public void onSuccess(KlineBean result) {
                super.onSuccess(result);
                klineView.getKline(result);
            }

            @Override
            public void onEmpty(KlineBean result) {
                super.onEmpty(result);

            }

            @Override
            public void onBusinessFailure(Exception e) {
                super.onBusinessFailure(e);
                klineView.showError(e.getMessage());
            }

            @Override
            public void onFinalResponse(boolean isSuccess) {
                super.onFinalResponse(isSuccess);
                klineView.hideLoading();
            }
        });
    }
}
