package com.huijinlong.newretail.ui.wallet.withdrawcash;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.huijinlong.newretail.R;
import com.huijinlong.newretail.base.BaseActivity;
import com.huijinlong.newretail.base.BasePresenter;
import com.huijinlong.newretail.bean.WithDrawsBean;
import com.huijinlong.newretail.ui.wallet.WalletDetailFragment;
import com.huijinlong.newretail.ui.wallet.WalletDetailView;
import com.huijinlong.newretail.ui.wallet.WithDrawsPresent;
import com.huijinlong.newretail.util.ToastUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author zsg
 * @date 2018/11/20 17:16
 * @Version 1.0
 * @Description: (提现记录)
 */
public class WithDrawListActivity extends BaseActivity {

    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.tv_currency)
    TextView tvCurrency;
    @BindView(R.id.tv_type)
    TextView tvType;
    @BindView(R.id.tv_count)
    TextView tvCount;
    @BindView(R.id.tv_opreat)
    TextView tvOpreat;
    @BindView(R.id.content)
    LinearLayout content;
    @BindView(R.id.fragment)
    FrameLayout fragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
    }

    @Override
    public BasePresenter initPresenter() {
        return null;
    }

    @Override
    protected void initView() {

        Intent intent = getIntent();
        String title=intent.getStringExtra("title");
        setToolbarTitle(title);

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragment, WalletDetailFragment.newInstance(title.substring(0,2),getIntent().getStringExtra("currency")))
                .commit();
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_withdrawlist;
    }

}
