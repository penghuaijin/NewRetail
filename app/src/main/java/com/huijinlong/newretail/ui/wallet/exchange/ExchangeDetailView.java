package com.huijinlong.newretail.ui.wallet.exchange;

import com.huijinlong.newretail.base.BaseView;
import com.huijinlong.newretail.bean.ExchangeDetailBean;

/**
 * @author zsg
 * @date 2018/11/20 15:02
 * @Version 1.0
 * @Description: ()
 */
public interface ExchangeDetailView extends BaseView{

    void getexchangdetail(ExchangeDetailBean exchangeDetailBean);

}
