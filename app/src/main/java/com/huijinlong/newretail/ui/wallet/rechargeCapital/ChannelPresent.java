package com.huijinlong.newretail.ui.wallet.rechargeCapital;

import com.huijinlong.newretail.base.BasePresenter;
import com.huijinlong.newretail.bean.ExchangeDetailBean;
import com.huijinlong.newretail.bean.WthdrawChannelBean;
import com.huijinlong.newretail.net.MyCallback;
import com.huijinlong.newretail.net.RequestBuilder;
import com.huijinlong.newretail.ui.wallet.exchange.ExchangeDetailView;
import com.huijinlong.newretail.util.MD5Util;
import com.huijinlong.newretail.util.SpUtil;

import java.util.TreeMap;

/**
 * @author zsg
 * @date 2018/11/20 15:04
 * @Version 1.0
 * @Description: ()
 */
public class ChannelPresent extends BasePresenter<ChannelView>{
    private ChannelView channelView;

    public ChannelPresent(ChannelView mViews) {
        this.channelView = mViews;
    }

    public void getChannel() {
        channelView.showLoading();
        TreeMap<String, String> params = new TreeMap<>();
        params.put("deviceType", "android");
        params.put("language", SpUtil.getString(SpUtil.LANGUAGE,"zh-cn"));

        RequestBuilder.execute(RequestBuilder.getNetService(false).getWthdrawChannel(MD5Util.getSignValue(params), params),
                "", new MyCallback<WthdrawChannelBean>() {
            @Override
            public void onSuccess(WthdrawChannelBean result) {
                super.onSuccess(result);
                channelView.getChannelSuccess(result);
            }

            @Override
            public void onEmpty(WthdrawChannelBean result) {
                super.onEmpty(result);

            }

            @Override
            public void onBusinessFailure(Exception e) {
                super.onBusinessFailure(e);
                channelView.showError(e.getMessage());
            }

            @Override
            public void onFinalResponse(boolean isSuccess) {
                super.onFinalResponse(isSuccess);
                channelView.hideLoading();
            }
        });
    }
}
