package com.huijinlong.newretail.ui.mine.authenticationsecond;

import com.huijinlong.newretail.base.BaseBean;
import com.huijinlong.newretail.base.BaseView;
import com.huijinlong.newretail.requestbean.CountriesBean;
import com.huijinlong.newretail.requestbean.CurrentCountryIdBean;
import com.huijinlong.newretail.requestbean.UserIdentityAuthBean;

/**
 * Created by penghuaijin on 2018/9/12.
 */

public interface IdentityAuthView extends BaseView{
        void editIdentityAuthSuccess(BaseBean bean);
        void getUserIdentityAuthSuccess(UserIdentityAuthBean bean);

}
