package com.huijinlong.newretail.ui.mine.accountSecurity.bindphone.countries;

import com.huijinlong.newretail.base.BasePresenter;
import com.huijinlong.newretail.net.MyCallback;
import com.huijinlong.newretail.net.RequestBuilder;
import com.huijinlong.newretail.requestbean.CountriesBean;
import com.huijinlong.newretail.util.MD5Util;
import com.huijinlong.newretail.util.SpUtil;

import java.util.TreeMap;

/**
 * Created by penghuaijin on 2018/9/12.
 */

public class CountriesPresenter extends BasePresenter<CountriesView> {

    private CountriesView countriesView;

    public CountriesPresenter(CountriesView mViews) {
        this.countriesView = mViews;
    }

    public void getCountries() {
        TreeMap<String, String> params = new TreeMap<>();
        params.put("deviceType", "android");
        params.put("language", SpUtil.getString(SpUtil.LANGUAGE,"zh-cn"));
        RequestBuilder.execute(RequestBuilder.getNetService().getCountries(MD5Util.getSignValue(params), params), "", new MyCallback<CountriesBean>() {
            @Override
            public void onSuccess(CountriesBean result) {
                super.onSuccess(result);
                countriesView.getCountriesSuccess(result);
            }

            @Override
            public void onEmpty(CountriesBean result) {
                super.onEmpty(result);

            }

            @Override
            public void onBusinessFailure(Exception e) {
                super.onBusinessFailure(e);
                countriesView.showError(e.getMessage());

            }

            @Override
            public void onFinalResponse(boolean isSuccess) {
                super.onFinalResponse(isSuccess);
                countriesView.hideLoading();

            }
        });
    }

}
