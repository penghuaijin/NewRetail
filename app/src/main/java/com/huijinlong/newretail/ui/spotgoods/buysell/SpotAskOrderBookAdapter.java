package com.huijinlong.newretail.ui.spotgoods.buysell;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.huijinlong.newretail.R;
import com.huijinlong.newretail.requestbean.DepthBean;
import com.huijinlong.newretail.util.LogUtils;
import com.huijinlong.newretail.util.ScreenUtil;

import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author penghuaijin
 * @date 2018/10/23 11:00 AM
 * @Description: ()
 */
public class SpotAskOrderBookAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int EMPTY_VIEW = 0;
    private final int NORMAL_VIEW = 1;

    private Context context;
    private List<DepthBean.DataBean.AsksBean> list;
    private int width;
    private NumberFormat priceNumberFormat ;
    private NumberFormat qtyNumberFormat;

    public AskClickListener clickListener;

    public void setClickListener(AskClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public void setPriceDecimal(int priceDecimal,int qtyDecimal) {


        priceNumberFormat.setMaximumFractionDigits(priceDecimal);
        priceNumberFormat.setMinimumFractionDigits(priceDecimal);
        qtyNumberFormat.setMaximumFractionDigits(qtyDecimal);
        qtyNumberFormat.setMinimumFractionDigits(qtyDecimal);

    }

    public SpotAskOrderBookAdapter(Context context, List<DepthBean.DataBean.AsksBean> list,int priceDecimal,int qtyDecimal) {
        this.context = context;
        this.list = list;
        this.width= ScreenUtil.getScreenWidth(context)/2;
        priceNumberFormat = NumberFormat.getNumberInstance();
        qtyNumberFormat = NumberFormat.getNumberInstance();

        priceNumberFormat.setMaximumFractionDigits(priceDecimal);
        priceNumberFormat.setMinimumFractionDigits(priceDecimal);
        qtyNumberFormat.setMaximumFractionDigits(qtyDecimal);
        qtyNumberFormat.setMinimumFractionDigits(qtyDecimal);

        priceNumberFormat.setRoundingMode(RoundingMode.DOWN);//表示舍
        qtyNumberFormat.setRoundingMode(RoundingMode.DOWN);//表示舍
    }

    @Override
    public int getItemViewType(int position) {

        if (position < list.size()) {
            return NORMAL_VIEW;
        } else {
            return EMPTY_VIEW;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view;
        if (viewType == NORMAL_VIEW) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_spot_recycle_depth, viewGroup, false);
            return new NormalViewHolder(view);
        } else {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_spot_recycle_depth, viewGroup, false);
            return new EmptyViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {

        if (viewHolder instanceof EmptyViewHolder) {
            ((EmptyViewHolder) viewHolder).tvNum.setText("ㄧㄧ");
            ((EmptyViewHolder) viewHolder).tvPrice.setText("    ㄧㄧ");
//            ((EmptyViewHolder) viewHolder).tvPrice.setTextColor(context.getResources().getColor(R.color.c_55a));

        } else if (viewHolder instanceof NormalViewHolder) {
            DepthBean.DataBean.AsksBean bean = list.get(position);
            if (bean==null){
                return;
            }
            CopyOnWriteArrayList<DepthBean.DataBean.AsksBean> arrayList = new CopyOnWriteArrayList<>();
            arrayList.addAll(list);
            float maxDeep = 0;
            for (DepthBean.DataBean.AsksBean asksBean : arrayList) {
                if (asksBean!=null&&Float.parseFloat(asksBean.getQuantity()) > maxDeep) {
                    maxDeep = Float.parseFloat(asksBean.getQuantity());
                }
            }
            float currentDeep = Float.parseFloat(bean.getQuantity()) / maxDeep;


            final ViewGroup.LayoutParams lp = ((NormalViewHolder) viewHolder).tvDeep.getLayoutParams();
            lp.width = (int) (currentDeep * width);
            ((NormalViewHolder) viewHolder).tvDeep.setLayoutParams(lp);
            ((NormalViewHolder) viewHolder).tvDeep.setBackgroundColor(Color.parseColor("#55FF5B5B"));


//            ((NormalViewHolder) viewHolder).tvPrice.setTextColor(context.getResources().getColor(R.color.c_55a));
            String getQuantity = qtyNumberFormat.format(Double.parseDouble(bean.getQuantity()));
            ((NormalViewHolder) viewHolder).tvNum.setText(getQuantity);


            String getPrice = priceNumberFormat.format(Double.parseDouble(bean.getPrice()));
            ((NormalViewHolder) viewHolder).tvPrice.setText(getPrice);

            ((NormalViewHolder) viewHolder).llItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickListener.onItemClick(bean.getPrice());
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return 8;
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    static class NormalViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_deep)
        TextView tvDeep;
        @BindView(R.id.tv_price)
        TextView tvPrice;
        @BindView(R.id.tv_num)
        TextView tvNum;
        @BindView(R.id.ll_item)
        LinearLayout llItem;
        NormalViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    static class EmptyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_deep)
        TextView tvDeep;
        @BindView(R.id.tv_price)
        TextView tvPrice;
        @BindView(R.id.tv_num)
        TextView tvNum;
        @BindView(R.id.ll_item)
        LinearLayout llItem;
        EmptyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public interface AskClickListener {
        void onItemClick(String price);
    }


}
