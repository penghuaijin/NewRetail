package com.huijinlong.newretail.ui.login.forget;

import com.huijinlong.newretail.base.BasePresenter;
import com.huijinlong.newretail.base.BaseBean;
import com.huijinlong.newretail.net.MyCallback;
import com.huijinlong.newretail.net.RequestBuilder;
import com.huijinlong.newretail.util.MD5Util;
import com.huijinlong.newretail.util.SpUtil;

import java.util.Map;
import java.util.TreeMap;

/**
 *
 * Created by penghuaijin on 2018/9/12.
 */

public class ForgetPasswordPresenter extends BasePresenter<ForgetPasswordView> {

    private ForgetPasswordView forgetPasswordView;

    public ForgetPasswordPresenter(ForgetPasswordView mViews) {
        this.forgetPasswordView = mViews;
    }


    public void forgetPwd(String key,String password,String rePassword,String verificationCode,String resetType) {

        TreeMap<String, String> params = new TreeMap<>();
        params.put("key", key);
        params.put("resetType", resetType);
        params.put("password", password);
        params.put("rePassword", rePassword);
        params.put("verificationCode", verificationCode);
        params.put("deviceType", "android");
        params.put("language", SpUtil.getString(SpUtil.LANGUAGE,"zh-cn"));
        RequestBuilder.execute(RequestBuilder.getNetService(true).forgetPwd(MD5Util.getSignValue(params), params), "", new MyCallback<BaseBean>() {
            @Override
            public void onSuccess(BaseBean result) {
                super.onSuccess(result);
                forgetPasswordView.forgetPwdSuccess(result);
            }

            @Override
            public void onEmpty(BaseBean result) {
                super.onEmpty(result);

            }

            @Override
            public void onBusinessFailure(Exception e) {
                super.onBusinessFailure(e);
                forgetPasswordView.showError(e.getMessage());

            }

            @Override
            public void onFinalResponse(boolean isSuccess) {
                super.onFinalResponse(isSuccess);
                forgetPasswordView.hideLoading();

            }
        });
    }

    public void getPhoneCaptcha( String mobile, Map<String, String> map) {
        TreeMap<String, String> params = new TreeMap<>();
        params.put("mobile", mobile);
        params.put("action", "forgetPwd");
        params.put("deviceType", "android");
        params.put("challenge", map.get("geetest_challenge"));
        params.put("validate", map.get("geetest_validate"));
        params.put("seccode", map.get("geetest_seccode"));
        params.put("language", SpUtil.getString(SpUtil.LANGUAGE, "zh-cn"));
        RequestBuilder.execute(RequestBuilder.getNetService().getPhoneCaptcha(MD5Util.getSignValue(params), params), "", new MyCallback<BaseBean>() {
            @Override
            public void onSuccess(BaseBean result) {
                super.onSuccess(result);
                forgetPasswordView.getCaptchaSuccess(result);
            }

            @Override
            public void onEmpty(BaseBean result) {
                super.onEmpty(result);

            }

            @Override
            public void onBusinessFailure(Exception e) {
                super.onBusinessFailure(e);
                forgetPasswordView.showError(e.getMessage());

            }

            @Override
            public void onFinalResponse(boolean isSuccess) {
                super.onFinalResponse(isSuccess);
                forgetPasswordView.hideLoading();

            }
        });
    }


}
