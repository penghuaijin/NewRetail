package com.huijinlong.newretail.ui.wallet.withdrawcash;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;

import com.huijinlong.newretail.R;
import com.huijinlong.newretail.base.BaseActivity;
import com.huijinlong.newretail.base.BasePresenter;
import com.huijinlong.newretail.ui.wallet.recharge.RechargeActivity;
import com.huijinlong.newretail.ui.wallet.recharge.RechargeAdressFragment;
import com.huijinlong.newretail.ui.wallet.walletDetail.WalletDetailActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author zsg
 * @date 2018/11/22 19:36
 * @Version 1.0
 * @Description: (我的钱包充值)
 */
public class RechargeAdressActivity extends BaseActivity {

    @BindView(R.id.fragment)
    FrameLayout fragment;

    @Override
    public BasePresenter initPresenter() {
        return null;
    }

    @Override
    protected void initView() {
        setToolbarTitle(getString(R.string.wdqbcz));
        setmToolbarRightBtnColor(R.color.c_55a);
        setToolbarRightBtn(true, false, getString(R.string.czjl), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(RechargeAdressActivity.this, WithDrawListActivity.class);
                it.putExtra("title", getString(R.string.cbjl));
                it.putExtra("currency",getIntent().getStringExtra("currency"));
                startActivity(it);
            }
        });
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragment, RechargeAdressFragment.newInstance(getIntent().getStringExtra("cid"),getIntent().getStringExtra("currency")))
                .commit();
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_rechargeadress;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
