package com.huijinlong.newretail.ui.wallet.exchange;

import com.huijinlong.newretail.base.BasePresenter;
import com.huijinlong.newretail.bean.ExchangeDetailBean;
import com.huijinlong.newretail.net.MyCallback;
import com.huijinlong.newretail.net.RequestBuilder;
import com.huijinlong.newretail.util.MD5Util;
import com.huijinlong.newretail.util.SpUtil;

import java.util.TreeMap;

/**
 * @author zsg
 * @date 2018/11/20 15:04
 * @Version 1.0
 * @Description: ()
 */
public class ExchangeDetailPresent extends BasePresenter<ExchangeDetailView>{
    private ExchangeDetailView exchangeDetailView;

    public ExchangeDetailPresent(ExchangeDetailView mViews) {
        this.exchangeDetailView = mViews;
    }

    public void getwithdraws(int currentPage,int pageSize,String currency) {
        exchangeDetailView.showLoading();
        TreeMap<String, String> params = new TreeMap<>();
        params.put("deviceType", "android");
        params.put("language", SpUtil.getString(SpUtil.LANGUAGE,"zh-cn"));

        params.put("currentPage", String.valueOf(currentPage));
        params.put("pageSize",  String.valueOf(pageSize));
        params.put("currency",  currency);

        RequestBuilder.execute(RequestBuilder.getNetService(true).getExchanggeDetail(MD5Util.getSignValue(params), params),
                "", new MyCallback<ExchangeDetailBean>() {
            @Override
            public void onSuccess(ExchangeDetailBean result) {
                super.onSuccess(result);
                exchangeDetailView.getexchangdetail(result);
            }

            @Override
            public void onEmpty(ExchangeDetailBean result) {
                super.onEmpty(result);

            }

            @Override
            public void onBusinessFailure(Exception e) {
                super.onBusinessFailure(e);
                exchangeDetailView.showError(e.getMessage());
            }

            @Override
            public void onFinalResponse(boolean isSuccess) {
                super.onFinalResponse(isSuccess);
                exchangeDetailView.hideLoading();
            }
        });
    }
}
