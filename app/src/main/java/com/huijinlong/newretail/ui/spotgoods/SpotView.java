package com.huijinlong.newretail.ui.spotgoods;

import com.huijinlong.newretail.base.BaseView;
import com.huijinlong.newretail.requestbean.SpotProductsBean;

public interface SpotView extends BaseView{
		void spotProductsSuccess(SpotProductsBean spotProductsBean);
}
