package com.huijinlong.newretail.ui.wallet.recharge;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.huijinlong.newretail.R;
import com.huijinlong.newretail.base.BaseFragment;
import com.huijinlong.newretail.bean.CurrenciesBean;
import com.huijinlong.newretail.requestbean.WalletAddressBean;
import com.huijinlong.newretail.ui.wallet.CurrencyActivity;
import com.huijinlong.newretail.util.FontUtil;
import com.huijinlong.newretail.util.ToastUtils;
import com.uuzuche.lib_zxing.activity.CodeUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * @author zsg
 * @date 2018/10/26 16:23
 * @Version 1.0
 * @Description: (地址充币)
 */
public class RechargeAdressFragment extends BaseFragment<RechargeAddressView, RechargeAddressPresent> implements RechargeAddressView {

    @BindView(R.id.tv_choose)
    TextView tvChoose;
    @BindView(R.id.bt_add)
    Button btAdd;
    Unbinder unbinder;
    @BindView(R.id.img)
    ImageView img;
    @BindView(R.id.tv_address)
    TextView tvAddress;
    @BindView(R.id.tv_currency)
    TextView tvCurrency;

    private static final String ARG_PARAM = "param";
    private static final String CURRENCY = "currency";
    @BindView(R.id.re_spinner)
    RelativeLayout reSpinner;
    @BindView(R.id.tv_tssm)
    TextView tvTssm;
    @BindView(R.id.tv_czsm)
    TextView tvCzsm;
    private String currencys;
    private String mParam;
    private String limit;

    public static RechargeAdressFragment newInstance(String types, String currency) {
        RechargeAdressFragment fragment = new RechargeAdressFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM, types);
        args.putString(CURRENCY, currency);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.fragment_rechargeadress;
    }

    @SuppressLint("StringFormatMatches")
    @Override
    protected void initData() {
        FontUtil.injectFont(tvChoose);
        if (TextUtils.isEmpty(mParam)) {
            mPresenter.getRechargeAddress(1);
            tvCurrency.setText(getString(R.string.qingxuanze));
        } else {
            mPresenter.getRechargeAddress(Integer.valueOf(mParam));
            tvCurrency.setText(currencys);
        }
        tvTssm.setText(String.format(getString(R.string.tssm), currencys, currencys, currencys,limit));
        tvCzsm.setText(String.format(getString(R.string.czsm), currencys, currencys));
    }

    @Override
    protected RechargeAddressPresent initPresenter() {
        return new RechargeAddressPresent(this);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam = getArguments().getString(ARG_PARAM);
            currencys = getArguments().getString(CURRENCY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        EventBus.getDefault().register(this);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        EventBus.getDefault().unregister(this);
    }


    @Override
    public void showLoading() {
        mLoading.show();
    }

    @Override
    public void hideLoading() {
        mLoading.dismiss();
    }

    @Override
    public void showError(String error) {
        ToastUtils.toast(error);
        if (error.equals("此功能暂未开放！")) {
            tvAddress.setText("null");
        }
    }

    @Override
    public void getAddressSuccess(WalletAddressBean walletAddressBean) {
        tvAddress.setText(walletAddressBean.getData().getAddress());
        Bitmap bitmap = CodeUtils.createImage(walletAddressBean.getData().getAddress(), 480, 480, null);
        img.setImageBitmap(bitmap);
    }

    @SuppressLint("StringFormatMatches")
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getadresscurrency(CurrenciesBean.DataBean dataBean) {
        if (dataBean.getType().equals("RechargeAdress")) {
            tvCurrency.setText(dataBean.getCurrency() + "");
            mPresenter.getRechargeAddress(dataBean.getCid());
            limit=dataBean.getLimit();

            tvTssm.setText(String.format(getString(R.string.tssm), dataBean.getCurrency(), dataBean.getCurrency(), dataBean.getCurrency(),limit));
            tvCzsm.setText(String.format(getString(R.string.czsm), dataBean.getCurrency(), dataBean.getCurrency()));
        }
    }


    @OnClick({R.id.re_spinner, R.id.bt_add})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.re_spinner:
                Intent it = new Intent(getContext(), CurrencyActivity.class);
                it.putExtra("type", "RechargeAdress");
                startActivity(it);
                break;
            case R.id.bt_add:
                // 获取系统剪贴板
                ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                // 创建一个剪贴数据集，包含一个普通文本数据条目（需要复制的数据）
                ClipData clipData = ClipData.newPlainText(null, tvAddress.getText().toString());
                // 把数据集设置（复制）到剪贴板
                clipboard.setPrimaryClip(clipData);
                ToastUtils.toast("复制成功");

                break;
        }
    }


    //系统剪贴板-获取:   s为内容
    public static String getCopy(Context context) {
        // 获取系统剪贴板
        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        // 返回数据
        ClipData clipData = clipboard.getPrimaryClip();
        if (clipData != null && clipData.getItemCount() > 0) {
            // 从数据集中获取（粘贴）第一条文本数据
            return clipData.getItemAt(0).getText().toString();
        }
        return null;
    }


}
