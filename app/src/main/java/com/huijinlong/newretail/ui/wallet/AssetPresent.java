package com.huijinlong.newretail.ui.wallet;

import com.huijinlong.newretail.base.BasePresenter;
import com.huijinlong.newretail.bean.AssetBean;
import com.huijinlong.newretail.net.MyCallback;
import com.huijinlong.newretail.net.RequestBuilder;
import com.huijinlong.newretail.util.MD5Util;
import com.huijinlong.newretail.util.SpUtil;

import java.util.TreeMap;

/**
 * @author zsg
 * @date 2018/10/26 11:14
 * @Version 1.0
 * @Description: ()
 */
public class AssetPresent extends BasePresenter<AssetView>{

    private AssetView assetView;

    public AssetPresent(AssetView mViews) {
        this.assetView = mViews;
    }

    public void getasset(int hidePenny,String currency,int type)
    {
        assetView.showLoading();
        TreeMap<String, String> params = new TreeMap<>();
        params.put("deviceType", "android");
        params.put("language", SpUtil.getString(SpUtil.LANGUAGE,"zh-cn"));

        params.put("hidePenny", String.valueOf(hidePenny));
        params.put("currency", currency);
        params.put("type", String.valueOf(type));

        RequestBuilder.execute(RequestBuilder.getNetService(true).getAsset(MD5Util.getSignValue(params), params), "", new MyCallback<AssetBean>() {
            @Override
            public void onSuccess(AssetBean result) {
                super.onSuccess(result);
                assetView.getAsset(result);
            }

            @Override
            public void onEmpty(AssetBean result) {
                super.onEmpty(result);

            }

            @Override
            public void onBusinessFailure(Exception e) {
                super.onBusinessFailure(e);
                assetView.showError(e.getMessage());
            }

            @Override
            public void onFinalResponse(boolean isSuccess) {
                super.onFinalResponse(isSuccess);
                assetView.hideLoading();
            }
        });
    }
}
