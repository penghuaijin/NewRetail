package com.huijinlong.newretail.ui.spotgoods.buysell;

import com.huijinlong.newretail.base.BasePresenter;
import com.huijinlong.newretail.base.BaseBean;
import com.huijinlong.newretail.net.MyCallback;
import com.huijinlong.newretail.net.OpenRequestBuilder;
import com.huijinlong.newretail.net.RequestBuilder;
import com.huijinlong.newretail.requestbean.DepthBean;
import com.huijinlong.newretail.bean.AssetBean;
import com.huijinlong.newretail.util.MD5Util;
import com.huijinlong.newretail.util.SpUtil;

import java.util.TreeMap;

/**
 * Created by penghuaijin on 2018/9/12.
 */

public class DepthlPresenter extends BasePresenter<DepthlView> {

    private DepthlView depthlView;

    public DepthlPresenter(DepthlView depthlViews) {
        depthlView = depthlViews;
    }

    public void getDepth(String instrument, String limit) {
        depthlView.showLoading();
        TreeMap<String, String> params = new TreeMap<>();
        params.put("instrument", instrument);
        params.put("limit", limit);
        OpenRequestBuilder.execute(OpenRequestBuilder.getNetService().getDepth(instrument, limit), "", new MyCallback<DepthBean>() {
            @Override
            public void onSuccess(DepthBean result) {
                super.onSuccess(result);
                depthlView.getSuccess(result);
            }

            @Override
            public void onEmpty(DepthBean result) {
                super.onEmpty(result);

            }

            @Override
            public void onBusinessFailure(Exception e) {
                super.onBusinessFailure(e);
                depthlView.showGetResult(e.getMessage());

            }

            @Override
            public void onFinalResponse(boolean isSuccess) {
                super.onFinalResponse(isSuccess);
                depthlView.hideLoading();
            }
        });
    }

    public void getWalletAsset(String currency, int hidePenny) {
        depthlView.showLoading();
        TreeMap<String, String> params = new TreeMap<>();
        params.put("currency", currency);
        params.put("hidePenny", hidePenny + "");
        params.put("deviceType", "android");
        params.put("language", SpUtil.getString(SpUtil.LANGUAGE, "zh-cn"));
        RequestBuilder.execute(RequestBuilder.getNetService(true).getAsset(MD5Util.getSignValue(params), params), "", new MyCallback<AssetBean>() {
            @Override
            public void onSuccess(AssetBean result) {
                super.onSuccess(result);
                depthlView.getWalletAssetSuccess(result);
            }

            @Override
            public void onEmpty(AssetBean result) {
                super.onEmpty(result);

            }

            @Override
            public void onBusinessFailure(Exception e) {
                super.onBusinessFailure(e);
                depthlView.showError(e.getMessage());

            }

            @Override
            public void onFinalResponse(boolean isSuccess) {
                super.onFinalResponse(isSuccess);
                depthlView.hideLoading();
            }
        });
    }

    /**
     *
     * @param instrument 货币标识 (btcusdt)
     * @param side 订单操作 bid = 买 ask = 卖
     * @param type 订单类型 limit = 限价 market = 市价
     * @param fill_strategy 成交类型，默认是gtc gtc = 一直有效 ioc = 立即执行或取消 fok = 全部成交或取消
     * @param qty 限价单表示下单数量，市价买单时表示买多少钱，市价卖单时表示卖多少币
     * @param price 下单价格，市价不需要
     */
    public void orderPlace(String instrument, String side,String type,String fill_strategy,String qty,String price) {
        depthlView.showLoading();
        TreeMap<String, String> params = new TreeMap<>();
        params.put("instrument", instrument);
        params.put("side", side );
        params.put("type", type );
        params.put("fill_strategy", fill_strategy );
        params.put("qty", qty );
        params.put("price", price );
        params.put("deviceType", "android");
        params.put("language", SpUtil.getString(SpUtil.LANGUAGE, "zh-cn"));
        RequestBuilder.execute(RequestBuilder.getNetService(true).orderPlace(MD5Util.getSignValue(params), params), "", new MyCallback<BaseBean>() {
            @Override
            public void onSuccess(BaseBean result) {
                super.onSuccess(result);
                depthlView.orderPlaceSuccess(result);
            }

            @Override
            public void onEmpty(BaseBean result) {
                super.onEmpty(result);

            }

            @Override
            public void onBusinessFailure(Exception e) {
                super.onBusinessFailure(e);
                depthlView.showError(e.getMessage());

            }

            @Override
            public void onFinalResponse(boolean isSuccess) {
                super.onFinalResponse(isSuccess);
                depthlView.hideLoading();
            }
        });
    }




}
