package com.huijinlong.newretail.ui.wallet.addressmanage;

import com.huijinlong.newretail.base.BaseBean;

/**
 * @author zsg
 * @date 2018/10/22 14:28
 * @Version 1.0
 * @Description: ()
 */
public class EditAddressWithdrawBean extends BaseBean {

    /**
     * status : 200
     * msg : ok
     * data : {"verifyMobile":1,"verifyEmail":1,"verifyGoogleAuthenticator":0}
     */
    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * verifyMobile : 1
         * verifyEmail : 1
         * verifyGoogleAuthenticator : 0
         */

        private int verifyMobile;
        private int verifyEmail;
        private int verifyGoogleAuthenticator;

        public int getVerifyMobile() {
            return verifyMobile;
        }

        public void setVerifyMobile(int verifyMobile) {
            this.verifyMobile = verifyMobile;
        }

        public int getVerifyEmail() {
            return verifyEmail;
        }

        public void setVerifyEmail(int verifyEmail) {
            this.verifyEmail = verifyEmail;
        }

        public int getVerifyGoogleAuthenticator() {
            return verifyGoogleAuthenticator;
        }

        public void setVerifyGoogleAuthenticator(int verifyGoogleAuthenticator) {
            this.verifyGoogleAuthenticator = verifyGoogleAuthenticator;
        }
    }
}
