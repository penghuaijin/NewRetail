package com.huijinlong.newretail.ui.spotgoods;


import android.text.TextUtils;

import com.huijinlong.newretail.base.BasePresenter;
import com.huijinlong.newretail.net.MyCallback;
import com.huijinlong.newretail.net.RequestBuilder;
import com.huijinlong.newretail.requestbean.SpotProductsBean;
import com.huijinlong.newretail.util.MD5Util;
import com.huijinlong.newretail.util.SpUtil;

import java.util.TreeMap;

public class SpotPresenter extends BasePresenter<SpotView> {
    private SpotView spotView;

    public SpotPresenter(SpotView mViews) {
        this.spotView = mViews;
    }
    public void getsSpotProducts() {
        spotView.showLoading();

        TreeMap<String, String> params = new TreeMap<>();

        params.put("deviceType", "android");
        params.put("language", SpUtil.getString(SpUtil.LANGUAGE, "zh-cn"));
        boolean isNeedAuthorization= !TextUtils.isEmpty(SpUtil.getString(SpUtil.AUTHORIZATION,""));
        RequestBuilder.execute(RequestBuilder.getNetService(isNeedAuthorization).getsSpotProducts(MD5Util.getSignValue(params), params), "", new MyCallback<SpotProductsBean>() {
            @Override
            public void onSuccess(SpotProductsBean result) {
                super.onSuccess(result);
                spotView.spotProductsSuccess(result);
            }

            @Override
            public void onEmpty(SpotProductsBean result) {
                super.onEmpty(result);

            }

            @Override
            public void onBusinessFailure(Exception e) {
                super.onBusinessFailure(e);
                spotView.showError(e.getMessage());

            }

            @Override
            public void onFinalResponse(boolean isSuccess) {
                super.onFinalResponse(isSuccess);
                spotView.hideLoading();
            }
        });
    }
}
