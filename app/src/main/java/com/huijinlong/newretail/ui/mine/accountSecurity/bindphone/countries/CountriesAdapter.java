package com.huijinlong.newretail.ui.mine.accountSecurity.bindphone.countries;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.huijinlong.newretail.R;
import com.huijinlong.newretail.bean.GDCountriesBean;
import com.huijinlong.newretail.util.SpUtil;

import java.util.List;

/**
 * Created by penghuaijin on 2018/9/27.
 */

public class CountriesAdapter extends RecyclerView.Adapter<CountriesAdapter.ViewHolder> {
    private List<GDCountriesBean> mData;
    private Activity activity;

    public CountriesAdapter(Activity activity, List<GDCountriesBean> mData) {
        this.mData = mData;
        this.activity = activity;
    }

    public void setData(List<GDCountriesBean> data) {
        mData = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(activity).inflate(R.layout.item_countries, parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {

        viewHolder.tv_nameEn.setText(mData.get(i).getNameEn());
        viewHolder.tv_nameCn.setText(mData.get(i).getNameCn());
        viewHolder.ll_country.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SpUtil.putString(SpUtil.MOBILE_CODE, mData.get(i).getCode()+"");
                Intent intent = new Intent();
                intent.putExtra("code", mData.get(i).getCode()+"");
                activity.setResult(Activity.RESULT_OK, intent);

                activity.finish();
            }
        });
    }



    @Override
    public int getItemCount() {
        return mData == null ? 0 : mData.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_nameEn;
        TextView tv_nameCn;
        LinearLayout ll_country;

        ViewHolder(View itemView) {
            super(itemView);
            tv_nameEn = itemView.findViewById(R.id.tv_nameEn);
            tv_nameCn = itemView.findViewById(R.id.tv_nameCn);
            ll_country = itemView.findViewById(R.id.ll_country);
        }
    }
}
