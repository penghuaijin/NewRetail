package com.huijinlong.newretail.ui.spotgoods.commission;

import com.huijinlong.newretail.base.BaseView;
import com.huijinlong.newretail.base.BaseBean;
import com.huijinlong.newretail.requestbean.CurrentCommissionBean;
import com.huijinlong.newretail.requestbean.HistoryBean;

/**
 * @author penghuaijin
 * @date 2018/9/12 5:24 PM
 * @Description: ()
 */

public interface CommissionView extends BaseView {


    void getHistorySuccess(HistoryBean bean);

    void getCurrentSuccess(CurrentCommissionBean bean);

    void orderSingleCancel(BaseBean bean);

}
