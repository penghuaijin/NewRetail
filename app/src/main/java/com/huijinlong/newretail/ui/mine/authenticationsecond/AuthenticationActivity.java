package com.huijinlong.newretail.ui.mine.authenticationsecond;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.huijinlong.newretail.R;
import com.huijinlong.newretail.base.BaseActivity;
import com.huijinlong.newretail.base.BaseBean;
import com.huijinlong.newretail.base.BasePresenter;
import com.huijinlong.newretail.requestbean.UserIdentityAuthBean;
import com.huijinlong.newretail.util.FontUtil;
import com.huijinlong.newretail.util.ToastUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by penghuaijin on 2018/9/29.
 */

public class AuthenticationActivity extends BaseActivity<IdentityAuthView, IdentityAuthPresenter> implements IdentityAuthView {
    @BindView(R.id.tv_first_authentication)
    TextView tvFirstAuthentication;
    @BindView(R.id.tv_lv1_arrow)
    TextView tvLv1Arrow;
    @BindView(R.id.tv_second_authentication)
    TextView tvSecondAuthentication;
    @BindView(R.id.tv_lv2_arrow)
    TextView tvLv2Arrow;
    @BindView(R.id.tv_third_authentication)
    TextView tvThirdAuthentication;
    @BindView(R.id.tv_lv3_arrow)
    TextView tvLv3Arrow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_authentication);
        ButterKnife.bind(this);
//        initializeActionToolBar();
//        initView();
    }

    @Override
    protected void initView() {
        setToolbarTitle(getString(R.string.sfrz));
        FontUtil.injectFont(tvLv1Arrow);
        FontUtil.injectFont(tvLv2Arrow);
        FontUtil.injectFont(tvLv3Arrow);
        mPresenter.getuserIdentityAuth();
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_authentication;
    }

    @Override
    public IdentityAuthPresenter initPresenter() {
        return new IdentityAuthPresenter(this);
    }

//    private void initializeActionToolBar() {
//        Toolbar mToolbar = findViewById(R.id.toolbar);
//        TextView tvTitle = findViewById(R.id.toolbar_title);
//        tvTitle.setText("身份认证");
//        initToolBar(mToolbar, true, "");
//    }

    @OnClick({R.id.tv_first_authentication, R.id.tv_second_authentication, R.id.tv_third_authentication})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_first_authentication:
                Intent intent1 = new Intent(this, AuthenticationFirstActivity.class);
                startActivity(intent1);
                break;
            case R.id.tv_second_authentication:
                Intent intent2 = new Intent(this, AuthenticationSecondActivity.class);
                startActivity(intent2);
                break;
            case R.id.tv_third_authentication:
                Intent intent3 = new Intent(this, AuthenticationThirdActivity.class);
                startActivity(intent3);
                break;
        }
    }

    @Override
    public void showLoading() {
        mLoadingDialog.show();
    }

    @Override
    public void hideLoading() {
        mLoadingDialog.hide();
    }

    @Override
    public void showError(String error) {
        ToastUtils.toast(error);
    }

    @Override
    public void editIdentityAuthSuccess(BaseBean bean) {

    }

    @Override
    public void getUserIdentityAuthSuccess(UserIdentityAuthBean bean) {

        if (bean != null) {
            switch (bean.getData().getAuthOk()) {
                case 1:
                case 2:
                    tvFirstAuthentication.setText("");
                    break;
                case 0:
                case 3:

                    break;
            }
        }
    }
}
