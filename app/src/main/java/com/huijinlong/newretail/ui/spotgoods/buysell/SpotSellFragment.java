package com.huijinlong.newretail.ui.spotgoods.buysell;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.huijinlong.newretail.R;
import com.huijinlong.newretail.base.BaseFragment;
import com.huijinlong.newretail.bean.AssetBean;
import com.huijinlong.newretail.base.BaseBean;
import com.huijinlong.newretail.bean.OrderBookSocketEntity;
import com.huijinlong.newretail.eventbus.OrderPlaceEvent;
import com.huijinlong.newretail.eventbus.ProductTypeEvent;
import com.huijinlong.newretail.eventbus.SpotTitleEvent;
import com.huijinlong.newretail.manager.WrapContentLinearLayoutManager;
import com.huijinlong.newretail.requestbean.DepthBean;
import com.huijinlong.newretail.util.FontUtil;
import com.huijinlong.newretail.util.ListUtil;
import com.huijinlong.newretail.util.LogUtils;
import com.huijinlong.newretail.util.NumberUtil;
import com.huijinlong.newretail.util.SocketUtil;
import com.huijinlong.newretail.util.ThreadPool;
import com.huijinlong.newretail.util.ToastUtils;
import com.huijinlong.newretail.util.ViewUtil;
import com.xw.repo.BubbleSeekBar;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import butterknife.Unbinder;

/**
 * @author penghuaijin
 * @date 2018/10/9 4:15 PM
 * @Description: (卖出ask)
 * 注释的都是可以删的@zsg
 */

public class SpotSellFragment extends BaseFragment<DepthlView, DepthlPresenter> implements DepthlView {
    @BindView(R.id.nice_spinner)
    Spinner niceSpinner;
    @BindView(R.id.seekBar)
    BubbleSeekBar seekBar;
    @BindView(R.id.btn_buy)
    Button btnBuy;
    @BindView(R.id.rv_asks)
    RecyclerView rvAsks;
    @BindView(R.id.tv_up)
    TextView tvUp;
    @BindView(R.id.tv_change)
    TextView tvChange;
    @BindView(R.id.rv_bids)
    RecyclerView rvBids;
    Unbinder unbinder;
    @BindView(R.id.tv_sub_price)
    TextView tvSubPrice;
    @BindView(R.id.et_price)
    EditText etPrice;
    @BindView(R.id.tv_add_price)
    TextView tvAddPrice;
    @BindView(R.id.tv_amount)
    TextView tvAmount;
    @BindView(R.id.tv_sub_num)
    TextView tvSubNum;
    @BindView(R.id.et_num)
    EditText etNum;
    @BindView(R.id.tv_add_num)
    TextView tvAddNum;
    @BindView(R.id.tv_available)
    TextView tvAvailable;
    @BindView(R.id.tv_can_buy)
    TextView tvCanBuy;
    @BindView(R.id.tv_price_title)
    TextView tvPriceTitle;
    @BindView(R.id.tv_num_title)
    TextView tvNumTitle;
    @BindView(R.id.tv_available_type)
    TextView tvAvailableType;
    @BindView(R.id.tv_can_buy_type)
    TextView tvCanBuyType;
    @BindView(R.id.tv_current_price)
    TextView tvCurrentPrice;
    @BindView(R.id.iv_spinner_arrow)
    ImageView ivSpinnerArrow;
    @BindView(R.id.view_line_1)
    View viewLine1;
    @BindView(R.id.view_line_2)
    View viewLine2;
    @BindView(R.id.view_line_3)
    View viewLine3;
    @BindView(R.id.view_line_4)
    View viewLine4;


    private String key = "";
    private String instrument = "";
    private String type = "limit";

    private SpotAskOrderBookAdapter mAskAdapter;
    private ArrayList<DepthBean.DataBean.AsksBean> mAskList = new ArrayList<>();
    private SpotBidOrderBookAdapter mBidAdapter;
    private ArrayList<DepthBean.DataBean.BidsBean> mBidList = new ArrayList<>();
    private ProductTypeEvent.Product mProduct;
    private NumberFormat amountNumberFormat;
    private NumberFormat priceNumberFormat;
    private NumberFormat qtyNumberFormat;
    //    private SpotConfigBean spotConfigBean;
    private SpotTitleEvent.EnvBean envBean;


    private void initView() {

        FontUtil.injectFont(tvUp);
        FontUtil.injectFont(tvChange);
        niceSpinner.setAdapter(new TestArrayAdapter(getContext(), getResources().getStringArray(R.array.spot_spinner2)));
        niceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TextView textView = (TextView) view;
                textView.setTextColor(getResources().getColor(R.color.white));
                if (position == 0) {
                    etPrice.setEnabled(true);
                    etPrice.setHint(getString(R.string.mcjiage));

                    if (envBean != null) {
                        if ("0".equals(envBean.getLimit().getLs().getP())) {
//                            etPrice.setText("1.0");
                            tvAddPrice.setVisibility(View.VISIBLE);
                            tvSubPrice.setVisibility(View.VISIBLE);
                            viewLine1.setVisibility(View.VISIBLE);
                            viewLine2.setVisibility(View.VISIBLE);
                            etNum.setHint(getString(R.string.mcshuliang));

                        } else {
                            etPrice.setText(envBean.getLimit().getLs().getP());
                            tvAddPrice.setVisibility(View.GONE);
                            tvSubPrice.setVisibility(View.GONE);
                            etNum.setHint(getString(R.string.mcshuliang));
                            viewLine1.setVisibility(View.GONE);
                            viewLine2.setVisibility(View.GONE);

                        }
                    } else {
                        etPrice.setText("1");
                        tvAddPrice.setVisibility(View.VISIBLE);
                        tvSubPrice.setVisibility(View.VISIBLE);
                        viewLine1.setVisibility(View.VISIBLE);
                        viewLine2.setVisibility(View.VISIBLE);
                        etNum.setHint(getString(R.string.mcshuliang));

                    }
                    type = "limit";

                } else {
                    etPrice.setHint(getString(R.string.zuiyoumc));
                    etPrice.setText("");
                    etPrice.setEnabled(false);
                    etNum.setText("1");
                    etNum.setHint(getString(R.string.mcshuliang));
                    type = "market";
                    tvAddPrice.setVisibility(View.GONE);
                    tvSubPrice.setVisibility(View.GONE);
                    viewLine1.setVisibility(View.GONE);
                    viewLine2.setVisibility(View.GONE);
                    if (!envBean.getMarket().getP().equals("0")&&envBean.getShow().getMarket().getAsk().equals("0")){
                        etNum.setVisibility(View.INVISIBLE);
                    }else {
                        etNum.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        seekBar.getConfigBuilder()

                .progress(0)
                .sectionCount(4)
                .trackColor(getResources().getColor(R.color.c_2f3))
                .trackSize(4)
                .secondTrackColor(getResources().getColor(R.color.c_2f3))
                .secondTrackSize(4)
                .thumbColor(getResources().getColor(R.color.c_55a))
                .showSectionText()
                .sectionTextColor(getResources().getColor(R.color.c_687))
                .sectionTextSize(10)
                .thumbRadius(4)
                .showThumbText()
                .thumbTextColor(getResources().getColor(R.color.c_55a))
                .thumbTextSize(18)
                .bubbleColor(getResources().getColor(R.color.c_55a))
                .bubbleTextSize(18)

//                .seekStepSection()
//                .autoAdjustSectionMark()

//                .showSectionMark()
//                .touchToSeek()
                .sectionTextPosition(BubbleSeekBar.TextPosition.BELOW_SECTION_MARK)
                .build();

        seekBar.setCustomSectionTextArray((sectionCount, array) -> {
            array.clear();
            array.put(0, "0%");
            array.put(1, "25%");
            array.put(2, "50%");
            array.put(3, "75%");
            array.put(4, "100%");
            return array;
        });


        seekBar.setOnProgressChangedListener(new BubbleSeekBar.OnProgressChangedListener() {
            @Override
            public void onProgressChanged(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat, boolean fromUser) {

                if (fromUser){
                    if (!ViewUtil.checkTextNum(etPrice)) {
                        etPrice.setText("1");
                    }
                    BigDecimal price = new BigDecimal(etPrice.getText().toString());
                    BigDecimal available = new BigDecimal(tvAvailable.getText().toString());
                    double value = available.divide(price, mProduct.getQtyDecimal(), RoundingMode.DOWN).doubleValue();

                    String format = qtyNumberFormat.format(  value*progressFloat/100).replace(",", "");
                    etNum.setText(format);

                }
//
//                if (progressFloat < 100f) {
//
//                    if (TextUtils.isEmpty(etPrice.getText().toString())) {
//                        etPrice.setText("1");
//                    }
//
//                    double available = Double.parseDouble(tvAvailable.getText().toString());
//                    double price = Double.parseDouble(etPrice.getText().toString());
//
//                    String s = String.valueOf(available * progressFloat / price / 100);
//
//                    while (!NumberUtil.isOnlyPointNumber(s, mProduct.getQtyDecimal())) {
//                        s = s.substring(0, s.length() - 1);
//                    }
//
//                    if (mProduct.getQtyDecimal() == 0) {
//                        s = s.substring(0, s.length() - 1);
//                    }
//                    etNum.setText(s);
//                } else {
//                    if (TextUtils.isEmpty(etPrice.getText().toString())) {
//                        etPrice.setText("1");
//                    }
//
//                    double available = Double.parseDouble(tvAvailable.getText().toString());
//                    double price = Double.parseDouble(etPrice.getText().toString());
//
//                    String s = String.valueOf(available / price);
//
//                    while (!NumberUtil.isOnlyPointNumber(s, mProduct.getQtyDecimal())) {
//                        s = s.substring(0, s.length() - 1);
//                    }
//
//                    if (mProduct.getQtyDecimal() == 0) {
//                        s = s.substring(0, s.length() - 1);
//                    }
//                    etNum.setText(s);
//                }

            }

            @Override
            public void getProgressOnActionUp(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat) {
            }

            @Override
            public void getProgressOnFinally(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat, boolean fromUser) {

            }
        });


        LinearLayoutManager askLinearLayoutManager = new WrapContentLinearLayoutManager(getContext());
        askLinearLayoutManager.setReverseLayout(true);
        LinearLayoutManager bidLinearLayoutManager = new WrapContentLinearLayoutManager(getContext());

        rvAsks.setItemAnimator(new DefaultItemAnimator());
        rvAsks.setLayoutManager(askLinearLayoutManager);

        rvBids.setLayoutManager(bidLinearLayoutManager);
        rvBids.setItemAnimator(new DefaultItemAnimator());

        mAskAdapter = new SpotAskOrderBookAdapter(getContext(), mAskList,mProduct.getPriceDecimal(),mProduct.getQtyDecimal());
        mAskAdapter.setClickListener(price -> {
            if (type.equals("limit")) {
                if (envBean.getLimit().getLs().getP().equals("0")) {

                    etPrice.setText(price);
                }
            }
        });


        mBidAdapter = new SpotBidOrderBookAdapter(getContext(), mBidList,mProduct.getPriceDecimal(),mProduct.getQtyDecimal());

        mBidAdapter.setClickListener(price -> {
            if (type.equals("limit")) {
                if (envBean.getLimit().getLs().getP().equals("0")) {

                    etPrice.setText(price);
                }
            }
        });
        mAskAdapter.setHasStableIds(true);
        mBidAdapter.setHasStableIds(true);

        rvAsks.setAdapter(mAskAdapter);
        rvBids.setAdapter(mBidAdapter);
        mAskAdapter.notifyDataSetChanged();
        rvAsks.scrollToPosition(0);

        mBidAdapter.notifyDataSetChanged();

        setSymbolView(key);

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();

        unbinder.unbind();

    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);

    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_spot_sell;
    }

    @Override
    protected void initData() {
        initView();
//        mPresenter.getSpotConfig();
        mPresenter.getDepth(instrument, "8");
        mPresenter.getWalletAsset(key.split("/")[0], 0);
    }

    @Override
    protected DepthlPresenter initPresenter() {
        return new DepthlPresenter(this);
    }

    @Override
    public void showGetResult(String resultMessage) {

        ToastUtils.toast(resultMessage);
    }

    @Override
    public void getSuccess(DepthBean bean) {

        mAskList.clear();
        mAskList.addAll(bean.getData().getAsks());

        mBidList.clear();
        mBidList.addAll(bean.getData().getBids());


        if (mAskAdapter != null) {

            mAskAdapter.notifyDataSetChanged();
            rvAsks.scrollToPosition(0);
        }
        if (mBidAdapter != null) {

            mBidAdapter.notifyDataSetChanged();
        }


        SocketUtil.getInstance().setOrderBookListenerSell(new SocketUtil.OrderBookListener() {
            @Override
            public void onOrderBookMsg(OrderBookSocketEntity orderBookSocketEntity, String currentPrice) {

                if (orderBookSocketEntity.getOrderbook().getAsk() != null) {
                    mAskList = ListUtil.getSpotBuyAskList(orderBookSocketEntity.getOrderbook().getAsk().getHashMap(), mAskList);

                    ThreadPool.newUITask(new Runnable() {
                        @Override
                        public void run() {
                            synchronized (this) {
                                LogUtils.e("mAskList--" + orderBookSocketEntity.getInstrument()+orderBookSocketEntity.getOrderbook().getAsk().getHashMap().toString());
                                tvCurrentPrice.setTextColor(getResources().getColor(R.color.c_ff5));
                                tvUp.setTextColor(getResources().getColor(R.color.c_ff5));
                                tvCurrentPrice.setText(currentPrice);

//                                mAskAdapter.refresh(mAskList);
                                if (mAskAdapter != null&&mAskList!=null) {
                                    rvAsks.scrollToPosition(0);
                                    mAskAdapter.notifyDataSetChanged();
                                }
                            }

                        }
                    });
                }

                if (orderBookSocketEntity.getOrderbook().getBid() != null) {
                    mBidList = ListUtil.getSpotBuyBidList(orderBookSocketEntity.getOrderbook().getBid().getHashMap(), mBidList);

                    ThreadPool.newUITask(new Runnable() {
                        @Override
                        public void run() {
                            synchronized (this) {
                                LogUtils.e("mBidList--" + orderBookSocketEntity.getInstrument()+orderBookSocketEntity.getOrderbook().getBid().getHashMap().toString());

                                tvCurrentPrice.setTextColor(getResources().getColor(R.color.c_55a));
                                tvUp.setTextColor(getResources().getColor(R.color.c_55a));
                                tvCurrentPrice.setText(currentPrice);

//                                mBidAdapter.refresh(mBidList);
                                if (mBidAdapter != null&&mBidList!=null) {
                                    mBidAdapter.notifyDataSetChanged();
                                }
                            }

                        }
                    });
                }


            }
        });


    }

    @Override
    public void getWalletAssetSuccess(AssetBean bean) {
        tvAvailable.setText(bean.getData().get(0).getBalance());
//        double price = 0;
//        if (!TextUtils.isEmpty(etPrice.getText().toString())) {
//            price = Double.parseDouble(etPrice.getText().toString());
//        }
//        if (price == 0 || TextUtils.isEmpty(etPrice.getText().toString())) {
//
//        } else {
//            double v = Double.parseDouble(bean.getData().get(0).getBalance());
//            tvCanBuy.setText(String.format("%s", numberFormat.format(v / price)).replace(",", ""));
//        }
    }

    @Override
    public void orderPlaceSuccess(BaseBean bean) {
        ToastUtils.toast(bean.getMsg());

        ThreadPool.newUITask(new Runnable() {
            @Override
            public void run() {
                mPresenter.getWalletAsset(key.split("/")[0],0);

            }
        },500);
        EventBus.getDefault().post(new OrderPlaceEvent(key,instrument));

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        EventBus.getDefault().register(this);

        return rootView;
    }

    @Override
    public void showLoading() {
        mLoading.show();
    }

    @Override
    public void hideLoading() {
        mLoading.dismiss();
    }

    @Override
    public void showError(String error) {
        ToastUtils.toast(error);
    }

    @OnClick({R.id.tv_sub_price, R.id.tv_add_price, R.id.tv_sub_num, R.id.tv_add_num, R.id.btn_buy,R.id.iv_spinner_arrow})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_buy:
                if (envBean.getLimit().getAsk().equals("0")&&envBean.getMarket().getAsk().equals("0")){
                    ToastUtils.toast("禁止交易");
                    return;
                }
                if (type.equals("market")) {
                    if (!ViewUtil.checkTextNum(etNum)) {
                        ToastUtils.toast("请输入卖出数量");
                    }
                    mPresenter.orderPlace(instrument, "ask", type, "gtc",
                            etNum.getText().toString(), "");
                } else {
                    if (!ViewUtil.checkTextNum(etNum)) {
                        ToastUtils.toast(getString(R.string.srsl));
                    }
                    if (!ViewUtil.checkTextNum(etPrice)) {
                        ToastUtils.toast(getString(R.string.srjg));
                    }
                    mPresenter.orderPlace(instrument, "ask", type, "gtc",
                            etNum.getText().toString(), etPrice.getText().toString());
                }
                break;
            case R.id.tv_sub_price:
                if (!ViewUtil.checkTextNum(etPrice)) {
                    return;
                }
                BigDecimal itemNum = NumberUtil.getLastNum(etPrice.getText().toString(), true);
                BigDecimal value = new BigDecimal(etPrice.getText().toString()).subtract(itemNum);

                etPrice.setText(String.valueOf(value));
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                //隐藏软键盘
                imm.hideSoftInputFromWindow(etPrice.getWindowToken(), 0);
                break;
            case R.id.tv_add_price:
                if (!ViewUtil.checkTextNum(etPrice)) {
                    etPrice.setText(String.valueOf(0));
                }
                BigDecimal itemNum2 = NumberUtil.getLastNum(etPrice.getText().toString(), false);
                BigDecimal value2 = new BigDecimal(etPrice.getText().toString()).add(itemNum2);

                etPrice.setText(String.valueOf(value2));
                InputMethodManager imm2 = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                //隐藏软键盘
                imm2.hideSoftInputFromWindow(etPrice.getWindowToken(), 0);
                break;
            case R.id.tv_sub_num:
                if (!ViewUtil.checkTextNum(etNum)) {
                    return;
                }
                BigDecimal itemNum3 = NumberUtil.getLastNum(etNum.getText().toString(), true);
                BigDecimal value3 = new BigDecimal(etNum.getText().toString()).subtract(itemNum3);

                etNum.setText(String.valueOf(value3));
                InputMethodManager imm3 = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                //隐藏软键盘
                imm3.hideSoftInputFromWindow(etNum.getWindowToken(), 0);
                break;
            case R.id.tv_add_num:
                if (!ViewUtil.checkTextNum(etNum)) {
                    etNum.setText(String.valueOf(0));
                }
                BigDecimal itemNum4 = NumberUtil.getLastNum(etNum.getText().toString(), false);
                BigDecimal value4 = new BigDecimal(etNum.getText().toString()).add(itemNum4);

                etNum.setText(String.valueOf(value4));
                InputMethodManager imm4 = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                //隐藏软键盘
                imm4.hideSoftInputFromWindow(etNum.getWindowToken(), 0);
                break;
            case R.id.iv_spinner_arrow:
                niceSpinner.performClick();
                break;
        }
    }

    @OnTextChanged(value = R.id.et_price, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    void afterPriceChange(Editable s) {
        int selectionStart = etPrice.getSelectionStart();
        int selectionEnd = etPrice.getSelectionEnd();
        if (!ViewUtil.checkTextNum(s.toString())) {
            tvAmount.setText(String.format("≈%sCNY", 0));
            return;
        }
        if (!NumberUtil.isOnlyPointNumber(s.toString(), mProduct.getPriceDecimal())) {
            ToastUtils.toast(String.format(getString(R.string.blxs), mProduct.getPriceDecimal()));
            s.delete(selectionStart - 1, selectionEnd);
            etPrice.setText(s);

        }
        if (ViewUtil.checkTextNum(etNum)) {

            BigDecimal price = new BigDecimal(etPrice.getText().toString());
            BigDecimal rate = new BigDecimal("6.8");
            tvAmount.setText(String.format("≈%sCNY", amountNumberFormat.format(price.multiply(rate))).replace(",", ""));


            BigDecimal num = new BigDecimal(etNum.getText().toString());
            tvCanBuy.setText(String.format("%s", amountNumberFormat.format(price.multiply(num))).replace(",", ""));

            BigDecimal available = new BigDecimal(tvAvailable.getText().toString());
            float progress = (price.multiply(num)).divide(available, 2, RoundingMode.HALF_UP).floatValue() * 100;
            seekBar.setProgress(progress > 100 ? 100 : progress);

            switch (available.compareTo(num.multiply(price))) {
                case 1:
                    //没有超过了可用值

                    break;
                case -1:
                    //超过了可用值

                    String format = priceNumberFormat.format(available.divide(num, mProduct.getPriceDecimal(), RoundingMode.DOWN)).replace(",", "");
                    etPrice.setText(format);
                    break;

            }
        }


    }

    @OnTextChanged(value = R.id.et_num, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    void afterNumberChange(Editable s) {
        int selectionStart = etNum.getSelectionStart();
        int selectionEnd = etNum.getSelectionEnd();

        if (!ViewUtil.checkTextNum(s.toString())) {
            tvAmount.setText(String.format("≈%sCNY", 0));
            return;
        }
        if (!NumberUtil.isOnlyPointNumber(s.toString(), mProduct.getQtyDecimal())) {
            ToastUtils.toast(String.format(getString(R.string.blxs), mProduct.getQtyDecimal()));
            s.delete(selectionStart - 1, selectionEnd);
            etNum.setText(s);
        }
        if (ViewUtil.checkTextNum(etPrice)) {

            BigDecimal price = new BigDecimal(etPrice.getText().toString());
            BigDecimal rate = new BigDecimal("6.8");
            tvAmount.setText(String.format("≈%sCNY", amountNumberFormat.format(price.multiply(rate))).replace(",", ""));


            BigDecimal num = new BigDecimal(etNum.getText().toString());
            tvCanBuy.setText(String.format("%s", amountNumberFormat.format(price.multiply(num))).replace(",", ""));


            BigDecimal available = new BigDecimal(tvAvailable.getText().toString());
            float progress = (price.multiply(num)).divide(available, 2, RoundingMode.HALF_UP).floatValue() * 100;
            seekBar.setProgress(progress > 100 ? 100 : progress);

            switch (available.compareTo(num.multiply(price))) {
                case 1:
                    //没有超过了可用值

                    break;
                case -1:
                    //超过了可用值

                    String format = qtyNumberFormat.format(available.divide(price, mProduct.getQtyDecimal(), RoundingMode.DOWN)).replace(",", "");
                    etNum.setText(format);
                    break;

            }
        }


    }


    @Subscribe()
    public void onEvent(SpotTitleEvent event) {
        LogUtils.e("SpotTitleEvent:" + event.getSymbol());
        key = event.getSymbol();
        setSymbolView(key);
        tvCurrentPrice.setText(event.getLast());

        mPresenter.getDepth(instrument, "8");
        mPresenter.getWalletAsset(key.split("/")[0], 0);

        envBean = event.getEnv();


        if (envBean.getLimit().getAsk().equals("0") && envBean.getMarket().getAsk().equals("0")) {
            //不能限价买，不能市价买
            btnBuy.setBackgroundResource(R.drawable.bg_round_btn_gray2);
            niceSpinner.setEnabled(false);
            ivSpinnerArrow.setVisibility(View.GONE);

        } else if (envBean.getLimit().getAsk().equals("1") && envBean.getMarket().getAsk().equals("0")) {
            //能限价买，不能市价买
            type = "limit";
            niceSpinner.setEnabled(false);
            if (Float.parseFloat(envBean.getLimit().getLs().getP()) != 0) {
                etPrice.setText(envBean.getLimit().getLs().getP());
                etPrice.setEnabled(false);
                tvAddPrice.setVisibility(View.GONE);
                tvSubPrice.setVisibility(View.GONE);
                viewLine1.setVisibility(View.GONE);
                viewLine2.setVisibility(View.GONE);

                etNum.setHint(getString(R.string.mcshuliang));

            }
            ivSpinnerArrow.setVisibility(View.GONE);

        } else if (envBean.getLimit().getAsk().equals("0") && envBean.getMarket().getAsk().equals("1")) {
            //不能能限价买，能市价买
            type = "market";
            niceSpinner.setSelection(1);
            niceSpinner.setEnabled(false);

            etPrice.setHint(getString(R.string.zuiyoumc));
            etPrice.setText("");
            etPrice.setEnabled(false);
            etNum.setText("");
            etNum.setHint(String.format(getString(R.string.slzw), key.split("/")[0]));
            type = "market";
            tvAddPrice.setVisibility(View.GONE);
            tvSubPrice.setVisibility(View.GONE);
            ivSpinnerArrow.setVisibility(View.GONE);
            viewLine1.setVisibility(View.GONE);
            viewLine2.setVisibility(View.GONE);


        } else {
            if (Float.parseFloat(envBean.getLimit().getLs().getP()) != 0) {
                etPrice.setText(envBean.getLimit().getLs().getP());
                etPrice.setEnabled(false);
                tvAddPrice.setVisibility(View.GONE);
                tvSubPrice.setVisibility(View.GONE);
                etNum.setHint(getString(R.string.mcshuliang));
                viewLine1.setVisibility(View.GONE);
                viewLine2.setVisibility(View.GONE);

            }
        }

        if (!envBean.getLimit().getLs().getQ().equals("0")&&envBean.getShow().getAsk().getP().equals("0")) {
            etPrice.setVisibility(View.INVISIBLE);
        }
        if (envBean.getShow().getAsk().getP().equals("1")) {
            etPrice.setVisibility(View.VISIBLE);
        }
        if (!envBean.getLimit().getLs().getQ().equals("0")&&envBean.getShow().getAsk().getQ().equals("0")) {
            etNum.setVisibility(View.INVISIBLE);
        }
        if (envBean.getShow().getAsk().getQ().equals("1")) {
            etNum.setVisibility(View.VISIBLE);
        }
        if (!envBean.getMarket().getP().equals("0")&&envBean.getShow().getMarket().getAsk().equals("0")){
            etNum.setVisibility(View.INVISIBLE);
        }else {
            etNum.setVisibility(View.VISIBLE);
        }
        mAskList.clear();
        mBidList.clear();
        mAskAdapter.setPriceDecimal(event.getPriceDecimal(),event.getQtyDecimal());
        mAskAdapter.notifyDataSetChanged();
        rvAsks.scrollToPosition(0);

        mBidAdapter.setPriceDecimal(event.getPriceDecimal(),event.getQtyDecimal());
        mBidAdapter.notifyDataSetChanged();
    }


    public void setSymbol(ProductTypeEvent.Product product) {
        mProduct = product;
        key = mProduct.getSymbol();

        amountNumberFormat = NumberFormat.getNumberInstance();
        amountNumberFormat.setMaximumFractionDigits(product.getAmountDecimal());
        amountNumberFormat.setRoundingMode(RoundingMode.DOWN);//表示舍

        priceNumberFormat = NumberFormat.getNumberInstance();
        priceNumberFormat.setMaximumFractionDigits(product.getPriceDecimal());
        priceNumberFormat.setRoundingMode(RoundingMode.DOWN);//表示舍

        qtyNumberFormat = NumberFormat.getNumberInstance();
        qtyNumberFormat.setMaximumFractionDigits(product.getQtyDecimal());
        qtyNumberFormat.setRoundingMode(RoundingMode.DOWN);//表示舍

    }

    public void setSymbolView(String firstSymbol) {
        key = firstSymbol;
        instrument = key.split("/")[0] + key.split("/")[1];
        tvPriceTitle.setText(String.format(getString(R.string.jgzw), key.split("/")[1]));
        tvNumTitle.setText(String.format(getString(R.string.slzw), key.split("/")[0]));
        btnBuy.setText(String.format(getString(R.string.mczw), key.split("/")[0]));
        tvAvailableType.setText(key.split("/")[0]);
        tvCanBuyType.setText(key.split("/")[0]);
        etNum.setHint(String.format(getString(R.string.slzw), key.split("/")[0]));
    }

    @Subscribe()
    public void onEvent(OrderPlaceEvent event) {
        ThreadPool.newUITask(new Runnable() {
            @Override
            public void run() {
                mPresenter.getWalletAsset(event.getSymbol().split("/")[0],0);

            }
        },500);
    }
}
