package com.huijinlong.newretail.ui.wallet.addressmanage;

import com.huijinlong.newretail.base.BaseView;
import com.huijinlong.newretail.bean.CurrenciesBean;

/**
 * @author zsg
 * @date 2018/10/19 15:52
 * @Version 1.0
 * @Description: ()
 */
public interface CurrenciesView extends BaseView{
    void getCurrencise(CurrenciesBean currenciesBean);
    void getEditAddressWithdraw(EditAddressWithdrawBean editAddressWithdrawBean);
}
