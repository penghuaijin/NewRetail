package com.huijinlong.newretail.ui.mine.accountSecurity.editpassword;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.huijinlong.newretail.R;
import com.huijinlong.newretail.base.BaseActivity;
import com.huijinlong.newretail.base.BaseBean;
import com.huijinlong.newretail.util.ToastUtils;
import com.huijinlong.newretail.util.ViewUtil;

/**
 * Created by penghuaijin on 2018/9/26.
 */

public class EditPasswordActivity extends BaseActivity<EditPasswordView, EditPasswordPresenter> implements EditPasswordView, View.OnClickListener {

    // private EditPasswordPresenter mPresenter;
    private EditText et_old_password;
    private EditText et_new_password;
    private EditText et_re_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.acitvity_edit_password);
//        initializeActionToolBar();
//        initView();
    }

    @Override
    public EditPasswordPresenter initPresenter() {
        return new EditPasswordPresenter(this);
    }


    @Override
    protected void initView() {
        setToolbarTitle(getString(R.string.xgmm));
        et_old_password = findViewById(R.id.et_old_password);
        et_new_password = findViewById(R.id.et_new_password);
        et_re_password = findViewById(R.id.et_re_password);
        ViewUtil.setEditTextHintSize(et_old_password, getString(R.string.qsrjmm), (int) getResources().getDimension(R.dimen.normal_text_size));
        ViewUtil.setEditTextHintSize(et_new_password, getString(R.string.qsrxmm), (int) getResources().getDimension(R.dimen.normal_text_size));
        ViewUtil.setEditTextHintSize(et_re_password, getString(R.string.qrxmm), (int) getResources().getDimension(R.dimen.normal_text_size));
        findViewById(R.id.btn_edit_password).setOnClickListener(this);

    }

    @Override
    protected int getContentView() {
        return R.layout.acitvity_edit_password;
    }

//    private void initializeActionToolBar() {
//        Toolbar mToolbar = findViewById(R.id.toolbar);
//        TextView tvTitle = findViewById(R.id.toolbar_title);
//        tvTitle.setText("修改密码");
//        initToolBar(mToolbar, true, "");
//    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_edit_password: {
                if (checkEditText()) {
                    mPresenter.editPwd(et_old_password.getText().toString(),
                            et_new_password.getText().toString(),
                            et_re_password.getText().toString());
                }
            }
        }
    }

    private boolean checkEditText() {

        return ViewUtil.checkEditTextForPassword(et_old_password, 8, getString(R.string.jmm))
                && ViewUtil.checkEditTextForPassword(et_new_password, 8, getString(R.string.xmm))
                && ViewUtil.checkEditTextForPassword(et_re_password, 8, getString(R.string.qrxmm));
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void editPwdSuccess(BaseBean bean) {
        ToastUtils.toast(bean.getMsg());
    }

    @Override
    public void showLoading() {
        mLoadingDialog.show();
    }

    @Override
    public void hideLoading() {
        mLoadingDialog.dismiss();
    }

    @Override
    public void showError(String error) {

    }
}
