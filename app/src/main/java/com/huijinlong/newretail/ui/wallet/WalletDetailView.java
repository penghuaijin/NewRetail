package com.huijinlong.newretail.ui.wallet;

import com.huijinlong.newretail.base.BaseView;
import com.huijinlong.newretail.bean.WithDrawsBean;

/**
 * Created by penghuaijin on 2018/9/12.
 */

public interface WalletDetailView extends BaseView {

    void getWithDraw(WithDrawsBean withDrawsBean);
}
