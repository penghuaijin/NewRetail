package com.huijinlong.newretail.ui.wallet.transactionDetails;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.huijinlong.newretail.R;
import com.huijinlong.newretail.base.BaseActivity;
import com.huijinlong.newretail.base.BasePresenter;
import com.huijinlong.newretail.ui.wallet.WalletFragment;
import com.huijinlong.newretail.util.FontUtil;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author zsg
 * @date 2018/11/19 15:42
 * @Version 1.0
 * @Description: (交易账户)
 */
public class TransactionDetailsActivity extends BaseActivity {

//    @BindView(R.id.toolbar_title)
//    TextView toolbarTitle;
//    @BindView(R.id.toolbar_func1)
//    TextView toolbarFunc1;
//    @BindView(R.id.toolbar_func)
//    TextView toolbarFunc;
//    @BindView(R.id.ll_right_func)
//    LinearLayout llRightFunc;
//    @BindView(R.id.toolbar)
//    Toolbar toolbar;
    @BindView(R.id.fragment)
    FrameLayout fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_transactiondetails);
        ButterKnife.bind(this);


//
//        toolbarTitle.setText("交易账户");
//        //toolbarFunc.setText("api列表");
//        initToolBar(toolbar, true, "");

    }

    @Override
    public BasePresenter initPresenter() {
        return null;
    }

    @Override
    protected void initView() {
        setToolbarTitle("交易账户");
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragment, WalletFragment.newInstance("交易账户"))
                .commit();
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_transactiondetails;
    }

}
