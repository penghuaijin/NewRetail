package com.huijinlong.newretail.ui.wallet;

import com.huijinlong.newretail.base.BasePresenter;
import com.huijinlong.newretail.bean.WithDrawsBean;
import com.huijinlong.newretail.net.MyCallback;
import com.huijinlong.newretail.net.RequestBuilder;
import com.huijinlong.newretail.util.MD5Util;
import com.huijinlong.newretail.util.SpUtil;

import java.util.TreeMap;

/**
 * @author zsg
 * @date 2018/10/25 17:14
 * @Version 1.0
 * @Description: ()
 */
public class WithDrawsPresent extends BasePresenter<WalletDetailView> {

    private WalletDetailView withDrawsView;

    public WithDrawsPresent(WalletDetailView mViews) {
        this.withDrawsView = mViews;
    }

    public void getwithdraws(int currentPage,int pageSize,String currency) {
        withDrawsView.showLoading();
        TreeMap<String, String> params = new TreeMap<>();
        params.put("deviceType", "android");
        params.put("language", SpUtil.getString(SpUtil.LANGUAGE,"zh-cn"));

        params.put("currentPage", String.valueOf(currentPage));
        params.put("pageSize",  String.valueOf(pageSize));
        params.put("currency",  currency);


        RequestBuilder.execute(RequestBuilder.getNetService(true).getWithDraws(MD5Util.getSignValue(params), params), "", new MyCallback<WithDrawsBean>() {
            @Override
            public void onSuccess(WithDrawsBean result) {
                super.onSuccess(result);
                withDrawsView.getWithDraw(result);
            }

            @Override
            public void onEmpty(WithDrawsBean result) {
                super.onEmpty(result);

            }

            @Override
            public void onBusinessFailure(Exception e) {
                super.onBusinessFailure(e);
                withDrawsView.showError(e.getMessage());
            }

            @Override
            public void onFinalResponse(boolean isSuccess) {
                super.onFinalResponse(isSuccess);
                withDrawsView.hideLoading();
            }
        });
    }

    public void getdeposits (int currentPage,int pageSize,String currency) {
        withDrawsView.showLoading();
        TreeMap<String, String> params = new TreeMap<>();
        params.put("deviceType", "android");
        params.put("language", SpUtil.getString(SpUtil.LANGUAGE,"zh-cn"));

        params.put("currentPage", String.valueOf(currentPage));
        params.put("pageSize",  String.valueOf(pageSize));
        params.put("currency",  currency);

        RequestBuilder.execute(RequestBuilder.getNetService(true).getDeposites(MD5Util.getSignValue(params), params), "", new MyCallback<WithDrawsBean>() {
            @Override
            public void onSuccess(WithDrawsBean result) {
                super.onSuccess(result);
                withDrawsView.getWithDraw(result);
            }

            @Override
            public void onEmpty(WithDrawsBean result) {
                super.onEmpty(result);

            }

            @Override
            public void onBusinessFailure(Exception e) {
                super.onBusinessFailure(e);
                withDrawsView.showError(e.getMessage());
            }

            @Override
            public void onFinalResponse(boolean isSuccess) {
                super.onFinalResponse(isSuccess);
                withDrawsView.hideLoading();
            }
        });
    }

}
