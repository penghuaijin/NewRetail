package com.huijinlong.newretail.ui.spotgoods.buysell;

import com.huijinlong.newretail.base.BaseView;
import com.huijinlong.newretail.base.BaseBean;
import com.huijinlong.newretail.requestbean.DepthBean;
import com.huijinlong.newretail.bean.AssetBean;

/**
 * Created by penghuaijin on 2018/9/12.
 */

public interface DepthlView extends BaseView {

    void showGetResult(String resultMessage);

    void getSuccess(DepthBean bean);

    void getWalletAssetSuccess(AssetBean bean);

    void orderPlaceSuccess(BaseBean bean);



}
