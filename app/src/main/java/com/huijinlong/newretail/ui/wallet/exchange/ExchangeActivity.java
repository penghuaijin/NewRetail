package com.huijinlong.newretail.ui.wallet.exchange;

import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.huijinlong.newretail.R;
import com.huijinlong.newretail.base.BaseActivity;
import com.huijinlong.newretail.base.BaseBean;
import com.huijinlong.newretail.bean.WithdrawsListBean;
import com.huijinlong.newretail.ui.wallet.addressmanage.withdraws.WithDrawsPresenter;
import com.huijinlong.newretail.ui.wallet.addressmanage.withdraws.WithDrawsView;
import com.huijinlong.newretail.util.ToastUtils;

import java.math.BigDecimal;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author zsg
 * @date 2018/11/20 11:02
 * @Version 1.0
 * @Description: (兑换)
 */
public class ExchangeActivity extends BaseActivity<WithDrawsView, WithDrawsPresenter> implements WithDrawsView, TransferOutlView {

    @BindView(R.id.tv_currency)
    TextView tvCurrency;
    @BindView(R.id.ed_adresss)
    EditText edAdresss;
    @BindView(R.id.bt_add)
    Button btAdd;
    @BindView(R.id.tv_select)
    ImageView tvSelect;
    @BindView(R.id.re_spinner)
    RelativeLayout reSpinner;
    @BindView(R.id.tv_transferFee)
    TextView tvTransferFee;
    @BindView(R.id.tv_transferred)
    TextView tvTransferred;
    @BindView(R.id.tv_limit)
    TextView tvLimit;
    @BindView(R.id.tv_currencys)
    TextView tvCurrencys;

    private int page = 1;
    private WithdrawsListBean dataBean;
    private PopupWindow alarmWindow;
    private LinearLayout popupWindowView;
    private int cid;
    private TransferOutPresent transferOutPresent;

    @Override
    protected void initView() {
        setToolbarTitle("兑换");
        setmToolbarRightBtnColor(R.color.c_55a);
        setToolbarRightBtn(true, false, "兑换记录", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startToActivity(ExchangeActivity.this, ExchangeDetailActivity.class);
            }
        });
        initWindow();
        edAdresss.addTextChangedListener(textWatcher);
        cid = getIntent().getIntExtra("cid", 0);
        mPresenter.getwithdraws(page, 5, 2);
        transferOutPresent = new TransferOutPresent(this);
       // tvLimit.setText("最小兑换数量为：" + getIntent().getStringExtra("limit") + " " + getIntent().getStringExtra("currency"));
        //tvCurrencys.setText(getIntent().getStringExtra("currency"));
       // tvTransferred.setText("到账数量 0 "+getIntent().getStringExtra("currency"));
    }

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (!TextUtils.isEmpty(edAdresss.getText().toString())) {
                BigDecimal balance = new BigDecimal(edAdresss.getText().toString());
                BigDecimal frozen;
                if (TextUtils.isEmpty(getIntent().getStringExtra("transferFee"))) {
                    frozen = new BigDecimal("0");
                } else {
                    frozen = new BigDecimal(getIntent().getStringExtra("transferFee"));
                }

                BigDecimal decimal = balance.subtract(frozen);
               // tvTransferred.setText("到账数量 " + decimal.stripTrailingZeros().toPlainString() + " "+getIntent().getStringExtra("currency"));
                tvTransferred.setText("到账数量 " + decimal.stripTrailingZeros().toPlainString() + " FER");
            }

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    @Override
    protected int getContentView() {
        return R.layout.activity_exchange;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
    }

    @Override
    public WithDrawsPresenter initPresenter() {
        return new WithDrawsPresenter(this);
    }

    public void initWindow() {
        View nemuView = LayoutInflater.from(this).inflate(R.layout.popup_window_view, null, false);
        popupWindowView = (LinearLayout) nemuView.findViewById(R.id.ll_pop_item);

        alarmWindow = new PopupWindow(nemuView, LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
//        alarmWindow.setAnimationStyle(R.style.popAnimationPreview);
        alarmWindow.setBackgroundDrawable(new BitmapDrawable());
        alarmWindow.setOutsideTouchable(false);
        alarmWindow.setFocusable(true);
        alarmWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                Drawable rightDrawable = getResources().getDrawable(R.mipmap.dropdown);// 获取res下的图片drawable
                rightDrawable.setBounds(0, 0, rightDrawable.getMinimumWidth(), rightDrawable.getMinimumHeight());// 一定要设置setBounds();
                tvSelect.setBackground(rightDrawable);
                alarmWindow.dismiss();
            }
        });
    }

    public void showWindow(View v, WithdrawsListBean dataBean) {
        if (!alarmWindow.isShowing()) {
            popupWindowView.removeAllViews();
            for (WithdrawsListBean.DataBean.ListBean bean : dataBean.getData().getList()) {
                View popItem = LayoutInflater.from(this).inflate(R.layout.popup_window_item, null, false);
                TextView tvDateName = (TextView) popItem.findViewById(R.id.tv_user_name);

                tvDateName.setText(bean.getAddress());

                tvDateName.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        tvCurrency.setText(bean.getAddress() + "");
                        //mycid = currenciesBean.getCid();
                        alarmWindow.dismiss();
                    }
                });
                popupWindowView.addView(popItem);
            }
            alarmWindow.showAsDropDown(v, LinearLayout.LayoutParams.MATCH_PARENT, 1);
        } else {
            alarmWindow.dismiss();
        }
    }


    @OnClick({R.id.bt_add, R.id.re_spinner})
    public void onViewClicked(View v) {
        switch (v.getId()) {
            case R.id.bt_add:
                transferOutPresent.getwithdraws(String.valueOf(cid), edAdresss.getText().toString(), tvCurrency.getText().toString());
                break;
            case R.id.re_spinner:
                if (dataBean != null) {
                    Drawable rightDrawable = getResources().getDrawable(R.mipmap.dropup);// 获取res下的图片drawable
                    rightDrawable.setBounds(0, 0, rightDrawable.getMinimumWidth(), rightDrawable.getMinimumHeight());// 一定要设置setBounds();
                    tvSelect.setBackground(rightDrawable);
                    showWindow(reSpinner, dataBean);
                }
                break;
        }
    }

    @Override
    public void getaddressWithdrawList(WithdrawsListBean addressWithdrawListBean) {
        dataBean = addressWithdrawListBean;
        if (TextUtils.isEmpty(getIntent().getStringExtra("transferFee"))) {
            //tvTransferFee.setText("手续费 0 " + getIntent().getStringExtra("currency"));
            tvTransferFee.setText("手续费 0 FER" );
        } else {
           // tvTransferFee.setText("手续费 " + getIntent().getStringExtra("transferFee") + " " + getIntent().getStringExtra("currency"));
            tvTransferFee.setText("手续费 " + getIntent().getStringExtra("transferFee") + " FER");
        }

    }

    @Override
    public void getWithdraw(BaseBean baseBean) {

    }

    @Override
    public void showLoading() {
        mLoadingDialog.show();
    }

    @Override
    public void hideLoading() {
        mLoadingDialog.dismiss();
    }

    @Override
    public void showError(String error) {
        ToastUtils.toast(error);
    }

    @Override
    public void gettransferOut(BaseBean baseBean) {
        ToastUtils.toast("兑换成功");
        finish();
    }
}
