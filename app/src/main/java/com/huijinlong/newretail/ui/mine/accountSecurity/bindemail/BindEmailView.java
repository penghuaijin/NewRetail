package com.huijinlong.newretail.ui.mine.accountSecurity.bindemail;

import com.huijinlong.newretail.base.BaseView;
import com.huijinlong.newretail.requestbean.GetPhoneCaptcha4BindBean;
import com.huijinlong.newretail.requestbean.VerifyPhone4BindBean;

import java.util.Map;

/**
 * Created by penghuaijin on 2018/9/12.
 */

public interface BindEmailView extends BaseView{
        void showGetResult(String resultMessage);
        void getEmailCaptchaSuccess(GetPhoneCaptcha4BindBean bean);
        void bindEmailSuccess(VerifyPhone4BindBean bean);

}
