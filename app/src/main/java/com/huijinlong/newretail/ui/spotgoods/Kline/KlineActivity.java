package com.huijinlong.newretail.ui.spotgoods.kline;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.TextView;

import com.huijinlong.newretail.R;
import com.huijinlong.newretail.base.BaseActivity;
import com.huijinlong.newretail.requestbean.KlineBean;
import com.huijinlong.newretail.util.FontUtil;
import com.huijinlong.newretail.util.ToastUtils;
import com.huijinlong.newretail.util.kline.KLineView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author zsg
 * @date 2018/10/31 17:07
 * @Version 1.0
 * @Description: (K线图)
 */
public class KlineActivity extends BaseActivity<KlineView, KlinePresent> implements KlineView {
    @BindView(R.id.klv_main)
    KLineView mKLineView;
    @BindView(R.id.btn_ma)
    Button btnMa;
    @BindView(R.id.btn_ema)
    Button btnEma;
    @BindView(R.id.btn_boll)
    Button btnBoll;
    @BindView(R.id.btn_deputy)
    Button btnDeputy;
    @BindView(R.id.btn_macd)
    Button btnMacd;
    @BindView(R.id.btn_kdj)
    Button btnKdj;
    @BindView(R.id.btn_rsi)
    Button btnRsi;
    @BindView(R.id.btn_kline_reset)
    Button btnKlineReset;
    @BindView(R.id.btn_depth_activity)
    Button btnDepthActivity;
    @BindView(R.id.min)
    TextView min;
    @BindView(R.id.tv_ma)
    TextView tvMa;
    @BindView(R.id.tv_index)
    TextView tvIndex;
    @BindView(R.id.tv_set)
    TextView tvSet;
    @BindView(R.id.tv_setbig)
    TextView tvSetbig;
    @BindView(R.id.tv_top)
    TextView tvTop;

    private Handler mHandler;
    private Runnable dataListAddRunnable;
    private Runnable singleDataAddRunnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_kline);
        ButterKnife.bind(this);

        EventBus.getDefault().register(this);



    }

//    private void initData(){
//        //初始化控件加载数据
//        mKLineView.initKDataList(getKDataList(10));
//        //设置十字线移动模式，默认为0：固定指向收盘价
//        mKLineView.setCrossHairMoveMode(KLineView.CROSS_HAIR_MOVE_OPEN);
//
//        mHandler = new Handler();
//        dataListAddRunnable = new Runnable() {
//            @Override
//            public void run() {
//                //分页加载时添加多条数据
//                mKLineView.addPreDataList(getKDataList(10), true);
//            }
//        };
//
//        singleDataAddRunnable = new Runnable() {
//            @Override
//            public void run() {
//                //实时刷新时添加单条数据
//                mKLineView.addSingleData(getKDataList(10).get(0));
//                mHandler.postDelayed(this, 5000);
//            }
//        };
//        mHandler.postDelayed(singleDataAddRunnable, 2000);
//
//    }

    private void initData(KlineBean klineBean) {
        //初始化控件加载数据getKDataList(10)
        mKLineView.initKDataList(klineBean.getData());
        //设置十字线移动模式，默认为0：固定指向收盘价
        mKLineView.setCrossHairMoveMode(KLineView.CROSS_HAIR_MOVE_FREE);
        mKLineView.setMainImgType(KLineView.MAIN_IMG_EMA);

        mKLineView.setDeputyPicShow(true);
        mKLineView.setMainImgType(KLineView.DEPUTY_IMG_KDJ);

        mHandler = new Handler();
        dataListAddRunnable = new Runnable() {
            @Override
            public void run() {
                //分页加载时添加多条数据
                mKLineView.addPreDataList(klineBean.getData(), true);
            }
        };

        singleDataAddRunnable = new Runnable() {
            @Override
            public void run() {
                //实时刷新时添加单条数据
                // mKLineView.addSingleData(getKDataList(0.1).get(0));

               //   mHandler.postDelayed(this, 1000);
            }
        };
     //   mHandler.postDelayed(singleDataAddRunnable, 2000);
        /**
         * 当控件显示数据属于总数据量的前三分之一时，会自动调用该接口，用于预加载数据，保证控件操作过程中的流畅性，
         * 虽然做了预加载，当总数据量较小时，也会出现用户滑到左边界了，但数据还未获取到，依然会有停顿。
         * 所以数据量越大，越不会出现停顿，也就越流畅
         */
        mKLineView.setOnRequestDataListListener(new KLineView.OnRequestDataListListener() {
            @Override
            public void requestData() {
                //延时3秒执行，模拟网络请求耗时
                mHandler.postDelayed(dataListAddRunnable, 3000);
            }
        });
    }

    //模拟K线数据
    private List<KlineBean.DataBean> getKDataList(double num) {
        long start = System.currentTimeMillis();

        Random random = new Random();
        List<KlineBean.DataBean> dataList = new ArrayList<>();
        double openPrice = 100;
        double closePrice;//开
        double maxPrice;//高
        double minPrice;//低
        double volume;//成交额

        for (int x = 0; x < num * 10; x++) {
            for (int i = 0; i < 12; i++) {
                start += 60 * 1000 * 5;
                closePrice = openPrice + getAddRandomDouble();
                maxPrice = closePrice + getAddRandomDouble();
                minPrice = openPrice - getSubRandomDouble();
                volume = random.nextInt(100) * 1000 + random.nextInt(10) * 10 + random.nextInt(10) + random.nextDouble();
//                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
//                String songtime = sdf.format(start);
                dataList.add(new KlineBean.DataBean(start, openPrice, closePrice, maxPrice, minPrice, volume));
                openPrice = closePrice;
            }

            for (int i = 0; i < 8; i++) {
                start += 60 * 1000 * 5;
                closePrice = openPrice - getSubRandomDouble();
                maxPrice = openPrice + getAddRandomDouble();
                minPrice = closePrice - getSubRandomDouble();
                volume = random.nextInt(100) * 1000 + random.nextInt(10) * 10 + random.nextInt(10) + random.nextDouble();
//                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
//                String songtime = sdf.format(start);
                dataList.add(new KlineBean.DataBean(start, openPrice, closePrice, maxPrice, minPrice, volume));
                openPrice = closePrice;
            }

        }
        long end = System.currentTimeMillis();
        return dataList;
    }
    private double getAddRandomDouble() {
        Random random = new Random();
        return random.nextInt(5) * 5 + random.nextDouble();
    }

    private double getSubRandomDouble() {
        Random random = new Random();
        return random.nextInt(5) * 5 - random.nextDouble();
    }

    private int dp2px(float dpValue) {
        final float scale = getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //退出页面时停止子线程并置空，便于回收，避免内存泄露
        mKLineView.cancelQuotaThread();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe()
    public void onEvent(KlineBean.DataBean event) {
        //实时刷新时添加单条数据
        //mKLineView.addSingleData(event);
    }

    @Override
    public KlinePresent initPresenter() {
        return new KlinePresent(this);
    }

    @Override
    protected void initView() {
        setToolbarTitle("BTC/ETC");

        FontUtil.injectFont(min);
        FontUtil.injectFont(tvMa);
        FontUtil.injectFont(tvIndex);
        FontUtil.injectFont(tvSet);
        FontUtil.injectFont(tvSetbig);
        FontUtil.injectFont(tvTop);


        mPresenter.getKline("FERUSDT", "30min");

    }

    @Override
    protected int getContentView() {
        return R.layout.activity_kline;
    }

    @Override
    public void getKline(KlineBean klineBean) {

        if (klineBean.getData() != null) {
            initData(klineBean);
        }

    }

    @Override
    public void showLoading() {
        mLoadingDialog.show();
    }

    @Override
    public void hideLoading() {
        mLoadingDialog.dismiss();
    }

    @Override
    public void showError(String error) {
        ToastUtils.toast(error);
    }
}
