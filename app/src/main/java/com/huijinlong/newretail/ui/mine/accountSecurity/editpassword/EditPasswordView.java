package com.huijinlong.newretail.ui.mine.accountSecurity.editpassword;

import com.huijinlong.newretail.base.BaseView;
import com.huijinlong.newretail.base.BaseBean;

/**
 * Created by penghuaijin on 2018/9/12.
 */

public interface EditPasswordView extends BaseView{
        void editPwdSuccess(BaseBean bean);

}
