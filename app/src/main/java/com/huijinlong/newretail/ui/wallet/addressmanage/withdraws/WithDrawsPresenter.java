package com.huijinlong.newretail.ui.wallet.addressmanage.withdraws;

import com.huijinlong.newretail.base.BaseBean;
import com.huijinlong.newretail.base.BasePresenter;
import com.huijinlong.newretail.bean.WithdrawsListBean;
import com.huijinlong.newretail.net.MyCallback;
import com.huijinlong.newretail.net.RequestBuilder;
import com.huijinlong.newretail.util.MD5Util;
import com.huijinlong.newretail.util.SpUtil;

import java.math.BigDecimal;
import java.util.TreeMap;

/**
 * Created by penghuaijin on 2018/9/12.
 */

public class WithDrawsPresenter extends BasePresenter<WithDrawsView> {

    private WithDrawsView withDrawsView;

    public WithDrawsPresenter(WithDrawsView mViews) {
        this.withDrawsView = mViews;
    }

    public void getwithdraws(int currentPage,int pageSize,int channel) {
        withDrawsView.showLoading();
        TreeMap<String, String> params = new TreeMap<>();
        params.put("deviceType", "android");
        params.put("language", SpUtil.getString(SpUtil.LANGUAGE,"zh-cn"));

        params.put("currentPage", String.valueOf(currentPage));
        params.put("pageSize",  String.valueOf(pageSize));
        params.put("channel",String.valueOf(channel));

        RequestBuilder.execute(RequestBuilder.getNetService(true).getWithdrawList(MD5Util.getSignValue(params), params), "", new MyCallback<WithdrawsListBean>() {
            @Override
            public void onSuccess(WithdrawsListBean result) {
                super.onSuccess(result);
                withDrawsView.getaddressWithdrawList(result);
            }

            @Override
            public void onEmpty(WithdrawsListBean result) {
                super.onEmpty(result);

            }

            @Override
            public void onBusinessFailure(Exception e) {
                super.onBusinessFailure(e);
                withDrawsView.showError(e.getMessage());
            }

            @Override
            public void onFinalResponse(boolean isSuccess) {
                super.onFinalResponse(isSuccess);
                withDrawsView.hideLoading();
            }
        });
    }

    public void getwithdraw(int cid, String amount, String address)
    {
        withDrawsView.showLoading();
        TreeMap<String, String> params = new TreeMap<>();
        params.put("deviceType", "android");
        params.put("language", SpUtil.getString(SpUtil.LANGUAGE,"zh-cn"));

        params.put("cid", String.valueOf(cid));
        params.put("amount",  String.valueOf(amount));
        params.put("address",address);

        RequestBuilder.execute(RequestBuilder.getNetService(true).getWithDraw(MD5Util.getSignValue(params), params), "", new MyCallback<BaseBean>() {
            @Override
            public void onSuccess(BaseBean result) {
                super.onSuccess(result);
                withDrawsView.getWithdraw(result);
            }

            @Override
            public void onEmpty(BaseBean result) {
                super.onEmpty(result);

            }

            @Override
            public void onBusinessFailure(Exception e) {
                super.onBusinessFailure(e);
                withDrawsView.showError(e.getMessage());
            }

            @Override
            public void onFinalResponse(boolean isSuccess) {
                super.onFinalResponse(isSuccess);
                withDrawsView.hideLoading();
            }
        });
    }
}
