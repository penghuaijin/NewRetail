package com.huijinlong.newretail.ui.mine.authenticationsecond;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.huijinlong.newretail.R;
import com.huijinlong.newretail.base.BaseRecyclerAdapter;
import com.huijinlong.newretail.base.SmartViewHolder;
import com.huijinlong.newretail.base.BaseActivity;
import com.huijinlong.newretail.base.BasePresenter;
import com.huijinlong.newretail.bean.IdCardBean;
import com.huijinlong.newretail.util.FontUtil;
import com.huijinlong.newretail.util.ToastUtils;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.entity.LocalMedia;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by penghuaijin on 2018/9/29.
 */

public class AuthenticationSecondActivity extends BaseActivity implements View.OnClickListener {
    final RxPermissions rxPermissions = new RxPermissions(this);
    private ImageView ivCardPeople;
    private ImageView ivCardNationalEmblem;
    private boolean isCardPeople;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_authentication_second);
//        initializeActionToolBar();
//        initView();
    }

    @Override
    public BasePresenter initPresenter() {
        return null;
    }

    @Override
    protected void initView() {
        setToolbarTitle(getString(R.string.sfrz));
        ivCardPeople = findViewById(R.id.iv_card_people);
        ivCardNationalEmblem = findViewById(R.id.iv_card_national_emblem);
        ivCardPeople.setOnClickListener(this);
        ivCardNationalEmblem.setOnClickListener(this);

        final ArrayList<IdCardBean> list = new ArrayList<>();
        list.add(new IdCardBean(R.mipmap.pic_id_card_normal, getResources().getString(R.string.xe613), getString(R.string.idcardbz)));
        list.add(new IdCardBean(R.mipmap.pic_id_card_error1, getResources().getString(R.string.xe601), getString(R.string.idcardqs)));
        list.add(new IdCardBean(R.mipmap.pic_id_card_error2, getResources().getString(R.string.xe601), getString(R.string.idcardmh)));
        list.add(new IdCardBean(R.mipmap.pic_id_card_error3, getResources().getString(R.string.xe601), getString(R.string.idcardsg)));
        RecyclerView rv_card = findViewById(R.id.rv_idCard_type);
        GridLayoutManager layoutManager = new GridLayoutManager(this, 4);

        rv_card.setLayoutManager(layoutManager);
        rv_card.setAdapter(new BaseRecyclerAdapter<IdCardBean>(list, R.layout.item_id_card_type, null, this) {
            @Override
            protected void onBindViewHolder(SmartViewHolder holder, IdCardBean model, int position) {
                ImageView pic = holder.itemView.findViewById(R.id.iv_card);
                pic.setImageResource(list.get(position).getPic());
                TextView tv_status = holder.itemView.findViewById(R.id.tv_status);
                FontUtil.injectFont(tv_status);
                if (position == 0) {
                    tv_status.setTextColor(getResources().getColor(R.color.c_57b));
                }
                holder.text(R.id.tv_status, model.getStatus());
                holder.text(R.id.tv_description, model.getDescription());

            }


        });
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_authentication_second;
    }

//    private void initializeActionToolBar() {
//        Toolbar mToolbar = findViewById(R.id.toolbar);
//        TextView tvTitle = findViewById(R.id.toolbar_title);
//        tvTitle.setText("身份认证");
//        initToolBar(mToolbar, true, "");
//    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_card_people: {
                selectPic(true);
                break;
            }
            case R.id.iv_card_national_emblem: {
                selectPic(false);
                break;
            }
        }
    }

    private void selectPic(boolean cardPeople) {
        rxPermissions
                .requestEach(Manifest.permission.CAMERA,
                        Manifest.permission.READ_EXTERNAL_STORAGE)
                .subscribe(permission -> {
                    // will emit 2 Permission objects
                    if (permission.granted) {
                        // `permission.name` is granted !
                        PictureSelector.create(this)
                                .openGallery(PictureMimeType.ofImage())
                                .isCamera(true)// 是否显示拍照按钮
                                .maxSelectNum(1)// 最大图片选择数量
                                .minSelectNum(1)
                                .enableCrop(true)// 是否裁剪 true or false
                                .forResult(PictureConfig.CHOOSE_REQUEST);
                        isCardPeople = cardPeople;
                    } else if (permission.shouldShowRequestPermissionRationale) {
                        ToastUtils.toast(getString(R.string.jjqxdd));
                    } else {
                        ToastUtils.toast(getString(R.string.jjqxbz));

                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case PictureConfig.CHOOSE_REQUEST:
                    // 图片、视频、音频选择结果回调
                    List<LocalMedia> selectList = PictureSelector.obtainMultipleResult(data);
                    // 例如 LocalMedia 里面返回三种path
                    // 1.media.getPath(); 为原图path
                    // 2.media.getCutPath();为裁剪后path，需判断media.isCut();是否为true  注意：音视频除外
                    // 3.media.getCompressPath();为压缩后path，需判断media.isCompressed();是否为true  注意：音视频除外
                    // 如果裁剪并压缩了，以取压缩路径为准，因为是先裁剪后压缩的
                    if (isCardPeople) {
                        Glide.with(this).load(selectList.get(0).getPath()).into(ivCardPeople);
                    }else {
                        Glide.with(this).load(selectList.get(0).getCutPath()).into(ivCardNationalEmblem);
                    }

                    break;
            }
        }
    }
}
