package com.huijinlong.newretail.ui.spotgoods.buysell;

import android.content.Context;

/**
 * @author penghuaijin
 * @date 2018/10/19 11:28 AM
 * @Description: ()
 */
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.huijinlong.newretail.R;

public class TestArrayAdapter extends ArrayAdapter<String> {
    private Context mContext;
    private String[] mStringArray;

    public TestArrayAdapter(Context context, String[] stringArray) {
        super(context, android.R.layout.simple_spinner_item, stringArray);
        mContext = context;
        mStringArray = stringArray;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        //修改Spinner展开后的字体颜色
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
        }

        //此处text1是Spinner默认的用来显示文字的TextView
        TextView tv = (TextView) convertView.findViewById(android.R.id.text1);
//        tv.setBackgroundColor(mContext.getResources().getColor(R.color.c_2f3));
        tv.setText(mStringArray[position]);
        tv.setTextSize(22f);
//        tv.setTextColor(mContext.getResources().getColor(R.color.c_687));

        return convertView;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // 修改Spinner选择后结果的字体颜色
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(android.R.layout.simple_spinner_item, parent, false);
        }

        //此处text1是Spinner默认的用来显示文字的TextView
        TextView tv = (TextView) convertView.findViewById(android.R.id.text1);
//        tv.setBackgroundColor(mContext.getResources().getColor(R.color.c_2f3));
        tv.setText(mStringArray[position]);
        tv.setTextSize(18f);
//        tv.setTextColor(mContext.getResources().getColor(R.color.c_687));
        return convertView;
    }

}
