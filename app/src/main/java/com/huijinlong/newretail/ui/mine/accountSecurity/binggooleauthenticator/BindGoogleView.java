package com.huijinlong.newretail.ui.mine.accountSecurity.binggooleauthenticator;

import com.huijinlong.newretail.base.BaseView;
import com.huijinlong.newretail.requestbean.GoogleAuthenticatorSecretBean;
import com.huijinlong.newretail.requestbean.VerifyPhone4BindBean;

/**
 * Created by penghuaijin on 2018/9/12.
 */

public interface BindGoogleView extends BaseView{

        void showGetResult(String resultMessage);
        void getGoogleAuthenticatorSecretSuccess(GoogleAuthenticatorSecretBean bean);
        void bindGoogleSuccess(VerifyPhone4BindBean bean);
    }

