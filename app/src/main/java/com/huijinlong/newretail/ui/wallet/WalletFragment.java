package com.huijinlong.newretail.ui.wallet;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Selection;
import android.text.Spannable;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.huijinlong.newretail.R;
import com.huijinlong.newretail.base.BaseFragment;
import com.huijinlong.newretail.bean.AssetBean;
import com.huijinlong.newretail.eventbus.WalletEvent;
import com.huijinlong.newretail.ui.wallet.recharge.RechargeActivity;
import com.huijinlong.newretail.ui.wallet.rechargeCapital.RechargeCapitalActivity;
import com.huijinlong.newretail.ui.wallet.withdrawcash.WithDrawcashActivity;
import com.huijinlong.newretail.util.FontUtil;
import com.huijinlong.newretail.util.ToastUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.math.BigDecimal;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * @author zsg
 * @date 2018/10/25 14:40
 * @Version 1.0
 * @Description: (资产)
 */

public class WalletFragment extends BaseFragment<AssetView, AssetPresent> implements AssetView {

    @BindView(R.id.toolbar_func_left)
    ImageView toolbarFuncLeft;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar_func_fight)
    TextView toolbarFuncFight;
    @BindView(R.id.ll_right_func)
    LinearLayout llRightFunc;
    @BindView(R.id.toolbar_wallet)
    Toolbar toolbarWallet;
    @BindView(R.id.wallet_fragment_textview)
    TextView walletFragmentTextview;
    @BindView(R.id.wallet_btc)
    TextView walletBtc;
    @BindView(R.id.wallet_usdt)
    TextView walletUsdt;
    @BindView(R.id.tv_recharge_ic)
    TextView tvRechargeIc;
    @BindView(R.id.tv_recharge)
    TextView tvRecharge;
    @BindView(R.id.lin_recharge)
    LinearLayout linRecharge;
    @BindView(R.id.tv_withdraw_ic)
    TextView tvWithdrawIc;
    @BindView(R.id.tv_withdraw)
    TextView tvWithdraw;
    @BindView(R.id.tv_transaction_ic)
    TextView tvTransactionIc;
    @BindView(R.id.tv_transaction)
    TextView tvTransaction;
    @BindView(R.id.tv_feedback)
    TextView tvFeedback;
    @BindView(R.id.tv_feedback_right)
    CheckBox tvFeedbackRight;
    Unbinder unbinder;
    @BindView(R.id.lin_withdraw)
    LinearLayout linWithdraw;
    @BindView(R.id.lin_transaction)
    LinearLayout linTransaction;
    @BindView(R.id.lin)
    LinearLayout lin;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.tv_search)
    TextView tvSearch;
    @BindView(R.id.btn_iscanlook)
    ImageView btnIscanlook;
    @BindView(R.id.lin_nocontent)
    LinearLayout linNocontent;

    private AssetAdapter assetAdapter;
    private OnNavigationShowListener mOnNavigationShowListener;
    private boolean isshow = true;

    private static final String ARG_PARAM = "param";
    private String mParam;

    private static final int MESSAGE_SEARCH = 0x1;
    private static long INTERVAL = 1000; // 输入变化时间间隔

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        EventBus.getDefault().unregister(this);
        unbinder.unbind();
    }

    public static WalletFragment newInstance(String types) {
        WalletFragment fragment = new WalletFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM, types);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam = getArguments().getString(ARG_PARAM);
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.wallet_fragment;
    }

    @Override
    protected void initData() {
        EventBus.getDefault().register(this);

        tvFeedback.addTextChangedListener(textWatcher);
        mPresenter.getasset(0, tvFeedback.getText().toString(), 0);
        if (mParam.equals("交易账户")) {
            toolbarWallet.setVisibility(View.GONE);
        } else {

//            FontUtil.injectFont(toolbarFuncLeft);
            toolbarTitle.setText(getResources().getString(R.string.wallet));
            FontUtil.injectFont(toolbarFuncFight);
            toolbarFuncFight.setVisibility(View.GONE);
        }
        FontUtil.injectFont(tvRechargeIc);
        FontUtil.injectFont(tvWithdrawIc);
        FontUtil.injectFont(tvTransactionIc);
        FontUtil.injectFont(tvSearch);

    }


    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == MESSAGE_SEARCH) {
                if (isshow) {
                    mPresenter.getasset(1, tvFeedback.getText().toString() + "", 0);
                    isshow = false;
                } else {
                    mPresenter.getasset(0, tvFeedback.getText().toString() + "", 0);
                    isshow = true;
                }
            }
        }
    };

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (tvFeedback.getText().toString() != null) {
                if (mHandler.hasMessages(MESSAGE_SEARCH)) {
                    mHandler.removeMessages(MESSAGE_SEARCH);
                }
                mHandler.sendEmptyMessageDelayed(MESSAGE_SEARCH, INTERVAL);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };


    @Override
    protected AssetPresent initPresenter() {
        return new AssetPresent(this);
    }

    @Subscribe()
    public void onEvent(WalletEvent event) {
        if (event.isIsrefresh()) {
            mPresenter.getasset(0, tvFeedback.getText().toString(), 0);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);

        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void getAsset(AssetBean assetBean) {
        if (assetBean.getData() != null && assetBean.getData().size() != 0) {
            ToBtc(assetBean.getData());
            ToUSDT(assetBean.getData());
            assetAdapter = new AssetAdapter(assetBean.getData());
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
            assetAdapter.bindToRecyclerView(recyclerView);
            linNocontent.setVisibility(View.GONE);
        }
        else {
            linNocontent.setVisibility(View.VISIBLE);
           // ToastUtils.toast("该币种不存在");
        }
        // ToUSDT(assetBean.getData());
    }

    private void ToUSDT(List<AssetBean.DataBean> dataBeans) {
        BigDecimal all = new BigDecimal(0);
        for (AssetBean.DataBean dataBean : dataBeans) {
            if (!dataBean.getCurrency().equals("CNY")) {
                if (dataBean.getRate().compareTo(BigDecimal.ZERO) != 0)//不为0
                {
                    BigDecimal balance = new BigDecimal(dataBean.getBalance());
                    BigDecimal frozen = new BigDecimal(dataBean.getFrozen());
                    all = all.add(balance.multiply(dataBean.getRate())).add(frozen);
                }
            } else {
                BigDecimal balance = new BigDecimal(dataBean.getBalance());
                BigDecimal frozen = new BigDecimal(dataBean.getFrozen());
                all = all.add(balance.add(frozen));
            }
        }
        walletUsdt.setText(String.format("≈%sCNY", all.toString()));
    }

    private void ToBtc(List<AssetBean.DataBean> dataBeans) {
        BigDecimal all = new BigDecimal(0);
        BigDecimal rate = new BigDecimal(0);
        for (AssetBean.DataBean dataBean : dataBeans) {
            if (!dataBean.getCurrency().equals("USDT")) {
                if (dataBean.getRate().compareTo(BigDecimal.ZERO) != 0)//不为0
                {
                    BigDecimal balance = new BigDecimal(dataBean.getBalance());
                    BigDecimal frozen = new BigDecimal(dataBean.getFrozen());
                    all = all.add(balance.multiply(dataBean.getRate())).add(frozen);
                }
            } else {
                BigDecimal balance = new BigDecimal(dataBean.getBalance());
                BigDecimal frozen = new BigDecimal(dataBean.getFrozen());
                rate = dataBean.getRate();
                all = all.add(balance.add(frozen));
            }
        }

        if (all.toString().length() > 16) {
            walletBtc.setText(all.toString().substring(0, 17) + "USDT");
            //  walletUsdt.setText(String.format("≈%sUSDT", all.multiply(rate).toPlainString().substring(0,17)));
        } else {
            walletBtc.setText(all.toString() + "USDT");
        }
        walletUsdt.setText(String.format("≈%sCNY", all.multiply(rate).toPlainString()));
    }

    @Override
    public void showLoading() {
        mLoading.show();
    }

    @Override
    public void hideLoading() {
        mLoading.dismiss();
    }

    @Override
    public void showError(String error) {
        ToastUtils.toast(error);
    }

    @OnClick({R.id.toolbar_func_left, R.id.lin_recharge, R.id.lin_withdraw, R.id.lin_transaction, R.id.tv_feedback_right, R.id.btn_iscanlook})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.toolbar_func_left:
                mOnNavigationShowListener.OnNavigationShow();
                break;
            case R.id.lin_recharge:
                startToActivity(getActivity(), RechargeActivity.class);
                break;
            case R.id.lin_withdraw:
                startToActivity(getActivity(), WithDrawcashActivity.class);
                break;
            case R.id.lin_transaction:
                startToActivity(getActivity(), RechargeCapitalActivity.class);
                break;
            case R.id.tv_feedback_right:
                if (isshow) {
                    mPresenter.getasset(1, tvFeedback.getText().toString(), 0);
                    isshow = false;
                } else {
                    mPresenter.getasset(0, tvFeedback.getText().toString(), 0);
                    isshow = true;
                }
                break;
            case R.id.btn_iscanlook:
                if (btnIscanlook.isSelected()) {
                    btnIscanlook.setSelected(false);
                    btnIscanlook.setImageResource(R.mipmap.ic_eye_close);
                    showOrHidePwd(false, walletBtc, walletUsdt);

                } else {
                    btnIscanlook.setSelected(true);
                    btnIscanlook.setImageResource(R.mipmap.ic_eye_open);
                    showOrHidePwd(true, walletBtc, walletUsdt);
                }
                break;

        }
    }

    private void showOrHidePwd(boolean isSelected, TextView walletBtc, TextView walletUsdt) {
        if (isSelected) {
            walletBtc.setTransformationMethod(HideReturnsTransformationMethod
                    .getInstance());
            walletUsdt.setTransformationMethod(HideReturnsTransformationMethod
                    .getInstance());
        } else {
            walletBtc.setTransformationMethod(PasswordTransformationMethod
                    .getInstance());
            walletUsdt.setTransformationMethod(PasswordTransformationMethod
                    .getInstance());
        }
        walletBtc.postInvalidate();
        walletUsdt.postInvalidate();

        CharSequence charSequence = walletBtc.getText();
        if (charSequence instanceof Spannable) {
            Spannable spanText = (Spannable) charSequence;
            Selection.setSelection(spanText, charSequence.length());
        }

        CharSequence charSequences = walletUsdt.getText();
        if (charSequences instanceof Spannable) {
            Spannable spanText = (Spannable) charSequence;
            Selection.setSelection(spanText, charSequence.length());
        }
    }

    public interface OnNavigationShowListener {
        void OnNavigationShow();
    }

    public void setOnNavigationShowListener(OnNavigationShowListener onNavigationShowListener) {
        mOnNavigationShowListener = onNavigationShowListener;
    }
}
