package com.huijinlong.newretail.ui.wallet.addressmanage.withdraws;

import com.huijinlong.newretail.base.BaseBean;
import com.huijinlong.newretail.base.BaseView;
import com.huijinlong.newretail.bean.WithdrawsListBean;

/**
 * @author zsg
 * @date 2018/10/19 15:52
 * @Version 1.0
 * @Description: ()
 */
public interface WithDrawsView extends BaseView{
   void getaddressWithdrawList(WithdrawsListBean addressWithdrawListBean);
   void getWithdraw(BaseBean baseBean);
}
