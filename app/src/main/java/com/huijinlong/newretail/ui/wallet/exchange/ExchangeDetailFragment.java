package com.huijinlong.newretail.ui.wallet.exchange;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.huijinlong.newretail.R;
import com.huijinlong.newretail.base.BaseFragment;
import com.huijinlong.newretail.bean.ExchangeDetailBean;
import com.huijinlong.newretail.util.ToastUtils;
import com.scwang.smartrefresh.header.MaterialHeader;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * @author zsg
 * @date 2018/11/23 12:25
 * @Version 1.0
 * @Description: (兑换记录)
 */
public class ExchangeDetailFragment extends BaseFragment<ExchangeDetailView, ExchangeDetailPresent> implements ExchangeDetailView {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.refresh_layout)
    SmartRefreshLayout refreshLayout;
    Unbinder unbinder;
    @BindView(R.id.lin_nocontent)
    LinearLayout linNocontent;

    private ExchangeDetailAdapter exchangeDetailAdapter;
    private int page = 1;
    private List<ExchangeDetailBean.DataBean.ListBean> list = new ArrayList<>();

    public static ExchangeDetailFragment newInstance() {
        ExchangeDetailFragment fragment = new ExchangeDetailFragment();
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_exchangedetail;
    }

    @Override
    protected void initData() {
        mPresenter.getwithdraws(page, 10, "");

        exchangeDetailAdapter = new ExchangeDetailAdapter(list);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        exchangeDetailAdapter.bindToRecyclerView(recyclerView);

        refreshLayout.setRefreshHeader(new MaterialHeader(getContext()));
        refreshLayout.setOnRefreshListener(refreshlayout -> {
            page = 1;
            mPresenter.getwithdraws(page, 10, "");
            refreshLayout.setEnableLoadmore(true);

        });
        refreshLayout.setOnLoadmoreListener(refreshlayout -> {
            page += 1;
            mPresenter.getwithdraws(page, 10, "");
        });
    }

    @Override
    protected ExchangeDetailPresent initPresenter() {
        return new ExchangeDetailPresent(this);
    }

    @Override
    public void getexchangdetail(ExchangeDetailBean exchangeDetailBean) {
        if(exchangeDetailBean.getData().getList().size()==0)
        {
            linNocontent.setVisibility(View.VISIBLE);
        }else
        {
            linNocontent.setVisibility(View.GONE);
            if (page == 1) {
                list.clear();
                refreshLayout.finishRefresh();
            } else {
                refreshLayout.finishLoadmore();
            }
            list.addAll(exchangeDetailBean.getData().getList());

            if (list.size() == exchangeDetailBean.getData().getTotal()) {
                refreshLayout.setEnableLoadmore(false);
            }
            exchangeDetailAdapter.notifyDataSetChanged();
        }

    }

    @Override
    public void showLoading() {
        mLoading.show();
    }

    @Override
    public void hideLoading() {
        mLoading.dismiss();
    }

    @Override
    public void showError(String error) {
        ToastUtils.toast(error);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
