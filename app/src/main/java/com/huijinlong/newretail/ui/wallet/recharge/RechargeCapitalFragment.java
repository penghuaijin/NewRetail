package com.huijinlong.newretail.ui.wallet.recharge;

import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.huijinlong.newretail.R;
import com.huijinlong.newretail.base.BaseBean;
import com.huijinlong.newretail.base.BaseFragment;
import com.huijinlong.newretail.bean.CurrenciesBean;
import com.huijinlong.newretail.bean.WithdrawsListBean;
import com.huijinlong.newretail.bean.WthdrawChannelBean;
import com.huijinlong.newretail.ui.wallet.CurrencyActivity;
import com.huijinlong.newretail.ui.wallet.addressmanage.withdraws.WithDrawsPresenter;
import com.huijinlong.newretail.ui.wallet.addressmanage.withdraws.WithDrawsView;
import com.huijinlong.newretail.ui.wallet.exchange.TransferOutPresent;
import com.huijinlong.newretail.ui.wallet.exchange.TransferOutlView;
import com.huijinlong.newretail.ui.wallet.rechargeCapital.ChannelPresent;
import com.huijinlong.newretail.ui.wallet.rechargeCapital.ChannelView;
import com.huijinlong.newretail.util.FontUtil;
import com.huijinlong.newretail.util.ToastUtils;
import com.weigan.loopview.LoopView;
import com.weigan.loopview.OnItemSelectedListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * @author zsg
 * @date 2018/10/26 16:23
 * @Version 1.0
 * @Description: (资金划转)
 */
public class RechargeCapitalFragment extends BaseFragment<ChannelView, ChannelPresent> implements ChannelView, TransferOutlView, WithDrawsView {

    @BindView(R.id.tv)
    TextView tv;
    @BindView(R.id.tv_account)
    TextView tvAccount;
    @BindView(R.id.im_down)
    ImageView imDown;
    @BindView(R.id.tv_currency)
    TextView tvCurrency;
    @BindView(R.id.tv_chooses)
    TextView tvChooses;
    @BindView(R.id.lin_currency)
    LinearLayout linCurrency;
    @BindView(R.id.et_amount)
    EditText etAmount;
    @BindView(R.id.tv_address)
    TextView tvAddress;
    @BindView(R.id.tv_choose)
    ImageView tvChoose;
    @BindView(R.id.lin_address)
    LinearLayout linAddress;
    @BindView(R.id.tv_balance)
    TextView tvBalance;
    @BindView(R.id.bt_add)
    Button btAdd;
    Unbinder unbinder;
    private WthdrawChannelBean bean;
    private PopupWindow alarmWindow;
    private LinearLayout popupWindowView;
    private String type = "";
    private int cid;

    private ArrayList<String> list = new ArrayList<String>();

    private TransferOutPresent transferOutPresent;
    private WithDrawsPresenter withDrawsPresenter;

    public static RechargeCapitalFragment newInstance() {
        RechargeCapitalFragment fragment = new RechargeCapitalFragment();
//        Bundle args = new Bundle();
//        args.putString(ARG_PARAM, types);
//        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_rechargecapital;
    }

    @Override
    protected void initData() {
        FontUtil.injectFont(tv);
        FontUtil.injectFont(tvChooses);
        mPresenter.getChannel();
        initWindow();
        transferOutPresent = new TransferOutPresent(this);
        withDrawsPresenter = new WithDrawsPresenter(this);
    }

    public void initWindow() {
        View nemuView = LayoutInflater.from(getContext()).inflate(R.layout.popup_window_view, null, false);
        popupWindowView = (LinearLayout) nemuView.findViewById(R.id.ll_pop_item);

        alarmWindow = new PopupWindow(nemuView, LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
//        alarmWindow.setAnimationStyle(R.style.popAnimationPreview);
        alarmWindow.setBackgroundDrawable(new BitmapDrawable());
        alarmWindow.setOutsideTouchable(false);
        alarmWindow.setFocusable(true);
        alarmWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                Drawable rightDrawable = getResources().getDrawable(R.mipmap.dropdown);// 获取res下的图片drawable
                rightDrawable.setBounds(0, 0, rightDrawable.getMinimumWidth(), rightDrawable.getMinimumHeight());// 一定要设置setBounds();
                imDown.setBackground(rightDrawable);
                tvChoose.setBackground(rightDrawable);
                alarmWindow.dismiss();
            }
        });
    }

    public void showWindow(View v, WthdrawChannelBean wthdrawChannelBean) {
        if (!alarmWindow.isShowing()) {
            popupWindowView.removeAllViews();

            View popItem = LayoutInflater.from(getContext()).inflate(R.layout.dialog_wheelview, null, false);
            LoopView loopView = (LoopView) popItem.findViewById(R.id.loopView);
            TextView tv_cancle = (TextView) popItem.findViewById(R.id.tv_cancle);
            TextView tv_sure = (TextView) popItem.findViewById(R.id.tv_sure);

            ArrayList<String> list = new ArrayList<>();

            for (int x = 0; x < wthdrawChannelBean.getData().size(); x++) {
                list.add(String.valueOf(wthdrawChannelBean.getData().get(x).getName()));
            }
            type = list.get(0);

            loopView.setNotLoop();
            tv_cancle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alarmWindow.dismiss();
                }
            });
            tv_sure.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tvAccount.setText(type);
                    alarmWindow.dismiss();
                }
            });

            // 滚动监听
            loopView.setListener(new OnItemSelectedListener() {
                @Override
                public void onItemSelected(int index) {
                    type = String.valueOf(list.get(index));
                }
            });
            // 设置原始数据
            loopView.setItems(list);
            loopView.setTextSize(18);
            // loopView.setDividerColor(R.color.white);

            popupWindowView.addView(popItem);
            alarmWindow.showAtLocation(v, Gravity.BOTTOM, 0, 0);

        } else {
            alarmWindow.dismiss();
        }
    }

    @Override
    protected ChannelPresent initPresenter() {
        return new ChannelPresent(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = super.onCreateView(inflater, container, savedInstanceState);

        EventBus.getDefault().register(this);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
        unbinder.unbind();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getrechargecurrency(CurrenciesBean.DataBean dataBean) {
        if (dataBean.getType().equals("RechargeCapital")) {
            tvCurrency.setText(dataBean.getCurrency() + "");
            cid = dataBean.getCid();
            // address = dataBean.getExplorer();

            tvBalance.setText(getString(R.string.zdkz) + " " + dataBean.getBalance() + " " + dataBean.getCurrency());
            tvBalance.setVisibility(View.VISIBLE);
        }

    }

    @OnClick({R.id.lin_currency, R.id.tv_account, R.id.bt_add, R.id.lin_address})
    public void onViewClicked(View v) {
        switch (v.getId()) {
            case R.id.lin_currency:
                if (!tvAccount.getText().toString().equals(getString(R.string.qianbaotd)) &&
                        !tvAccount.getText().toString().equals(getString(R.string.jbptd))) {
                    ToastUtils.toast(getString(R.string.qxxzhzfs));
                } else {
                    Intent it = new Intent(getContext(), CurrencyActivity.class);
                    it.putExtra("type", "RechargeCapital");
                    startActivity(it);
                }
                break;
            case R.id.tv_account:
                if (bean != null) {
                    Drawable rightDrawable = getResources().getDrawable(R.mipmap.dropup);// 获取res下的图片drawable
                    rightDrawable.setBounds(0, 0, rightDrawable.getMinimumWidth(), rightDrawable.getMinimumHeight());// 一定要设置setBounds();
                    imDown.setBackground(rightDrawable);
                    showWindow(tvAccount, bean);
                }

                break;
            case R.id.bt_add:
                if (tvAccount.getText().toString().equals(getString(R.string.qianbaotd))) {
                    //钱包
                    withDrawsPresenter.getwithdraw(cid, etAmount.getText().toString(), type);
                } else {
                    //聚宝盆
                    transferOutPresent.getwithdraws(String.valueOf(cid), etAmount.getText().toString(), type);
                }
                break;
            case R.id.lin_address:
                Drawable rightDrawable = getResources().getDrawable(R.mipmap.dropup);// 获取res下的图片drawable
                rightDrawable.setBounds(0, 0, rightDrawable.getMinimumWidth(), rightDrawable.getMinimumHeight());// 一定要设置setBounds();
                tvChoose.setBackground(rightDrawable);

                if (tvAccount.getText().toString().equals(getString(R.string.qianbaotd))) {
                    withDrawsPresenter.getwithdraws(1, 100, 1);
                } else if (tvAccount.getText().toString().equals(getString(R.string.jbptd))) {
                    withDrawsPresenter.getwithdraws(1, 100, 2);
                } else {
                    ToastUtils.toast(getString(R.string.qxxzhzfs));
                    // Drawable rightDrawable = getResources().getDrawable(R.mipmap.dropdown);// 获取res下的图片drawable
                    rightDrawable.setBounds(0, 0, rightDrawable.getMinimumWidth(), rightDrawable.getMinimumHeight());// 一定要设置setBounds();
                    tvChoose.setBackground(rightDrawable);
                }
                break;
        }
    }

    public void showAddressWindow() {
        if (!alarmWindow.isShowing()) {
            popupWindowView.removeAllViews();
            // list.clear();
            View popItem = LayoutInflater.from(getContext()).inflate(R.layout.dialog_wheelview, null, false);
            LoopView loopView = (LoopView) popItem.findViewById(R.id.loopView);
            TextView tv_cancle = (TextView) popItem.findViewById(R.id.tv_cancle);
            TextView tv_sure = (TextView) popItem.findViewById(R.id.tv_sure);

            type = list.get(0);

            loopView.setNotLoop();
            tv_cancle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alarmWindow.dismiss();
                }
            });
            tv_sure.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tvAddress.setText(type);
                    alarmWindow.dismiss();
                }
            });

            // 滚动监听
            loopView.setListener(new OnItemSelectedListener() {
                @Override
                public void onItemSelected(int index) {
                    type = String.valueOf(list.get(index));
                }
            });
            // 设置原始数据
            loopView.setItems(list);
            loopView.setTextSize(18);
            // loopView.setDividerColor(R.color.c_2f3);
            popupWindowView.addView(popItem);
            alarmWindow.showAtLocation(linAddress, Gravity.BOTTOM, 0, 0);

        } else {
            alarmWindow.dismiss();
        }
    }

    @Override
    public void getChannelSuccess(WthdrawChannelBean wthdrawChannelBean) {
        bean = wthdrawChannelBean;
    }

    @Override
    public void showLoading() {
        mLoading.show();
    }

    @Override
    public void hideLoading() {
        mLoading.dismiss();
    }

    @Override
    public void showError(String error) {
        ToastUtils.toast(error);
    }

    @Override
    public void getaddressWithdrawList(WithdrawsListBean addressWithdrawListBean) {

        list.clear();
        for (int x = 0; x < addressWithdrawListBean.getData().getList().size(); x++) {
            if (tvCurrency.getText().toString().equals(addressWithdrawListBean.getData().getList().get(x).getCurrency())) {
                list.add(addressWithdrawListBean.getData().getList().get(x).getAddress());
            }
        }
        if (list.size() == 0) {
            ToastUtils.toast("无地址");
            Drawable rightDrawable = getResources().getDrawable(R.mipmap.dropdown);// 获取res下的图片drawable
            rightDrawable.setBounds(0, 0, rightDrawable.getMinimumWidth(), rightDrawable.getMinimumHeight());// 一定要设置setBounds();
            tvChoose.setBackground(rightDrawable);
        } else {
            showAddressWindow();
        }

    }

    @Override
    public void getWithdraw(BaseBean baseBean) {
        ToastUtils.toast(getString(R.string.hzcg));
        getActivity().finish();
    }

    @Override
    public void gettransferOut(BaseBean baseBean) {
        ToastUtils.toast(getString(R.string.hzcg));
        getActivity().finish();
    }
}
