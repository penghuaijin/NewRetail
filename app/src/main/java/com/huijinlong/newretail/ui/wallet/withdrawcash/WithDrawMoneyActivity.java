package com.huijinlong.newretail.ui.wallet.withdrawcash;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.huijinlong.newretail.R;
import com.huijinlong.newretail.base.BaseActivity;
import com.huijinlong.newretail.base.BaseBean;
import com.huijinlong.newretail.bean.WithdrawsListBean;
import com.huijinlong.newretail.ui.wallet.addressmanage.withdraws.WithDrawsPresenter;
import com.huijinlong.newretail.ui.wallet.addressmanage.withdraws.WithDrawsView;
import com.huijinlong.newretail.util.ToastUtils;
import com.weigan.loopview.LoopView;
import com.weigan.loopview.OnItemSelectedListener;

import java.math.BigDecimal;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author zsg
 * @date 2018/11/13 17:21
 * @Version 1.0
 * @Description: (提现)
 */
public class WithDrawMoneyActivity extends BaseActivity<WithDrawsView, WithDrawsPresenter> implements WithDrawsView {

    @BindView(R.id.bt_add)
    Button btAdd;
    @BindView(R.id.lin_withdraw)
    FrameLayout linWithdraw;
    @BindView(R.id.tv_address)
    TextView tvAddress;
    @BindView(R.id.tv_choose)
    ImageView tvChoose;
    @BindView(R.id.lin_address)
    LinearLayout linAddress;
    @BindView(R.id.tv_currency1)
    TextView tvCurrency1;
    @BindView(R.id.tv_currency2)
    TextView tvCurrency2;
    @BindView(R.id.tv_currency3)
    TextView tvCurrency3;
    @BindView(R.id.ed_num)
    EditText edNum;
    @BindView(R.id.tv_fee)
    TextView tvFee;
    @BindView(R.id.tv_acount)
    TextView tvAcount;
    @BindView(R.id.tv_limit)
    TextView tvLimit;
    @BindView(R.id.tv_hight)
    TextView tvHight;
    @BindView(R.id.tv_fees)
    TextView tvFees;

    private PopupWindow alarmWindow;
    private LinearLayout popupWindowView;
    private String type;
    private ArrayList<String> list = new ArrayList<>();

    @SuppressLint("StringFormatMatches")
    @Override
    protected void initView() {
        setToolbarTitle(getString(R.string.tixian));
        setmToolbarRightBtnColor(R.color.c_55a);
        setToolbarRightBtn(true, false, getString(R.string.txjl), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(WithDrawMoneyActivity.this, WithDrawListActivity.class);
                it.putExtra("title", getString(R.string.txjl));
//                if(getIntent().getStringExtra("type").equals("only"))
//                {
                it.putExtra("currency", getIntent().getStringExtra("currency"));
//                }else
////                {
////                    it.putExtra("currency","");
////                }
                startActivity(it);
            }
        });
        initWindow();
        tvCurrency1.setText(getIntent().getStringExtra("currency"));
        tvCurrency2.setText(getIntent().getStringExtra("currency"));
        tvCurrency3.setText(getIntent().getStringExtra("currency"));
        if (TextUtils.isEmpty(getIntent().getStringExtra("fee"))) {
            tvFee.setText("0.00000000");
        } else {
            tvFee.setText(getIntent().getStringExtra("fee") + "");
        }
        edNum.addTextChangedListener(textWatcher);


        tvLimit.setText(String.format(getString(R.string.zfje), getIntent().getStringExtra("limit"), getIntent().getStringExtra("currency")));
        tvHight.setText(String.format(getString(R.string.txxe), "100000", getIntent().getStringExtra("currency")));
        tvFees.setText(String.format(getString(R.string.txsxf), "0", getIntent().getStringExtra("fee"), getIntent().getStringExtra("currency")));

//        else
//            mPresenter.getwithdraws(1, 10, 1);

    }

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (!TextUtils.isEmpty(edNum.getText().toString())) {
                BigDecimal balance = new BigDecimal(edNum.getText().toString());
                BigDecimal frozen;
                if (TextUtils.isEmpty(getIntent().getStringExtra("fee"))) {
                    frozen = new BigDecimal("0");
                    tvFee.setText("0.00000000");
                } else {
                    frozen = new BigDecimal(getIntent().getStringExtra("fee"));
                }
                BigDecimal decimal = balance.subtract(frozen);
                tvAcount.setText(decimal.setScale(8).stripTrailingZeros().toPlainString() + "");
            }

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    public void initWindow() {
        View nemuView = LayoutInflater.from(this).inflate(R.layout.popup_window_view, null, false);
        popupWindowView = (LinearLayout) nemuView.findViewById(R.id.ll_pop_item);

        alarmWindow = new PopupWindow(nemuView, LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
//        alarmWindow.setAnimationStyle(R.style.popAnimationPreview);
        alarmWindow.setBackgroundDrawable(new BitmapDrawable());
        alarmWindow.setOutsideTouchable(false);
        alarmWindow.setFocusable(true);
        alarmWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                Drawable rightDrawable = getResources().getDrawable(R.mipmap.dropdown);// 获取res下的图片drawable
                rightDrawable.setBounds(0, 0, rightDrawable.getMinimumWidth(), rightDrawable.getMinimumHeight());// 一定要设置setBounds();
                tvChoose.setBackground(rightDrawable);
                alarmWindow.dismiss();
            }
        });
    }

    public void showWindow(View v) {
        if (!alarmWindow.isShowing()) {
            popupWindowView.removeAllViews();
            list.clear();
            View popItem = LayoutInflater.from(this).inflate(R.layout.dialog_wheelview, null, false);
            LoopView loopView = (LoopView) popItem.findViewById(R.id.loopView);
            TextView tv_cancle = (TextView) popItem.findViewById(R.id.tv_cancle);
            TextView tv_sure = (TextView) popItem.findViewById(R.id.tv_sure);

            ArrayList<String> addresslist=new ArrayList<>();
            addresslist=getIntent().getStringArrayListExtra("addresslist");
                for (int x = 0; x < addresslist.size(); x++) {
                    list.add(addresslist.get(x));
                }
            type = list.get(0);


            loopView.setNotLoop();
            tv_cancle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alarmWindow.dismiss();
                }
            });
            tv_sure.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tvAddress.setText(type);
                    alarmWindow.dismiss();
                }
            });

            // 滚动监听
            loopView.setListener(new OnItemSelectedListener() {
                @Override
                public void onItemSelected(int index) {
                    type = String.valueOf(list.get(index));
                }
            });
            // 设置原始数据
            loopView.setItems(list);
            loopView.setTextSize(18);
            // loopView.setDividerColor(R.color.c_2f3);
            popupWindowView.addView(popItem);
            alarmWindow.showAtLocation(v, Gravity.BOTTOM, 0, 0);

        } else {
            alarmWindow.dismiss();
        }
    }


    @Override
    protected int getContentView() {
        return R.layout.activity_withdrawmoney;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
    }

    @Override
    public WithDrawsPresenter initPresenter() {
        return new WithDrawsPresenter(this);
    }

    @OnClick({R.id.tv_choose, R.id.bt_add, R.id.lin_address})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_choose:
                break;
            case R.id.bt_add:
                mPresenter.getwithdraw(getIntent().getIntExtra("cid", 0), edNum.getText().toString(), tvAddress.getText().toString());
                break;
            case R.id.lin_address:
                Drawable rightDrawable = getResources().getDrawable(R.mipmap.dropup);// 获取res下的图片drawable
                rightDrawable.setBounds(0, 0, rightDrawable.getMinimumWidth(), rightDrawable.getMinimumHeight());// 一定要设置setBounds();
                tvChoose.setBackground(rightDrawable);
                showWindow(view);
                break;

        }
    }

    @Override
    public void getaddressWithdrawList(WithdrawsListBean addressWithdrawListBean) {
//        for (int x = 0; x < addressWithdrawListBean.getData().getList().size(); x++) {
//            if (getIntent().getStringExtra("currency").equals(addressWithdrawListBean.getData().getList().get(x).getCurrency())) {
//                list.add(addressWithdrawListBean.getData().getList().get(x).getAddress());
//            }
//        }
//        if(list.size()==0)
//        {
//           ToastUtils.toast("当前币种无地址");
//        }

    }

    @Override
    public void getWithdraw(BaseBean baseBean) {
        ToastUtils.toast(getString(R.string.czcg));
        finish();
    }

    @Override
    public void showLoading() {
        mLoadingDialog.show();
    }

    @Override
    public void hideLoading() {
        mLoadingDialog.dismiss();
    }

    @Override
    public void showError(String error) {
        ToastUtils.toast(error);
    }
}
