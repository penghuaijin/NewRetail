package com.huijinlong.newretail.ui.spotgoods.commission;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.huijinlong.newretail.R;
import com.huijinlong.newretail.requestbean.HistoryBean;
import com.huijinlong.newretail.util.FontUtil;

import java.text.DecimalFormat;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author penghuaijin
 * @date 2018/10/16 8:16 PM
 * @Description: ()
 */

public class HistoryCommissionAdapter extends RecyclerView.Adapter<HistoryCommissionAdapter.ViewHolder> {

    private Context context;
    private List<HistoryBean.DataBean.ListBean> list;

    public HistoryCommissionAdapter(Context context, List<HistoryBean.DataBean.ListBean> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_history_commission, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        HistoryBean.DataBean.ListBean bean = list.get(position);
        FontUtil.injectFont(holder.tvArrow);
        holder.tvSide.setText(bean.getSide());
        holder.tvSymbol.setText(bean.getSymbol());
        holder.tvStatus.setText(bean.getStatusX());

        holder.tvTime.setText(bean.getTime());
        holder.tvPriceName.setText(String.format(context.getString(R.string.wtjzw), bean.getSymbol().split("/")[1]));
        holder.tvPrice.setText(bean.getPrice());
        holder.tvQtyName.setText(String.format(context.getString(R.string.wtlzw), bean.getSymbol().split("/")[0]));
        holder.tvQty.setText(bean.getQty());

        double avgPrice = Double.parseDouble(bean.getAvgPrice());
        double filled = Double.parseDouble(bean.getFilled());
        DecimalFormat dateFormat = new DecimalFormat("#.00000000");
        String total = dateFormat.format(avgPrice * filled);
        if (total.startsWith(".")){
            total="0"+total;
        }
        holder.tvTotal.setText(total);
        holder.tvAvgPrice.setText(bean.getAvgPrice());
        holder.tvFilled.setText(bean.getFilled());
        holder.tvFilledName.setText(String.format(context.getString(R.string.cjlzw), bean.getSymbol().split("/")[0]));

        if (bean.getSide().equals(context.getString(R.string.mc))) {
            holder.tvSide.setTextColor(context.getResources().getColor(R.color.main_red));
        } else {
            holder.tvSide.setTextColor(context.getResources().getColor(R.color.main_blue));
        }

        if (bean.getCancelStatus() == 0) {
            holder.tvArrow.setVisibility(View.VISIBLE);
            holder.tvStatus.setTextColor(Color.WHITE);
        } else {
            holder.tvArrow.setVisibility(View.GONE);
            holder.tvStatus.setTextColor(context.getResources().getColor(R.color.c_687));

        }


    }

    @Override
    public int getItemCount() {
        return list != null ? list.size() : 0;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_side)
        TextView tvSide;
        @BindView(R.id.tv_symbol)
        TextView tvSymbol;
        @BindView(R.id.tv_status)
        TextView tvStatus;
        @BindView(R.id.tv_arrow)
        TextView tvArrow;
        @BindView(R.id.tv_price_name)
        TextView tvPriceName;
        @BindView(R.id.tv_qty_name)
        TextView tvQtyName;
        @BindView(R.id.tv_time)
        TextView tvTime;
        @BindView(R.id.tv_price)
        TextView tvPrice;
        @BindView(R.id.tv_qty)
        TextView tvQty;
        @BindView(R.id.tv_filled_name)
        TextView tvFilledName;
        @BindView(R.id.tv_total)
        TextView tvTotal;
        @BindView(R.id.tv_avgPrice)
        TextView tvAvgPrice;
        @BindView(R.id.tv_filled)
        TextView tvFilled;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
