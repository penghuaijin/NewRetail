package com.huijinlong.newretail.ui.mine.apimanger;

import com.huijinlong.newretail.base.BasePresenter;
import com.huijinlong.newretail.net.MyCallback;
import com.huijinlong.newretail.net.RequestBuilder;
import com.huijinlong.newretail.requestbean.EditApiBean;
import com.huijinlong.newretail.util.MD5Util;
import com.huijinlong.newretail.util.SpUtil;

import java.util.TreeMap;

/**
 * @author zsg
 * @date 2018/10/25 14:40
 * @Version 1.0
 * @Description: ()
 */
public class ApiMangerPresent extends BasePresenter<ApiMangerView> {
    private ApiMangerView apiMangerView;

    public ApiMangerPresent(ApiMangerView mViews) {
        this.apiMangerView = mViews;
    }

    public void editApiKey(String remark,String bindIp) {
        apiMangerView.showLoading();
        TreeMap<String, String> params = new TreeMap<>();
        params.put("remark",remark);
        params.put("bindIp", bindIp);
        params.put("deviceType", "android");
        params.put("language", SpUtil.getString(SpUtil.LANGUAGE,"zh-cn"));
        RequestBuilder.execute(RequestBuilder.getNetService(true).editApiKey(MD5Util.getSignValue(params), params), "", new MyCallback<EditApiBean>() {
            @Override
            public void onSuccess(EditApiBean result) {
                super.onSuccess(result);
                apiMangerView.getEditApiSuccess(result);
            }

            @Override
            public void onEmpty(EditApiBean result) {
                super.onEmpty(result);

            }

            @Override
            public void onBusinessFailure(Exception e) {
                super.onBusinessFailure(e);
                apiMangerView.showError(e.getMessage());
            }

            @Override
            public void onFinalResponse(boolean isSuccess) {
                super.onFinalResponse(isSuccess);
                apiMangerView.hideLoading();
            }
        });
    }
}
