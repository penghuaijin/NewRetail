package com.huijinlong.newretail.ui.market;

import android.content.Context;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.google.gson.Gson;
import com.huijinlong.newretail.R;
import com.huijinlong.newretail.eventbus.MarketItemCLickEvent;
import com.huijinlong.newretail.requestbean.SpotProductsBean;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

/**
 * @author penghuaijin
 * @date 2018/11/27 11:36 AM
 * @Description: ()
 */
public class MarketListAdapter extends BaseQuickAdapter<SpotProductsBean.DataBean.ProductsBean.AreaBean.MainBean,BaseViewHolder> {

    private Context mContext;

    public MarketListAdapter(@Nullable List<SpotProductsBean.DataBean.ProductsBean.AreaBean.MainBean> data, Context context) {
        super(R.layout.item_market_list_fragment,data);
        mContext = context;

    }






    @Override
    protected void convert(BaseViewHolder holder, SpotProductsBean.DataBean.ProductsBean.AreaBean.MainBean model) {
        TextView tv_rate = holder.itemView.findViewById(R.id.tv_rate);
        TextView tv_price = holder.itemView.findViewById(R.id.tv_price);
        if (model.getRate().startsWith("-")) {
            tv_rate.setBackgroundResource(R.drawable.bg_round_btn_red);
            tv_price.setTextColor(mContext.getResources().getColor(R.color.c_ff5));
        }else {
            tv_rate.setBackgroundResource(R.drawable.bg_round_btn_blue);
            tv_price.setTextColor(mContext.getResources().getColor(R.color.main_blue));
        }
        holder.setText(R.id.tv_rate, model.getRate());
        String[] split = model.getSymbol().split("/");
        holder.setText(R.id.tv_symbol_name, split[0]);
        holder.setText(R.id.tv_symbol_type_name, "/" + split[1]);
        holder.setText(R.id.tv_price, model.getLast());
    }


    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }
}
