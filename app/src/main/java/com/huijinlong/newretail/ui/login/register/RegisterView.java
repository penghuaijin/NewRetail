package com.huijinlong.newretail.ui.login.register;

import com.huijinlong.newretail.base.BaseView;
import com.huijinlong.newretail.requestbean.LoginBean;

import java.util.Map;

/**
 * Created by penghuaijin on 2018/9/12.
 */

public interface RegisterView extends BaseView{
        void registerSuccess(LoginBean loginBean);
        void getCaptchaSuccess();
}
