package com.huijinlong.newretail.ui.mine.setting;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.huijinlong.newretail.R;
import com.huijinlong.newretail.base.BaseActivity;
import com.huijinlong.newretail.base.BasePresenter;
import com.huijinlong.newretail.constant.Constant;
import com.huijinlong.newretail.ui.mine.accountSecurity.bindemail.BindEmailActivity;
import com.huijinlong.newretail.util.SpUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UserSettingActivity extends BaseActivity {
    @BindView(R.id.tv_nick)
    TextView tvNick;
    @BindView(R.id.tv_account)
    TextView tvAccount;
    @BindView(R.id.tv_email)
    TextView tvEmail;
    @BindView(R.id.tv_bind_email)
    TextView tvBindEmail;
    @BindView(R.id.tv_uid)
    TextView tvUid;
//
//    @BindView(R.id.tv_nick)
//    TextView tvNick;
//    @BindView(R.id.tv_account)
//    TextView tvAccount;
//    @BindView(R.id.tv_email)
//    TextView tvEmail;
//    @BindView(R.id.tv_bind_email)
//    TextView tvBindEmail;
//    @BindView(R.id.tv_uid)
//    TextView tvUid;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
////        setContentView(R.layout.activity_user_setting);
//        ButterKnife.bind(this);
////        initializeActionToolBar();
//    }

    @Override
    public BasePresenter initPresenter() {
        return null;
    }

    @Override
    protected void initView() {
        tvNick.setText(SpUtil.getString(SpUtil.NICK_NAME, ""));
        tvEmail.setText(SpUtil.getString(SpUtil.EMAIL, ""));
        tvAccount.setText(SpUtil.getString(SpUtil.MOBILE, ""));
        tvUid.setText(SpUtil.getString(SpUtil.USER_ID, ""));
//        setToolbarVisibility(true);
//        setToolbarClick(true);
        setToolbarTitle(getString(R.string.grsz));
//        setTitle("个人设置");
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_user_setting;
    }

//    private void initializeActionToolBar() {
//        Toolbar mToolbar = findViewById(R.id.toolbar);
//        TextView tvTitle = findViewById(R.id.toolbar_title);
//        tvTitle.setText("身份认证");
//        initToolBar(mToolbar, true, "");
//    }

    @OnClick(R.id.tv_bind_email)
    public void onViewClicked(View v) {
        switch (v.getId()) {
            case R.id.tv_bind_email:
                Intent intent = new Intent(this, BindEmailActivity.class);
                if (TextUtils.isEmpty(SpUtil.getString(SpUtil.EMAIL, ""))) {
                    intent.putExtra("action", Constant.BIND_EMAIL);
                } else {
                    intent.putExtra("action", Constant.REBIND_EMAIL);
                }
                startActivity(intent);
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }


}
