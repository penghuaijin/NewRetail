package com.huijinlong.newretail.ui.mine.authenticationsecond;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.huijinlong.newretail.R;
import com.huijinlong.newretail.base.BaseActivity;
import com.huijinlong.newretail.base.BaseBean;
import com.huijinlong.newretail.requestbean.UserIdentityAuthBean;
import com.huijinlong.newretail.util.FontUtil;
import com.huijinlong.newretail.util.ToastUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by penghuaijin on 2018/9/29.
 */

public class AuthenticationFirstActivity extends BaseActivity<IdentityAuthView, IdentityAuthPresenter> implements IdentityAuthView {
    @BindView(R.id.tv_lv1_arrow)
    TextView tvLv1Arrow;
    @BindView(R.id.tv_lv2_arrow)
    TextView tvLv2Arrow;
    @BindView(R.id.tv_lv3_arrow)
    TextView tvLv3Arrow;
    @BindView(R.id.btn_ok)
    Button btnOk;
    @BindView(R.id.tv_nationality)
    TextView tvNationality;
    @BindView(R.id.et_name)
    EditText etName;
    @BindView(R.id.et_id)
    EditText etId;
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_authentication_first);
//        initializeActionToolBar();
//        initView();
//    }

    @Override
    protected void initView() {
        ButterKnife.bind(this);
        setToolbarTitle(getString(R.string.sfrz));
        FontUtil.injectFont(tvLv1Arrow);
        FontUtil.injectFont(tvLv2Arrow);
        FontUtil.injectFont(tvLv3Arrow);
        mPresenter.getuserIdentityAuth();
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_authentication_first;
    }

    @Override
    public IdentityAuthPresenter initPresenter() {
        return new IdentityAuthPresenter(this);
    }

    @Override
    public void showLoading() {
        mLoadingDialog.show();
    }

    @Override
    public void hideLoading() {
        mLoadingDialog.hide();
    }

    @Override
    public void showError(String error) {
        ToastUtils.toast(error);
    }

    @Override
    public void editIdentityAuthSuccess(BaseBean bean) {
        ToastUtils.toast(bean.getMsg());
        btnOk.setBackgroundResource(R.drawable.bg_round_btn_gray2);
        btnOk.setEnabled(false);
        etId.setEnabled(false);
        etName.setEnabled(false);
    }

    @Override
    public void getUserIdentityAuthSuccess(UserIdentityAuthBean bean) {

        if (bean != null) {
            switch (bean.getData().getAuthOk()) {
                case 1:
                case 2:
                    etId.setEnabled(false);
                    etId.setText(bean.getData().getIdentityNumber());
                    etName.setEnabled(false);
                    etName.setText(bean.getData().getTruename());
                    btnOk.setEnabled(false);
                    tvLv1Arrow.setVisibility(View.GONE);
                    tvLv2Arrow.setVisibility(View.GONE);
                    tvLv3Arrow.setVisibility(View.GONE);
                    btnOk.setVisibility(View.GONE);
//                    btnOk.setBackgroundResource(R.drawable.bg_round_btn_gray2);
                    break;
                case 0:
                case 3:
                    etName.setText(bean.getData().getTruename());
                    etId.setText(bean.getData().getIdentityNumber());
                    break;
            }
        }
    }


    @OnClick({R.id.tv_nationality, R.id.btn_ok})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_nationality:

                break;
            case R.id.btn_ok:
                if (TextUtils.isEmpty(etName.getText())) {
                    ToastUtils.toast(getString(R.string.srname));
                    return;
                }
                if (TextUtils.isEmpty(etId.getText())) {
                    ToastUtils.toast(getString(R.string.srsfz));
                    return;
                }
                mPresenter.editIdentityAuth(etName.getText().toString(), etId.getText().toString());
                break;
        }
    }

//    private void initializeActionToolBar() {
//        Toolbar mToolbar = findViewById(R.id.toolbar);
//        TextView tvTitle = findViewById(R.id.toolbar_title);
//        tvTitle.setText("身份认证");
//        initToolBar(mToolbar, true, "");
//    }
}
