package com.huijinlong.newretail.ui.spotgoods;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.google.gson.Gson;
import com.huijinlong.newretail.R;
import com.huijinlong.newretail.eventbus.MarketItemCLickEvent;
import com.huijinlong.newretail.ui.market.ProjectInfoPagerAdapter;
import com.huijinlong.newretail.base.BaseFragment;
import com.huijinlong.newretail.eventbus.ProductTypeEvent;
import com.huijinlong.newretail.eventbus.SpotTitleEvent;
import com.huijinlong.newretail.requestbean.SpotProductsBean;
import com.huijinlong.newretail.ui.spotgoods.kline.KlineActivity;
import com.huijinlong.newretail.ui.spotgoods.buysell.SpotBuyFragment;
import com.huijinlong.newretail.ui.spotgoods.buysell.SpotSellFragment;
import com.huijinlong.newretail.ui.spotgoods.commission.CurrentCommissionFragment;
import com.huijinlong.newretail.ui.spotgoods.commission.HistoryCommissionFragment;
import com.huijinlong.newretail.util.FontUtil;
import com.huijinlong.newretail.util.SocketUtil;
import com.huijinlong.newretail.util.ToastUtils;

import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.ViewPagerHelper;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.CommonPagerTitleView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by penghuaijin on 2018/9/10.
 */

public class SpotFragment extends BaseFragment<SpotView, SpotPresenter> implements SpotView {
    @BindView(R.id.toolbar_func_left)
    TextView toolbarFuncLeft;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar_func_fight)
    TextView toolbarFuncFight;
    @BindView(R.id.ll_right_func)
    LinearLayout llRightFunc;
    @BindView(R.id.toolbar_market)
    Toolbar toolbarMarket;
    @BindView(R.id.magic_indicator_spot)
    MagicIndicator magicIndicatorSpot;
    @BindView(R.id.view_pager_spot)
    ViewPager viewPagerSpot;
    Unbinder unbinder;
    @BindView(R.id.toolbar_func_fight2)
    TextView toolbarFuncFight2;

    private ArrayList<Fragment> fragmentList = new ArrayList<>();
    private ProjectInfoPagerAdapter projectInfoPagerAdapter;
    private List<String> stringList = new ArrayList<>();
    private CommonNavigator commonNavigator;
    private PopupWindow alarmWindow;
    private LinearLayout popupWindowView;
    private List<ProductTypeEvent.Product> productsList = new ArrayList<>();


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.spot_fragment;
    }

    @Override
    protected void initData() {
        EventBus.getDefault().register(this);

        FontUtil.injectFont(toolbarFuncLeft);
       // toolbarFuncLeft.setVisibility(View.GONE);
        FontUtil.injectFont(toolbarFuncFight);
        mPresenter.getsSpotProducts();
        initDatas();


    }

    @Override
    protected SpotPresenter initPresenter() {
        return new SpotPresenter(this);
    }


    private void initDatas() {

        fragmentList.clear();
        stringList.clear();

        stringList.add(getString(R.string.mr));
        stringList.add(getString(R.string.mc));
        stringList.add(getString(R.string.dqwt));
        stringList.add(getString(R.string.lswt));
        viewPagerSpot.setOffscreenPageLimit(2);


    }

    private void initMagicIndicator() {
        magicIndicatorSpot.setBackgroundColor(getResources().getColor(R.color.theme_color));
        commonNavigator = new CommonNavigator(getContext());
        commonNavigator.setAdjustMode(true);

        commonNavigator.setAdapter(new CommonNavigatorAdapter() {
            @Override
            public int getCount() {
                return stringList.size();
            }

            @Override
            public IPagerTitleView getTitleView(Context context, final int i) {
                final CommonPagerTitleView commonPagerTitleView = new CommonPagerTitleView(context);
                View inflate = LayoutInflater.from(context).inflate(R.layout.simple_pager_title_layout, null);
                final TextView textView = inflate.findViewById(R.id.title_text);
                textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                textView.setText(stringList.get(i));
                final ImageView icon = inflate.findViewById(R.id.title_icon);
                icon.setVisibility(View.GONE);
                commonPagerTitleView.setContentView(inflate);
                commonPagerTitleView.setOnPagerTitleChangeListener(new CommonPagerTitleView.OnPagerTitleChangeListener() {
                    @Override
                    public void onSelected(int i, int i1) {
                        textView.setTextColor(getResources().getColor(R.color.c_55a));
                        if (i < 2) {
                            llRightFunc.setVisibility(View.GONE);
                        } else {
                            llRightFunc.setVisibility(View.VISIBLE);

                        }

                    }

                    @Override
                    public void onDeselected(int i, int i1) {
                        textView.setTextColor(getResources().getColor(R.color.white));

                    }

                    @Override
                    public void onLeave(int i, int i1, float v, boolean b) {

                    }

                    @Override
                    public void onEnter(int i, int i1, float v, boolean b) {


                    }
                });
                commonPagerTitleView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        viewPagerSpot.setCurrentItem(i);

                    }
                });

                return commonPagerTitleView;
            }

            @Override
            public IPagerIndicator getIndicator(Context context) {
                LinePagerIndicator linePagerIndicator = new LinePagerIndicator(context);
                linePagerIndicator.setMode(LinePagerIndicator.MODE_WRAP_CONTENT);
                linePagerIndicator.setColors(getResources().getColor(R.color.c_55a));
                return linePagerIndicator;
            }
        });

        magicIndicatorSpot.setNavigator(commonNavigator);
        commonNavigator.notifyDataSetChanged();
        ViewPagerHelper.bind(magicIndicatorSpot, viewPagerSpot);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);

        unbinder.unbind();
    }


    @Override
    public void spotProductsSuccess(SpotProductsBean spotProductsBean) {

        toolbarTitle.setText(spotProductsBean.getData().getProducts().get(0).getArea().getMain().get(0).getSymbol());

        for (SpotProductsBean.DataBean.ProductsBean productsBean : spotProductsBean.getData().getProducts()) {
            for (SpotProductsBean.DataBean.ProductsBean.AreaBean.MainBean mainBean : productsBean.getArea().getMain()) {

                String json = new Gson().toJson(mainBean);
                ProductTypeEvent.Product product = new Gson().fromJson(json, ProductTypeEvent.Product.class);
                productsList.add(product);
            }
        }

        SpotBuyFragment spotBuyFragment = new SpotBuyFragment();

        SpotSellFragment spotSellFragment = new SpotSellFragment();

        CurrentCommissionFragment currentCommissionFragment = new CurrentCommissionFragment();

        HistoryCommissionFragment historyCommissionFragment = new HistoryCommissionFragment();

        if (productsList != null && productsList.size() > 0) {
            spotBuyFragment.setSymbol(productsList.get(0));
            spotSellFragment.setSymbol(productsList.get(0));
            currentCommissionFragment.setInstrument(productsList.get(0).getInstrument());
            historyCommissionFragment.setInstrument(productsList.get(0).getInstrument());
            EventBus.getDefault().post(productsList.get(0));
        }


        fragmentList.add(spotBuyFragment);
        fragmentList.add(spotSellFragment);
        fragmentList.add(currentCommissionFragment);
        fragmentList.add(historyCommissionFragment);


        projectInfoPagerAdapter = new ProjectInfoPagerAdapter(getFragmentManager(), fragmentList);
        viewPagerSpot.setAdapter(projectInfoPagerAdapter);
        initMagicIndicator();

        initWindow();
        commonNavigator.notifyDataSetChanged();
        projectInfoPagerAdapter.notifyDataSetChanged();


        String toJson = new Gson().toJson(productsList.get(0).getEnv());
        SpotTitleEvent.EnvBean envBean = new Gson().fromJson(toJson, SpotTitleEvent.EnvBean.class
        );
        EventBus.getDefault().post(new SpotTitleEvent(
                productsList.get(0).getInstrument(),
                productsList.get(0).getSymbol(),
                productsList.get(0).getPriceDecimal(),
                productsList.get(0).getQtyDecimal(),
                productsList.get(0).getAmountDecimal(),
                productsList.get(0).getLast(),
                productsList.get(0).getRate(),
                envBean
        ));
        SocketUtil.getInstance().destroy();
        SocketUtil.getInstance().init5000(spotProductsBean.getData().getProducts().get(0).getArea().getMain().get(0).getInstrument(), true, false);
    }


    @Override
    public void showLoading() {
        mLoading.show();
    }

    @Override
    public void hideLoading() {
        mLoading.dismiss();

    }

    @Override
    public void showError(String error) {
        ToastUtils.toast(error);
    }

    public void initWindow() {
        View nemuView = LayoutInflater.from(getContext()).inflate(R.layout.popup_window_view, null, false);
        popupWindowView = (LinearLayout) nemuView.findViewById(R.id.ll_pop_item);

        alarmWindow = new PopupWindow(nemuView, LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        alarmWindow.setBackgroundDrawable(new BitmapDrawable());
        alarmWindow.setOutsideTouchable(false);
        alarmWindow.setFocusable(true);
        alarmWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                Drawable rightDrawable = getResources().getDrawable(R.mipmap.dropdown);// 获取res下的图片drawable
                rightDrawable.setBounds(0, 0, rightDrawable.getMinimumWidth(), rightDrawable.getMinimumHeight());// 一定要设置setBounds();
                alarmWindow.dismiss();
            }
        });
    }

    public void showWindow(View v, List<ProductTypeEvent.Product> list) {
        if (!alarmWindow.isShowing()) {
            popupWindowView.removeAllViews();
            for (ProductTypeEvent.Product item : list) {
                View popItem = LayoutInflater.from(getContext()).inflate(R.layout.popup_window_item, null, false);
                TextView tvDateName = (TextView) popItem.findViewById(R.id.tv_user_name);

                tvDateName.setText(item.getSymbol());

                tvDateName.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        toolbarTitle.setText(item.getSymbol());
                        SocketUtil.getInstance().destroy();
                        SocketUtil.getInstance().init5000(item.getInstrument(), true, false);

                        alarmWindow.dismiss();

                        String toJson = new Gson().toJson(item.getEnv());
                        SpotTitleEvent.EnvBean envBean = new Gson().fromJson(toJson, SpotTitleEvent.EnvBean.class
                        );
                        EventBus.getDefault().post(new SpotTitleEvent(
                                item.getInstrument(),
                                item.getSymbol(),
                                item.getPriceDecimal(),
                                item.getQtyDecimal(),
                                item.getAmountDecimal(),
                                item.getLast(),
                                item.getRate(),
                                envBean
                        ));
                    }
                });
                popupWindowView.addView(popItem);
            }
            alarmWindow.showAsDropDown(v, LinearLayout.LayoutParams.MATCH_PARENT, 1);
        } else {
            alarmWindow.dismiss();
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }


    @OnClick({R.id.toolbar_title, R.id.toolbar_func_fight, R.id.ll_right_func, R.id.toolbar_func_left})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.toolbar_title:
                showWindow(toolbarTitle, productsList);

                break;
            case R.id.ll_right_func:
                ToastUtils.toast(getString(R.string.sx));
                break;
            case R.id.toolbar_func_left:
                //k线图界面
                startActivity(new Intent(getContext(), KlineActivity.class));
                break;
        }
    }

    @Subscribe()
    public void onEvent(MarketItemCLickEvent event) {

        toolbarTitle.setText(event.getSymbol());
        SocketUtil.getInstance().destroy();
        SocketUtil.getInstance().init5000(event.getInstrument(), true, false);

        alarmWindow.dismiss();

        String toJson = new Gson().toJson(event.getEnv());
        SpotTitleEvent.EnvBean envBean = new Gson().fromJson(toJson, SpotTitleEvent.EnvBean.class
        );
        EventBus.getDefault().post(new SpotTitleEvent(
                event.getInstrument(),
                event.getSymbol(),
                event.getPriceDecimal(),
                event.getQtyDecimal(),
                event.getAmountDecimal(),
                event.getLast(),
                event.getRate(),
                envBean
        ));
    }

}
