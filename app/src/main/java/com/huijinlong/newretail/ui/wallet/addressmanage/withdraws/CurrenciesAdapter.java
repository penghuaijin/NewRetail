package com.huijinlong.newretail.ui.wallet.addressmanage.withdraws;

import android.content.Context;
import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.huijinlong.newretail.R;
import com.huijinlong.newretail.bean.CurrenciesBean;

import java.util.List;

/**
 * @author zsg
 * @date 2018/10/24 12:00
 * @Version 1.0
 * @Description: ()
 */
public class CurrenciesAdapter extends BaseQuickAdapter<CurrenciesBean.DataBean, BaseViewHolder> {

    private Context context;

    public CurrenciesAdapter(@Nullable List<CurrenciesBean.DataBean> data, Context contexts) {
        super(R.layout.popup_window_item, data);
        context = contexts;
    }

    @Override
    protected void convert(BaseViewHolder helper, CurrenciesBean.DataBean item) {
        helper.setText(R.id.tv_user_name, item.getCurrency());
        helper.setBackgroundColor(R.id.lin, context.getResources().getColor(R.color.c_525));

        helper.addOnClickListener(R.id.tv_user_name);

    }
}
