package com.huijinlong.newretail.ui.wallet.addressmanage.withdraws;

import com.huijinlong.newretail.base.BasePresenter;
import com.huijinlong.newretail.base.BaseBean;
import com.huijinlong.newretail.bean.DeleteAdressBean;
import com.huijinlong.newretail.net.MyCallback;
import com.huijinlong.newretail.net.RequestBuilder;
import com.huijinlong.newretail.ui.wallet.addressmanage.EditAddressWithdrawBean;
import com.huijinlong.newretail.util.MD5Util;
import com.huijinlong.newretail.util.SpUtil;

import java.util.TreeMap;

/**
 * @author zsg
 * @date 2018/10/23 18:32
 * @Version 1.0
 * @Description: ()
 */
public class WithDrawsGetCodePresenter extends BasePresenter<WithDrawsGetCodeView> {

   private WithDrawsGetCodeView withDrawsGetCodeView;

    public WithDrawsGetCodePresenter(WithDrawsGetCodeView views) {
        this.withDrawsGetCodeView = views;
    }

    public void  getCode()
    {
        withDrawsGetCodeView.showLoading();
        TreeMap<String, String> params = new TreeMap<>();
        params.put("action", "deleteAddress");
        params.put("deviceType", "android");
        params.put("language", SpUtil.getString(SpUtil.LANGUAGE,"zh-cn"));

        RequestBuilder.execute(RequestBuilder.getNetService(true).getPhoneCodeVerify(MD5Util.getSignValue(params), params), "", new MyCallback<BaseBean>() {
            @Override
            public void onSuccess(BaseBean result) {
                super.onSuccess(result);
                withDrawsGetCodeView.getCodeSuccess(result);
            }

            @Override
            public void onEmpty(BaseBean result) {
                super.onEmpty(result);

            }

            @Override
            public void onBusinessFailure(Exception e) {
                super.onBusinessFailure(e);
                withDrawsGetCodeView.showError(e.getMessage());

            }

            @Override
            public void onFinalResponse(boolean isSuccess) {
                super.onFinalResponse(isSuccess);
                withDrawsGetCodeView.hideLoading();
            }
        });
    }

    public void getDeleteAdress(int ids)
    {
        withDrawsGetCodeView.showLoading();
        TreeMap<String, String> params = new TreeMap<>();
        params.put("deviceType", "android");
        params.put("id",String.valueOf(ids));
        params.put("language", SpUtil.getString(SpUtil.LANGUAGE,"zh-cn"));

        RequestBuilder.execute(RequestBuilder.getNetService(true).getDeleteAdress(MD5Util.getSignValue(params), params), "", new MyCallback<DeleteAdressBean>() {
            @Override
            public void onSuccess(DeleteAdressBean result) {
                super.onSuccess(result);
                withDrawsGetCodeView.getDelete(result);
            }

            @Override
            public void onEmpty(DeleteAdressBean result) {
                super.onEmpty(result);

            }

            @Override
            public void onBusinessFailure(Exception e) {
                super.onBusinessFailure(e);
                withDrawsGetCodeView.showError(e.getMessage());

            }

            @Override
            public void onFinalResponse(boolean isSuccess) {
                super.onFinalResponse(isSuccess);
                withDrawsGetCodeView.hideLoading();
            }
        });
    }

    public void getEditAddressWithdraw(int cid,String address,String remark,int id ,String channel)
    {
        withDrawsGetCodeView.showLoading();
        TreeMap<String, String> params = new TreeMap<>();
        params.put("deviceType", "android");
        params.put("language", SpUtil.getString(SpUtil.LANGUAGE,"zh-cn"));

        params.put("cid",String.valueOf(cid));
        params.put("id",String.valueOf(id));
        params.put("address",String.valueOf(address));
        params.put("remark",String.valueOf(remark));
        params.put("channel",channel);

        RequestBuilder.execute(RequestBuilder.getNetService(true).getEditAddressWithdraw(MD5Util.getSignValue(params), params), "", new MyCallback<EditAddressWithdrawBean>() {
            @Override
            public void onSuccess(EditAddressWithdrawBean result) {
                super.onSuccess(result);
                withDrawsGetCodeView.getEdit(result);
            }

            @Override
            public void onEmpty(EditAddressWithdrawBean result) {
                super.onEmpty(result);

            }

            @Override
            public void onBusinessFailure(Exception e) {
                super.onBusinessFailure(e);
                withDrawsGetCodeView.showError(e.getMessage());

            }

            @Override
            public void onFinalResponse(boolean isSuccess) {
                super.onFinalResponse(isSuccess);
                withDrawsGetCodeView.hideLoading();
            }
        });
    }

    public void verifySecurity(String mobileVerificationCode, String action)
    {
        withDrawsGetCodeView.showLoading();
        TreeMap<String, String> params = new TreeMap<>();
        params.put("action",action);
        params.put("deviceType", "android");
        params.put("mobileVerificationCode",mobileVerificationCode);
        params.put("language", SpUtil.getString(SpUtil.LANGUAGE,"zh-cn"));

        RequestBuilder.execute(RequestBuilder.getNetService(true).verifySecurity(MD5Util.getSignValue(params), params), "", new MyCallback<BaseBean>() {
            @Override
            public void onSuccess(BaseBean result) {
                super.onSuccess(result);
                withDrawsGetCodeView.getPhoneCapt(result);
            }

            @Override
            public void onEmpty(BaseBean result) {
                super.onEmpty(result);

            }

            @Override
            public void onBusinessFailure(Exception e) {
                super.onBusinessFailure(e);
                withDrawsGetCodeView.showError(e.getMessage());

            }

            @Override
            public void onFinalResponse(boolean isSuccess) {
                super.onFinalResponse(isSuccess);
                withDrawsGetCodeView.hideLoading();
            }
        });
    }


}
