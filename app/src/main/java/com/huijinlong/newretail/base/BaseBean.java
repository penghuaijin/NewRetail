package com.huijinlong.newretail.base;

import java.io.Serializable;

/**
 * Created by penghuaijin on 2018/8/26.
 */

public class BaseBean implements Serializable{



    /**
     * code : 200
     * msg : 状态码说明
     */
    private String msg;
    private int status;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
