package com.huijinlong.newretail.base;

import android.app.ActivityManager;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.finddreams.languagelib.MultiLanguageUtil;
import com.gyf.barlibrary.ImmersionBar;
import com.huijinlong.newretail.R;
import com.huijinlong.newretail.manager.MyActivityManager;
import com.huijinlong.newretail.ui.login.GestureLoginActivity;
import com.huijinlong.newretail.util.FontUtil;
import com.huijinlong.newretail.util.LogUtils;
import com.huijinlong.newretail.util.ProgressDialogUtil;
import com.huijinlong.newretail.util.SocketUtil;
import com.huijinlong.newretail.util.SpUtil;
import com.huijinlong.newretail.util.ToastUtils;
import com.noober.background.BackgroundLibrary;

import java.lang.ref.WeakReference;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by Administrator on 2017/11/8.
 */

public abstract class BaseActivity<V extends BaseView, T extends BasePresenter<V>> extends AppCompatActivity {
    //
    public LinearLayout mActivityRoot;
    @BindView(R.id.toolbar_contenttitle)
    TextView mContentTitle;
    @BindView(R.id.toolbar_right_text)
    TextView mTvMenu;
    @BindView(R.id.toolbar_right_img)
    ImageView mIvMenu;
    @BindView(R.id.btn_toolbar_right)
    RelativeLayout mBtnMenu;
    @BindView(R.id.home_toolbar)
    Toolbar mToolbar;
    @BindView(R.id.back_btn)
    RelativeLayout mBackBtn;
    @BindView(R.id.iv_back)
    ImageView mImageView;
    @BindView(R.id.mv_view)
    View mvView;
    @BindView(R.id.root_layout)
    LinearLayout rootLayout;
    private FragmentManager mFragmentManager;
    public T mPresenter;
    private ImmersionBar mImmersionBar;
    public static boolean isForeground = false;
    public Dialog mLoadingDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        BackgroundLibrary.inject(this);
        MultiLanguageUtil.getInstance().setConfiguration();

        setContentView(R.layout.activity_base2);
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        mActivityRoot = findViewById(R.id.root_layout);
        int parentContentId = getParentContentView();
        View parentContent = mActivityRoot;
        if (parentContentId != 0) {
            parentContent = LayoutInflater.from(this).inflate(getParentContentView(), null);
            mActivityRoot.addView(parentContent);
        }
        LayoutInflater.from(this).inflate(getContentView(), (ViewGroup) parentContent, true);
        ButterKnife.bind(this);
        mLoadingDialog = ProgressDialogUtil.createLoadingDialog(this);
        MyActivityManager.getManager().push(new WeakReference<>(this));
        mPresenter = initPresenter();
        if (isImmersionBarEnabled()) {
            mImmersionBar = ImmersionBar.with(this);
            mImmersionBar.statusBarColor(R.color.theme_color);
            mImmersionBar.statusBarDarkFont(false);
            mImmersionBar.init();
        }
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            //根据资源ID获取响应的尺寸值
            int statusBarHeight = getResources().getDimensionPixelSize(resourceId);
            LinearLayout.LayoutParams sp_params = new LinearLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT,
                    RelativeLayout.LayoutParams.WRAP_CONTENT);
            sp_params.height = statusBarHeight;
            mvView.setLayoutParams(sp_params);
        }

        initView();
    }
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(MultiLanguageUtil.attachBaseContext(base));
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();

        MyActivityManager.getManager().finishActivity(this);
        if (mImmersionBar != null)
            mImmersionBar.destroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mLoadingDialog.dismiss();
    }

    /**
     * 是否在Fragment使用沉浸式
     *
     * @return the boolean
     */
    protected boolean isImmersionBarEnabled() {
        return true;
    }

    public ImmersionBar getmImmersionBar() {
        return mImmersionBar;
    }

    public abstract T initPresenter();

    protected abstract void initView();


    protected int getParentContentView() {
        return 0;
    }

    @LayoutRes
    protected abstract int getContentView();


    public Toolbar getmToolbar() {
        return mToolbar;
    }

    public void setToolbarClick(boolean flag) {
        if (flag) {
            mBackBtn.setVisibility(View.VISIBLE);
            mBackBtn.setOnClickListener(view -> finish());
        } else {
            mBackBtn.setVisibility(View.GONE);
        }
    }

    public void setToolbarClick(boolean flag, View.OnClickListener onClickListenerl) {
        if (flag) {
            mBackBtn.setVisibility(View.VISIBLE);
            mBackBtn.setOnClickListener(onClickListenerl);
        } else {
            mBackBtn.setVisibility(View.GONE);
        }
    }

    public void setToolbarRightBtn(boolean showText, String text, boolean isIconFont) {
        if (showText) {
            mTvMenu.setText(text);
            mTvMenu.setVisibility(View.VISIBLE);
        } else {
            mTvMenu.setVisibility(View.GONE);
        }
        if (isIconFont) {
            FontUtil.injectFont(mTvMenu);
        }
    }

    public void setToolbarRightBtn(boolean showImg, View.OnClickListener onClickListener) {
        if (showImg) {
            mBtnMenu.setOnClickListener(onClickListener);
            mIvMenu.setVisibility(View.VISIBLE);
        } else {
            mIvMenu.setVisibility(View.GONE);
        }
    }

    public void setToolbarRightBtn(boolean showText, boolean isIconFont, String text, View.OnClickListener onClick) {
        setToolbarRightBtn(showText, text, isIconFont);
        mTvMenu.setOnClickListener(onClick);
    }

    public void setmToolbarRightBtnColor(int color) {
        mTvMenu.setTextColor(getResources().getColor(color));
    }

    public void setToolbarTitle(@Nullable String titleText) {
        if (null != mToolbar) {
            mContentTitle.setText(titleText);
            mBackBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
            setSupportActionBar(mToolbar);
        }
    }


    public void setToolbarTitle(@Nullable int titleText) {
        if (null != mToolbar) {
            mContentTitle.setText(titleText);
            setSupportActionBar(mToolbar);
        }
    }


    public void setToolbarBg(@Nullable int res) {
        if (null != mToolbar) {
            mImageView.setImageDrawable(getResources().getDrawable(res));
            setSupportActionBar(mToolbar);
        }
    }


    public void setToolbarVisibility(boolean flag) {
        if (flag) {
            mToolbar.setVisibility(View.VISIBLE);
        } else {
            mToolbar.setVisibility(View.GONE);
        }
    }


    public FragmentManager getBaseFragmentManager() {
        if (mFragmentManager == null) {
            mFragmentManager = getSupportFragmentManager();
        }
        return mFragmentManager;
    }


    public void addFragment(Fragment fragment) {
        addFragment(fragment, true);
    }

    public void addFragment(Fragment fragment, boolean isAddToBackStack) {
        if (!fragment.isAdded()) {
            String tag = getClass().getSimpleName();
            FragmentTransaction transaction = this.getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.base_fade_in, R.anim.base_fade_out, R.anim.base_fade_in, R.anim.base_fade_out);
            transaction.add(android.R.id.content, fragment, tag);
            if (isAddToBackStack) {
                transaction.addToBackStack(tag);
            }
            transaction.commitAllowingStateLoss();
        }
    }


    /**
     * [页面跳转]
     *
     * @param clz
     */
    public void startToActivity(Context context, Class clz) {
        startActivity(new Intent(context, clz));
    }


    /**
     * [携带数据的页面跳转]
     *
     * @param clz
     * @param bundle
     */
    public void startToActivityForBundle(Context context, Class clz, Bundle bundle) {
        Intent intent = new Intent();
        intent.setClass(context, clz);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
    }

    /**
     * [含有Bundle通过Class打开编辑界面]
     *
     * @param cls
     * @param bundle
     * @param requestCode
     */
    public void startActivityForResultForBundle(Context context, Class<?> cls, Bundle bundle,
                                                int requestCode) {
        Intent intent = new Intent();
        intent.setClass(context, cls);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivityForResult(intent, requestCode);
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        isForeground = SpUtil.getBoolean(SpUtil.IS_FOREGROUND, true);
        if (!isForeground) {
            Log.i("app", "APP重新进入前台");
            long comeTime = System.currentTimeMillis();
            SpUtil.putLong(SpUtil.COME_TIME,comeTime);
            long intervalTime = SpUtil.getLong(SpUtil.INTERVAL_TIME, 2 * 60 * 1000);
            long outTime = SpUtil.getLong(SpUtil.OUT_TIME, 0);
            if (comeTime-outTime>intervalTime){
                if (SpUtil.getBoolean(SpUtil.LOGINGESTURE, true)){
                    ToastUtils.toast("需要手势验证");
                    startToActivity(this, GestureLoginActivity.class);
                }
            }


        }
        isForeground = true;
        SpUtil.putBoolean("isForeground", true);

        if (mPresenter != null) {
            mPresenter.attachView((V) this);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (!isAppOnForeground()) {
            LogUtils.e("APP进入后台");
            isForeground = false;
            SpUtil.putBoolean("isForeground", false);
            SpUtil.putLong(SpUtil.OUT_TIME,System.currentTimeMillis());
            SocketUtil.getInstance().destroy();

        }
    }

    /**
     * 程序是否在前台运行
     *
     * @return
     */
    public boolean isAppOnForeground() {
        ActivityManager activityManager = (ActivityManager) getApplicationContext()
                .getSystemService(Context.ACTIVITY_SERVICE);
        String packageName = getApplicationContext().getPackageName();
        /**
         * 获取Android设备中所有正在运行的App
         */
        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager
                .getRunningAppProcesses();
        if (appProcesses == null)
            return false;

        for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
            // The name of the process that this object is associated with.
            if (appProcess.processName.equals(packageName)
                    && appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                return true;
            }
        }

        return false;
    }


}
