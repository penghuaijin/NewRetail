package com.huijinlong.newretail.base;

import android.text.TextUtils;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;

/**
 * @author zsg
 * @date 2018/10/18 12:15
 * @Version 1.0
 * @Description: ()
 */
public class BasePresenter<T extends BaseView> {

    protected Reference<T> viewRef;
    public T mView;

    public void attachView(T view) {
        this.mView = view;
        viewRef = new WeakReference<T>(mView);
    }

    public void detachView() {
        if (viewRef != null) {
            viewRef.clear();
            viewRef = null;
        }
    }

    //错误时的操作
    public void showError(String error) {
        if (mView != null) {
            mView.hideLoading();
            mView.showError(error);
        }
    }

    public boolean isEmpty(String string) {
        if (TextUtils.isEmpty(string)) {
            mView.showError("数据为空");
            return true;
        }
        return false;
    }
}
