package com.huijinlong.newretail.base;


import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.finddreams.languagelib.MultiLanguageUtil;
import com.huijinlong.newretail.R;
import com.huijinlong.newretail.manager.BroadcastManager;
import com.huijinlong.newretail.manager.MyActivityManager;
import com.huijinlong.newretail.util.LogUtils;
import com.huijinlong.newretail.util.ProgressDialogUtil;
import com.huijinlong.newretail.util.SocketUtil;
import com.huijinlong.newretail.util.SpUtil;
import com.huijinlong.newretail.view.SwipeLayout;
import com.kingja.loadsir.core.LoadService;
import com.noober.background.BackgroundLibrary;

import java.lang.ref.WeakReference;
import java.util.List;


public abstract class BaseActivityBefor<V extends BaseView, T extends BasePresenter<V>> extends AppCompatActivity {
    private SwipeLayout swipeLayout;
    public static boolean isForeground = false;
    public String TAG = this.getClass().getSimpleName();
    private LinearLayout rootLayout;
    private Handler handler = new Handler();
    private LoadService loadService;
    public Dialog mLoadingDialog;
    public T mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        BackgroundLibrary.inject(this);

        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_base);
        MyActivityManager.getManager().push(new WeakReference<>(this));

        setTheme(R.style.AppTheme);
        initToolbar();
        initSwipView();
        registerBroadCast();
        mLoadingDialog  = ProgressDialogUtil.createLoadingDialog(this);
        mPresenter = initPresenter();

    }
    public abstract T initPresenter();
    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        isForeground = SpUtil.getBoolean(SpUtil.IS_FOREGROUND, true);
        if (!isForeground) {
            Log.i("app", "APP重新进入前台");
//            ThreadPool.newTask(new Runnable() {
//                @Override
//                public void run() {
//                    SocketUtil.getInstance().init5000("BTCUSDT", true, false);
//                    SocketUtil.getInstance().init5005("kline_1min.BTCUSDT");
//                }
//            });
            // 检查更新
            //checkVersionAuto();
        }
        isForeground = true;
        SpUtil.putBoolean("isForeground", true);

        if (mPresenter != null) {
            mPresenter.attachView((V) this);
        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(MultiLanguageUtil.attachBaseContext(base));
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (!isAppOnForeground()) {
            LogUtils.e("APP进入后台");
            isForeground = false;
            SpUtil.putBoolean("isForeground", isForeground);
            SocketUtil.getInstance().destroy();

        }
    }

    /**
     * 程序是否在前台运行
     *
     * @return
     */
    public boolean isAppOnForeground() {
        ActivityManager activityManager = (ActivityManager) getApplicationContext()
                .getSystemService(Context.ACTIVITY_SERVICE);
        String packageName = getApplicationContext().getPackageName();
        /**
         * 获取Android设备中所有正在运行的App
         */
        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager
                .getRunningAppProcesses();
        if (appProcesses == null)
            return false;

        for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
            // The name of the process that this object is associated with.
            if (appProcess.processName.equals(packageName)
                    && appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                return true;
            }
        }

        return false;
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }
    }

    @Override
    public void startActivity(Intent intent, Bundle options) {
        super.startActivity(intent, options);
        overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode, @Nullable Bundle options) {
        super.startActivityForResult(intent, requestCode, options);
        overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
    }

    public void registerBroadCast() {
        IntentFilter myIntentFilter = new IntentFilter();
        myIntentFilter.addAction(BroadcastManager.ACTION_EXIT_APP);
        registerReceiver(mBroadcastReceiver, myIntentFilter);
    }

    public BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {//退出登录广播
            // TODO Auto-generated method stub
            String action = intent.getAction();
            if (BroadcastManager.ACTION_EXIT_APP.equals(action)) {
                finish();
            }
        }
    };

    @Override
    public void setContentView(int layoutId) {
        setContentView(View.inflate(this, layoutId, null));
    }

    @Override
    public void setContentView(View view) {


        rootLayout = (LinearLayout) findViewById(R.id.root_layout);
//        rootLayout.setBackgroundDrawable(new WaterMarkBg(waterStr));

        if (rootLayout == null)
            return;
        rootLayout.addView(view, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        initToolbar();
    }

    private void initSwipView() {
        swipeLayout = new SwipeLayout(this);
        swipeLayout.setSwipeAnyWhere(false);
        swipeLayout.setSwipeEnabled(isSwipeEnabled());
    }

    protected boolean isSwipeEnabled() {
        return false;
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        swipeLayout.replaceLayer(this);
    }

    @Override
    public void finish() {
        if (!swipeLayout.isSwipeFinished()) {
            swipeLayout.cancelPotentialAnimation();
        }
        super.finish();
        overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();
        unregisterReceiver(mBroadcastReceiver);

        if (mPresenter != null) {
            mPresenter.detachView();
        }
        MyActivityManager.getManager().finishActivity(this);

    }

    @Override
    protected void onStart() {
        super.onStart();
        initSystemBarTint();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:// 点击返回图标事件
                finish();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }


    /**
     * 子类可以重写改变状态栏颜色
     */
    protected int setStatusBarColor() {
//        return getColorPrimary();
        return getResources().getColor(R.color.theme_color);
    }

    /**
     * 子类可以重写决定是否使用透明状态栏
     */
    protected boolean translucentStatusBar() {
        return false;
    }

    /**
     * 设置状态栏颜色
     */
    protected void initSystemBarTint() {
        Window window = getWindow();
        if (translucentStatusBar()) {
            // 设置状态栏全透明
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(Color.TRANSPARENT);
            } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            }
            return;
        }
        // 沉浸式状态栏
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //5.0以上使用原生方法
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(setStatusBarColor());
        }
    }

    /**
     * 初始化 Toolbar
     */
    public void initToolBar(Toolbar toolbar, boolean homeAsUpEnabled, String title) {
        toolbar.setTitle(title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(homeAsUpEnabled);
    }

    public void initToolBar(Toolbar toolbar, boolean homeAsUpEnabled, int resTitle) {
        initToolBar(toolbar, homeAsUpEnabled, getString(resTitle));
    }


    public void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    public void jump2Activity(Class clazz) {
        Intent intent = new Intent();
        intent.setClass(this, clazz);
        startActivity(intent);
    }

    public void jump2Activity(Class clazz, Intent intent) {
        intent.setClass(this, clazz);
        startActivity(intent);
    }

    public void jump2ActivityForResult(Class clazz, Intent intent, int requestCode) {
        intent.setClass(this, clazz);
        startActivityForResult(intent, requestCode);
    }

}


