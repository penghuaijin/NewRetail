package com.huijinlong.newretail.base;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gyf.barlibrary.ImmersionBar;
import com.huijinlong.newretail.R;
import com.huijinlong.newretail.util.ProgressDialogUtil;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


public abstract class BaseFragment<V extends BaseView, T extends BasePresenter<V>> extends Fragment {
    public Context context;
    public T mPresenter;
    public Dialog mLoading;
    private BaseActivity mBaseActivity;
    protected boolean isFragmentVisible; //是否可见
    protected boolean isFirstFragmentVisible; //第一次加载
    protected boolean mIsImmersion;//是否使用沉浸式
    private static final String TAG = BaseFragment.class.getSimpleName();
    protected LayoutInflater inflater;
    private Unbinder mUnbinder;
    protected ImmersionBar mImmersionBar;
    private View rootView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.inflater = inflater;
        assert context != null;
//        context = mBaseActivity.getBaseContext();
        context =getContext();
        if (rootView == null) {
            rootView = inflater.from(context).inflate(getLayoutId(), container, false);
            mUnbinder = ButterKnife.bind(this, rootView);
            mPresenter = initPresenter();
            mLoading = ProgressDialogUtil.createLoadingDialog(getBaseActivity());
        }
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getUserVisibleHint()) {
            if (isFirstFragmentVisible) {
                initData();
                onClick(rootView);
                isFirstFragmentVisible = false;
            } else {
                onFragmentVisibleChange(true);
                isFragmentVisible = true;
                if (isImmersionBarEnabled())
                    initImmersionBar();
            }
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initVariable();
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (rootView == null) {
            return;
        }
        if (isFirstFragmentVisible && isVisibleToUser) {
            initData();
            isFirstFragmentVisible = false;
        }
        if (isVisibleToUser) {
            onFragmentVisibleChange(true);
            isFragmentVisible = true;
            return;
        }
        if (isFragmentVisible) {
            onFragmentVisibleChange(false);
            isFragmentVisible = false;
        }
    }

    /**
     * 是否懒加载
     *
     * @return the boolean
     */
    protected boolean isLazyLoad() {
        return true;
    }


    protected void onClick(View view) {

    }

    /**
     * 是否在Fragment使用沉浸式
     *
     * @return the boolean
     */
    protected boolean isImmersionBarEnabled() {
        return true;
    }


    public void addFragment(Fragment fragment) {
        addFragment(fragment, true);
    }

    public void addFragment(Fragment fragment, boolean isAddToBackStack) {
        if (!fragment.isAdded()) {
            String tag = getClass().getSimpleName();
            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.base_fade_in, R.anim.base_fade_out, R.anim.base_fade_in, R.anim.base_fade_out);
            transaction.add(android.R.id.content, fragment, tag);
            if (isAddToBackStack) {
                transaction.addToBackStack(tag);
            }
            transaction.commitAllowingStateLoss();
        }
    }

    @SuppressLint("CheckResult")
    public void finishTopFragment() {
        if (getActivity() != null) {
            Flowable.just("exit")
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(s
                            -> getActivity().getSupportFragmentManager().popBackStackImmediate(null, 0));
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        initVariable();
        mUnbinder.unbind();
        if (mPresenter != null) {
            mPresenter.detachView();
        }
        if (mImmersionBar != null)
            mImmersionBar.destroy();
    }


    @Override
    public void onResume() {
        super.onResume();
        if (mPresenter != null) {
            mPresenter.attachView((V) this);
        }
    }

    public BaseActivity getBaseActivity() {
        if (null == mBaseActivity) {
            mBaseActivity = (BaseActivity) getContext();
        }
        return mBaseActivity;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mBaseActivity = (BaseActivity) context;
    }


    /**
     * [页面跳转]
     *
     * @param clz
     */
    public void startToActivity(Activity activity, Class clz) {
        startActivity(new Intent(activity, clz));
    }

    /**
     * [携带数据的页面跳转]
     *
     * @param clz
     * @param bundle
     */
    public void startActivity(Activity activity, Class clz, Bundle bundle) {
        Intent intent = new Intent();
        intent.setClass(activity, clz);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
    }

    @Override
    public Context getContext() {
        return getActivity();
    }

    /**
     * 初始化沉浸式
     */
    protected void initImmersionBar() {
        mImmersionBar = ImmersionBar.with(this);
        mImmersionBar.statusBarColor(R.color.white);
        mImmersionBar.statusBarDarkFont(true);
        mImmersionBar.keyboardEnable(true).navigationBarWithKitkatEnable(false).init();
    }


    private void initVariable() {
        isFirstFragmentVisible = true;
        isFragmentVisible = false;
        rootView = null;
    }

    /**
     * 去除setUserVisibleHint()多余的回调场景，保证只有当fragment可见状态发生变化时才回调
     * 回调时机在view创建完后，所以支持ui操作，解决在setUserVisibleHint()里进行ui操作有可能报null异常的问题
     * <p>
     * 可在该回调方法里进行一些ui显示与隐藏
     *
     * @param isVisible true  不可见 -> 可见
     *                  false 可见  -> 不可见
     */
    protected void onFragmentVisibleChange(boolean isVisible) {

    }

    @Override
    public void onPause() {
        super.onPause();
        mLoading.dismiss();
    }

    @LayoutRes
    protected abstract int getLayoutId();

    protected abstract void initData();

    protected abstract T initPresenter();
}