package com.huijinlong.newretail.base;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

/**
 * @author zsg
 * @date 2018/7/16 下午7:25
 * @ClassName PagerAdapter
 * @Version 1.0
 * @Description: ()
 */
public class PagerAdapter extends FragmentPagerAdapter {
    private List<String> titles;
    private List<Fragment> fragments;
    private FragmentManager fm;
    /**
     * 当数据发生改变时的回调接口
     */
    //private OnReloadListener mListener;

    public PagerAdapter(FragmentManager fm, List<String> titles, List<Fragment> fragments) {
        super(fm);
        this.fm=fm;
        this.titles = titles;
        this.fragments = fragments;
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles.get(position);
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

}
