package com.huijinlong.newretail.base;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;

import com.com.huijinlong.greendao.DaoMaster;
import com.com.huijinlong.greendao.DaoSession;
import com.finddreams.languagelib.MultiLanguageUtil;
import com.haoge.easyandroid.EasyAndroid;

import com.huijinlong.newretail.util.SpUtil;
import com.huijinlong.newretail.util.ToastUtils;
import com.squareup.leakcanary.LeakCanary;
import com.uuzuche.lib_zxing.activity.ZXingLibrary;

import org.greenrobot.greendao.database.Database;


import java.lang.ref.WeakReference;
import java.util.Locale;
import java.util.Stack;

/**
 * Created by penghuaijin on 2018/8/26.
 */

public class BaseApplication extends Application {

    private static BaseApplication mInstance;
    private DaoSession daoSession;
    public static Context applicationContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        applicationContext = this;
        EasyAndroid.init(mInstance);
        ZXingLibrary.initDisplayOpinion(this);
        SpUtil.putString(SpUtil.SIGN, "69482f2510ae7db28c3ffacb945f507c");
        initDB();
        if (LeakCanary.isInAnalyzerProcess(this)) {
            return;
        }
        LeakCanary.install(this);
        MultiLanguageUtil.init(this);
    }

    // 取得版本号
    public static String GetVersion() {
        try {
            PackageInfo manager = getInstance().getPackageManager().getPackageInfo(
                    getInstance().getPackageName(), 0);
            return manager.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            return "Unknown";
        }
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        MultiLanguageUtil.getInstance().setConfiguration();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);

    }

    public static synchronized BaseApplication getInstance() {
        if (mInstance == null) {
            throw new RuntimeException("在AndroidManifest文件中注册");
        }
        return mInstance;
    }

    private void initDB() {

        // regular SQLite database
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "nr-db");
        Database db = helper.getWritableDb();

        // encrypted SQLCipher database
        // note: you need to add SQLCipher to your dependencies, check the build.gradle file
        // DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "nr-db-encrypted");
        // Database db = helper.getEncryptedWritableDb("encryption-key");

        daoSession = new DaoMaster(db).newSession();
    }


    public DaoSession getDaoSession() {
        return daoSession;
    }

}
