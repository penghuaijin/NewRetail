package com.huijinlong.newretail.base;

import android.content.Context;

/**
 * Created by ZX on 2018/5/15.
 */
public interface BaseView {
    void showLoading();

    void hideLoading();

    void showError(String error);

}
