package com.huijinlong.newretail.net;


import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;


import com.huijinlong.newretail.base.BaseApplication;
import com.huijinlong.newretail.base.BaseBean;
import com.huijinlong.newretail.manager.MyActivityManager;
import com.huijinlong.newretail.ui.login.LoginActivity;
import com.huijinlong.newretail.util.SocketUtil;
import com.huijinlong.newretail.util.SpUtil;
import com.huijinlong.newretail.util.ToastUtils;

import java.lang.ref.WeakReference;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by shoulder on 16/7/5.
 */

public abstract class MyCallback<T extends BaseBean> extends SlibCallback<T> {

    /**
     * @param call     请求
     * @param response 返回
     * @return 对所有接口返回进行预处理
     */
    @Override
    final public T preProcess(Call<T> call, Response<T> response) {

        return beforeSuccess(call, response);
    }

    @Override
    public void afterProcess(T result, boolean success, Exception e) {
        super.afterProcess(result, success, e);
        //特殊的555是被踢下线   5000是服务器维护
        if (success && result != null) {
            if (203 == result.getStatus()) {
                onEmpty(result);
            } else if (200 == result.getStatus()) {
                onSuccess(result);
            } else if (1001 == result.getStatus()) {

                SocketUtil.getInstance().destroy();
                SpUtil.clear();


                final Activity activity = MyActivityManager.getManager().getTop();
                if (activity != null) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                    builder.setTitle(result.getMsg());
//                    builder.setMessage(result.getMsg());
                    builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            MyActivityManager.getManager().finishAll();
                            System.exit(0);
                        }
                    });
                    builder.setPositiveButton("确认", new DialogInterface.OnClickListener() {


                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            MyActivityManager.getManager().finishAll();
                            Intent intent = new Intent(activity, LoginActivity.class);
//                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            activity.startActivity(intent);
                        }
                    });

                    builder.show();


                } else
                    ToastUtils.toast(e.getMessage());

            } else {
                onBusinessFailure(new RuntimeException(result.getMsg()));
            }
            onFinalResponse(true);
        } else {
            String info = (result != null && !TextUtils.isEmpty(result.getMsg())) ? result.getMsg() : "请求失败";
            onBusinessFailure(e == null ? new RuntimeException(info) : e);
            onFinalResponse(false);
        }
    }

    /**
     * @param call     请求
     * @param response 返回
     * @return 对单个接口返回进行预处理，不阻塞主线程
     */
    public T beforeSuccess(Call<T> call, Response<T> response) {
        return response.body();
    }

    @Override
    public void onSuccess(T result) {

    }

    @Override
    public void onEmpty(T result) {

    }

    @Override
    public void onBusinessFailure(Exception e) {
        final Activity activity = MyActivityManager.getManager().getTop();
        if (activity != null) {


        } else
            ToastUtils.toast(e.getMessage());
    }

    @Override
    public void onFinalResponse(boolean isSuccess) {

    }
}
