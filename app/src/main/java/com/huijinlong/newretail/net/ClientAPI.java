package com.huijinlong.newretail.net;


import com.huijinlong.newretail.base.BaseBean;
import com.huijinlong.newretail.bean.CheckUpdateBean;
import com.huijinlong.newretail.bean.ExchangeDetailBean;
import com.huijinlong.newretail.bean.WthdrawChannelBean;
import com.huijinlong.newretail.requestbean.CaptchaParamsBean;
import com.huijinlong.newretail.requestbean.CountriesBean;
import com.huijinlong.newretail.requestbean.CurrentCommissionBean;
import com.huijinlong.newretail.requestbean.CurrentCountryIdBean;
import com.huijinlong.newretail.requestbean.EditApiBean;
import com.huijinlong.newretail.requestbean.GoogleAuthenticatorSecretBean;
import com.huijinlong.newretail.requestbean.HistoryBean;
import com.huijinlong.newretail.requestbean.LoginBean;
import com.huijinlong.newretail.requestbean.SpotConfigBean;
import com.huijinlong.newretail.requestbean.SpotProductsBean;
import com.huijinlong.newretail.requestbean.GetPhoneCaptcha4BindBean;
import com.huijinlong.newretail.requestbean.DepthBean;
import com.huijinlong.newretail.requestbean.UserIdentityAuthBean;
import com.huijinlong.newretail.requestbean.VerifyPhone4BindBean;
import com.huijinlong.newretail.bean.SecurityInfoBean;
import com.huijinlong.newretail.bean.AssetBean;
import com.huijinlong.newretail.bean.WithDrawsBean;
import com.huijinlong.newretail.bean.CurrenciesBean;
import com.huijinlong.newretail.requestbean.KlineBean;
import com.huijinlong.newretail.requestbean.WalletAddressBean;
import com.huijinlong.newretail.ui.wallet.addressmanage.EditAddressWithdrawBean;
import com.huijinlong.newretail.bean.DeleteAdressBean;
import com.huijinlong.newretail.bean.WithdrawsListBean;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by penghuaijin on 2018/8/26.
 */

public interface ClientAPI {


    //获取极验验证参数
    @FormUrlEncoded
    @POST("system/getCaptchaParams")
    Call<CaptchaParamsBean> getCaptchaParams(@Field("sign") String sign, @FieldMap Map<String, String> params);

    //登录
    @FormUrlEncoded
    @POST("user/login")
    Call<LoginBean> login(@Field("sign") String sign, @FieldMap Map<String, String> params);

    //注册
    @FormUrlEncoded
    @POST("user/register")
    Call<LoginBean> register(@Field("sign") String sign, @FieldMap Map<String, String> params);

    //通过邮件获取验证码
    @FormUrlEncoded
    @POST("system/getEmailCaptcha")
    Call<LoginBean> getEmailCaptcha(@Field("sign") String sign, @FieldMap Map<String, String> params);

    //通过手机号码获取短信验证码
    @FormUrlEncoded
    @POST("system/getPhoneCaptcha")
    Call<BaseBean> getPhoneCaptcha(@Field("sign") String sign, @FieldMap Map<String, String> params);

    //进行安全认证认证时，发送手机验证码
    @FormUrlEncoded
    @POST("user/getPhoneCaptcha4Verify")
    Call<BaseBean> getPhoneCodeVerify(@Field("sign") String sign, @FieldMap Map<String, String> params);

    //进行安全认证认证时，发送邮箱验证码
    @FormUrlEncoded
    @POST("user/getEmailCaptcha4Verify")
    Call<BaseBean> getEmailCodeVerify(@Field("sign") String sign, @FieldMap Map<String, String> params);

    //删除提币地址
    @FormUrlEncoded
    @POST("wallet/deleteAddressWithdraw")
    Call<DeleteAdressBean> getDeleteAdress(@Field("sign") String sign, @FieldMap Map<String, String> params);

    //进行安全认证认用于绑定账号、开启或关闭安全认证
    @FormUrlEncoded
    @POST("user/verifySecurity")
    Call<BaseBean> verifySecurity(@Field("sign") String sign, @FieldMap Map<String, String> params);

    //K线
//    @FormUrlEncoded
//    @POST("Spot/kline")
//    Call<KlineBean> getKline(@Field("sign") String sign, @FieldMap Map<String, String> params);

    @GET("Spot/kline")
    Call<KlineBean> getKline(@Query("instrument") String instrument,
                             @Query("span") String limit);


    //获取国家列表
    @FormUrlEncoded
    @POST("system/getCountries")
    Call<CountriesBean> getCountries(@Field("sign") String sign, @FieldMap Map<String, String> params);

    //根据IP地址，获取当前国家id
    @FormUrlEncoded
    @POST("system/getCurrentCountryId")
    Call<CurrentCountryIdBean> getCurrentCountryId(@Field("sign") String sign, @FieldMap Map<String, String> params);


    //ticker全量
    @FormUrlEncoded
    @POST("spot/products")
    Call<SpotProductsBean> getsSpotProducts(@Field("sign") String sign, @FieldMap Map<String, String> params);

    //system/checkUpdate
    //检查app的版本更新
    @FormUrlEncoded
    @POST("system/checkUpdate")
    Call<CheckUpdateBean> getcheckUpdate(@Field("sign") String sign, @FieldMap Map<String, String> params);

    //修改密码
    @FormUrlEncoded
    @POST("user/editPwd")
    Call<BaseBean> editPwd(@Field("sign") String sign, @FieldMap Map<String, String> params);

    //币种列表
    @FormUrlEncoded
    @POST("wallet/currencies")
    Call<CurrenciesBean> getCurrencies(@Field("sign") String sign, @FieldMap Map<String, String> params);

    //退出登录
    @FormUrlEncoded
    @POST("user/logout")
    Call<BaseBean> getLoginOut(@Field("sign") String sign, @FieldMap Map<String, String> params);

    //充币地址
    @FormUrlEncoded
    @POST("wallet/address")
    Call<WalletAddressBean> getaddress(@Field("sign") String sign, @FieldMap Map<String, String> params);

    //获取账号安全信息
    @FormUrlEncoded
    @POST("user/securityInfo")
    Call<SecurityInfoBean> getSecurity(@Field("sign") String sign, @FieldMap Map<String, String> params);

    //用户提币地址列表
    @FormUrlEncoded
    @POST("wallet/addressWithdrawList")
    Call<WithdrawsListBean> getWithdrawList(@Field("sign") String sign, @FieldMap Map<String, String> params);

    //交易所用户转账到聚宝盆钱包
    @FormUrlEncoded
    @POST("wallet/transferOut")
    Call<BaseBean> getTransferOut(@Field("sign") String sign, @FieldMap Map<String, String> params);

    //用户提币申请
    @FormUrlEncoded
    @POST("wallet/withdraw")
    Call<BaseBean> getWithDraw(@Field("sign") String sign, @FieldMap Map<String, String> params);


    //用户提币记录列表
    @FormUrlEncoded
    @POST("wallet/withdraws")
    Call<WithDrawsBean> getWithDraws(@Field("sign") String sign, @FieldMap Map<String, String> params);


    //用户转账聚宝盆记录列表
    @FormUrlEncoded
    @POST("wallet/transfers")
    Call<ExchangeDetailBean> getExchanggeDetail(@Field("sign") String sign, @FieldMap Map<String, String> params);

    //提现通道列表
    @FormUrlEncoded
    @POST("wallet/withdrawChannel")
    Call<WthdrawChannelBean> getWthdrawChannel(@Field("sign") String sign, @FieldMap Map<String, String> params);


    //用户充币记录列表
    @FormUrlEncoded
    @POST("wallet/deposits")
    Call<WithDrawsBean> getDeposites(@Field("sign") String sign, @FieldMap Map<String, String> params);

    //我的资产
    @FormUrlEncoded
    @POST("wallet/asset")
    Call<AssetBean> getAsset(@Field("sign") String sign, @FieldMap Map<String, String> params);

    //编辑提币地址
    @FormUrlEncoded
    @POST("wallet/editAddressWithdraw")
    Call<EditAddressWithdrawBean> getEditAddressWithdraw(@Field("sign") String sign, @FieldMap Map<String, String> params);

    //忘记密码
    @FormUrlEncoded
    @POST("user/forgetPwd")
    Call<BaseBean> forgetPwd(@Field("sign") String sign, @FieldMap Map<String, String> params);


    //绑定手机号时，发送验证码
    @FormUrlEncoded
    @POST("user/getPhoneCaptcha4Bind")
    Call<GetPhoneCaptcha4BindBean> getPhoneCaptcha4Bind(@Field("sign") String sign, @FieldMap Map<String, String> params);

    //绑定手机号时，校验验证码
    @FormUrlEncoded
    @POST("user/verifyPhone4Bind")
    Call<VerifyPhone4BindBean> verifyPhone4Bind(@Field("sign") String sign, @FieldMap Map<String, String> params);


    //绑定邮箱时，发送验证码
    @FormUrlEncoded
    @POST("user/getEmailCaptcha4Bind")
    Call<GetPhoneCaptcha4BindBean> getEmailCaptcha4Bind(@Field("sign") String sign, @FieldMap Map<String, String> params);

    //绑定邮箱时，校验验证码
    @FormUrlEncoded
    @POST("user/verifyEmail4Bind")
    Call<VerifyPhone4BindBean> verifyEmail4Bind(@Field("sign") String sign, @FieldMap Map<String, String> params);


    //获取google验证器密钥
    @FormUrlEncoded
    @POST("user/getGoogleAuthSecret")
    Call<GoogleAuthenticatorSecretBean> getGoogleAuthSecret(@Field("sign") String sign, @FieldMap Map<String, String> params);

    //绑定google验证器时，校验google验证器的验证码
    @FormUrlEncoded
    @POST("user/verifyGoogleAuth4Bind")
    Call<VerifyPhone4BindBean> verifyGoogleAuth4Bind(@Field("sign") String sign, @FieldMap Map<String, String> params);

    //获取行情order book 数据查询接口
    @GET("spot/depth")
    Call<DepthBean> getDepth(@Query("instrument") String instrument,
                             @Query("limit") String limit);


    //获取交易系统配置
    @FormUrlEncoded
    @POST("system/getSpotConfig")
    Call<SpotConfigBean> getSpotConfig(@Field("sign") String sign, @FieldMap Map<String, String> params);

    //用户下单
    @FormUrlEncoded
    @POST("spot/orderPlace")
    Call<BaseBean> orderPlace(@Field("sign") String sign, @FieldMap Map<String, String> params);


    //用户当前委托列表
    @FormUrlEncoded
    @POST("spot/current")
    Call<CurrentCommissionBean> getCurrent(@Field("sign") String sign, @FieldMap Map<String, String> params);

    //用户撤销一个订单
    @FormUrlEncoded
    @POST("spot/orderSingleCancel")
    Call<BaseBean> orderSingleCancel(@Field("sign") String sign, @FieldMap Map<String, String> params);


    //用户历史委托列表
    @FormUrlEncoded
    @POST("spot/history")
    Call<HistoryBean> getHistory(@Field("sign") String sign, @FieldMap Map<String, String> params);


    //编辑用户API Key
    @FormUrlEncoded
    @POST("user/editApiKey")
    Call<EditApiBean> editApiKey(@Field("sign") String sign, @FieldMap Map<String, String> params);

    //获取个人身份认证信息
    @FormUrlEncoded
    @POST("user/userIdentityAuth")
    Call<UserIdentityAuthBean> getuserIdentityAuth(@Field("sign") String sign, @FieldMap Map<String, String> params);

    //编辑用户身份认证信息
    @FormUrlEncoded
    @POST("user/editIdentityAuth")
    Call<BaseBean> editIdentityAuth(@Field("sign") String sign, @FieldMap Map<String, String> params);


}
