package com.huijinlong.newretail.net;

import retrofit2.Call;

/**
 * Created by scanor on 2016/7/7.
 */

public class CallWrapper {
    private Call mCall;
    private String mTag;

    public CallWrapper(Call call, String tag) {
        mCall = call;
        mTag = tag;
    }

    public Call getCall() {
        return mCall;
    }

    public void setCall(Call call) {
        mCall = call;
    }

    public String getTag() {
        return mTag;
    }

    public void setTag(String tag) {
        mTag = tag;
    }
}
