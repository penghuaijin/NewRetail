package com.huijinlong.newretail.net;

import com.huijinlong.newretail.util.LogUtils;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;

/**
 * Created by penghuaijin on 2018/10/10.
 */

public class WebSocketWorker extends WebSocketClient {

    public WebSocketWorker(URI serverUri, Draft protocolDraft) {
        super(serverUri, protocolDraft);
    }

    @Override
    public void onOpen(ServerHandshake handshakedata) {
        LogUtils.e("WebSocketWorker--message--onOpen--:"+handshakedata.getHttpStatusMessage());

    }


    @Override
    public void onMessage(String message) {
       // KlineBean.DataBean dataBean=new Gson().fromJson(message,KlineBean.DataBean.class);
        //EventBus.getDefault().post(dataBean);
        LogUtils.e("WebSocketWorker--message--:");
    }

    @Override
    public void onClose(int code, String reason, boolean remote) {

        LogUtils.e("WebSocketWorker--message--onClose--:"+reason);

    }

    @Override
    public void onError(Exception ex) {
        LogUtils.e("WebSocketWorker--message--onError--:"+ex.getMessage());

    }
}
