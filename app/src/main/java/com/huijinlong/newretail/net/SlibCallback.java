package com.huijinlong.newretail.net;

import android.text.TextUtils;

import com.huijinlong.newretail.util.LogUtils;
import com.huijinlong.newretail.util.ThreadPool;
import com.huijinlong.newretail.util.ToastUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by shoulder on 16/7/5.
 */

public abstract class SlibCallback<T> implements Callback<T> {
    @Override
    final public void onResponse(final Call<T> call, final Response<T> response) {
        if (response.code() != 200) {
            onFailure(call, new RuntimeException(response.code() + ""));
            //不是200即出现网络错误
            return;
        }
        ThreadPool.newTask(new ThreadPool.AsyncWork<T>() {
            @Override
            public T doHardWork() {
                return preProcess(call, response);
            }

            @Override
            public void onCallback(T result, boolean success, Exception e) {
                afterProcess(result, success, e);
            }
        });
    }

    public void afterProcess(T result, boolean success, Exception e) {

    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        LogUtils.e("请求失败:" + t.toString());
        String msg = t.getMessage();
        if (TextUtils.isEmpty(msg))
            msg = "网络出错";
        if (msg.contains("Unable to resolve host") || msg.contains("failed to connect"))
            msg = "请重新尝试";
        /*else

        msg = TextUtils.isEmpty(msg) ? "请重新尝试" : msg;*/
        if (!msg.equals("Canceled") && !msg.equals("Socket closed"))
            ToastUtils.toast("请求失败:" + msg);
        onFinalResponse(false);
    }

    public abstract void onEmpty(T result);

    /**
     * @param call     请求
     * @param response 回调
     * @return 返回异步处理的结果
     */
    public abstract T preProcess(Call<T> call, Response<T> response);

    public abstract void onSuccess(T result);

    public abstract void onBusinessFailure(Exception e);

    /**
     * @param isSuccess 成功为true， 失败为false
     *                  无论成功失败，只要进入了onResponse或者onFailure都会调用，
     *                  其中错误包括网络错误和业务处理错误
     */
    public abstract void onFinalResponse(boolean isSuccess);
}
