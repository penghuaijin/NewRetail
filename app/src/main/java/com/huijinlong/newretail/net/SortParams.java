package com.huijinlong.newretail.net;

import java.util.TreeMap;

/**
 */
public class SortParams {

    public  static String paramssort(String params)
    {
        String result = "";
        String temp[] = params.split("&");
        String key[] = new String[temp.length];
        String value[] = new String[temp.length];

        for(int i = 0;i<temp.length;i++) {
            key[i] = temp[i].split("=")[0];
            value[i]=temp[i].split("=")[1];
        }

        for (int i = 0; i < key.length-1; i++) {
            for (int j =i+1; j < key.length; j++) {
                int intTemp = key[i].compareToIgnoreCase(key[j]);
                String strTemp;
                String strTempValue;
                if(intTemp>0){
                    strTemp = key[j];
                    strTempValue = value[j];
                    key[j] = key[i];
                    value[j]=value[i];
                    key[i] = strTemp;
                    value[i]=strTempValue;
                }
            }
        }

        for(int k=0;k<key.length-1;k++)
        {
            result +=key[k]+"="+value[k]+"&";
        }

        result+=key[key.length-1]+"="+value[key.length-1];

        return result;
    }

    public static String getParams(TreeMap<String,String> map){

        String params = map.toString();
        params = params.substring(1, params.length() - 1);
        params = params.replaceAll(", ", "&");
        return params;
    }
}
