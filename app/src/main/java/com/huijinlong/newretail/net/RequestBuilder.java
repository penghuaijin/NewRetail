package com.huijinlong.newretail.net;


import com.huijinlong.newretail.base.BaseApplication;
import com.huijinlong.newretail.util.LogUtils;
import com.huijinlong.newretail.util.SpUtil;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RequestBuilder {
    private static final String BASE_URL = "https://apittt.nr.exchange/api/v1/";
    //    private static final String BASE_URL = "https://146.196.55.160/";
    private OkHttpClient client;

    private static final Retrofit RETROFIT = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            // .addCallAdapterFactory()
            .client(genericClient(false))

            .addConverterFactory(GsonConverterFactory.create())
            .build();

    private static final ClientAPI NET_SERVICE = RETROFIT.create(ClientAPI.class);

    public static ClientAPI getNetService() {
        return NET_SERVICE;
    }


    public static ClientAPI getNetService(boolean isNeedAuthorization) {
        if (isNeedAuthorization) {
            return RETROFITAUTHORIZATION.create(ClientAPI.class);
        } else {
            return NET_SERVICE;
        }
    }

    private static final Retrofit RETROFITAUTHORIZATION = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            // .addCallAdapterFactory()
            .client(genericClient(true))

            .addConverterFactory(GsonConverterFactory.create())
            .build();

    public static <T> Call<T> execute(Call<T> call, String tag, SlibCallback<T> callback) {
        RequestManager.getRequestManager().enqueue(new CallWrapper(call, tag));
        call.enqueue(callback);
        return call;
    }

    public static RequestBody toRequestBody(String value) {
        return RequestBody.create(MediaType.parse("text/plain"), value);
    }

    public static RequestBody toRequestBody(byte[] img) {
        return RequestBody.create(MediaType.parse("image/jpeg"), img);
    }

    public static Map<String, RequestBody> toPartMap(Map<String, String> map) {
        Map<String, RequestBody> params = new HashMap<>();
        Iterator<String> iterator = map.keySet().iterator();
        while (iterator.hasNext()) {
            String key = iterator.next();
            params.put(key, RequestBuilder.toRequestBody(map.get(key)));
        }
        return params;
    }


    private static OkHttpClient genericClient(boolean isNeedAuthorization) {  //创建底层okhttpclient，定义相关的header参数
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
//                .sslSocketFactory(getSSLSocketFactory())
                .readTimeout(1, TimeUnit.MINUTES)
                .connectTimeout(1, TimeUnit.MINUTES)
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        String authorization = SpUtil.getString(SpUtil.AUTHORIZATION, "");
                        Request request;
                        if (isNeedAuthorization) {
                            request = chain.request()
                                    .newBuilder()
                                    .addHeader("AUTHORIZATION", authorization)
                                    .build();
                        } else {
                            request = chain.request()
                                    .newBuilder()
                                    .build();
                        }
                        return chain.proceed(request);
                    }
                });
     /*builder.addInterceptor(new ErrorHandlerInterceptor());*/
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            @Override
            public void log(String message) {
                //打印retrofit日志
                LogUtils.d("retrofitBack = " + message);
            }
        });
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        builder.addInterceptor(/*new ErrorHandlerInterceptor()*/loggingInterceptor);
        return builder.build();
    }

//    public static SSLSocketFactory getSSLSocketFactory() {
//        final String CLIENT_TRUST_PASSWORD = "123";//信任证书密码，该证书默认密码是123456
//        final String CLIENT_AGREEMENT = "TLS";//使用协议
//        final String CLIENT_TRUST_KEYSTORE = "BKS";
//        SSLContext sslContext = null;
//        try {
//            //取得SSL的SSLContext实例
//            sslContext = SSLContext.getInstance(CLIENT_AGREEMENT);
//            //取得TrustManagerFactory的X509密钥管理器实例
//            TrustManagerFactory trustManager = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
//            //取得BKS密库实例
//            KeyStore tks = KeyStore.getInstance(CLIENT_TRUST_KEYSTORE);
//            InputStream is = NRApplication.getInstance().getResources().openRawResource(R.raw.touce);
//            try {
//                tks.load(is, CLIENT_TRUST_PASSWORD.toCharArray());
//            } finally {
//                is.close();
//            }
//            //初始化密钥管理器
//            trustManager.init(tks);
//            //初始化SSLContext
//            sslContext.init(null, trustManager.getTrustManagers(), null);
//        } catch (Exception e) {
//            e.printStackTrace();
//            Log.e("SslContextFactory", e.getMessage());
//        }
//        return sslContext.getSocketFactory();
//    }

}
