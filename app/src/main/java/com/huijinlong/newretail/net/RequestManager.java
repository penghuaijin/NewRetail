package com.huijinlong.newretail.net;

import java.util.LinkedList;

import retrofit2.Call;

/**
 * Created by scanor on 2016/6/30.
 */

public class RequestManager {
    private static RequestManager sRequestManager = new RequestManager();
    private LinkedList<CallWrapper> mCallWrappers = new LinkedList<>();

    public static RequestManager getRequestManager() {
        return sRequestManager;
    }

    public void enqueue(CallWrapper callWrapper) {
        if (callWrapper == null) {
            return;
        }
        mCallWrappers.push(callWrapper);
    }

    /**
     * 取消特定的call
     *
     * @param call
     */
    public void cancel(Call call) {
        if (call == null) {
            return;
        }
        for (int i = 0; i < mCallWrappers.size(); i++) {
            CallWrapper callWrapper = mCallWrappers.get(i);
            if (call.equals(callWrapper.getCall())) {
                call.cancel();
                mCallWrappers.remove(i);
                break;
            }
        }
    }

    /**
     * 取消这个tag的所有请求
     *
     * @param tag
     */
    public void cancel(String tag) {
        if (tag == null) {
            return;
        }
        LinkedList<CallWrapper> tempList = new LinkedList<>(mCallWrappers);
        for (int i = 0; i < mCallWrappers.size(); i++) {
            CallWrapper callWrapper = mCallWrappers.get(i);
            if (tag.equals(callWrapper.getTag())) {
                callWrapper.getCall().cancel();
                tempList.remove(callWrapper);
            }
        }
        mCallWrappers.clear();
        mCallWrappers.addAll(tempList);
    }
}
