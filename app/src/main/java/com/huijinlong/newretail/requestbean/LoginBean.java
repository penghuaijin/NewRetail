package com.huijinlong.newretail.requestbean;

import com.huijinlong.newretail.base.BaseBean;

/**
 * Created by penghuaijin on 2018/9/10.
 */

public class LoginBean extends BaseBean {

    /**
     * status : 200
     * data : {"tokenType":"Bearer","expiresIn":86400,"accessToken":"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImJhMzBjNmI5MThiMGFlZjI5ZGE2NmZhMDUwNWY0NjZiZGE3MTI1YjBhY2YyZjU5M2IzMmFhZTMyMzRjYTFkYTZiZDZkYzM3MDE2MjAzMTM3In0.eyJhdWQiOiIxIiwianRpIjoiYmEzMGM2YjkxOGIwYWVmMjlkYTY2ZmEwNTA1ZjQ2NmJkYTcxMjViMGFjZjJmNTkzYjMyYWFlMzIzNGNhMWRhNmJkNmRjMzcwMTYyMDMxMzciLCJpYXQiOjE1MzM4MTExMDksIm5iZiI6MTUzMzgxMTEwOSwiZXhwIjoxNTMzODk3NTA5LCJzdWIiOiI0Iiwic2NvcGVzIjpbXX0.mP3b22Yuiq3PRmCvL96F_R3x24fDRUjcjpNcumK_uhzNrlA1LTnC8uBbMqyzXZKqIQ6csmEm0R9D4tGypVPfHBFho6gq7rmDLmb_IPoFsuH25wIkEGoYN9RJPqIJ9B0LggdBCDuIlZkrFoyeKko7AtR5pxad8wBEQ4Nps0KdIBOGiBsoyVEakcrkn_wZDk0rR5GMPh1AXXSTGKwcdhpuJmptMKUp63xi2Hb06E2wX1xAHrQLiWntaFstjlElxQD6lYzLwmnlgQsCYRV2lHnZNC8fo6nxBXhPge-Kqo4F5uMn4b-PTVgL_1lh_vPF7mRS_gfGQ1ZyMIzM-yjRVMAXBQ","REFRESH_TOKEN":"def50200019572f1d872d9076ca1344db40dbf6ce2aad3895cf6a0fa871abf92fcb671f6f0307a522e6791bd9dd2fc1ddf24db43ac27d1cdd218bcf5ce242f8e25639984b8b589b9d4fd833433d41dd1726404af893d29328ec61c04c32c58b56f42c404ad783c2f0e1a379e77ef9169223542453d6d3ed65ca91ec0b13ad52a16e9035d6c7a478c9d5d0145e2b372cebef144f1702ef744ad5e6e6f17b20f6914264a5fe846411192aab0cd01b5cf7e52fe1d7325bcb2b34ae7d5aca9881d8d3f06cdd8bd77877e59e57f6bfffdd69191a949bba4ff705ec9dfa7939d9071ed467c020135ecb9ac323213c5b031c46c9b361d85bd99109bcb39a90d12864dfe0272c09dd9fe312fc6b4b14ab98c10e8278e5c0d559acf54aa36ead1f11964a341f5723301fcf23c5a4dc0d7a0f02a43423a3df8d3506707baa352b590fe0c363192e652415b437071becf919eadc1daaedd5f8655a82da7be795f42ab13bea1b2","nickName":"139*****555","verifyGoogleAuthenticator":0,"mobile":"139*****555","email":"555****77@qq.com","userId":"10000319"}
     */

    private DataBean data;


    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * tokenType : Bearer
         * expiresIn : 86400
         * accessToken : eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImJhMzBjNmI5MThiMGFlZjI5ZGE2NmZhMDUwNWY0NjZiZGE3MTI1YjBhY2YyZjU5M2IzMmFhZTMyMzRjYTFkYTZiZDZkYzM3MDE2MjAzMTM3In0.eyJhdWQiOiIxIiwianRpIjoiYmEzMGM2YjkxOGIwYWVmMjlkYTY2ZmEwNTA1ZjQ2NmJkYTcxMjViMGFjZjJmNTkzYjMyYWFlMzIzNGNhMWRhNmJkNmRjMzcwMTYyMDMxMzciLCJpYXQiOjE1MzM4MTExMDksIm5iZiI6MTUzMzgxMTEwOSwiZXhwIjoxNTMzODk3NTA5LCJzdWIiOiI0Iiwic2NvcGVzIjpbXX0.mP3b22Yuiq3PRmCvL96F_R3x24fDRUjcjpNcumK_uhzNrlA1LTnC8uBbMqyzXZKqIQ6csmEm0R9D4tGypVPfHBFho6gq7rmDLmb_IPoFsuH25wIkEGoYN9RJPqIJ9B0LggdBCDuIlZkrFoyeKko7AtR5pxad8wBEQ4Nps0KdIBOGiBsoyVEakcrkn_wZDk0rR5GMPh1AXXSTGKwcdhpuJmptMKUp63xi2Hb06E2wX1xAHrQLiWntaFstjlElxQD6lYzLwmnlgQsCYRV2lHnZNC8fo6nxBXhPge-Kqo4F5uMn4b-PTVgL_1lh_vPF7mRS_gfGQ1ZyMIzM-yjRVMAXBQ
         * REFRESH_TOKEN : def50200019572f1d872d9076ca1344db40dbf6ce2aad3895cf6a0fa871abf92fcb671f6f0307a522e6791bd9dd2fc1ddf24db43ac27d1cdd218bcf5ce242f8e25639984b8b589b9d4fd833433d41dd1726404af893d29328ec61c04c32c58b56f42c404ad783c2f0e1a379e77ef9169223542453d6d3ed65ca91ec0b13ad52a16e9035d6c7a478c9d5d0145e2b372cebef144f1702ef744ad5e6e6f17b20f6914264a5fe846411192aab0cd01b5cf7e52fe1d7325bcb2b34ae7d5aca9881d8d3f06cdd8bd77877e59e57f6bfffdd69191a949bba4ff705ec9dfa7939d9071ed467c020135ecb9ac323213c5b031c46c9b361d85bd99109bcb39a90d12864dfe0272c09dd9fe312fc6b4b14ab98c10e8278e5c0d559acf54aa36ead1f11964a341f5723301fcf23c5a4dc0d7a0f02a43423a3df8d3506707baa352b590fe0c363192e652415b437071becf919eadc1daaedd5f8655a82da7be795f42ab13bea1b2
         * nickName : 139*****555
         * verifyGoogleAuthenticator : 0
         * mobile : 139*****555
         * email : 555****77@qq.com
         * userId : 10000319
         */

        private String tokenType;
        private int expiresIn;
        private String accessToken;
        private String refreshToken;
        private String nickName;
        private int verifyGoogleAuthenticator;
        private String mobile;
        private String email;
        private String userId;

        public String getTokenType() {
            return tokenType;
        }

        public void setTokenType(String tokenType) {
            this.tokenType = tokenType;
        }

        public int getExpiresIn() {
            return expiresIn;
        }

        public void setExpiresIn(int expiresIn) {
            this.expiresIn = expiresIn;
        }

        public String getAccessToken() {
            return accessToken;
        }

        public void setAccessToken(String accessToken) {
            this.accessToken = accessToken;
        }

        public String getRefreshToken() {
            return refreshToken;
        }

        public void setRefreshToken(String refreshToken) {
            this.refreshToken = refreshToken;
        }

        public String getNickName() {
            return nickName;
        }

        public void setNickName(String nickName) {
            this.nickName = nickName;
        }

        public int getVerifyGoogleAuthenticator() {
            return verifyGoogleAuthenticator;
        }

        public void setVerifyGoogleAuthenticator(int verifyGoogleAuthenticator) {
            this.verifyGoogleAuthenticator = verifyGoogleAuthenticator;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }
    }
}
