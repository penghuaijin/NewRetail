package com.huijinlong.newretail.requestbean;

import com.huijinlong.newretail.base.BaseBean;

/**
 * Created by penghuaijin on 2018/9/26.
 */

public class GetPhoneCaptcha4BindBean extends BaseBean {


    /**
     * data : {"mobile":"139*****555","email":"222****44@qq.com"}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * mobile : 139*****555
         * email : 222****44@qq.com
         */

        private String mobile;
        private String email;

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }
    }
}
