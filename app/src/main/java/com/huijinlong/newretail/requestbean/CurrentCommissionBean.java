package com.huijinlong.newretail.requestbean;

import com.huijinlong.newretail.base.BaseBean;

import java.util.List;

/**
 * @author penghuaijin
 * @date 2018/10/25 11:13 AM
 * @Description: ()
 */
public class CurrentCommissionBean extends BaseBean {

    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 507
         * time : 2018-09-10 15:54:00
         * side : 买入
         * symbol : BTC/USDT
         * price : 0.02
         * qty : 0.5481
         * filled : 0.000000
         * unfilled : 0.548100
         * cancelStatus : 1
         */

        private int id;
        private String time;
        private String side;
        private String symbol;
        private String price;
        private String qty;
        private String filled;
        private String unfilled;
        private int cancelStatus;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getSide() {
            return side;
        }

        public void setSide(String side) {
            this.side = side;
        }

        public String getSymbol() {
            return symbol;
        }

        public void setSymbol(String symbol) {
            this.symbol = symbol;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getQty() {
            return qty;
        }

        public void setQty(String qty) {
            this.qty = qty;
        }

        public String getFilled() {
            return filled;
        }

        public void setFilled(String filled) {
            this.filled = filled;
        }

        public String getUnfilled() {
            return unfilled;
        }

        public void setUnfilled(String unfilled) {
            this.unfilled = unfilled;
        }

        public int getCancelStatus() {
            return cancelStatus;
        }

        public void setCancelStatus(int cancelStatus) {
            this.cancelStatus = cancelStatus;
        }
    }
}
