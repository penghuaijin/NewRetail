package com.huijinlong.newretail.requestbean;

import com.huijinlong.newretail.base.BaseBean;

/**
 * @author penghuaijin
 * @date 2018/12/3 11:39 AM
 * @Description: ()
 */
public class UserIdentityAuthBean extends BaseBean {


    /**
     * data : {"truename":"王小伟","identityNumber":"45262********50562","authOk":1,"nationality":"China(中国)"}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * truename : 王小伟
         * identityNumber : 45262********50562
         * authOk : 1
         * nationality : China(中国)
         */

        private String truename;
        private String identityNumber;
        private int authOk;
        private String nationality;

        public String getTruename() {
            return truename;
        }

        public void setTruename(String truename) {
            this.truename = truename;
        }

        public String getIdentityNumber() {
            return identityNumber;
        }

        public void setIdentityNumber(String identityNumber) {
            this.identityNumber = identityNumber;
        }

        public int getAuthOk() {
            return authOk;
        }

        public void setAuthOk(int authOk) {
            this.authOk = authOk;
        }

        public String getNationality() {
            return nationality;
        }

        public void setNationality(String nationality) {
            this.nationality = nationality;
        }
    }
}
