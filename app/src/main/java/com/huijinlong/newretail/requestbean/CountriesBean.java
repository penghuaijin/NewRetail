package com.huijinlong.newretail.requestbean;

import com.huijinlong.newretail.base.BaseBean;

import java.util.List;

/**
 * Created by penghuaijin on 2018/9/12.
 */
public class CountriesBean extends BaseBean {


    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 38
         * nameCn : 中国
         * nameEn : China
         * code : 86
         */
        private int id;
        private String nameCn;
        private String nameEn;
        private String code;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getNameCn() {
            return nameCn;
        }

        public void setNameCn(String nameCn) {
            this.nameCn = nameCn;
        }

        public String getNameEn() {
            return nameEn;
        }

        public void setNameEn(String nameEn) {
            this.nameEn = nameEn;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }
    }
}
