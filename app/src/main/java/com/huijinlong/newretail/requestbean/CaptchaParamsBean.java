package com.huijinlong.newretail.requestbean;

import com.huijinlong.newretail.base.BaseBean;

/**
 * Created by penghuaijin on 2018/9/25.
 */

public class CaptchaParamsBean extends BaseBean {


    /**
     * data : {"success":1,"gt":"518e64d18bb21a872227bf250ff17ae1","challenge":"53b7b20fe29a6d9cb3f4cd206804f8c1","new_captcha":1}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * success : 1
         * gt : 518e64d18bb21a872227bf250ff17ae1
         * challenge : 53b7b20fe29a6d9cb3f4cd206804f8c1
         * new_captcha : 1
         */

        private int success;
        private String gt;
        private String challenge;
        private int new_captcha;

        public int getSuccess() {
            return success;
        }

        public void setSuccess(int success) {
            this.success = success;
        }

        public String getGt() {
            return gt;
        }

        public void setGt(String gt) {
            this.gt = gt;
        }

        public String getChallenge() {
            return challenge;
        }

        public void setChallenge(String challenge) {
            this.challenge = challenge;
        }

        public int getNew_captcha() {
            return new_captcha;
        }

        public void setNew_captcha(int new_captcha) {
            this.new_captcha = new_captcha;
        }
    }
}
