package com.huijinlong.newretail.requestbean;

import com.huijinlong.newretail.base.BaseBean;

import java.util.List;

/**
 * Created by penghuaijin on 2018/10/10.
 */

public class DepthBean extends BaseBean {

    /**
     * data : {"lastUpdateId":"72773","bids":[{"price":"1.12","quantity":"5.0000"},{"price":"1.12","quantity":"5.0000"},{"price":"1.12","quantity":"5.0000"}],"asks":[{"price":"1.12","quantity":"5.0000"},{"price":"1.12","quantity":"5.0000"},{"price":"1.12","quantity":"5.0000"}]}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * lastUpdateId : 72773
         * bids : [{"price":"1.12","quantity":"5.0000"},{"price":"1.12","quantity":"5.0000"},{"price":"1.12","quantity":"5.0000"}]
         * asks : [{"price":"1.12","quantity":"5.0000"},{"price":"1.12","quantity":"5.0000"},{"price":"1.12","quantity":"5.0000"}]
         */

        private String lastUpdateId;
        private List<BidsBean> bids;
        private List<AsksBean> asks;

        public String getLastUpdateId() {
            return lastUpdateId;
        }

        public void setLastUpdateId(String lastUpdateId) {
            this.lastUpdateId = lastUpdateId;
        }

        public List<BidsBean> getBids() {
            return bids;
        }

        public void setBids(List<BidsBean> bids) {
            this.bids = bids;
        }

        public List<AsksBean> getAsks() {
            return asks;
        }

        public void setAsks(List<AsksBean> asks) {
            this.asks = asks;
        }

        public static class BidsBean {
            public BidsBean(double price, double quantity) {
                this.price = price+"";
                this.quantity = quantity+"";
            }

            /**
             * price : 1.12
             * quantity : 5.0000
             */

            private String price;
            private String quantity;

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getQuantity() {
                return quantity;
            }

            public void setQuantity(String quantity) {
                this.quantity = quantity;
            }
        }

        public static class AsksBean {
            public AsksBean(double price, double quantity) {
                this.price = price+"";
                this.quantity = quantity+"";
            }

            /**
             * price : 1.12
             * quantity : 5.0000
             */


            private String price;
            private String quantity;

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getQuantity() {
                return quantity;
            }

            public void setQuantity(String quantity) {
                this.quantity = quantity;
            }
        }
    }
}
