package com.huijinlong.newretail.requestbean;

import com.google.gson.annotations.SerializedName;
import com.huijinlong.newretail.base.BaseBean;

import java.util.List;

/**
 * @author penghuaijin
 * @date 2018/10/24 5:10 PM
 * @Description: ()
 */
public class HistoryBean extends BaseBean {
    /**
     * data : {"currentPage":1,"total":13,"pageSize":5,"list":[{"id":374,"time":"2018-10-24 16:11:39","type":"限价","side":"买入","symbol":"BTC/USDT","price":"12.00","qty":"1.0000","filled":"1.000000","avgPrice":"11.000000","orderStatus":2,"cancelStatus":0,"status":"完全成交"},{"id":373,"time":"2018-10-24 16:09:28","type":"限价","side":"买入","symbol":"BTC/USDT","price":"110.00","qty":"1.0000","filled":"1.000000","avgPrice":"11.000000","orderStatus":2,"cancelStatus":0,"status":"完全成交"},{"id":372,"time":"2018-10-24 14:36:04","type":"限价","side":"买入","symbol":"BTC/USDT","price":"11.00","qty":"1.0000","filled":"1.000000","avgPrice":"11.000000","orderStatus":2,"cancelStatus":0,"status":"完全成交"},{"id":371,"time":"2018-10-24 14:32:25","type":"限价","side":"买入","symbol":"BTC/USDT","price":"11.00","qty":"1.0000","filled":"1.000000","avgPrice":"11.000000","orderStatus":2,"cancelStatus":0,"status":"完全成交"},{"id":368,"time":"2018-10-24 12:03:57","type":"限价","side":"卖出","symbol":"BTC/USDT","price":"5.00","qty":"1.0000","filled":"1.000000","avgPrice":"5.000000","orderStatus":2,"cancelStatus":0,"status":"完全成交"}]}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * currentPage : 1
         * total : 13
         * pageSize : 5
         * list : [{"id":374,"time":"2018-10-24 16:11:39","type":"限价","side":"买入","symbol":"BTC/USDT","price":"12.00","qty":"1.0000","filled":"1.000000","avgPrice":"11.000000","orderStatus":2,"cancelStatus":0,"status":"完全成交"},{"id":373,"time":"2018-10-24 16:09:28","type":"限价","side":"买入","symbol":"BTC/USDT","price":"110.00","qty":"1.0000","filled":"1.000000","avgPrice":"11.000000","orderStatus":2,"cancelStatus":0,"status":"完全成交"},{"id":372,"time":"2018-10-24 14:36:04","type":"限价","side":"买入","symbol":"BTC/USDT","price":"11.00","qty":"1.0000","filled":"1.000000","avgPrice":"11.000000","orderStatus":2,"cancelStatus":0,"status":"完全成交"},{"id":371,"time":"2018-10-24 14:32:25","type":"限价","side":"买入","symbol":"BTC/USDT","price":"11.00","qty":"1.0000","filled":"1.000000","avgPrice":"11.000000","orderStatus":2,"cancelStatus":0,"status":"完全成交"},{"id":368,"time":"2018-10-24 12:03:57","type":"限价","side":"卖出","symbol":"BTC/USDT","price":"5.00","qty":"1.0000","filled":"1.000000","avgPrice":"5.000000","orderStatus":2,"cancelStatus":0,"status":"完全成交"}]
         */

        private int currentPage;
        private int total;
        private int pageSize;
        private List<ListBean> list;

        public int getCurrentPage() {
            return currentPage;
        }

        public void setCurrentPage(int currentPage) {
            this.currentPage = currentPage;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public static class ListBean {
            /**
             * id : 374
             * time : 2018-10-24 16:11:39
             * type : 限价
             * side : 买入
             * symbol : BTC/USDT
             * price : 12.00
             * qty : 1.0000
             * filled : 1.000000
             * avgPrice : 11.000000
             * orderStatus : 2
             * cancelStatus : 0
             * status : 完全成交
             */

            private int id;
            private String time;
            private String type;
            private String side;
            private String symbol;
            private String price;
            private String qty;
            private String filled;
            private String avgPrice;
            private int orderStatus;
            private int cancelStatus;
            @SerializedName("status")
            private String statusX;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getTime() {
                return time;
            }

            public void setTime(String time) {
                this.time = time;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getSide() {
                return side;
            }

            public void setSide(String side) {
                this.side = side;
            }

            public String getSymbol() {
                return symbol;
            }

            public void setSymbol(String symbol) {
                this.symbol = symbol;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getQty() {
                return qty;
            }

            public void setQty(String qty) {
                this.qty = qty;
            }

            public String getFilled() {
                return filled;
            }

            public void setFilled(String filled) {
                this.filled = filled;
            }

            public String getAvgPrice() {
                return avgPrice;
            }

            public void setAvgPrice(String avgPrice) {
                this.avgPrice = avgPrice;
            }

            public int getOrderStatus() {
                return orderStatus;
            }

            public void setOrderStatus(int orderStatus) {
                this.orderStatus = orderStatus;
            }

            public int getCancelStatus() {
                return cancelStatus;
            }

            public void setCancelStatus(int cancelStatus) {
                this.cancelStatus = cancelStatus;
            }

            public String getStatusX() {
                return statusX;
            }

            public void setStatusX(String statusX) {
                this.statusX = statusX;
            }
        }
    }
}
