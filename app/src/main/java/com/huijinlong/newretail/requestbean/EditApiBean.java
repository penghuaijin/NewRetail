package com.huijinlong.newretail.requestbean;

import com.huijinlong.newretail.base.BaseBean;

/**
 * @author penghuaijin
 * @date 2018/10/25 8:26 PM
 * @Description: ()
 */
public class EditApiBean extends BaseBean {

    /**
     * data : {"verifyMobile":1,"verifyEmail":1,"verifyGoogleAuthenticator":0}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * verifyMobile : 1
         * verifyEmail : 1
         * verifyGoogleAuthenticator : 0
         */

        private int verifyMobile;
        private int verifyEmail;
        private int verifyGoogleAuthenticator;

        public int getVerifyMobile() {
            return verifyMobile;
        }

        public void setVerifyMobile(int verifyMobile) {
            this.verifyMobile = verifyMobile;
        }

        public int getVerifyEmail() {
            return verifyEmail;
        }

        public void setVerifyEmail(int verifyEmail) {
            this.verifyEmail = verifyEmail;
        }

        public int getVerifyGoogleAuthenticator() {
            return verifyGoogleAuthenticator;
        }

        public void setVerifyGoogleAuthenticator(int verifyGoogleAuthenticator) {
            this.verifyGoogleAuthenticator = verifyGoogleAuthenticator;
        }
    }
}
