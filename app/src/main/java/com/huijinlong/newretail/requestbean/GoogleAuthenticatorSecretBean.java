package com.huijinlong.newretail.requestbean;

import com.huijinlong.newretail.base.BaseBean;

/**
 * Created by penghuaijin on 2018/9/26.
 */

public class GoogleAuthenticatorSecretBean extends BaseBean {


    /**
     * data : {"verifyMobile":1,"verifyEmail":0,"verifyGoogleAuthenticator":1,"mobile":"139*****555","email":"","secret":"J6XORCCRXEF6MENU","qrCodeContent":"otpauth://totp/13923885555--2018-08-13,20:58:15?secret=J6XORCCRXEF6MENU&issuer=New+Retail+Exchange"}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * verifyMobile : 1
         * verifyEmail : 0
         * verifyGoogleAuthenticator : 1
         * mobile : 139*****555
         * email :
         * secret : J6XORCCRXEF6MENU
         * qrCodeContent : otpauth://totp/13923885555--2018-08-13,20:58:15?secret=J6XORCCRXEF6MENU&issuer=New+Retail+Exchange
         */

        private int verifyMobile;
        private int verifyEmail;
        private int verifyGoogleAuthenticator;
        private String mobile;
        private String email;
        private String secret;
        private String qrCodeContent;

        public int getVerifyMobile() {
            return verifyMobile;
        }

        public void setVerifyMobile(int verifyMobile) {
            this.verifyMobile = verifyMobile;
        }

        public int getVerifyEmail() {
            return verifyEmail;
        }

        public void setVerifyEmail(int verifyEmail) {
            this.verifyEmail = verifyEmail;
        }

        public int getVerifyGoogleAuthenticator() {
            return verifyGoogleAuthenticator;
        }

        public void setVerifyGoogleAuthenticator(int verifyGoogleAuthenticator) {
            this.verifyGoogleAuthenticator = verifyGoogleAuthenticator;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getSecret() {
            return secret;
        }

        public void setSecret(String secret) {
            this.secret = secret;
        }

        public String getQrCodeContent() {
            return qrCodeContent;
        }

        public void setQrCodeContent(String qrCodeContent) {
            this.qrCodeContent = qrCodeContent;
        }
    }
}
