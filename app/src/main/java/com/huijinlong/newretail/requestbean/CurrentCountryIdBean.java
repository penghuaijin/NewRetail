package com.huijinlong.newretail.requestbean;

import com.huijinlong.newretail.base.BaseBean;

/**
 * Created by penghuaijin on 2018/9/12.
 */

public class CurrentCountryIdBean extends BaseBean{


    /**
     * data : {"id":0}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 0
         */

        private int id;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }
}
