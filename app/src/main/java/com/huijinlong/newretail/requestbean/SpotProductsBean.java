package com.huijinlong.newretail.requestbean;

import com.huijinlong.newretail.base.BaseBean;

import java.util.List;

/**
 * Created by penghuaijin on 2018/9/19.
 */

public class SpotProductsBean extends BaseBean {

    /**
     * data : {"products":[{"name":"USDT","area":{"main":[{"instrument":"FERUSDT","symbol":"FER/USDT","priceDecimal":5,"qtyDecimal":0,"amountDecimal":5,"env":{"limit":{"bid":"1","ask":"1","cancel":"0","lb":{"p":"0.0145","q":"0"},"ls":{"p":"0","q":"0"}},"market":{"bid":"0","ask":"0","cancel":"0","p":"0","q":"0"},"show":{"bid":{"p":"1","q":"0"},"ask":{"p":"0","q":"1"},"market":{"bid":"1","ask":"1"}}},"last":"0.00","rate":"0.00%"},{"instrument":"JBLUSDT","symbol":"JBL/USDT","priceDecimal":2,"qtyDecimal":2,"amountDecimal":5,"env":{"limit":{"bid":"1","ask":"1","cancel":"0","lb":{"p":"0.19","q":"0"},"ls":{"p":"0","q":"0"}},"market":{"bid":"0","ask":"0","cancel":"0","p":"0","q":"0"},"show":{"bid":{"p":"1","q":"0"},"ask":{"p":"0","q":"1"},"market":{"bid":"1","ask":"1"}}},"last":"0.00","rate":"0.00%"}]}}]}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        private List<ProductsBean> products;

        public List<ProductsBean> getProducts() {
            return products;
        }

        public void setProducts(List<ProductsBean> products) {
            this.products = products;
        }

        public static class ProductsBean {
            /**
             * name : USDT
             * area : {"main":[{"instrument":"FERUSDT","symbol":"FER/USDT","priceDecimal":5,"qtyDecimal":0,"amountDecimal":5,"env":{"limit":{"bid":"1","ask":"1","cancel":"0","lb":{"p":"0.0145","q":"0"},"ls":{"p":"0","q":"0"}},"market":{"bid":"0","ask":"0","cancel":"0","p":"0","q":"0"},"show":{"bid":{"p":"1","q":"0"},"ask":{"p":"0","q":"1"},"market":{"bid":"1","ask":"1"}}},"last":"0.00","rate":"0.00%"},{"instrument":"JBLUSDT","symbol":"JBL/USDT","priceDecimal":2,"qtyDecimal":2,"amountDecimal":5,"env":{"limit":{"bid":"1","ask":"1","cancel":"0","lb":{"p":"0.19","q":"0"},"ls":{"p":"0","q":"0"}},"market":{"bid":"0","ask":"0","cancel":"0","p":"0","q":"0"},"show":{"bid":{"p":"1","q":"0"},"ask":{"p":"0","q":"1"},"market":{"bid":"1","ask":"1"}}},"last":"0.00","rate":"0.00%"}]}
             */

            private String name;
            private AreaBean area;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public AreaBean getArea() {
                return area;
            }

            public void setArea(AreaBean area) {
                this.area = area;
            }

            public static class AreaBean {
                private List<MainBean> main;

                public List<MainBean> getMain() {
                    return main;
                }

                public void setMain(List<MainBean> main) {
                    this.main = main;
                }

                public static class MainBean {
                    /**
                     * instrument : FERUSDT
                     * symbol : FER/USDT
                     * priceDecimal : 5
                     * qtyDecimal : 0
                     * amountDecimal : 5
                     * env : {"limit":{"bid":"1","ask":"1","cancel":"0","lb":{"p":"0.0145","q":"0"},"ls":{"p":"0","q":"0"}},"market":{"bid":"0","ask":"0","cancel":"0","p":"0","q":"0"},"show":{"bid":{"p":"1","q":"0"},"ask":{"p":"0","q":"1"},"market":{"bid":"1","ask":"1"}}}
                     * last : 0.00
                     * rate : 0.00%
                     */

                    private String instrument;
                    private String symbol;
                    private int priceDecimal;
                    private int qtyDecimal;
                    private int amountDecimal;
                    private EnvBean env;
                    private String last;
                    private String rate;

                    public String getInstrument() {
                        return instrument;
                    }

                    public void setInstrument(String instrument) {
                        this.instrument = instrument;
                    }

                    public String getSymbol() {
                        return symbol;
                    }

                    public void setSymbol(String symbol) {
                        this.symbol = symbol;
                    }

                    public int getPriceDecimal() {
                        return priceDecimal;
                    }

                    public void setPriceDecimal(int priceDecimal) {
                        this.priceDecimal = priceDecimal;
                    }

                    public int getQtyDecimal() {
                        return qtyDecimal;
                    }

                    public void setQtyDecimal(int qtyDecimal) {
                        this.qtyDecimal = qtyDecimal;
                    }

                    public int getAmountDecimal() {
                        return amountDecimal;
                    }

                    public void setAmountDecimal(int amountDecimal) {
                        this.amountDecimal = amountDecimal;
                    }

                    public EnvBean getEnv() {
                        return env;
                    }

                    public void setEnv(EnvBean env) {
                        this.env = env;
                    }

                    public String getLast() {
                        return last;
                    }

                    public void setLast(String last) {
                        this.last = last;
                    }

                    public String getRate() {
                        return rate;
                    }

                    public void setRate(String rate) {
                        this.rate = rate;
                    }

                    public static class EnvBean {
                        /**
                         * limit : {"bid":"1","ask":"1","cancel":"0","lb":{"p":"0.0145","q":"0"},"ls":{"p":"0","q":"0"}}
                         * market : {"bid":"0","ask":"0","cancel":"0","p":"0","q":"0"}
                         * show : {"bid":{"p":"1","q":"0"},"ask":{"p":"0","q":"1"},"market":{"bid":"1","ask":"1"}}
                         */

                        private LimitBean limit;
                        private MarketBean market;
                        private ShowBean show;

                        public LimitBean getLimit() {
                            return limit;
                        }

                        public void setLimit(LimitBean limit) {
                            this.limit = limit;
                        }

                        public MarketBean getMarket() {
                            return market;
                        }

                        public void setMarket(MarketBean market) {
                            this.market = market;
                        }

                        public ShowBean getShow() {
                            return show;
                        }

                        public void setShow(ShowBean show) {
                            this.show = show;
                        }

                        public static class LimitBean {
                            /**
                             * bid : 1
                             * ask : 1
                             * cancel : 0
                             * lb : {"p":"0.0145","q":"0"}
                             * ls : {"p":"0","q":"0"}
                             */

                            private String bid;
                            private String ask;
                            private String cancel;
                            private LbBean lb;
                            private LsBean ls;

                            public String getBid() {
                                return bid;
                            }

                            public void setBid(String bid) {
                                this.bid = bid;
                            }

                            public String getAsk() {
                                return ask;
                            }

                            public void setAsk(String ask) {
                                this.ask = ask;
                            }

                            public String getCancel() {
                                return cancel;
                            }

                            public void setCancel(String cancel) {
                                this.cancel = cancel;
                            }

                            public LbBean getLb() {
                                return lb;
                            }

                            public void setLb(LbBean lb) {
                                this.lb = lb;
                            }

                            public LsBean getLs() {
                                return ls;
                            }

                            public void setLs(LsBean ls) {
                                this.ls = ls;
                            }

                            public static class LbBean {
                                /**
                                 * p : 0.0145
                                 * q : 0
                                 */

                                private String p;
                                private String q;

                                public String getP() {
                                    return p;
                                }

                                public void setP(String p) {
                                    this.p = p;
                                }

                                public String getQ() {
                                    return q;
                                }

                                public void setQ(String q) {
                                    this.q = q;
                                }
                            }

                            public static class LsBean {
                                /**
                                 * p : 0
                                 * q : 0
                                 */

                                private String p;
                                private String q;

                                public String getP() {
                                    return p;
                                }

                                public void setP(String p) {
                                    this.p = p;
                                }

                                public String getQ() {
                                    return q;
                                }

                                public void setQ(String q) {
                                    this.q = q;
                                }
                            }
                        }

                        public static class MarketBean {
                            /**
                             * bid : 0
                             * ask : 0
                             * cancel : 0
                             * p : 0
                             * q : 0
                             */

                            private String bid;
                            private String ask;
                            private String cancel;
                            private String p;
                            private String q;

                            public String getBid() {
                                return bid;
                            }

                            public void setBid(String bid) {
                                this.bid = bid;
                            }

                            public String getAsk() {
                                return ask;
                            }

                            public void setAsk(String ask) {
                                this.ask = ask;
                            }

                            public String getCancel() {
                                return cancel;
                            }

                            public void setCancel(String cancel) {
                                this.cancel = cancel;
                            }

                            public String getP() {
                                return p;
                            }

                            public void setP(String p) {
                                this.p = p;
                            }

                            public String getQ() {
                                return q;
                            }

                            public void setQ(String q) {
                                this.q = q;
                            }
                        }

                        public static class ShowBean {
                            /**
                             * bid : {"p":"1","q":"0"}
                             * ask : {"p":"0","q":"1"}
                             * market : {"bid":"1","ask":"1"}
                             */

                            private BidBean bid;
                            private AskBean ask;
                            private MarketBeanX market;

                            public BidBean getBid() {
                                return bid;
                            }

                            public void setBid(BidBean bid) {
                                this.bid = bid;
                            }

                            public AskBean getAsk() {
                                return ask;
                            }

                            public void setAsk(AskBean ask) {
                                this.ask = ask;
                            }

                            public MarketBeanX getMarket() {
                                return market;
                            }

                            public void setMarket(MarketBeanX market) {
                                this.market = market;
                            }

                            public static class BidBean {
                                /**
                                 * p : 1
                                 * q : 0
                                 */

                                private String p;
                                private String q;

                                public String getP() {
                                    return p;
                                }

                                public void setP(String p) {
                                    this.p = p;
                                }

                                public String getQ() {
                                    return q;
                                }

                                public void setQ(String q) {
                                    this.q = q;
                                }
                            }

                            public static class AskBean {
                                /**
                                 * p : 0
                                 * q : 1
                                 */

                                private String p;
                                private String q;

                                public String getP() {
                                    return p;
                                }

                                public void setP(String p) {
                                    this.p = p;
                                }

                                public String getQ() {
                                    return q;
                                }

                                public void setQ(String q) {
                                    this.q = q;
                                }
                            }

                            public static class MarketBeanX {
                                /**
                                 * bid : 1
                                 * ask : 1
                                 */

                                private String bid;
                                private String ask;

                                public String getBid() {
                                    return bid;
                                }

                                public void setBid(String bid) {
                                    this.bid = bid;
                                }

                                public String getAsk() {
                                    return ask;
                                }

                                public void setAsk(String ask) {
                                    this.ask = ask;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
