package com.huijinlong.newretail.requestbean;

import com.huijinlong.newretail.base.BaseBean;

import java.util.List;

/**
 * @author zsg
 * @date 2018/10/31 15:05
 * @Version 1.0
 * @Description: ()
 */
public class KlineBean  extends BaseBean{
    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * open : 0
         * type : kline_1min.btcusdt
         * close : 0
         * high : 0
         * id : 1536911400
         * quote_vol : 0
         * base_vol : 0
         * low : 0
         * datetime : 1550
         * seq : 1536911400000
         * atTime : 1549
         */
        private double open;//开
        private String type;
        private double close;//收
        private double high;//高
        private long id;
        private double quote_vol;//成交量
        private double base_vol;//成交额
        private double low;//低
        private String datetime;//日期
        private long seq;
        private String atTime;

//        double openPrice = 100;
//        double closePrice;//开
//        double maxPrice;//高
//        double minPrice;//低
//        double volume;//成交额
        //时间戳
  //      private long time;
        //涨跌额
        private double upDnAmount;
        //涨跌幅
        private double upDnRate;
        private double priceMa5;
        private double priceMa10;
        private double priceMa30;
        private double ema5;
        private double ema10;
        private double ema30;
        private double ema;
        private double volumeMa5;
        private double volumeMa10;
        private double bollMb;
        private double bollUp;
        private double bollDn;
        private double macd;
        private double dea;
        private double dif;
        private double k;
        private double d;
        private double j;
        private double rs1;
        private double rs2;
        private double rs3;
        private double leftX;
        private double rightX;
        private double closeY;
        private double openY;
        private boolean initFinish;

        public DataBean(long ids,double open, double close, double high, double low, double quote_vol) {
            this.open = open;
            this.close = close;
            this.high = high;
            this.low = low;
            this.quote_vol = quote_vol;
            this.id = ids;
        }

        public double getOpen() {
            return open;
        }

        public void setOpen(double open) {
            this.open = open;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public double getClose() {
            return close;
        }

        public void setClose(double close) {
            this.close = close;
        }

        public double getHigh() {
            return high;
        }

        public void setHigh(double high) {
            this.high = high;
        }

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public double getQuote_vol() {
            return quote_vol;
        }

        public void setQuote_vol(double quote_vol) {
            this.quote_vol = quote_vol;
        }

        public double getBase_vol() {
            return base_vol;
        }

        public void setBase_vol(double base_vol) {
            this.base_vol = base_vol;
        }

        public double getLow() {
            return low;
        }

        public void setLow(double low) {
            this.low = low;
        }

        public String getDatetime() {
            return datetime;
        }

        public void setDatetime(String datetime) {
            this.datetime = datetime;
        }

        public long getSeq() {
            return seq;
        }

        public void setSeq(long seq) {
            this.seq = seq;
        }

        public String getAtTime() {
            return atTime;
        }

        public void setAtTime(String atTime) {
            this.atTime = atTime;
        }

        public double getUpDnAmount() {
            return upDnAmount;
        }

        public void setUpDnAmount(double upDnAmount) {
            this.upDnAmount = upDnAmount;
        }

        public double getUpDnRate() {
            return upDnRate;
        }

        public void setUpDnRate(double upDnRate) {
            this.upDnRate = upDnRate;
        }

        public double getPriceMa5() {
            return priceMa5;
        }

        public void setPriceMa5(double priceMa5) {
            this.priceMa5 = priceMa5;
        }

        public double getPriceMa10() {
            return priceMa10;
        }

        public void setPriceMa10(double priceMa10) {
            this.priceMa10 = priceMa10;
        }

        public double getPriceMa30() {
            return priceMa30;
        }

        public void setPriceMa30(double priceMa30) {
            this.priceMa30 = priceMa30;
        }

        public double getEma5() {
            return ema5;
        }

        public void setEma5(double ema5) {
            this.ema5 = ema5;
        }

        public double getEma10() {
            return ema10;
        }

        public void setEma10(double ema10) {
            this.ema10 = ema10;
        }

        public double getEma30() {
            return ema30;
        }

        public void setEma30(double ema30) {
            this.ema30 = ema30;
        }

        public double getEma() {
            return ema;
        }

        public void setEma(double ema) {
            this.ema = ema;
        }

        public double getVolumeMa5() {
            return volumeMa5;
        }

        public void setVolumeMa5(double volumeMa5) {
            this.volumeMa5 = volumeMa5;
        }

        public double getVolumeMa10() {
            return volumeMa10;
        }

        public void setVolumeMa10(double volumeMa10) {
            this.volumeMa10 = volumeMa10;
        }

        public double getBollMb() {
            return bollMb;
        }

        public void setBollMb(double bollMb) {
            this.bollMb = bollMb;
        }

        public double getBollUp() {
            return bollUp;
        }

        public void setBollUp(double bollUp) {
            this.bollUp = bollUp;
        }

        public double getBollDn() {
            return bollDn;
        }

        public void setBollDn(double bollDn) {
            this.bollDn = bollDn;
        }

        public double getMacd() {
            return macd;
        }

        public void setMacd(double macd) {
            this.macd = macd;
        }

        public double getDea() {
            return dea;
        }

        public void setDea(double dea) {
            this.dea = dea;
        }

        public double getDif() {
            return dif;
        }

        public void setDif(double dif) {
            this.dif = dif;
        }

        public double getK() {
            return k;
        }

        public void setK(double k) {
            this.k = k;
        }

        public double getD() {
            return d;
        }

        public void setD(double d) {
            this.d = d;
        }

        public double getJ() {
            return j;
        }

        public void setJ(double j) {
            this.j = j;
        }

        public double getRs1() {
            return rs1;
        }

        public void setRs1(double rs1) {
            this.rs1 = rs1;
        }

        public double getRs2() {
            return rs2;
        }

        public void setRs2(double rs2) {
            this.rs2 = rs2;
        }

        public double getRs3() {
            return rs3;
        }

        public void setRs3(double rs3) {
            this.rs3 = rs3;
        }

        public double getLeftX() {
            return leftX;
        }

        public void setLeftX(double leftX) {
            this.leftX = leftX;
        }

        public double getRightX() {
            return rightX;
        }

        public void setRightX(double rightX) {
            this.rightX = rightX;
        }

        public double getCloseY() {
            return closeY;
        }

        public void setCloseY(double closeY) {
            this.closeY = closeY;
        }

        public double getOpenY() {
            return openY;
        }

        public void setOpenY(double openY) {
            this.openY = openY;
        }

        public boolean isInitFinish() {
            return initFinish;
        }

        public void setInitFinish(boolean initFinish) {
            this.initFinish = initFinish;
        }
    }
}
