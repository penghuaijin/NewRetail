package com.huijinlong.newretail.requestbean;

import com.huijinlong.newretail.base.BaseBean;

/**
 * @author zsg
 * @date 2018/11/7 17:10
 * @Version 1.0
 * @Description: ()
 */
public class WalletAddressBean extends BaseBean {

    /**
     * data : {"address":"1DB3u7QXLCLhRxXQRkYAPyabFGDjfKx9jg"}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * address : 1DB3u7QXLCLhRxXQRkYAPyabFGDjfKx9jg
         */

        private String address;

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }
    }
}
