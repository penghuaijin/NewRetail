package com.huijinlong.newretail.requestbean;

import com.huijinlong.newretail.base.BaseBean;

/**
 * @author penghuaijin
 * @date 2018/11/6 2:42 PM
 * @Description: ()
 */
public class SpotConfigBean extends BaseBean {

    /**
     * data : {"action":{"limit":{"sell":false,"buy":true},"market":{"sell":false,"buy":false}},"price":{"limit":{"buy":"0.0145","sell":"0"}}}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * action : {"limit":{"sell":false,"buy":true},"market":{"sell":false,"buy":false}}
         * price : {"limit":{"buy":"0.0145","sell":"0"}}
         */

        private ActionBean action;
        private PriceBean price;

        public ActionBean getAction() {
            return action;
        }

        public void setAction(ActionBean action) {
            this.action = action;
        }

        public PriceBean getPrice() {
            return price;
        }

        public void setPrice(PriceBean price) {
            this.price = price;
        }

        public static class ActionBean {
            /**
             * limit : {"sell":false,"buy":true}
             * market : {"sell":false,"buy":false}
             */

            private LimitBean limit;
            private MarketBean market;

            public LimitBean getLimit() {
                return limit;
            }

            public void setLimit(LimitBean limit) {
                this.limit = limit;
            }

            public MarketBean getMarket() {
                return market;
            }

            public void setMarket(MarketBean market) {
                this.market = market;
            }

            public static class LimitBean {
                /**
                 * sell : false
                 * buy : true
                 */

                private boolean sell;
                private boolean buy;

                public boolean isSell() {
                    return sell;
                }

                public void setSell(boolean sell) {
                    this.sell = sell;
                }

                public boolean isBuy() {
                    return buy;
                }

                public void setBuy(boolean buy) {
                    this.buy = buy;
                }
            }

            public static class MarketBean {
                /**
                 * sell : false
                 * buy : false
                 */

                private boolean sell;
                private boolean buy;

                public boolean isSell() {
                    return sell;
                }

                public void setSell(boolean sell) {
                    this.sell = sell;
                }

                public boolean isBuy() {
                    return buy;
                }

                public void setBuy(boolean buy) {
                    this.buy = buy;
                }
            }
        }

        public static class PriceBean {
            /**
             * limit : {"buy":"0.0145","sell":"0"}
             */

            private LimitBeanX limit;

            public LimitBeanX getLimit() {
                return limit;
            }

            public void setLimit(LimitBeanX limit) {
                this.limit = limit;
            }

            public static class LimitBeanX {
                /**
                 * buy : 0.0145
                 * sell : 0
                 */

                private String buy;
                private String sell;

                public String getBuy() {
                    return buy;
                }

                public void setBuy(String buy) {
                    this.buy = buy;
                }

                public String getSell() {
                    return sell;
                }

                public void setSell(String sell) {
                    this.sell = sell;
                }
            }
        }
    }
}
